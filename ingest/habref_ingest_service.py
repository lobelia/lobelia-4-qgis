import csv
import sys
from typing import List

from qgis.core import QgsProject

from ingest_service import IngestService


class HabrefIngestService(IngestService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)

    def get_habref_from_pg(self):
        sql = """SELECT cd_hab, cd_typo, lb_code, REGEXP_REPLACE(lb_hab_fr, E'<[^>]+>', '', 'gi') AS lb_hab_fr, cd_hab_sup
        FROM habref.habref_obv
        WHERE cd_typo IN (4, 7, 22) AND lb_hab_fr IS NOT NULL
        ORDER BY cd_typo, lb_code"""
        self.pg_cur.execute(sql)
        fieldnames = [field[0] for field in self.pg_cur.description]
        habref = self.pg_cur.fetchall()
        return habref, fieldnames

    def save_habref_into_csv(self, csv_file_name, habref, fieldnames):
        with open(self.home_path / csv_file_name, 'w', encoding='utf-8') as output_file:
            line = ';'.join(header for header in fieldnames) + '\n'
            output_file.write(line)
            for hab in habref:
                hab = ['"'+str(h).replace('"', '""')+'"' if h not in [None] else '' for h in hab]  # Guillemets doubles pour les retours à la ligne et les ;
                line = ';'.join(h for h in hab) + '\n'
                output_file.write(line)

    def build_target_dict(self, file_name: str) -> List[tuple]:
        target_data: List[tuple]
        with open(self.home_path / file_name, 'r', encoding='utf-8') as file:
            data = csv.DictReader(file, delimiter=';')
            target_data = [(line['cd_hab'], line['cd_typo'], line['lb_code'], line['lb_hab_fr'], line['cd_hab_sup'])
                           for line in data]
            # target_data = [tuple(list(line.values())) for line in data]
        return target_data

    def delete_current_content(self) -> bool:
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.execute('DELETE FROM habref;')
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def import_new_habref(self, data) -> bool:
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.executemany('INSERT INTO habref (cd_hab, cd_typo, lb_code, lb_hab_fr, cd_hab_sup)'
                                        ' VALUES (?, ?, ?, ?, ?);', data)
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def process_pg_to_sqlite(self, pg_file_name: str, csv_file_name: str, sqlite_file_name: str):
        self.pg_connection(pg_file_name)
        habref, fieldnames = self.get_habref_from_pg()
        self.save_habref_into_csv(csv_file_name, habref, fieldnames)
        self.sqlite_connection(sqlite_file_name)
        if len(habref) > 0 and self.sqlite_cur:
            self.delete_current_content()
            self.import_new_habref(habref)
        self.pg_conn.close()
        self.sqlite_conn.close()

    def process_pg_to_csv(self, pg_file_name: str, csv_file_name: str):
        self.pg_connection(pg_file_name)
        habref, fieldnames = self.get_habref_from_pg()
        self.save_habref_into_csv(csv_file_name, habref, fieldnames)
        self.pg_conn.close()

    def process_csv_to_sqlite(self, file_name: str, database_file_name: str):
        self.sqlite_connection(database_file_name)
        target_data = self.build_target_dict(file_name)
        if len(target_data) > 0 and self.sqlite_cur:
            self.delete_current_content()
            self.import_new_habref(target_data)
        self.sqlite_conn.close()


if __name__ == '__main__':
    ingest_service = HabrefIngestService()
    if sys.argv[1] == '-pg-to-sqlite':
        ingest_service.process_pg_to_sqlite('.lobelia_db.env', sys.argv[2], 'donnees_habitats.sqlite')
    elif sys.argv[1] == '-pg-to-csv':
        ingest_service.process_pg_to_csv('.lobelia_db.env', sys.argv[2])
    elif sys.argv[1] == '-csv-to-sqlite':
        ingest_service.process_csv_to_sqlite(sys.argv[2], 'donnees_habitats.sqlite')
    else:
        print("ERREUR : Le 1er argument (devant faire référence à la méthode d'import) n'a pas été reconnu")
