from sqlite3 import Connection, Cursor
from typing import Optional, List

from qgis.core import QgsProject, QgsFeature, QgsVectorLayer
from qgis.utils import iface

from ..helper.connection_helper import ConnectionHelper


class DataManager:
    def __init__(self, project: QgsProject = QgsProject.instance()):
        self.connection: Optional[Connection] = ConnectionHelper(iface).sqlite_connection(project)
        self.cursor: Optional[Cursor] = None
        if self.connection:
            self.cursor = self.connection.cursor()

    def get_list_values_from_list_name(self, list_name: str) -> List:
        result: List = []
        if self.cursor:
            request = 'SELECT key, value, relation FROM listes_valeurs WHERE list_name=?'
            result = self.cursor.execute(request, (list_name,)).fetchall()
        return result

    def get_list_values_from_list_name_and_criteria(self, list_name: str, chunks: List[str]) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT key, value, relation FROM listes_valeurs WHERE list_name='%s' AND value LIKE '%s' " \
                      "AND value LIKE '%s' ORDER BY value"
            first_chunk = '%' + chunks[0] + '%' if chunks[0] != '' else '%'
            second_chunk = '%' + chunks[1] + '%' if len(chunks) > 1 and chunks[1] != '' else '%'
            result = self.cursor.execute(request % (list_name, first_chunk, second_chunk)).fetchall()
        return result

    def get_value_from_key_and_list_name(self, key: str, list_name: str) -> str:
        result: List = []
        if self.cursor:
            request = 'SELECT value FROM listes_valeurs WHERE key=? AND list_name=?'
            result = self.cursor.execute(request, (key, list_name)).fetchone()
        return result[0] if len(result) > 0 else ""

    def get_taxons_from_criteria(self, chunks: List[str]) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM (SELECT *, CASE WHEN instr(lb_nom, ' ') = 0 THEN LENGTH(lb_nom) ELSE instr(lb_nom, ' ') END AS pos FROM taxref) " \
                      "WHERE substr(LOWER(lb_nom), 1, pos) LIKE '%s' AND substr(LOWER(lb_nom), pos+1) LIKE '%s'"
            first_chunk = chunks[0].lower() + '%' if chunks[0] != '' else '%'
            second_chunk = '%' + chunks[1].lower() + '%' if len(chunks) > 1 and chunks[1] != '' else '%'
            result = self.cursor.execute(request % (first_chunk, second_chunk)).fetchall()
        return result

    def get_jdd_list(self) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT uuid, libelle_court, libelle FROM jdd ORDER BY libelle_court"
            result = self.cursor.execute(request).fetchall()
        return result

    def get_jdd_name_from_uuid(self, uuid: str) -> str:
        result: List = []
        if self.cursor:
            request = "SELECT libelle FROM jdd WHERE uuid='%s'"
            result = self.cursor.execute(request % uuid).fetchone()
        return result[0]

    def check_jdd_uuid_exists(self, uuid: str) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM jdd WHERE uuid='%s'"
            result = self.cursor.execute(request % uuid).fetchall()
        return len(result) > 0

    def filter_jdd_list_by_name(self, chunks: List[str]) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT uuid, libelle_court, libelle FROM jdd WHERE LOWER(libelle_court) LIKE '%s' " \
                      "AND LOWER(libelle_court) LIKE '%s' ORDER BY libelle_court"
            first_chunk = '%' + chunks[0].lower() + '%' if chunks[0] != '' else '%'
            second_chunk = '%' + chunks[1].lower() + '%' if len(chunks) > 1 and chunks[1] != '' else '%'
            result = self.cursor.execute(request % (first_chunk, second_chunk)).fetchall()
        return result

    def copy_jdd_uuid_to_all_surveys(self, uuid: str):
        if self.cursor:
            request = "UPDATE releves SET rel_jdd_uuid='%s'"
            self.cursor.execute(request % uuid)
            self.connection.commit()

    def get_observers(self) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT obs_id, obs_prenom, obs_nom, obs_email FROM observateurs WHERE obs_turned_on=1"
            result = self.cursor.execute(request).fetchall()
        return result

    def get_linked_strates_to_survey(self, survey_id) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_releve_strate WHERE rel_id=%s"
            result = self.cursor.execute(request % survey_id).fetchall()
        return result

    def get_linked_factors_to_survey(self, survey_id) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_releve_influence WHERE rel_id=%s"
            result = self.cursor.execute(request % survey_id).fetchall()
        return result

    def get_strate_name(self, strate_id) -> str:
        result: List = []
        if self.cursor:
            request = "SELECT value FROM listes_valeurs WHERE list_name='STRATES_VALUES' AND key=%s"
            result = self.cursor.execute(request % strate_id).fetchone()
        return result[0]

    def get_survey_id_from_geometry(self, survey_feature: QgsFeature):
        result: List = []
        geometry_field: str = ""
        if survey_feature['rel_point_uuid']:
            geometry_field = 'rel_point_uuid'
        elif survey_feature['rel_ligne_uuid']:
            geometry_field = 'rel_ligne_uuid'
        elif survey_feature['rel_polygon_uuid']:
            geometry_field = 'rel_polygon_uuid'
        if self.cursor and geometry_field:
            request = "SELECT id FROM releves WHERE %s='%s'"
            result = self.cursor.execute(request % (geometry_field, survey_feature[geometry_field])).fetchone()
        return result[0]

    def get_station_id_from_geometry(self, geometry_layer: QgsVectorLayer, geometry_feature: QgsFeature):
        result: List = []
        uuid_index = geometry_layer.fields().indexFromName('uuid')
        if self.cursor:
            request = "SELECT id FROM %s WHERE uuid='%s'"
            result = self.cursor.execute(request % (geometry_layer.name(), geometry_feature[uuid_index])).fetchone()
        return result[0]

    def get_obs_syntaxon_id_from_survey_id(self, survey_id: int):
        result: List = []
        if self.cursor:
            request = "SELECT rel_obs_syntaxon_id FROM releves WHERE id=%s"
            result = self.cursor.execute(request % survey_id).fetchone()
        return result[0]

    def get_obs_habitat_id_from_uuid(self, obs_hab_uuid: str):
        result: List = []
        if self.cursor:
            request = "SELECT obs_hab_id FROM obs_habitats WHERE obs_hab_uuid='%s'"
            result = self.cursor.execute(request % obs_hab_uuid).fetchone()
        return result[0]

    def obs_syntaxon_id_exists(self, survey_id) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT rel_obs_syntaxon_id FROM releves WHERE id=%s"
            result = self.cursor.execute(request % survey_id).fetchone()
        return result and len(result) > 0 and result[0]

    def insert_links_between_obs_hab(self, obs_syntaxon_id: int, obs_habitat_id: int):
        if self.cursor:
            request = "INSERT INTO liens_obs_habitat (id_obs_hab_1, id_obs_hab_2) VALUES (%s, %s)"
            self.cursor.execute(request % (obs_syntaxon_id, obs_habitat_id))
            self.cursor.execute(request % (obs_habitat_id, obs_syntaxon_id))
            self.connection.commit()

    def remove_links_between_obs_hab(self, obs_syntaxon_id: int, obs_habitat_id: int):
        if self.cursor:
            request = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_1=%s AND id_obs_hab_2=%s"
            self.cursor.execute(request % (obs_syntaxon_id, obs_habitat_id))
            self.cursor.execute(request % (obs_habitat_id, obs_syntaxon_id))
            self.connection.commit()

    def remove_links_by_obs_hab_id(self, obs_habitat_id: int):
        if self.cursor:
            request_1 = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_1=%s"
            request_2 = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_2=%s"
            self.cursor.execute(request_1 % obs_habitat_id)
            self.cursor.execute(request_2 % obs_habitat_id)
            self.connection.commit()

    def remove_dominant_taxon_from_obs_hab_id(self, obs_hab_id: int):
        if self.cursor:
            request = "UPDATE obs_habitats SET syntaxon_comm_non_sat_id = NULL, syntaxon_comm_non_sat_taxon_dom = NULL," \
                      "syntaxon_comm_non_sat_taxon_dom_nom = NULL " \
                      "WHERE obs_hab_id = %s"
            self.cursor.execute(request % obs_hab_id)
            self.connection.commit()

    def are_obs_hab_linked(self, obs_hab_id_1: int, obs_hab_id_2: int) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_obs_habitat WHERE id_obs_hab_1=%s AND id_obs_hab_2=%s"
            result = self.cursor.execute(request % (obs_hab_id_1, obs_hab_id_2)).fetchone()
        return result and len(result) > 0 and result[0]

    def is_obs_hab_linked_to_other(self, obs_habitat_id: int, obs_syntaxon_id: int) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_obs_habitat WHERE id_obs_hab_1=%s AND id_obs_hab_2!=%s"
            result = self.cursor.execute(request % (obs_habitat_id, obs_syntaxon_id)).fetchone()
        return result and len(result) > 0 and result[0]

    def get_linked_obs_hab(self, obs_syntaxon_id: int) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id_obs_hab_2 FROM liens_obs_habitat WHERE id_obs_hab_1=%s"
            result = self.cursor.execute(request % obs_syntaxon_id).fetchall()
        return result

    def clean_obs_hab_link_in_assoc_survey(self, obs_hab_rel_assoc_id: int):
        if self.cursor:
            request = "UPDATE releves SET rel_obs_syntaxon_id = NULL WHERE id=%s"
            self.cursor.execute(request % obs_hab_rel_assoc_id)
            self.connection.commit()

    def clean_other_obs_hab_link_to_assoc_survey(self, obs_hab_rel_assoc_id: int):
        if self.cursor:
            request = "UPDATE obs_habitats SET obs_hab_rel_assoc_id = NULL WHERE obs_hab_rel_assoc_id=%s"
            self.cursor.execute(request % obs_hab_rel_assoc_id)
            self.connection.commit()

    def clean_link_to_assoc_survey(self, obs_habitat_id: int):
        if self.cursor:
            request = "UPDATE obs_habitats SET obs_hab_rel_assoc_id = NULL WHERE obs_hab_id=%s"
            self.cursor.execute(request % obs_habitat_id)
            self.connection.commit()

    def get_related_habitats_list_from_code_phyto(self, code_phyto: object, cd_typo: int = None):
        result: List = []
        if self.cursor:
            request = 'SELECT cd_hab, cd_typo, lb_code, lb_hab_fr FROM corresp_syntaxons_habitats WHERE code_phyto=?'
            if cd_typo:
                request += f' AND cd_typo={str(cd_typo)}'
            request += '  ORDER BY cd_typo, lb_code'
            result = self.cursor.execute(request, (code_phyto,)).fetchall()
        return result

    def get_habitat_from_cd_hab(self, cd_hab: int):
        result: List = []
        if self.cursor:
            request = 'SELECT cd_typo, lb_code, lb_hab_fr FROM habref WHERE cd_hab=?'
            result = self.cursor.execute(request, (str(cd_hab),)).fetchall()
        return result[0]

    def count_geometry_uuid(self, geometry_table: str, geometry_uuid: str) -> int:
        result: List = []
        if self.cursor:
            request = "SELECT COUNT(id) FROM %s WHERE uuid='%s'"
            result = self.cursor.execute(request % (geometry_table, geometry_uuid)).fetchone()
        return result[0] if len(result) > 0 else 0

    def get_survey_type_from_geometry(self, geometry_uuid: str) -> int:
        result: List = []
        if self.cursor:
            request = "SELECT rel_type_rel_id FROM releves WHERE rel_polygon_uuid='%s'"
            result = self.cursor.execute(request % geometry_uuid).fetchone()
        return result[0]
