from typing import List, Optional

from PyQt5.QtCore import Qt, QSize, QModelIndex
from PyQt5.QtGui import QColor, QBrush, QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QButtonGroup, QListWidget, QListWidgetItem, QTableWidgetItem, QWidget
from PyQt5.QtWidgets import QTableWidget, QLineEdit, QPlainTextEdit, QComboBox, QLabel, QTextEdit, QCheckBox
from qgis.PyQt.QtCore import NULL
from qgis.core import QgsFeature, QgsVectorLayer
from qgis.gui import QgsAttributeForm

from .survey_widget_package import ComboBox, LineEdit
from .survey_widget_service import WidgetService
from ..constants.header_labels_values import CORINE_HEADER_LABELS
from ..constants.header_labels_values import SYNTAXONS_HEADER_LABELS, HIC_HEADER_LABELS, EUNIS_HEADER_LABELS
from ..data.database_specification_service import DatabaseSpecificationService
from ..data.list_values_provider import ListValuesProvider
from ..data.sqlite_data_manager import DataManager


class TableWidgetService:
    @staticmethod
    def edit_syntaxon(survey_feature: QgsFeature, row: int, observations: QgsVectorLayer, sheet: QTableWidget,
                      form: QgsAttributeForm, add_widget: QLabel, save_widget: QLabel, add_survey_widgets: List[QLabel],
                      select_widget: QComboBox, typo_buttons: QButtonGroup, add_related_habitats_widget: QLabel):
        code_phyto = sheet.item(row, 0).text() if sheet.item(row, 0) and sheet.item(row, 0).text() != '' else None
        TableWidgetService.edit_observation(survey_feature, observations, form, add_widget, save_widget,
                                            add_survey_widgets, select_widget, typo_buttons, 'obs_habitat_',
                                            'obs_hab_rel_id', 'obs_hab_code_cite', 'obs_hab_nom_cite', code_phyto,
                                            sheet.item(row, 1).text(), add_related_habitats_widget)

    @staticmethod
    def edit_taxon(survey_feature: QgsFeature, row: int, observations: QgsVectorLayer, sheet: QTableWidget,
                   form: QgsAttributeForm, add_widget: QLabel, save_widget: QLabel, add_survey_widgets: List[QLabel],
                   select_widget: QComboBox, typo_buttons: QButtonGroup):
        cd_nom = int(sheet.item(row, 0).text()) if sheet.item(row, 0) and sheet.item(row, 0).text() != '' else None
        TableWidgetService.edit_observation(survey_feature, observations, form, add_widget, save_widget,
                                            add_survey_widgets, select_widget, typo_buttons, 'nom_', 'obs_rel_id',
                                            'cd_nom', 'nom_cite', cd_nom, sheet.item(row, 1).text())

    @staticmethod
    def edit_observation(survey_feature: QgsFeature, observations: QgsVectorLayer, form: QgsAttributeForm,
                         add_widget: QLabel, save_widget: QLabel, add_survey_widgets: List[QLabel],
                         select_widget: QComboBox, typo_buttons: QButtonGroup, keyword: str, obs_rel_field_name: str,
                         obs_code_field_name: str, obs_name_field_name: str, id_to_compare: object,
                         text_to_compare: object, add_related_habitats_widget: QLabel = None):
        form_inputs = DatabaseSpecificationService.get_inputs_specification(observations.name())
        if len(form_inputs) == 0:
            return
        observation = TableWidgetService.find_observation(id_to_compare, text_to_compare, obs_code_field_name,
                                                          obs_name_field_name, obs_rel_field_name, observations,
                                                          survey_feature)
        if observation:
            code_typo = TableWidgetService.get_code_typo(typo_buttons)
            TableWidgetService.hydrate_form(form, form_inputs, keyword, observation)
            if observations.name() == 'obs_habitats':
                TableWidgetService.populate_obs_assoc_list(survey_feature, observation, observations, form,
                                                           typo_buttons)
                if code_typo == 0:
                    TableWidgetService.populate_related_habitats_list(survey_feature, observations, form, id_to_compare)

            for button in typo_buttons.buttons():
                button.setEnabled(False)
            select_widget.setEnabled(False)
            add_widget.hide()
            save_widget.show()

            if add_related_habitats_widget and code_typo == 0:
                add_related_habitats_widget.show()

            for widget in add_survey_widgets:
                widget.show()

    @staticmethod
    def hydrate_form(form: QgsAttributeForm, form_inputs: List[dict], keyword: str, observation: QgsFeature):
        for form_input in form_inputs:
            widget = WidgetService.find_widget(form, form_input['input_type'],
                                               form_input['input_name'].split(',')[0])
            if widget:
                TableWidgetService.clean_widget(widget)
                field_names = [form_input['field_name'] for form_input in form_inputs
                               if form_input['input_name'].split(',')[0] == widget.objectName()]
                if len(field_names) > 0 and observation:
                    WidgetService.set_input_value(widget, observation[field_names[0]])
                if keyword in widget.objectName():
                    widget.setEnabled(False)

    @staticmethod
    def populate_obs_assoc_list(survey_feature: QgsFeature, observation: QgsFeature, observations: QgsVectorLayer,
                                form: QgsAttributeForm, typo_buttons: QButtonGroup):
        obs_hab_list_widget: QListWidget = form.findChild(QListWidget, 'list_hab_obs_assoc')
        try:
            obs_hab_list_widget.itemChanged.disconnect()
        except:
            pass
        obs_hab_list_widget.clear()
        if obs_hab_list_widget is not None:
            code_typo = TableWidgetService.get_code_typo(typo_buttons)
            consulted_obs_hab_id = observation['obs_hab_id']
            obs_hab_list = [observation for observation in observations.getFeatures()
                            if observation['obs_hab_rel_id'] == survey_feature.id()
                            and observation['obs_hab_code_typo'] != code_typo]
            for obs_hab in obs_hab_list:
                row = obs_hab_list_widget.count()
                obs_hab_item = TableWidgetService.create_obs_hab_assoc_item(consulted_obs_hab_id, obs_hab)
                obs_hab_list_widget.insertItem(row, obs_hab_item)
            obs_hab_list_widget.sortItems(Qt.DescendingOrder)
            obs_hab_list_widget.itemChanged.connect(
                lambda item: TableWidgetService.manage_association_between_obs_hab(item, obs_hab_list_widget,
                                                                                   observation, observations))

    @staticmethod
    def create_obs_hab_assoc_item(consulted_obs_hab_id: int, obs_hab: QgsFeature):
        sqlite_data_manager = DataManager()
        obs_hab_typo = obs_hab['obs_hab_code_typo']
        obs_hab_id = obs_hab['obs_hab_id']
        label = TableWidgetService.get_typo_label_from_code(obs_hab_typo) + ' - '
        label += str(obs_hab['obs_hab_code_cite']) + ' - ' + obs_hab['obs_hab_nom_cite']
        rec_list: List[str] = []
        if obs_hab['obs_hab_rec_pct'] not in (0, None, NULL):
            rec_list.append(str(obs_hab['obs_hab_rec_pct']) + '%')
        if obs_hab['obs_hab_recouvrement'] not in (0, None, NULL):
            list_values_provider = ListValuesProvider()
            recouvrement_values = list_values_provider.get_values_from_list_name('RECOUVREMENT_VALUES')
            rec_list.append('coef. ' + list_values_provider.get_value_from_list(
                recouvrement_values, obs_hab['obs_hab_recouvrement']))
        label += ' (recouvrement : ' + ', '.join(rec_list) + ')' if len(rec_list) > 0 else ''
        is_linked = sqlite_data_manager.are_obs_hab_linked(obs_hab_id, consulted_obs_hab_id)
        obs_hab_item = QListWidgetItem()
        obs_hab_item.setText(label)
        obs_hab_item.setData(Qt.UserRole, obs_hab_id)
        obs_hab_item.setBackground(TableWidgetService.get_typo_color_from_code(obs_hab_typo))
        obs_hab_item.setCheckState(Qt.Checked if is_linked else Qt.Unchecked)
        return obs_hab_item

    @staticmethod
    def manage_association_between_obs_hab(list_item: QListWidgetItem, obs_hab_list_widget: QListWidget,
                                           obs_hab: QgsFeature, observations: QgsVectorLayer):
        obs_hab_list_widget.itemChanged.disconnect()
        obs_hab_id = obs_hab['obs_hab_id']
        obs_hab_id_to_link = list_item.data(Qt.UserRole)
        obs_hab_to_link = observations.getFeature(obs_hab_id_to_link)
        sqlite_data_manager = DataManager()
        if list_item.checkState() == 2:
            sqlite_data_manager.insert_links_between_obs_hab(obs_hab_id, obs_hab_id_to_link)
        else:
            sqlite_data_manager.remove_links_between_obs_hab(obs_hab_id, obs_hab_id_to_link)
            if obs_hab['obs_hab_code_typo'] == 0:
                observations.startEditing()
                obs_hab_to_link['obs_hab_rel_assoc_id'] = None
                observations.updateFeature(obs_hab_to_link)
                observations.commitChanges()
            elif obs_hab_to_link['obs_hab_code_typo'] == 0:
                observations.startEditing()
                obs_hab['obs_hab_rel_assoc_id'] = None
                observations.updateFeature(obs_hab)
                observations.commitChanges()
        obs_hab_list_widget.itemChanged.connect(
            lambda item: TableWidgetService.manage_association_between_obs_hab(item, obs_hab_list_widget,
                                                                               obs_hab, observations))

    @staticmethod
    def populate_obs_assoc_consult_list(survey_feature_id: int, observations: QgsVectorLayer,
                                        obs_hab_assoc_list: QListWidget, typo_buttons: QButtonGroup):
        try:
            obs_hab_assoc_list.itemChanged.disconnect()
        except:
            pass
        obs_hab_assoc_list.clear()
        code_typo = TableWidgetService.get_code_typo(typo_buttons)
        obs_hab_list = [observation for observation in observations.getFeatures()
                        if observation['obs_hab_rel_id'] == survey_feature_id
                        and observation['obs_hab_code_typo'] != code_typo]
        for obs_hab in obs_hab_list:
            row = obs_hab_assoc_list.count()
            obs_hab_item = TableWidgetService.create_obs_hab_assoc_item(0, obs_hab)
            obs_hab_item.setFlags(Qt.NoItemFlags)
            obs_hab_item.setForeground(QBrush(QColor(50, 50, 50)))
            obs_hab_assoc_list.insertItem(row, obs_hab_item)
        obs_hab_assoc_list.sortItems(Qt.DescendingOrder)

    @staticmethod
    def populate_related_habitats_list(survey_feature: QgsFeature, observations: QgsVectorLayer,
                                       form: QgsAttributeForm, code_phyto: object):
        related_habitats_list_widget: QListWidget = form.findChild(QListWidget, 'list_related_habitats')
        try:
            related_habitats_list_widget.itemChanged.disconnect()
        except:
            pass
        related_habitats_list_widget.clear()
        if related_habitats_list_widget is not None:
            sqlite_data_manager = DataManager()
            related_habitats_list = sqlite_data_manager.get_related_habitats_list_from_code_phyto(code_phyto)
            if len(related_habitats_list) > 0:
                obs_hab_list = [str(obs_hab['obs_hab_code_typo']) + ' - ' + obs_hab['obs_hab_code_cite']
                                for obs_hab in observations.getFeatures()
                                if obs_hab['obs_hab_rel_id'] == survey_feature.id()
                                and obs_hab['obs_hab_code_typo'] != 0]
                for hab in related_habitats_list:
                    row = related_habitats_list_widget.count()
                    hab_item = TableWidgetService.create_related_hab_item(hab, obs_hab_list)
                    related_habitats_list_widget.insertItem(row, hab_item)
                related_habitats_list_widget.sortItems(Qt.DescendingOrder)

    @staticmethod
    def create_related_hab_item(hab, obs_hab_list: List[str]):
        hab_code_typo = hab['cd_typo']
        label = TableWidgetService.get_typo_label_from_code(hab_code_typo) + ' - '
        label += str(hab['lb_code']) + ' - ' + hab['lb_hab_fr']
        hab_item = QListWidgetItem()
        hab_item.setText(label)
        hab_item.setData(Qt.UserRole, hab['cd_hab'])
        hab_item.setBackground(TableWidgetService.get_typo_color_from_code(hab_code_typo))
        if str(hab['cd_typo']) + ' - ' + hab['lb_code'] not in obs_hab_list:
            hab_item.setCheckState(Qt.Unchecked)
        return hab_item

    @staticmethod
    def del_row(survey_feature: QgsFeature, row: int, observations: QgsVectorLayer, table: QTableWidget,
                obs_rel_field_name: str, obs_code_field_name: str, obs_name_field_name: str, id_to_compare: object,
                text_to_compare: object):
        observation = TableWidgetService.find_observation(id_to_compare, text_to_compare, obs_code_field_name,
                                                          obs_name_field_name, obs_rel_field_name, observations,
                                                          survey_feature)
        if observation:
            observations.deleteFeature(observation.id())
            observations.commitChanges()
            table.removeRow(row)

    @staticmethod
    def open_list_values_widget(survey_feature: QgsFeature, observations: QgsVectorLayer, table_widget: QTableWidget,
                                item_clicked: QTableWidgetItem, values: List[dict], obs_code_field_name: str,
                                obs_name_field_name: str, obs_rel_field_name: str):
        list_widget: ComboBox = ComboBox()
        item_model = QStandardItemModel()
        for value in values:
            item = QStandardItem()
            item.setSizeHint(QSize(18, 18))
            item.setText(value['value'])
            item.setData(value['id'])
            item_model.appendRow(item)
        list_widget.setModel(item_model)
        table_widget.setCellWidget(item_clicked.row(), item_clicked.column(), list_widget)
        list_widget.showPopup()
        out_signal = list_widget.out
        list_widget.view().pressed.connect(
            lambda choice: TableWidgetService.update_observation_value(survey_feature, observations, table_widget,
                                                                       item_clicked, list_widget, out_signal,
                                                                       values, choice, obs_code_field_name,
                                                                       obs_name_field_name, obs_rel_field_name))
        out_signal.connect(
            lambda: TableWidgetService.rewind_observation_value(survey_feature, observations, table_widget,
                                                                item_clicked, list_widget, out_signal, values,
                                                                obs_code_field_name, obs_name_field_name,
                                                                obs_rel_field_name))

    @staticmethod
    def edit_item(survey_feature: QgsFeature, observations: QgsVectorLayer, table_widget: QTableWidget,
                  item_clicked: QTableWidgetItem, obs_code_field_name: str, obs_name_field_name: str,
                  obs_rel_field_name: str):
        edit_widget: LineEdit = LineEdit()
        table_widget_name = table_widget.objectName()
        item_clicked_code, item_clicked_name = TableWidgetService.get_item_clicked_code_and_name(item_clicked,
                                                                                                 table_widget,
                                                                                                 table_widget_name)
        observation = TableWidgetService.find_observation(item_clicked_code, item_clicked_name, obs_code_field_name,
                                                          obs_name_field_name, obs_rel_field_name, observations,
                                                          survey_feature)
        edit_widget.setText(str(observation['obs_hab_rec_pct']) if observation['obs_hab_rec_pct'] != NULL else '')
        table_widget.setCellWidget(item_clicked.row(), item_clicked.column(), edit_widget)
        edit_widget.setFocus()
        out_signal = edit_widget.out
        edit_widget.returnPressed.connect(
            lambda: TableWidgetService.update_observation_value(survey_feature, observations, table_widget,
                                                                item_clicked, edit_widget, out_signal, [], None,
                                                                obs_code_field_name, obs_name_field_name,
                                                                obs_rel_field_name))
        out_signal.connect(
            lambda: TableWidgetService.rewind_observation_value(survey_feature, observations, table_widget,
                                                                item_clicked, edit_widget, out_signal, [],
                                                                obs_code_field_name, obs_name_field_name,
                                                                obs_rel_field_name))

    @staticmethod
    def update_observation_value(survey_feature: QgsFeature, observations: QgsVectorLayer,
                                 table_widget: QTableWidget, item_clicked: QTableWidgetItem, widget: QWidget,
                                 out_signal, values: List[dict], choice: QModelIndex, obs_code_field_name: str,
                                 obs_name_field_name: str, obs_rel_field_name: str):
        observation, table_widget_name = TableWidgetService.get_observation_id(item_clicked, obs_code_field_name,
                                                                               obs_name_field_name,
                                                                               obs_rel_field_name, observations,
                                                                               out_signal, survey_feature,
                                                                               table_widget, widget)
        column_label, value_to_store = TableWidgetService.get_field_and_value(choice, item_clicked, table_widget,
                                                                              table_widget_name, values, widget)
        if observation is not None and column_label != '' and (value_to_store is None or value_to_store > -1):
            observation[column_label] = value_to_store if value_to_store is not None else NULL
            observations.updateFeature(observation)
            observations.commitChanges()
            table_widget.removeCellWidget(item_clicked.row(), item_clicked.column())
            value_to_store = '' if value_to_store is None else value_to_store
            table_widget.setItem(item_clicked.row(), item_clicked.column(), QTableWidgetItem(
                str(value_to_store) if choice is None else choice.data(), 0))

    @staticmethod
    def rewind_observation_value(survey_feature: QgsFeature, observations: QgsVectorLayer,
                                 table_widget: QTableWidget, item_clicked: QTableWidgetItem, widget: QWidget,
                                 out_signal, values: List[dict], obs_code_field_name: str, obs_name_field_name: str,
                                 obs_rel_field_name: str):
        observation, table_widget_name = TableWidgetService.get_observation_id(item_clicked, obs_code_field_name,
                                                                               obs_name_field_name,
                                                                               obs_rel_field_name, observations,
                                                                               out_signal, survey_feature,
                                                                               table_widget, widget)
        column_header, column_label = '', ''
        if table_widget_name == 'table_taxons':
            column_label = table_widget.horizontalHeaderItem(item_clicked.column()).text()
        elif table_widget_name == 'table_habitats':
            column_header = table_widget.horizontalHeaderItem(item_clicked.column()).text()
            if column_header == 'forme':
                column_label = 'obs_hab_forme_id'
            elif column_header == 'coef. rec.':
                column_label = 'obs_hab_recouvrement'
            elif column_header == 'typ. struct.':
                column_label = 'syntaxon_typicite_struct_id'
            elif column_header == 'typ. flor.':
                column_label = 'syntaxon_typicite_flor_id'
            else:
                column_label = 'obs_hab_rec_pct'
        if observation is not None and column_label != '':
            table_widget.removeCellWidget(item_clicked.row(), item_clicked.column())
            value = observation[column_label]
            if column_header == '% rec.':
                value_to_display = value if value not in (0, None, NULL) else ''
            else:
                value_to_display = ListValuesProvider().get_value_from_list(
                    values, observation[column_label]) if value not in (0, None, NULL) else ''
            table_widget.setItem(item_clicked.row(), item_clicked.column(), QTableWidgetItem(str(value_to_display), 0))

    @staticmethod
    def get_observation_id(item_clicked, obs_code_field_name, obs_name_field_name, obs_rel_field_name, observations,
                           out_signal, survey_feature, table_widget, widget):
        if isinstance(widget, QComboBox):
            widget.view().pressed.disconnect()
        elif isinstance(widget, QLineEdit):
            widget.returnPressed.disconnect()
        out_signal.disconnect()
        table_widget_name = table_widget.objectName()
        item_clicked_code, item_clicked_name = TableWidgetService.get_item_clicked_code_and_name(item_clicked,
                                                                                                 table_widget,
                                                                                                 table_widget_name)
        observation = TableWidgetService.find_observation(item_clicked_code, item_clicked_name, obs_code_field_name,
                                                          obs_name_field_name, obs_rel_field_name, observations,
                                                          survey_feature)
        return observation, table_widget_name

    @staticmethod
    def get_item_clicked_code_and_name(item_clicked, table_widget, table_widget_name):
        item = table_widget.item(item_clicked.row(), 0)
        item_clicked_code = item.text() if item and item.text() != '' else None
        if table_widget_name == 'table_taxons' and item_clicked_code is not None:
            item_clicked_code = int(item_clicked_code)
        item_clicked_name = table_widget.item(item_clicked.row(), 1).text()
        return item_clicked_code, item_clicked_name

    @staticmethod
    def get_field_and_value(choice, item_clicked, table_widget, table_widget_name, values, widget):
        column_label, value_to_store = '', -1
        if table_widget_name == 'table_taxons':
            column_label = table_widget.horizontalHeaderItem(item_clicked.column()).text()
            value_to_store = ListValuesProvider().get_id_from_value(values, choice.data())
        elif table_widget_name == 'table_habitats':
            column_header = table_widget.horizontalHeaderItem(item_clicked.column()).text()
            if column_header == 'forme':
                column_label = 'obs_hab_forme_id'
            elif column_header == 'coef. rec.':
                column_label = 'obs_hab_recouvrement'
            elif column_header == 'typ. struct.':
                column_label = 'syntaxon_typicite_struct_id'
            elif column_header == 'typ. flor.':
                column_label = 'syntaxon_typicite_flor_id'
            else:
                column_label = 'obs_hab_rec_pct'
            if column_header == '% rec.':
                value_to_store = int(widget.text()) if widget.text() != '' else None
            else:
                value_to_store = ListValuesProvider().get_id_from_value(values, choice.data())
        return column_label, value_to_store

    @staticmethod
    def find_observation(id_to_compare, text_to_compare, obs_code_field_name, obs_name_field_name, obs_rel_field_name,
                         observations, survey_feature) -> Optional[QgsFeature]:
        survey_id = survey_feature['id']
        if survey_id is None or survey_id < 0:
            sqlite_data_manager = DataManager()
            survey_id = sqlite_data_manager.get_survey_id_from_geometry(survey_feature)
        id_to_compare = None if id_to_compare == '' else id_to_compare
        observation_result: List[QgsFeature] = [observation for observation in observations.getFeatures()
                                                if observation[obs_rel_field_name] == survey_id
                                                and observation[obs_code_field_name] == id_to_compare
                                                and observation[obs_name_field_name] == text_to_compare]
        if len(observation_result) == 0:
            return None
        else:
            if not observations.isEditable():
                observations.startEditing()
            return observation_result[0]

    @staticmethod
    def clean_widget(widget):
        if isinstance(widget, QLineEdit):
            widget.setText("")
        elif isinstance(widget, QPlainTextEdit) or isinstance(widget, QTextEdit):
            widget.setPlainText("")
        elif isinstance(widget, QComboBox):
            widget.setCurrentIndex(0)
        elif isinstance(widget, QCheckBox):
            widget.setCheckState(Qt.Unchecked)

    @staticmethod
    def get_code_typo(buttons: QButtonGroup) -> int:
        button_name = buttons.checkedButton().objectName()
        if button_name == "rb_phyto":
            return 0
        elif button_name == "rb_hic" or button_name == "rb_typo_hic":
            return 4
        elif button_name == "rb_eunis" or button_name == "rb_typo_eunis":
            return 7
        elif button_name == "rb_cb" or button_name == "rb_typo_cb":
            return 22
        return 0

    @staticmethod
    def get_header_labels_from_typo(code_typo: int) -> List:
        if code_typo == 0:
            return SYNTAXONS_HEADER_LABELS
        elif code_typo == 4:
            return HIC_HEADER_LABELS
        elif code_typo == 7:
            return EUNIS_HEADER_LABELS
        elif code_typo == 22:
            return CORINE_HEADER_LABELS
        return []

    @staticmethod
    def get_typo_label_from_code(code_typo: int) -> str:
        if code_typo == 0:
            return "Phyto"
        elif code_typo == 4:
            return "HIC"
        elif code_typo == 7:
            return "EUNIS"
        elif code_typo == 22:
            return "Corine"
        return ""

    @staticmethod
    def get_typo_color_from_code(code_typo: int) -> QColor:
        if code_typo == 0:
            return QColor(Qt.white)
        elif code_typo == 4:
            return QColor(221, 153, 255)
        elif code_typo == 7:
            return QColor(255, 178, 127)
        elif code_typo == 22:
            return QColor(153, 255, 137)
        return QColor(Qt.white)

    @staticmethod
    def do_nothing():
        pass

    @staticmethod
    def add_new_survey(self):
        pass
