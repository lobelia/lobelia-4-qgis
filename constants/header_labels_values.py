TAXONS_HEADER_LABELS = ['cd_nom', 'taxon_cite', 'strate_arbo', 'strate_arbu', 'strate_herb', 'strate_musc', 'suppr.']
SYNTAXONS_HEADER_LABELS = ['code_phyto', 'syntaxon_cite', '% rec.', 'coef. rec.', 'forme', 'typ. struct.', 'typ. flor.',
                           'suppr.']
HIC_HEADER_LABELS = ['code', 'libelle', '% rec.', 'coef. rec.', 'forme', 'typ. struct.', 'typ. flor.', 'suppr.']
EUNIS_HEADER_LABELS = ['code', 'libelle', '% rec.', 'coef. rec.', 'forme', 'typ. struct.', 'typ. flor.', 'suppr.']
CORINE_HEADER_LABELS = ['code', 'libelle', '% rec.', 'coef. rec.', 'forme', 'typ. struct.', 'typ. flor.', 'suppr.']
HABITATS_HEADER_LABELS = ['code', 'libelle', 'code typo', 'typologie', 'suppr.']
STRATES_STRUCT_LABELS = ['id', 'strate', 'rec.', 'h min', 'h mod.', 'h max', 'suppr.']
FACTORS_STRUCT_LABELS = ['id', 'facteur', 'impact', 'etat', 'suppr.']
