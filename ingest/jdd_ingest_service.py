import csv
import sys
from typing import List

from qgis.core import QgsProject

from ingest_service import IngestService


class JddIngestService(IngestService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)

    def get_datasets_from_pg(self):
        sql = """SELECT id, identifiant_jdd, libelle_court, libelle, description, date_debut, date_fin
        FROM metadonnees.jeu_de_donnees
        WHERE tag like '%cbnbp%' AND publier AND (date_part('year', date_fin) >= date_part('year', CURRENT_DATE) OR date_fin IS NULL)
        ORDER BY id"""
        self.pg_cur.execute(sql)
        fieldnames = [field[0] for field in self.pg_cur.description]
        datasets = self.pg_cur.fetchall()
        return datasets, fieldnames

    def save_datasets_into_csv(self, csv_file_name, datasets, fieldnames):
        with open(self.home_path / csv_file_name, 'w', encoding='utf-8') as output_file:
            line = ';'.join(header for header in fieldnames) + '\n'
            output_file.write(line)
            for dataset in datasets:
                dataset = ['"'+str(d).replace('"', '""')+'"' if d not in [None] else '' for d in dataset]  # Guillemets doubles pour les retours à la ligne et les ;
                line = ';'.join(d for d in dataset) + '\n'
                output_file.write(line)

    def build_target_dict(self, file_name: str) -> List[tuple]:
        target_data: List[tuple]
        with open(self.home_path / file_name, 'r', encoding='utf-8') as file:
            data = csv.DictReader(file, delimiter=';')
            target_data = [(line['id'], line['identifiant_jdd'], line['libelle_court'], line['libelle'],
                            line['description'], line['date_debut'], line['date_fin']) for line in data]
            # target_data = [tuple(list(line.values())) for line in data]
        return target_data

    def delete_current_content(self) -> bool:
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.execute('DELETE FROM jdd;')
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def import_new_datasets(self, data) -> bool:
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.executemany('INSERT INTO jdd (id, uuid, libelle_court, libelle, desc, date_debut, date_fin)'
                                        ' VALUES (?, ?, ?, ?, ?, ?, ?);', data)
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def process_pg_to_sqlite(self, pg_file_name: str, csv_file_name: str, sqlite_file_name: str):
        self.pg_connection(pg_file_name)
        datasets, fieldnames = self.get_datasets_from_pg()
        self.save_datasets_into_csv(csv_file_name, datasets, fieldnames)
        self.sqlite_connection(sqlite_file_name)
        if len(datasets) > 0 and self.sqlite_cur:
            self.delete_current_content()
            self.import_new_datasets(datasets)
        self.pg_conn.close()
        self.sqlite_conn.close()

    def process_pg_to_csv(self, pg_file_name: str, csv_file_name: str):
        self.pg_connection(pg_file_name)
        datasets, fieldnames = self.get_datasets_from_pg()
        self.save_datasets_into_csv(csv_file_name, datasets, fieldnames)
        self.pg_conn.close()

    def process_csv_to_sqlite(self, csv_file_name: str, sqlite_file_name: str):
        self.sqlite_connection(sqlite_file_name)
        target_data = self.build_target_dict(csv_file_name)
        if len(target_data) > 0 and self.sqlite_cur:
            self.delete_current_content()
            self.import_new_datasets(target_data)
        self.sqlite_conn.close()


if __name__ == '__main__':
    ingest_service = JddIngestService()
    if sys.argv[1] == '-pg-to-sqlite':
        ingest_service.process_pg_to_sqlite('.lobelia_db.env', sys.argv[2], 'donnees_habitats.sqlite')
    elif sys.argv[1] == '-pg-to-csv':
        ingest_service.process_pg_to_csv('.lobelia_db.env', sys.argv[2])
    elif sys.argv[1] == '-csv-to-sqlite':
        ingest_service.process_csv_to_sqlite(sys.argv[2], 'donnees_habitats.sqlite')
    else:
        print("ERREUR : Le 1er argument (devant faire référence à la méthode d'import) n'a pas été reconnu")
