import csv
import datetime
import sqlite3
import sys
from pathlib import Path
from sqlite3 import Connection, Cursor, Error, Row
from typing import Optional, List

from qgis.core import QgsProject


class ObsHabImportManager:
    def __init__(self, project: QgsProject = None):
        self.project: Optional[QgsProject] = project
        if self.project is None:
            self.project = QgsProject.instance()
        self.connection: Optional[Connection] = None
        self.cursor: Optional[Cursor] = None
        self.home_path: Path = Path(self.project.homePath())

    def check_survey_exists_by_id(self, survey_id: int) -> bool:
        result: List[Row] = []
        request = "SELECT id FROM releves WHERE id=%s"
        if self.cursor:
            result = self.cursor.execute(request % survey_id).fetchall()
        return len(result) > 0

    def check_obs_hab_exists_by_id(self, obs_hab_id: int) -> bool:
        result: List[Row] = []
        request = "SELECT obs_hab_id FROM obs_habitats WHERE obs_hab_id=%s"
        if self.cursor:
            result = self.cursor.execute(request % obs_hab_id).fetchall()
        return len(result) > 0

    def check_code_cite_exists_by_id(self, code: str, typo: int) -> bool:
        result: List[Row] = []
        request = "SELECT cd_hab FROM habref WHERE cd_typo=%s AND lb_code='%s'"
        if self.cursor:
            result = self.cursor.execute(request % (typo, code)).fetchall()
        return len(result) > 0

    def check_coverage_id_exists_by_id(self, coverage_id: int) -> bool:
        result: List[Row] = []
        request = "SELECT value FROM listes_valeurs WHERE list_name = 'RECOUVREMENT_VALUES' AND key = %s"
        if self.cursor:
            result = self.cursor.execute(request % coverage_id).fetchone()
        return len(result) > 0

    def check_form_id_exists_by_id(self, coverage_id: int) -> bool:
        result: List[Row] = []
        request = "SELECT value FROM listes_valeurs WHERE list_name = 'TYPE_FORME_VALUES' AND key = %s"
        if self.cursor:
            result = self.cursor.execute(request % coverage_id).fetchone()
        return len(result) > 0

    def get_obs_hab_id_by_values(self, obs_hab: dict) -> int:
        result: List[Row] = []
        request = "SELECT obs_hab_id FROM obs_habitats WHERE obs_hab_rel_id = %s AND obs_hab_nom_cite = \"%s\" " \
                  "AND obs_hab_code_cite = '%s' AND obs_hab_code_typo = %s AND obs_hab_recouvrement = %s"
        if self.cursor:
            result = self.cursor.execute(request % (obs_hab['obs_hab_rel_id'], obs_hab['obs_hab_nom_cite'],
                                                    obs_hab['obs_hab_code_cite'], obs_hab['obs_hab_code_typo'],
                                                    obs_hab['obs_hab_recouvrement'])).fetchall()
        return result[0]['obs_hab_id'] if (0 < len(result) < 2) else 0

    def insert_new_obs_hab(self, obs_hab: dict):
        request = "INSERT INTO obs_habitats (obs_hab_rel_id, obs_hab_nom_cite, obs_hab_code_cite, obs_hab_code_typo, " \
                  "obs_hab_recouvrement, obs_hab_forme_id, obs_hab_commentaire_habitat) " \
                  "VALUES (%s, \"%s\", '%s', %s, %s, %s, \"%s\")"
        if self.cursor:
            self.cursor.execute(request % (obs_hab['obs_hab_rel_id'], obs_hab['obs_hab_nom_cite'],
                                           obs_hab['obs_hab_code_cite'], obs_hab['obs_hab_code_typo'],
                                           obs_hab['obs_hab_recouvrement'], obs_hab['obs_hab_forme_id'],
                                           obs_hab['obs_hab_commentaire_habitat']))
            self.connection.commit()

    def insert_new_link(self, obs_hab_id_1: int, obs_hab_id_2: int):
        request = "INSERT INTO liens_obs_habitat (id_obs_hab_1, id_obs_hab_2) VALUES (%s, %s)"
        if self.cursor:
            self.cursor.execute(request % (obs_hab_id_1, obs_hab_id_2))
            self.connection.commit()

    def get_obs_hab_list(self, source_csv_file: str) -> List[dict]:
        target_list: List[dict]
        with open(self.home_path / source_csv_file, 'r', encoding='utf-8') as source:
            target_list = [{'obs_hab_rel_id': line['obs_hab_rel_id'],
                            'obs_hab_nom_cite': line['obs_hab_nom_cite'],
                            'obs_hab_code_cite': line['obs_hab_code_cite'],
                            'obs_hab_code_typo': line['obs_hab_code_typo'],
                            'obs_hab_recouvrement': line['obs_hab_recouvrement'],
                            'obs_hab_forme_id': line['obs_hab_forme_id'],
                            'obs_hab_commentaire_habitat': line['obs_hab_commentaire_habitat'],
                            'obs_hab_syntaxon_id_lien': [obs_hab_id.strip()
                                                         for obs_hab_id in line['obs_hab_syntaxon_id_lien'].split('/')
                                                         if obs_hab_id != '']}
                           for line in csv.DictReader(source, delimiter=';')]
        return target_list

    def check_obs_hab_list(self, obs_hab_list: List[dict]) -> bool:
        go: bool = True
        for obs_hab in obs_hab_list:
            if not self.check_survey_exists_by_id(obs_hab['obs_hab_rel_id']):
                go = False
                print("%s - obs_hab import - survey issue with id %s" %
                      (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), obs_hab['obs_hab_rel_id']))
            if not self.check_code_cite_exists_by_id(obs_hab['obs_hab_code_cite'], obs_hab['obs_hab_code_typo']):
                go = False
                print("%s - obs_hab import - code_cite issue with id %s and code_typo %s" %
                      (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), obs_hab['obs_hab_code_cite'],
                       obs_hab['obs_hab_code_typo']))
            if not self.check_coverage_id_exists_by_id(obs_hab['obs_hab_recouvrement']):
                go = False
                print("%s - obs_hab import - coverage issue with id %s" %
                      (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), obs_hab['obs_hab_recouvrement']))
            if not self.check_form_id_exists_by_id(obs_hab['obs_hab_forme_id']):
                go = False
                print("%s - obs_hab import - form issue with id %s" %
                      (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), obs_hab['obs_hab_forme_id']))
            for obs_hab_id in obs_hab['obs_hab_syntaxon_id_lien']:
                if not self.check_obs_hab_exists_by_id(obs_hab_id):
                    go = False
                    print("%s - obs_hab import - obs_hab to link issue with id %s" %
                          (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), obs_hab['obs_hab_syntaxon_id_lien']))
        return go

    def import_obs_hab_list(self, obs_hab_list: List[dict]):
        for obs_hab in obs_hab_list:
            self.insert_new_obs_hab(obs_hab)
            new_obs_hab_id: int = self.get_obs_hab_id_by_values(obs_hab)
            obs_hab_to_link_id_list: List[int] = obs_hab['obs_hab_syntaxon_id_lien']
            if new_obs_hab_id > 0:
                for obs_hab_to_link_id in obs_hab_to_link_id_list:
                    self.insert_new_link(new_obs_hab_id, obs_hab_to_link_id)
                    self.insert_new_link(obs_hab_to_link_id, new_obs_hab_id)
            else:
                print("%s - obs_hab import - obs_hab issue to get new obs_hab id %s" %
                      (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), obs_hab))

    def sqlite_connection(self, database_file_name) -> Optional[Connection]:
        try:
            self.connection = sqlite3.connect(self.home_path / database_file_name)
            self.connection.row_factory = sqlite3.Row
            self.connection.enable_load_extension(True)
            self.connection.load_extension("mod_spatialite")
        except Error as e:
            message = 'Connection failed - ' + str(e)
            print('Database connection', message)
        finally:
            if self.connection:
                self.cursor = self.connection.cursor()
            return self.connection

    def process(self, source_csv_file: str):
        self.sqlite_connection('donnees_habitats.sqlite')
        obs_hab_list_to_import = self.get_obs_hab_list(source_csv_file)
        if self.check_obs_hab_list(obs_hab_list_to_import):
            self.import_obs_hab_list(obs_hab_list_to_import)


if __name__ == '__main__':
    print("%s - Lobelia4QGis command line script launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    if hasattr(sys, 'argv') and len(sys.argv) > 2:
        option = sys.argv[1]
        source_file = sys.argv[2]
        if option == 'import':
            print("%s - links import launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            geometry_manager = ObsHabImportManager()
            geometry_manager.process(source_file)
            print("%s - links import ended" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        else:
            print("%s - unknown option : import available" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    else:
        print("# usage :", "python scripts/import_obs_hab_list_and_links.py import <csv source file>")
        print("# note 1:", "csv source file must be composed by obs_habitats columns")
        print("# note 1:", "at least : obs_hab_rel_id, code_cite, code_typo, recouvrement")
        print("# note 1:",
              "and an extra column obs_hab_syntaxon_id_lien to perform links between new obs_hab and existing one")
        print("# note 1:", "if you have multiple existing obs_hab to link, list them separated with a slash, "
                           "ex: id1/id2/id3")
        print("# note 2:", "always launch this command from project root directory")
    print("%s - Lobelia4QGis command line script closed" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
