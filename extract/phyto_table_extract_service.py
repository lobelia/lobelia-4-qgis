import csv
from sqlite3 import Row
from typing import List

from qgis.core import QgsProject, NULL
from qgis.utils import iface

from ..constants.connection_constants import SQLITE_FILE
from .a_extract_service import ExtractService


class PhytoTableExtractService(ExtractService):
    fieldnames: List[str]

    def __init__(self, project: QgsProject = None):
        super().__init__(project)
        self.filename = 'phyto_table.csv'

    def get_survey_list(self) -> List[Row]:
        query = """SELECT rel.id AS id, rel_uuid AS uuid, rel.date_obs AS date, rel.rel_rec_total AS recouvrement,
        rel.rel_rec_hauteur AS hauteur, rel.rel_lieu_dit AS lieu_dit, rel.rel_num AS num, rel.rel_surface AS surface,
        rel.rel_altitude AS altitude, rel_hab.obs_hab_code_cite AS code_syntaxon, rel_hab.obs_hab_nom_cite AS syntaxon,
        type.value AS type, forme.value AS forme, proto.value AS protocol, expo.value AS exposition, pente.value AS pente,
        MIN(hauteur_min) FILTER (WHERE strate.value LIKE '%Strate arborée%') AS hauteur_min_arbo,
        ROUND(AVG(hauteur_modale) FILTER (WHERE strate.value LIKE '%Strate arborée%'), 2) AS hauteur_mod_arbo,
        MAX(hauteur_max) FILTER (WHERE strate.value LIKE '%Strate arborée%') AS hauteur_max_arbo,
        MIN(hauteur_min) FILTER (WHERE strate.value LIKE '%Strate arbustive%') AS hauteur_min_arbu,
        ROUND(AVG(hauteur_modale) FILTER (WHERE strate.value LIKE '%Strate arbustive%'), 2) AS hauteur_mod_arbu,
        MAX(hauteur_max) FILTER (WHERE strate.value LIKE '%Strate arbustive%') AS hauteur_max_arbu,
        MIN(hauteur_min) FILTER (WHERE strate.value LIKE '%Strate herbacée%') AS hauteur_min_herb,
        ROUND(AVG(hauteur_modale) FILTER (WHERE strate.value LIKE '%Strate herbacée%'), 2) AS hauteur_mod_herb,
        MAX(hauteur_max) FILTER (WHERE strate.value LIKE '%Strate herbacée%') AS hauteur_max_herb,
        MIN(hauteur_min) FILTER (WHERE strate.value LIKE '%Recouvrement lichénique%' OR strate.value LIKE '%Strate à sphaignes%' OR strate.value LIKE '%Strate des autres bryophytes%' OR strate.value LIKE '%Strate bryolichenique%' OR strate.value LIKE '%Recouvrement bryophytique%') AS hauteur_min_musc,
        ROUND(AVG(hauteur_modale) FILTER (WHERE strate.value LIKE '%Recouvrement lichénique%' OR strate.value LIKE '%Strate à sphaignes%' OR strate.value LIKE '%Strate des autres bryophytes%' OR strate.value LIKE '%Strate bryolichenique%' OR strate.value LIKE '%Recouvrement bryophytique%'), 2) AS hauteur_mod_musc,
        MAX(hauteur_max) FILTER (WHERE strate.value LIKE '%Recouvrement lichénique%' OR strate.value LIKE '%Strate à sphaignes%' OR strate.value LIKE '%Strate des autres bryophytes%' OR strate.value LIKE '%Strate bryolichenique%' OR strate.value LIKE '%Recouvrement bryophytique%') AS hauteur_max_musc,
        MAX(recouvrement) FILTER (WHERE strate.value LIKE '%Strate arborée%') AS recouvrement_arbo,
        MAX(recouvrement) FILTER (WHERE strate.value LIKE '%Strate arbustive%') AS recouvrement_arbu,
        MAX(recouvrement) FILTER (WHERE strate.value LIKE '%Strate herbacée%') AS recouvrement_herb,
        MAX(recouvrement) FILTER (WHERE strate.value LIKE '%Recouvrement lichénique%' OR strate.value LIKE '%Strate à sphaignes%' OR strate.value LIKE '%Strate des autres bryophytes%' OR strate.value LIKE '%Strate bryolichenique%' OR strate.value LIKE '%Recouvrement bryophytique%') AS recouvrement_musc,
        MAX(recouvrement) FILTER (WHERE strate.value LIKE '%Recouvrement algal%') AS recouvrement_alg,
        tax.cd_nom, tax.nom_cite, tax.cd_nom_valide AS cd_ref, tax.nom_valide,
        coef_arbo.value AS coef_arbo, coef_arbu.value AS coef_arbu, coef_herb.value AS coef_herb, coef_musc.value AS coef_musc,
        rel.rel_point_uuid AS point_uuid, rel.rel_ligne_uuid AS line_uuid, rel.rel_polygon_uuid AS polygon_uuid
        FROM releves AS rel
        LEFT JOIN obs_habitats rel_hab ON rel.rel_obs_syntaxon_id = rel_hab.obs_hab_id
        LEFT JOIN observations tax ON rel.id = tax.obs_rel_id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'TYPE_REL_VALUES') type ON type.key = rel.rel_type_rel_id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'FORME_RELEVE_VALUES') forme ON forme.key = rel.rel_forme_id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'PROTOCOLE_VALUES') proto ON proto.key = rel.rel_protocole_id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'EXPOSITION_VALUES') expo ON expo.key = rel.rel_exposition_id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'PENTE_VALUES') pente ON pente.key = rel.rel_pente_id
        LEFT JOIN liens_releve_strate lrs ON lrs.rel_id = rel.id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'STRATES_VALUES') strate ON strate.key = lrs.strate_id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'COEF_AD_VALUES') coef_arbo ON coef_arbo.key = tax.strate_arbo
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'COEF_AD_VALUES') coef_arbu ON coef_arbu.key = tax.strate_arbu
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'COEF_AD_VALUES') coef_herb ON coef_herb.key = tax.strate_herb
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'COEF_AD_VALUES') coef_musc ON coef_musc.key = tax.strate_musc
        WHERE type.value LIKE 'Relevé Flore/%'
        AND (rel_hab.obs_hab_code_typo = 0 OR rel_hab.obs_hab_code_typo IS NULL)
        GROUP BY rel.id, cd_nom, nom_cite
        """
        return self.connection.execute(query).fetchall()

    @staticmethod
    def get_taxon_list(survey_list: List[dict]) -> List[dict]:
        taxon_list: List[dict] = []
        taxons_dict: List[dict] = [{'name': survey['nom_cite'], 'cd_nom': survey['cd_nom'],
                                    'nom_valide': survey['nom_valide'], 'cd_ref': survey['cd_ref'],
                                    'A_strate': survey['coef_arbo'], 'a_strate': survey['coef_arbu'],
                                    'h_strate': survey['coef_herb'], 'm_strate': survey['coef_musc']}
                                   for survey in survey_list if survey['nom_cite']]
        for taxon in taxons_dict:
            if taxon['A_strate']:
                taxon_list.append({'name': taxon['name'], 'cd_nom': taxon['cd_nom'], 'nom_valide': taxon['nom_valide'],
                                   'cd_ref': taxon['cd_ref'], 'strate': 'A'})
            if taxon['a_strate']:
                taxon_list.append({'name': taxon['name'], 'cd_nom': taxon['cd_nom'], 'nom_valide': taxon['nom_valide'],
                                   'cd_ref': taxon['cd_ref'], 'strate': 'a'})
            if taxon['h_strate']:
                taxon_list.append({'name': taxon['name'], 'cd_nom': taxon['cd_nom'], 'nom_valide': taxon['nom_valide'],
                                   'cd_ref': taxon['cd_ref'], 'strate': 'h'})
            if taxon['m_strate']:
                taxon_list.append({'name': taxon['name'], 'cd_nom': taxon['cd_nom'], 'nom_valide': taxon['nom_valide'],
                                   'cd_ref': taxon['cd_ref'], 'strate': 'm'})
            if taxon['A_strate'] is None and taxon['a_strate'] is None and taxon['h_strate'] is None and taxon['m_strate'] is None:
                taxon_list.append({'name': taxon['name'], 'cd_nom': taxon['cd_nom'], 'nom_valide': taxon['nom_valide'],
                                   'cd_ref': taxon['cd_ref'], 'strate': 'X'})
        taxon_list = [taxon for n, taxon in enumerate(taxon_list) if taxon not in taxon_list[n + 1:]]
        return taxon_list

    def get_uuid_and_geom_from_survey(self, survey: dict) -> (str, str):
        geom_uuid = self.concat([survey['point_uuid'], survey['line_uuid'], survey['polygon_uuid']])
        for geom in self.get_all_geom_list():
            if geom['uuid'] == geom_uuid:
                return geom_uuid, geom['geometry']
        return geom_uuid, None

    @staticmethod
    def concat_heights(survey: dict, stratum: str) -> str:
        try:
            return str(survey[f'hauteur_min_{stratum}'] if survey[f'hauteur_min_{stratum}'] is not None else 'NR') + \
                ' / ' + str(survey[f'hauteur_mod_{stratum}'] if survey[f'hauteur_mod_{stratum}'] is not None else 'NR') + \
                ' / ' + str(survey[f'hauteur_max_{stratum}'] if survey[f'hauteur_max_{stratum}'] is not None else 'NR')
        except Exception:
            iface.messageBar().pushWarning('dict', str(survey))
            return ''

    @staticmethod
    def calculate_taxon_number(survey_list: List[dict], survey_id: int) -> int:
        cd_noms = set([survey['cd_nom'] for survey in survey_list
                       if survey['id'] == survey_id and survey['cd_nom']])
        return len(cd_noms)

    @staticmethod
    def get_city_from_geom(geom: str) -> str:
        # TODO to reactivate when L4Q will be a plugin or manage it crossing city layers
        # city: str = ''
        # try:
        #     in_proj, out_proj = Proj('epsg:2154'), Proj('epsg:4326')
        #     geolocator = Nominatim(user_agent='Lobelia4Qgis')
        #     x1, y1 = wkt.loads(geom).centroid.coords[0]
        #     x2, y2 = transform(in_proj, out_proj, x1, y1)
        #     location = geolocator.reverse(str(x2) + ',' + str(y2))
        #     if location:
        #         address = location.raw['address']
        #         city = address.get('village', '') if address else ''
        # finally:
        #     return city
        return ''

    def get_unique_survey_list(self, survey_list: List[dict]) -> List[dict]:
        unique_survey_list: List[dict] = []
        survey_ids: List[int] = []
        for survey in survey_list:
            if survey['id'] not in survey_ids:
                survey_ids.append(survey['id'])
                geom_uuid, geom = self.get_uuid_and_geom_from_survey(survey)
                unique_survey_list.append({'id': survey['id'], 'type': survey['type'], 'rel_uuid': survey['uuid'],
                                           'protocole': survey['protocol'], 'forme releve': survey['forme'],
                                           'num': survey['num'], 'date': survey['date'], 'geom_uuid': geom_uuid,
                                           'geom': geom, 'commune': self.get_city_from_geom(geom),
                                           'lieu dit': survey['lieu_dit'], 'code syntaxon': survey['code_syntaxon'],
                                           'syntaxon': survey['syntaxon'],
                                           'observateur': self.calculate_observers(survey['id']),
                                           'surface': survey['surface'], 'altitude': survey['altitude'],
                                           'exposition': survey['exposition'], 'pente': survey['pente'],
                                           'hauteur arbo min/mod/max (m)': self.concat_heights(survey, 'arbo'),
                                           'hauteur arbu min/mod/max (m)': self.concat_heights(survey, 'arbu'),
                                           'hauteur herb min/mod/max (m)': self.concat_heights(survey, 'herb'),
                                           'hauteur musc min/mod/max (m)': self.concat_heights(survey, 'musc'),
                                           'hauteur totale (m)': survey['hauteur'],
                                           'rec arbo (%)': survey['recouvrement_arbo'],
                                           'rec arbu (%)': survey['recouvrement_arbu'],
                                           'rec herb (%)': survey['recouvrement_herb'],
                                           'rec musc (%)': survey['recouvrement_musc'],
                                           'rec alg (%)': survey['recouvrement_alg'],
                                           'rec total (%)': survey['recouvrement'],
                                           'nb taxons': self.calculate_taxon_number(survey_list, survey['id'])})
        return unique_survey_list

    @staticmethod
    def get_unique_survey_id_from_survey_id(survey: dict, unique_survey_list: List[dict]):
        i = 1
        for unique_survey in unique_survey_list:
            if survey['id'] == unique_survey['id']:
                return i
            i += 1
        return i

    @staticmethod
    def build_survey_block(target_dict: List[dict], unique_survey_list: List[dict]):
        rows_title = ['id', 'rel_uuid', 'type', 'protocole', 'forme releve', 'num', 'date', 'geom_uuid', 'geom',
                      'commune', 'lieu dit', 'code syntaxon', 'syntaxon', 'observateur', 'surface', 'altitude',
                      'exposition', 'pente', 'hauteur arbo min/mod/max (m)', 'hauteur arbu min/mod/max (m)',
                      'hauteur herb min/mod/max (m)', 'hauteur musc min/mod/max (m)', 'hauteur totale (m)',
                      'rec arbo (%)', 'rec arbu (%)', 'rec herb (%)', 'rec musc (%)', 'rec alg (%)', 'rec total (%)',
                      'nb taxons']
        for row in rows_title:
            line: dict = {'releve_nom_cite': row}
            for i in range(1, len(unique_survey_list) + 1):
                survey = unique_survey_list[i - 1]
                line['R' + str(i)] = survey[row] if row in survey.keys() else None
            target_dict.append(line)

    def build_taxon_block(self, target_dict: List[dict], taxon_list: List[dict], survey_list: List[dict],
                          unique_survey_list: List[dict]):
        for taxon in taxon_list:
            line: dict = {'releve_nom_cite': taxon['name'], 'cd_nom': taxon['cd_nom'],
                          'nom_valide': taxon['nom_valide'], 'cd_ref': taxon['cd_ref'], 'strate': taxon['strate']}
            strate: str = 'coef'
            if taxon['strate'] == 'A':
                strate = 'coef_arbo'
            elif taxon['strate'] == 'a':
                strate = 'coef_arbu'
            elif taxon['strate'] == 'h':
                strate = 'coef_herb'
            elif taxon['strate'] == 'm':
                strate = 'coef_musc'

            for survey in survey_list:
                if survey['cd_nom'] == taxon['cd_nom'] and survey['nom_cite'] == taxon['name'] and survey[
                     'coef_arbo'] is None and survey['coef_arbu'] is None and survey['coef_herb'] is None and survey[
                     'coef_musc'] is None and strate == 'coef':
                    line['R' + str(self.get_unique_survey_id_from_survey_id(survey, unique_survey_list))] = 'X'
                elif survey['cd_nom'] == taxon['cd_nom'] and survey['nom_cite'] == taxon[
                     'name'] and strate != 'coef' and survey[strate]:
                    line['R' + str(self.get_unique_survey_id_from_survey_id(survey, unique_survey_list))] = survey[
                        strate]
            target_dict.append(line)

    def build_target_dict(self) -> List[dict]:
        target_dict: List[dict] = []
        survey_list: List[dict] = self.row_to_dict(self.get_survey_list())
        taxon_list: List[dict] = self.get_taxon_list(survey_list)
        unique_survey_list = self.get_unique_survey_list(survey_list)

        self.fieldnames = ['releve_nom_cite', 'cd_nom', 'nom_valide', 'cd_ref', 'strate']
        for i in range(1, len(unique_survey_list) + 1):
            self.fieldnames.append('R' + str(i))

        self.build_survey_block(target_dict, unique_survey_list)
        self.build_taxon_block(target_dict, taxon_list, survey_list, unique_survey_list)
        return target_dict

    def export_csv(self, target_dict: List[dict]):
        with open(self.home_path / self.filename, 'w', encoding='utf-8', newline='') as file:
            writer = csv.DictWriter(file, fieldnames=self.fieldnames, delimiter=';')
            writer.writeheader()
            writer.writerows(target_dict)

    def process(self, database_file_name: str = SQLITE_FILE):
        self.sqlite_connection(database_file_name)
        target_data = self.build_target_dict()
        if len(target_data) > 0 and self.cursor:
            self.export_csv(target_data)
        self.connection.close()
        self.send_message('Export phyto', self.filename)
