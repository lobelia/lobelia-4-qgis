import psycopg2
from configparser import ConfigParser
import sqlite3
from pathlib import Path
from sqlite3 import Connection, Cursor, Error
from typing import Optional, List

from qgis.core import QgsProject
from qgis.utils import iface


class IngestService:
    def __init__(self, project: QgsProject = None):
        self.project: Optional[QgsProject] = project
        self.sqlite_conn: Optional[Connection] = None
        self.sqlite_cur: Optional[Cursor] = None
        self.pg_conn: Optional[Connection] = None
        self.pg_cur: Optional[Cursor] = None
        self.home_path: Path
        if self.project:
            self.home_path = Path(self.project.homePath())
        else:
            self.home_path = Path.cwd()

    def sqlite_connection(self, database_file_name) -> Optional[Connection]:
        try:
            self.sqlite_conn = sqlite3.connect(self.home_path / database_file_name)
        except Error as e:
            message = 'Connection failed - ' + str(e)
            iface.messageBar().pushWarning('Database connection', message)
        finally:
            if self.sqlite_conn:
                self.sqlite_cur = self.sqlite_conn.cursor()
            return self.sqlite_conn

    def pg_connection(self, pg_file_name):
        try:
            pg_db_params = ConfigParser()
            pg_db_params.read(pg_file_name)
            self.pg_conn = psycopg2.connect(
                host=pg_db_params.get('POSTGRES_CONNECTION', 'HOST'),
                database=pg_db_params.get('POSTGRES_CONNECTION', 'DATABASE'),
                user=pg_db_params.get('POSTGRES_CONNECTION', 'USER'),
                password=pg_db_params.get('POSTGRES_CONNECTION', 'PASSWORD'),
                port=pg_db_params.get('POSTGRES_CONNECTION', 'PORT')
            )
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if self.pg_conn:
                self.pg_cur = self.pg_conn.cursor()

    def build_target_dict(self, file_name: str) -> List[tuple]:
        return NotImplementedError

    def delete_current_content(self) -> bool:
        return NotImplementedError

    def process_pg_to_sqlite(self, pg_file_name: str, csv_file_name: str, sqlite_file_name: str):
        return NotImplementedError

    def process_pg_to_csv(self, pg_file_name: str, csv_file_name: str):
        return NotImplementedError

    def process_csv_to_sqlite(self, csv_file_name: str, sqlite_file_name: str):
        return NotImplementedError
