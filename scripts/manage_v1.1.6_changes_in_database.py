import datetime
import sqlite3
import sys
from pathlib import Path
from sqlite3 import Connection, Cursor, Error
from typing import Optional

from qgis.core import QgsProject


class DatabaseManager:
    def __init__(self, project: QgsProject = None):
        self.project: Optional[QgsProject] = project
        if self.project is None:
            self.project = QgsProject.instance()
        self.connection: Optional[Connection] = None
        self.cursor: Optional[Cursor] = None
        self.home_path: Path = Path(self.project.homePath())

    def update_phyto_survey_to_flora_one(self) -> bool:
        result: bool = False
        request = "UPDATE releves SET rel_type_rel_id = 1 WHERE rel_type_rel_id = 2"
        if self.cursor:
            self.cursor.execute(request)
            self.connection.commit()
            result = True
        return result

    def update_inventory_at_stake_protocol(self) -> bool:
        result: bool = False
        request = "UPDATE releves SET rel_protocole_id = 2 WHERE rel_protocole_id = 8"
        if self.cursor:
            self.cursor.execute(request)
            self.connection.commit()
            result = True
        return result

    def sqlite_connection(self, database_file_name) -> Optional[Connection]:
        try:
            self.connection = sqlite3.connect(self.home_path / database_file_name)
            self.connection.row_factory = sqlite3.Row
            self.connection.enable_load_extension(True)
            self.connection.load_extension("mod_spatialite")
        except Error as e:
            message = 'Connection failed - ' + str(e)
            print('Database connection', message)
        finally:
            if self.connection:
                self.cursor = self.connection.cursor()
            return self.connection

    def process(self):
        self.sqlite_connection('donnees_habitats.sqlite')
        self.update_phyto_survey_to_flora_one()
        self.update_inventory_at_stake_protocol()


if __name__ == '__main__':
    print("%s - Lobelia4QGis command line script launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    if hasattr(sys, 'argv') and len(sys.argv) > 1:
        option = sys.argv[1]
        if option == 'update':
            print("%s - update phyto survey to flora one launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            geometry_manager = DatabaseManager()
            geometry_manager.process()
            print("%s - update phyto survey to flora one ended" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        else:
            print("%s - unknown option : update available" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    else:
        print("# usage :", "python scripts/manage_v1.1.6_changes_in_database.py update")
        print("# note  :", "always launch this command from project root directory")
    print("%s - Lobelia4QGis command line script closed" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
