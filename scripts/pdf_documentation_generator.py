import re
import subprocess
from os.path import isfile
from pathlib import Path

from markdown import markdown
from weasyprint import HTML, CSS
from weasyprint.text.fonts import FontConfiguration

# USAGE
# adapt markdown_dir path if needed
# pull wiki repo first to refresh documentation to export
# launch this script from root directory : 'python3 scripts\pdf_documentation_generator.py'
dir_path = Path.cwd()
markdown_dir = dir_path.parent / 'lobelia-4-qgis.wiki'
files_to_pdfize = ['Utilisation-de-Lobelia4QGIS.md', 'Installation-de-Lobelia4QGIS.md']

for filename in files_to_pdfize:
    if isfile(markdown_dir.joinpath(filename)):
        input_file_path = markdown_dir / filename
        output_filename = filename.split('.')[0] + '.pdf'
        output_file_path = dir_path / output_filename

        with open(input_file_path, 'r') as f:
            html_text = markdown(f.read(), output_format='html')

        title = filename.split('.')[0].replace('-', ' ')
        html_text = '<h1>' + title.capitalize() + '</h1>' + html_text
        html_text = html_text.replace("<img alt=\"image\" src=\"uploads",
                                      "<img src=\"file://" + str(markdown_dir) + "/uploads")
        html_text = re.sub(r"href=\"(.*?).pdf\"",
                           r"href=\"https://gitlab.com/lobelia/lobelia-4-qgis/-/wikis/\1.pdf\"", html_text)
        html_text = html_text.replace("\\\"", "\"")

        html = HTML(string=html_text)
        font_config = FontConfiguration()
        css = CSS(string='''
                @page { size: A4; margin: 1.5cm;
                    @bottom-right { font-size: 0.7em; content: counter(page) " / " counter(pages); 
                        color: rgb(150, 150, 150); }
                    @bottom-left { font-size: 0.7em; content: "CBN du Bassin Parisien & CBN Sud-Atlantique";
                        color: rgb(150, 150, 150); }
                    @top-center { font-size: 0.7em; content: "Lobelia4QGis"; color: rgb(150, 150, 150); }
                }
                @font-face { font-family: Arial; }
                img { max-width: 100%; border-style: solid; border-width: 1px; border-color: rgb(200, 200, 200); 
                    margin-top: 6px; }
                h1 { text-align: center; color: rgb(74, 155, 40); font-size: 1.6em; }
                h2 { color: rgb(127, 201, 255); font-size: 1.4em; }
                h3 { color: rgb(255, 178, 127); font-style: italic; font-size: 1.2em; }
                h4 { color: rgb(74, 155, 40); font-style: italic; font-size: 1em; }
                p { font-size: 0.8em; text-align: justify; }
                li { font-size: 0.8em; }
                
            ''', font_config=font_config)
        html.write_pdf(output_file_path, stylesheets=[css], font_config=font_config)

        gs_args = ['gs', '-sDEVICE=pdfwrite', '-dCompatibilityLevel=1.4', '-dPDFSETTINGS=/prepress',
                   '-dNOPAUSE', '-dQUIET', '-dBATCH', '-dSAFER',
                   '-sOutputFile=' + output_file_path.__str__() + '_B.pdf', output_file_path.__str__()]
        subprocess.call(gs_args)
        subprocess.call(['mv', output_file_path.__str__() + '_B.pdf', output_file_path.__str__()])
