import sqlite3
import zipfile
from pathlib import Path

dir_path = Path.cwd()
files_to_package = ['database_specification.json', 'projet_lobelia4qgis.qgz', 'donnees_habitats.sqlite',
                    'survey_form.py', 'survey_form.ui', 'installation.pdf', 'documentation-utilisateur.pdf', 'LICENSE']
tables_to_clean = ['station_point', 'station_ligne', 'station_surface', 'releves', 'observations', 'obs_habitats',
                   'liens_releve_observateur', 'liens_releve_strate', 'liens_obs_habitat']

connection = sqlite3.connect('donnees_habitats.sqlite')
cursor = connection.cursor()

for table in tables_to_clean:
    cursor.execute('DELETE FROM %s' % table)
    cursor.execute("DELETE FROM sqlite_sequence WHERE name = '%s'" % table)
connection.commit()

version = cursor.execute("SELECT version FROM version WHERE id=1").fetchone()[0]
connection.close()

zip_name = 'lobelia_qgis_v' + version + '.zip'
zip_file = zipfile.ZipFile(zip_name, 'w', zipfile.ZIP_DEFLATED)
for file in files_to_package:
    zip_file.write(dir_path / file, file)
zip_file.close()

# Extended version
zip_name = 'lobelia_qgis_v' + version + '_extended.zip'
zip_file = zipfile.ZipFile(zip_name, 'w', zipfile.ZIP_DEFLATED)
files_to_package = files_to_package + ['carto_habitats.cpg', 'carto_habitats.dbf', 'carto_habitats.prj',
                                       'carto_habitats.shp', 'carto_habitats.shx']
for file in files_to_package:
    zip_file.write(dir_path / file, file)
zip_file.close()
