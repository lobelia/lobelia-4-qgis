# coding: utf-8
import csv
import json
import sqlite3
import uuid
from operator import itemgetter
from pathlib import Path
from sqlite3 import Connection
from sqlite3 import Cursor, Error
from sqlite3 import Row
from sys import platform
from typing import List, Optional

from PyQt5.QtCore import QDate, QVariant, QObject, pyqtSignal
from PyQt5.QtCore import QEvent
from PyQt5.QtCore import QUrl
from PyQt5.QtCore import Qt, QSize, QModelIndex
from PyQt5.QtGui import QColor, QBrush, QStandardItemModel, QStandardItem
from PyQt5.QtGui import QDesktopServices, QFont
from PyQt5.QtGui import QFocusEvent, QMouseEvent
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5.QtWidgets import QButtonGroup, QListWidget, QListWidgetItem, QTableWidgetItem, QWidget
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QHeaderView, QCalendarWidget, QFrame, QMessageBox
from PyQt5.QtWidgets import QTabWidget
from PyQt5.QtWidgets import QTableWidget, QLineEdit, QPlainTextEdit, QComboBox, QLabel, QTextEdit, QCheckBox
from qgis.PyQt.QtCore import NULL
from qgis.core import QgsApplication, QgsAuthMethodConfig, QgsAuthManager
from qgis.core import QgsFeature, QgsVectorLayer
from qgis.core import QgsGeometry, QgsWkbTypes
from qgis.core import QgsProject
from qgis.gui import QgisInterface
from qgis.gui import QgsAttributeForm
from qgis.utils import iface

SQLITE_FILE = "donnees_habitats.sqlite"

AUTH_CONFIG = 'lobelia_user'

SPECIFICATION_FILE = "database_specification.json"
TAXONS_HEADER_LABELS = ['cd_nom', 'taxon_cite', 'strate_arbo', 'strate_arbu', 'strate_herb', 'strate_musc', 'suppr.']
SYNTAXONS_HEADER_LABELS = ['code_phyto', 'syntaxon_cite', '% rec.', 'coef. rec.', 'forme', 'typ. struct.', 'typ. flor.',
                           'suppr.']
HIC_HEADER_LABELS = ['code', 'libelle', '% rec.', 'coef. rec.', 'forme', 'typ. struct.', 'typ. flor.', 'suppr.']
EUNIS_HEADER_LABELS = ['code', 'libelle', '% rec.', 'coef. rec.', 'forme', 'typ. struct.', 'typ. flor.', 'suppr.']
CORINE_HEADER_LABELS = ['code', 'libelle', '% rec.', 'coef. rec.', 'forme', 'typ. struct.', 'typ. flor.', 'suppr.']
HABITATS_HEADER_LABELS = ['code', 'libelle', 'code typo', 'typologie', 'suppr.']
STRATES_STRUCT_LABELS = ['id', 'strate', 'rec.', 'h min', 'h mod.', 'h max', 'suppr.']
FACTORS_STRUCT_LABELS = ['id', 'facteur', 'impact', 'etat', 'suppr.']


class DatabaseSpecificationService:
    @staticmethod
    def get_spec() -> Optional[dict]:
        spec: Optional[dict]
        project = QgsProject.instance()
        separator = "\\" if platform == "win32" else "/"
        with open(project.homePath() + separator + SPECIFICATION_FILE, 'r') as json_file:
            data = json_file.read()
            spec = json.loads(data)
        return spec

    @staticmethod
    def get_inputs_specification(table_name) -> List[dict]:
        form_inputs = []
        spec = DatabaseSpecificationService.get_spec()
        if spec['tables']:
            fields = [table['fields'] for table in spec['tables'] if table['name'] == table_name][0]
            form_inputs = [{"field_name": field['name'], "field_type": field['type'],
                            "input_name": field['input']['name'],
                            "input_type": field['input']['type']} for field in fields if 'input' in field]
        return form_inputs

    @staticmethod
    def get_combo_boxes_specification() -> List[dict]:
        combo_boxes_inputs = []
        spec = DatabaseSpecificationService.get_spec()
        if spec['tables']:
            fields = []
            grouped_fields = [table['fields'] for table in spec['tables']]
            for group_of_fields in grouped_fields:
                fields.extend(group_of_fields)
            for field in fields:
                if 'input' in field and field['input']['type'] == "QComboBox":
                    try:
                        combo_boxes_inputs.append({"input_name": field['input']['name'],
                                                   "input_list_name": field['input']['values']})
                    except:
                        pass
        return combo_boxes_inputs


class GeometryManager:
    def __init__(self, layer: QgsVectorLayer, feature: QgsFeature):
        self.geometry_layer: QgsVectorLayer = layer
        self.geometry_feature: QgsFeature = feature
        self.survey_feature: Optional[QgsFeature] = None

    def get_survey_feature(self, releves: QgsVectorLayer) -> QgsFeature:
        geometry_layer_name = self.geometry_layer.name()
        uuid_index = self.geometry_layer.fields().indexFromName('uuid')
        geometry_uuid = self.geometry_feature[uuid_index]  # geometry_uuid = self.geometry_feature['uuid']
        data_manager = DataManager()
        if geometry_uuid is None or geometry_uuid == "" \
                or data_manager.count_geometry_uuid(geometry_layer_name, geometry_uuid) > 1:
            geometry_uuid = self.set_geometry_uuid()

        rel_field_name = self.get_geometry_field_name(geometry_layer_name)

        releve_list = [releve for releve in releves.getFeatures() if releve[rel_field_name] == geometry_uuid]
        if len(releve_list) > 0:
            self.survey_feature = releve_list[0]
            survey_uuid = self.survey_feature['rel_uuid']
            if survey_uuid is None or survey_uuid == "" or survey_uuid == NULL:
                self.survey_feature.setAttribute('rel_uuid', str(uuid.uuid4()))
                releves.updateFeature(self.survey_feature)
                releves.commitChanges()
        else:
            self.survey_feature = self.create_new_releve(releves)
            self.survey_feature.setAttribute(rel_field_name, geometry_uuid)
            self.survey_feature.setAttribute('rel_uuid', str(uuid.uuid4()))
            releves.updateFeature(self.survey_feature)
            releves.commitChanges()

        return self.survey_feature

    def set_geometry_uuid(self) -> str:
        if not self.geometry_layer.isEditable():
            self.geometry_layer.startEditing()
        geometry_uuid = str(uuid.uuid4())
        self.geometry_feature.setAttribute(self.geometry_layer.fields().indexFromName('uuid'), geometry_uuid)
        self.geometry_layer.updateFeature(self.geometry_feature)
        self.geometry_layer.endEditCommand()
        self.geometry_layer.commitChanges()
        self.geometry_layer.startEditing()
        return geometry_uuid

    @staticmethod
    def get_geometry_field_name(geometry_layer_name) -> str:
        if geometry_layer_name == 'station_surface':
            return 'rel_polygon_uuid'
        elif geometry_layer_name == 'station_ligne':
            return 'rel_ligne_uuid'
        elif geometry_layer_name == 'station_point':
            return 'rel_point_uuid'
        else:
            return ''

    @staticmethod
    def create_new_releve(releves) -> QgsFeature:
        releve = QgsFeature()
        releve.setFields(releves.fields())
        if not releves.isEditable():
            releves.startEditing()
        releves.addFeature(releve)
        releves.updateFeature(releve)
        releves.endEditCommand()
        return releve

    def remove_overlaps(self) -> QgsGeometry:
        new_geometry: QgsGeometry = self.geometry_feature.geometry()
        if self.geometry_layer.geometryType() != QgsWkbTypes.PolygonGeometry:
            return new_geometry
        data_manager = DataManager()
        uuid_index = self.geometry_layer.fields().indexFromName('uuid')
        new_geometry_uuid = self.geometry_feature[uuid_index]
        new_geometry_survey_type = data_manager.get_survey_type_from_geometry(new_geometry_uuid)

        for geom_feature in self.geometry_layer.getFeatures():
            geom_uuid = geom_feature[uuid_index]
            geom_survey_type = data_manager.get_survey_type_from_geometry(geom_uuid)
            if geom_uuid != new_geometry_uuid and geom_survey_type == new_geometry_survey_type \
                    and geom_feature.geometry().intersects(new_geometry):
                new_geometry = new_geometry.difference(geom_feature.geometry())
        new_geometry = new_geometry.buffer(0, 20)

        if not self.geometry_layer.isEditable():
            self.geometry_layer.startEditing()
        new_geometry_feature_id = data_manager.get_station_id_from_geometry(self.geometry_layer, self.geometry_feature)
        new_geometry.convertToMultiType()
        self.geometry_layer.changeGeometry(new_geometry_feature_id, new_geometry)
        self.geometry_layer.commitChanges()
        return new_geometry


class ListValuesProvider:
    def __init__(self, project: QgsProject = QgsProject.instance()):
        self.data_manager = DataManager(project)

    def get_values_from_list_name(self, list_name: str) -> List[dict]:
        result_list = self.data_manager.get_list_values_from_list_name(list_name)
        returned_list = [{'id': value[0], 'value': value[1], 'order': value[2]}
                         for value in result_list if value[2] > -1]
        return returned_list

    def get_values_from_list_name_and_criteria(self, list_name: str, criteria: List[str]) -> List[dict]:
        result_list = self.data_manager.get_list_values_from_list_name_and_criteria(list_name, criteria)
        returned_list = [{'id': value[0], 'value': value[1], 'order': value[2]}
                         for value in result_list if value[2] > -1]
        return returned_list

    @staticmethod
    def get_value_from_list(_list: List[dict], _id: int):
        for line in _list:
            if line['id'] == _id:
                return line['value']
        return NULL

    @staticmethod
    def get_id_from_value(_list: List[dict], value: str):
        for line in _list:
            if line['value'] == value:
                return line['id']
        return None


class DataManager:
    def __init__(self, project: QgsProject = QgsProject.instance()):
        self.connection: Optional[Connection] = ConnectionHelper(iface).sqlite_connection(project)
        self.cursor: Optional[Cursor] = None
        if self.connection:
            self.cursor = self.connection.cursor()

    def get_list_values_from_list_name(self, list_name: str) -> List:
        result: List = []
        if self.cursor:
            request = 'SELECT key, value, relation FROM listes_valeurs WHERE list_name=?'
            result = self.cursor.execute(request, (list_name,)).fetchall()
        return result

    def get_list_values_from_list_name_and_criteria(self, list_name: str, chunks: List[str]) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT key, value, relation FROM listes_valeurs WHERE list_name='%s' AND value LIKE '%s' " \
                      "AND value LIKE '%s' ORDER BY value"
            first_chunk = '%' + chunks[0] + '%' if chunks[0] != '' else '%'
            second_chunk = '%' + chunks[1] + '%' if len(chunks) > 1 and chunks[1] != '' else '%'
            result = self.cursor.execute(request % (list_name, first_chunk, second_chunk)).fetchall()
        return result

    def get_value_from_key_and_list_name(self, key: str, list_name: str) -> str:
        result: List = []
        if self.cursor:
            request = 'SELECT value FROM listes_valeurs WHERE key=? AND list_name=?'
            result = self.cursor.execute(request, (key, list_name)).fetchone()
        return result[0] if len(result) > 0 else ""

    def get_taxons_from_criteria(self, chunks: List[str]) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM (SELECT *, CASE WHEN instr(lb_nom, ' ') = 0 THEN LENGTH(lb_nom) ELSE instr(lb_nom, ' ') END AS pos FROM taxref) " \
                      "WHERE substr(LOWER(lb_nom), 1, pos) LIKE '%s' AND substr(LOWER(lb_nom), pos+1) LIKE '%s'"
            first_chunk = chunks[0].lower() + '%' if chunks[0] != '' else '%'
            second_chunk = '%' + chunks[1].lower() + '%' if len(chunks) > 1 and chunks[1] != '' else '%'
            result = self.cursor.execute(request % (first_chunk, second_chunk)).fetchall()
        return result

    def get_jdd_list(self) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT uuid, libelle_court, libelle FROM jdd ORDER BY libelle_court"
            result = self.cursor.execute(request).fetchall()
        return result

    def get_jdd_name_from_uuid(self, uuid: str) -> str:
        result: List = []
        if self.cursor:
            request = "SELECT libelle FROM jdd WHERE uuid='%s'"
            result = self.cursor.execute(request % uuid).fetchone()
        return result[0]

    def check_jdd_uuid_exists(self, uuid: str) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM jdd WHERE uuid='%s'"
            result = self.cursor.execute(request % uuid).fetchall()
        return len(result) > 0

    def filter_jdd_list_by_name(self, chunks: List[str]) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT uuid, libelle_court, libelle FROM jdd WHERE LOWER(libelle_court) LIKE '%s' " \
                      "AND LOWER(libelle_court) LIKE '%s' ORDER BY libelle_court"
            first_chunk = '%' + chunks[0].lower() + '%' if chunks[0] != '' else '%'
            second_chunk = '%' + chunks[1].lower() + '%' if len(chunks) > 1 and chunks[1] != '' else '%'
            result = self.cursor.execute(request % (first_chunk, second_chunk)).fetchall()
        return result

    def copy_jdd_uuid_to_all_surveys(self, uuid: str):
        if self.cursor:
            request = "UPDATE releves SET rel_jdd_uuid='%s'"
            self.cursor.execute(request % uuid)
            self.connection.commit()

    def get_observers(self) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT obs_id, obs_prenom, obs_nom, obs_email FROM observateurs WHERE obs_turned_on=1"
            result = self.cursor.execute(request).fetchall()
        return result

    def get_linked_strates_to_survey(self, survey_id) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_releve_strate WHERE rel_id=%s"
            result = self.cursor.execute(request % survey_id).fetchall()
        return result

    def get_linked_factors_to_survey(self, survey_id) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_releve_influence WHERE rel_id=%s"
            result = self.cursor.execute(request % survey_id).fetchall()
        return result

    def get_strate_name(self, strate_id) -> str:
        result: List = []
        if self.cursor:
            request = "SELECT value FROM listes_valeurs WHERE list_name='STRATES_VALUES' AND key=%s"
            result = self.cursor.execute(request % strate_id).fetchone()
        return result[0]

    def get_survey_id_from_geometry(self, survey_feature: QgsFeature):
        result: List = []
        geometry_field: str = ""
        if survey_feature['rel_point_uuid']:
            geometry_field = 'rel_point_uuid'
        elif survey_feature['rel_ligne_uuid']:
            geometry_field = 'rel_ligne_uuid'
        elif survey_feature['rel_polygon_uuid']:
            geometry_field = 'rel_polygon_uuid'
        if self.cursor and geometry_field:
            request = "SELECT id FROM releves WHERE %s='%s'"
            result = self.cursor.execute(request % (geometry_field, survey_feature[geometry_field])).fetchone()
        return result[0]

    def get_station_id_from_geometry(self, geometry_layer: QgsVectorLayer, geometry_feature: QgsFeature):
        result: List = []
        uuid_index = geometry_layer.fields().indexFromName('uuid')
        if self.cursor:
            request = "SELECT id FROM %s WHERE uuid='%s'"
            result = self.cursor.execute(request % (geometry_layer.name(), geometry_feature[uuid_index])).fetchone()
        return result[0]

    def get_obs_syntaxon_id_from_survey_id(self, survey_id: int):
        result: List = []
        if self.cursor:
            request = "SELECT rel_obs_syntaxon_id FROM releves WHERE id=%s"
            result = self.cursor.execute(request % survey_id).fetchone()
        return result[0]

    def get_obs_habitat_id_from_uuid(self, obs_hab_uuid: str):
        result: List = []
        if self.cursor:
            request = "SELECT obs_hab_id FROM obs_habitats WHERE obs_hab_uuid='%s'"
            result = self.cursor.execute(request % obs_hab_uuid).fetchone()
        return result[0]

    def obs_syntaxon_id_exists(self, survey_id) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT rel_obs_syntaxon_id FROM releves WHERE id=%s"
            result = self.cursor.execute(request % survey_id).fetchone()
        return result and len(result) > 0 and result[0]

    def insert_links_between_obs_hab(self, obs_syntaxon_id: int, obs_habitat_id: int):
        if self.cursor:
            request = "INSERT INTO liens_obs_habitat (id_obs_hab_1, id_obs_hab_2) VALUES (%s, %s)"
            self.cursor.execute(request % (obs_syntaxon_id, obs_habitat_id))
            self.cursor.execute(request % (obs_habitat_id, obs_syntaxon_id))
            self.connection.commit()

    def remove_links_between_obs_hab(self, obs_syntaxon_id: int, obs_habitat_id: int):
        if self.cursor:
            request = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_1=%s AND id_obs_hab_2=%s"
            self.cursor.execute(request % (obs_syntaxon_id, obs_habitat_id))
            self.cursor.execute(request % (obs_habitat_id, obs_syntaxon_id))
            self.connection.commit()

    def remove_links_by_obs_hab_id(self, obs_habitat_id: int):
        if self.cursor:
            request_1 = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_1=%s"
            request_2 = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_2=%s"
            self.cursor.execute(request_1 % obs_habitat_id)
            self.cursor.execute(request_2 % obs_habitat_id)
            self.connection.commit()

    def remove_dominant_taxon_from_obs_hab_id(self, obs_hab_id: int):
        if self.cursor:
            request = "UPDATE obs_habitats SET syntaxon_comm_non_sat_id = NULL, syntaxon_comm_non_sat_taxon_dom = NULL," \
                      "syntaxon_comm_non_sat_taxon_dom_nom = NULL " \
                      "WHERE obs_hab_id = %s"
            self.cursor.execute(request % obs_hab_id)
            self.connection.commit()

    def are_obs_hab_linked(self, obs_hab_id_1: int, obs_hab_id_2: int) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_obs_habitat WHERE id_obs_hab_1=%s AND id_obs_hab_2=%s"
            result = self.cursor.execute(request % (obs_hab_id_1, obs_hab_id_2)).fetchone()
        return result and len(result) > 0 and result[0]

    def is_obs_hab_linked_to_other(self, obs_habitat_id: int, obs_syntaxon_id: int) -> bool:
        result: List = []
        if self.cursor:
            request = "SELECT id FROM liens_obs_habitat WHERE id_obs_hab_1=%s AND id_obs_hab_2!=%s"
            result = self.cursor.execute(request % (obs_habitat_id, obs_syntaxon_id)).fetchone()
        return result and len(result) > 0 and result[0]

    def get_linked_obs_hab(self, obs_syntaxon_id: int) -> List:
        result: List = []
        if self.cursor:
            request = "SELECT id_obs_hab_2 FROM liens_obs_habitat WHERE id_obs_hab_1=%s"
            result = self.cursor.execute(request % obs_syntaxon_id).fetchall()
        return result

    def clean_obs_hab_link_in_assoc_survey(self, obs_hab_rel_assoc_id: int):
        if self.cursor:
            request = "UPDATE releves SET rel_obs_syntaxon_id = NULL WHERE id=%s"
            self.cursor.execute(request % obs_hab_rel_assoc_id)
            self.connection.commit()

    def clean_other_obs_hab_link_to_assoc_survey(self, obs_hab_rel_assoc_id: int):
        if self.cursor:
            request = "UPDATE obs_habitats SET obs_hab_rel_assoc_id = NULL WHERE obs_hab_rel_assoc_id=%s"
            self.cursor.execute(request % obs_hab_rel_assoc_id)
            self.connection.commit()

    def clean_link_to_assoc_survey(self, obs_habitat_id: int):
        if self.cursor:
            request = "UPDATE obs_habitats SET obs_hab_rel_assoc_id = NULL WHERE obs_hab_id=%s"
            self.cursor.execute(request % obs_habitat_id)
            self.connection.commit()

    def get_related_habitats_list_from_code_phyto(self, code_phyto: object, cd_typo: int = None):
        result: List = []
        if self.cursor:
            request = 'SELECT cd_hab, cd_typo, lb_code, lb_hab_fr FROM corresp_syntaxons_habitats WHERE code_phyto=?'
            if cd_typo:
                request += f' AND cd_typo={str(cd_typo)}'
            request += '  ORDER BY cd_typo, lb_code'
            result = self.cursor.execute(request, (code_phyto,)).fetchall()
        return result

    def get_habitat_from_cd_hab(self, cd_hab: int):
        result: List = []
        if self.cursor:
            request = 'SELECT cd_typo, lb_code, lb_hab_fr FROM habref WHERE cd_hab=?'
            result = self.cursor.execute(request, (str(cd_hab),)).fetchall()
        return result[0]

    def count_geometry_uuid(self, geometry_table: str, geometry_uuid: str) -> int:
        result: List = []
        if self.cursor:
            request = "SELECT COUNT(id) FROM %s WHERE uuid='%s'"
            result = self.cursor.execute(request % (geometry_table, geometry_uuid)).fetchone()
        return result[0] if len(result) > 0 else 0

    def get_survey_type_from_geometry(self, geometry_uuid: str) -> int:
        result: List = []
        if self.cursor:
            request = "SELECT rel_type_rel_id FROM releves WHERE rel_polygon_uuid='%s'"
            result = self.cursor.execute(request % geometry_uuid).fetchone()
        return result[0]


class DatabaseBuilder:
    def __init__(self, project: QgsProject):
        self.spec: Optional[dict] = DatabaseSpecificationService.get_spec()
        self.connection: Optional[Connection] = ConnectionHelper(iface).sqlite_connection(project)
        self.cursor: Optional[Cursor]
        if self.connection:
            self.cursor = self.connection.cursor()

    def build_or_alter(self):
        existing_tables: List[str] = []
        tables_to_create: List[str] = []

        if self.get_database_version() != self.spec['version']:
            for table in self.spec['tables']:
                table_name = table['name']
                if self.table_exists(table_name):
                    existing_tables.append(table_name)
                else:
                    tables_to_create.append(table_name)

            for table_name in existing_tables:
                self.alter_table(table_name)

            for table_name in tables_to_create:
                self.create_table(table_name)

            self.update_database_version(self.spec['version'])

    def table_exists(self, table_name: str) -> bool:
        request = "SELECT count(name) FROM sqlite_master WHERE type='table' AND name='" + table_name + "'"
        self.cursor.execute(request)
        fetchone = self.cursor.fetchone()
        return fetchone[0] == 1

    def alter_table(self, table_name: str):
        altered = False
        table_specification = [table for table in self.spec['tables'] if table['name'] == table_name][0]
        target_columns = [(field['name'], field['type']) for field in table_specification['fields']]
        existing_columns = [i[1] for i in self.cursor.execute('PRAGMA table_info(%s)' % table_name)]

        for column in target_columns:
            if column[0] not in existing_columns:
                self.cursor.execute("ALTER TABLE " + table_name + " ADD COLUMN '%s' '%s'" % (column[0], column[1]))
                altered = True

        if table_specification['is_geometric']:
            geometry_specification = table_specification['geometry']
            geometry_name = geometry_specification['name']
            if geometry_name not in existing_columns:
                self.add_geometry_column(table_name, geometry_specification)
                altered = True

        if 'indices' in table_specification:
            self.add_indices(table_specification)

        if altered:
            iface.messageBar().pushInfo('Database builder', table_name + ' table altered')
        self.connection.commit()

    def create_table(self, table_name: str):
        table_specification = [table for table in self.spec['tables'] if table['name'] == table_name][0]
        columns = [(field['name'], field['type'], field['complement'] if 'complement' in field else '')
                   for field in table_specification['fields']]
        fields = []

        for column in columns:
            fields.append(' '.join(column))

        joined_fields = ", ".join(fields)
        request = "CREATE TABLE IF NOT EXISTS " + table_name + "(" + joined_fields + ")"
        self.cursor.execute(request)

        if table_specification['is_geometric']:
            self.add_geometry_column(table_name, table_specification['geometry'])

        if 'indices' in table_specification:
            self.add_indices(table_specification)

        iface.messageBar().pushInfo('Database builder', table_name + ' table created')
        self.connection.commit()

    def add_geometry_column(self, table_name: str, geometry):
        geom_specification = (table_name, geometry['name'], geometry['projection'], geometry['type'], geometry['axes'])
        geom_request = "SELECT AddGeometryColumn('%s', '%s', %s, '%s', '%s')"
        self.cursor.execute(geom_request % geom_specification)
        iface.messageBar().pushInfo('Database builder', table_name + ' - geometry added')

    def add_indices(self, table_specification: dict):
        table_name = table_specification['name']
        existing_indices = [i[1] for i in self.cursor.execute('PRAGMA index_list(%s)' % table_name)]
        indices = [(index['name'], table_name,
                    index['field'] + ' ' + index['complement'] if 'complement' in index else index['field'])
                   for index in table_specification['indices']]
        request = "CREATE INDEX %s ON %s (%s)"
        for index in indices:
            if index[0] not in existing_indices:
                self.cursor.execute(request % index)
                iface.messageBar().pushInfo('Database builder', table_name + ' - index ' + index[0] + ' added')

    def get_database_version(self) -> str:
        result: str = ""
        if self.cursor:
            request = "SELECT version FROM version WHERE id=1"
            result = self.cursor.execute(request).fetchone()[0]
        return result

    def update_database_version(self, version: str):
        if self.cursor:
            request = "UPDATE version SET version=? WHERE id=1"
            self.cursor.execute(request, (version,))
            self.connection.commit()


class DatabaseCleaner:
    def __init__(self, project: QgsProject = None):
        self.project: Optional[QgsProject] = project
        if self.project is None:
            self.project = QgsProject.instance()
        self.connection: Optional[Connection] = None
        self.cursor: Optional[Cursor] = None
        self.home_path: Path = Path(self.project.homePath())
        self.geometry_tables = [('station_point', 'point'), ('station_ligne', 'ligne'), ('station_surface', 'polygon')]

    def clean(self):
        self.clean_orphans()
        try:
            iface.messageBar().pushInfo('Database cleaning', 'orphans cleaning OK (1/2)')
        except AttributeError as e:
            pass
        self.correct_encoding()
        try:
            iface.messageBar().pushInfo('Database cleaning', 'encoding correction OK (2/2)')
        except AttributeError as e:
            pass

    def clean_orphans(self):
        self.sqlite_connection(SQLITE_FILE)
        self.clean_survey_orphans()
        self.clean_observations_orphans()
        self.clean_obs_syntaxon_orphans()
        self.clean_phantom_obs_hab()
        self.clean_duplicated_obs_syntaxon()
        self.clean_orphan_links()

    def clean_survey_orphans(self):
        orphans_request = "SELECT releves.id FROM releves LEFT JOIN "
        join_clause = []
        for geometry_table in self.geometry_tables:
            join_clause.append(geometry_table[0] + " on " + geometry_table[0] + ".uuid = releves.rel_" +
                               geometry_table[1] + "_uuid ")
        orphans_request += "LEFT JOIN ".join(join_clause) + "WHERE "
        where_clause = []
        for geometry_table in self.geometry_tables:
            where_clause.append(geometry_table[0] + ".uuid is NULL")
        orphans_request += " AND ".join(where_clause)
        self.cursor.execute(orphans_request)
        orphans = [orphan[0] for orphan in self.cursor.fetchall()]

        if len(orphans) > 0:
            delete_request: str
            if len(orphans) == 1:
                delete_request = "DELETE FROM releves WHERE id = " + str(orphans[0])
            else:
                delete_request = "DELETE FROM releves WHERE id in " + str(tuple(orphans))
            self.cursor.execute(delete_request)
            self.connection.commit()

    def clean_observations_orphans(self):
        request = "SELECT observations.obs_id FROM observations LEFT JOIN releves on releves.id = " \
                  "observations.obs_rel_id WHERE releves.id is NULL"
        self.cursor.execute(request)
        orphans = [orphan[0] for orphan in self.cursor.fetchall()]

        if len(orphans) > 0:
            delete_request: str
            if len(orphans) == 1:
                delete_request = "DELETE FROM observations WHERE obs_id = " + str(orphans[0])
            else:
                delete_request = "DELETE FROM observations WHERE obs_id in " + str(tuple(orphans))
            self.cursor.execute(delete_request)
            self.connection.commit()

    def delete_obs_hab_from_list(self, obs_hab_list_to_delete: List[int]):
        if len(obs_hab_list_to_delete) > 0:
            delete_request: str
            if len(obs_hab_list_to_delete) == 1:
                delete_request = "DELETE FROM obs_habitats WHERE obs_hab_id = " + str(obs_hab_list_to_delete[0])
            else:
                delete_request = "DELETE FROM obs_habitats WHERE obs_hab_id in " + str(tuple(obs_hab_list_to_delete))
            self.cursor.execute(delete_request)
            self.connection.commit()

    def clean_obs_syntaxon_orphans(self):
        obs_hab_request = "SELECT obs_habitats.obs_hab_id FROM obs_habitats"
        obs_hab_list = [obs[0] for obs in self.cursor.execute(obs_hab_request).fetchall()]
        veg_request = "SELECT obs_habitats.obs_hab_id FROM obs_habitats LEFT JOIN releves on releves.id = " \
                      "obs_habitats.obs_hab_rel_id WHERE releves.id is NOT NULL"
        veg_obs_hab_list = [obs[0] for obs in self.cursor.execute(veg_request).fetchall()]
        phyto_orphans = "SELECT obs_habitats.obs_hab_id FROM obs_habitats LEFT JOIN releves on releves.id = " \
                        "obs_habitats.obs_hab_rel_assoc_id WHERE releves.id is NOT NULL"
        phyto_obs_hab_list = [obs[0] for obs in self.cursor.execute(phyto_orphans).fetchall()]

        orphans: List[int] = []
        for obs in obs_hab_list:
            if obs not in veg_obs_hab_list and obs not in phyto_obs_hab_list:
                orphans.append(obs)
        self.delete_obs_hab_from_list(orphans)

    def clean_phantom_obs_hab(self):
        veg_survey_with_obs_hab_req = "SELECT id, rel_obs_syntaxon_id FROM releves " \
                                      "WHERE rel_type_rel_id = 2 AND rel_obs_syntaxon_id NOT NULL"
        veg_survey_with_obs_hab_list = self.cursor.execute(veg_survey_with_obs_hab_req).fetchall()
        obs_hab_to_delete = [survey[1] for survey in veg_survey_with_obs_hab_list]
        self.delete_obs_hab_from_list(obs_hab_to_delete)

        survey_update_req = "UPDATE releves SET rel_obs_syntaxon_id = NULL WHERE id = %s"
        for survey in veg_survey_with_obs_hab_list:
            self.cursor.execute(survey_update_req % survey[0])
        self.connection.commit()

    def clean_duplicated_obs_syntaxon(self):
        survey_with_duplicated_obs_syntaxon_request = "SELECT obs_hab_rel_assoc_id FROM obs_habitats " \
                                                      "WHERE obs_hab_code_typo = 0 AND obs_hab_rel_assoc_id NOT NULL " \
                                                      "GROUP BY obs_hab_rel_assoc_id, obs_hab_code_typo " \
                                                      "HAVING COUNT(*) > 1"
        get_obs_syntaxon_id_to_keep_request = "SELECT rel_obs_syntaxon_id FROM releves " \
                                              "WHERE id = %s AND rel_obs_syntaxon_id NOT NULL"
        duplicated_obs_hab_ids_request = "SELECT obs_hab_id FROM obs_habitats WHERE obs_hab_rel_assoc_id = %s " \
                                         "AND obs_hab_id != %s AND obs_hab_code_typo = 0"
        update_obs_hab_links_one = "UPDATE liens_obs_habitat SET id_obs_hab_1 = %s WHERE id_obs_hab_1 = %s"
        update_obs_hab_links_two = "UPDATE liens_obs_habitat SET id_obs_hab_2 = %s WHERE id_obs_hab_2 = %s"
        delete_request = "DELETE FROM obs_habitats WHERE obs_hab_id = %s"
        survey_with_duplicated_obs_syntaxon_list = [survey[0] for survey in self.cursor.execute(
            survey_with_duplicated_obs_syntaxon_request).fetchall()]
        for survey in survey_with_duplicated_obs_syntaxon_list:
            obs_syntaxon_id_to_keep = self.cursor.execute(get_obs_syntaxon_id_to_keep_request % survey).fetchone()
            if obs_syntaxon_id_to_keep:
                obs_hab_id_list_to_delete = [obs_hab[0] for obs_hab in self.cursor.execute(
                    duplicated_obs_hab_ids_request % (survey, obs_syntaxon_id_to_keep[0])).fetchall()]
                for obs_hab_id_to_delete in obs_hab_id_list_to_delete:
                    self.cursor.execute(update_obs_hab_links_one % (obs_syntaxon_id_to_keep[0], obs_hab_id_to_delete))
                    self.cursor.execute(update_obs_hab_links_two % (obs_syntaxon_id_to_keep[0], obs_hab_id_to_delete))
                    self.cursor.execute(delete_request % obs_hab_id_to_delete)
        self.connection.commit()

    def clean_orphan_links(self):
        get_links_request = "SELECT id_obs_hab_1, id_obs_hab_2 FROM liens_obs_habitat"
        get_obs_hab = "SELECT obs_hab_id FROM obs_habitats WHERE obs_hab_id = %s"
        delete_request = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_1 = %s OR id_obs_hab_2 = %s"
        for link in self.cursor.execute(get_links_request).fetchall():
            if not self.cursor.execute(get_obs_hab % link['id_obs_hab_1']).fetchone():
                self.cursor.execute(delete_request % (link['id_obs_hab_1'], link['id_obs_hab_1']))
        self.connection.commit()

    def correct_encoding(self):
        self.sqlite_connection(SQLITE_FILE)
        self.correct_obs_hab_labels()

    def correct_obs_hab_labels(self):
        obs_hab_request = "SELECT obs_hab_id, obs_hab_code_cite, obs_hab_code_typo, syntaxon_code_phyto_retenu " \
                          "FROM obs_habitats"
        obs_hab_list: List[Row] = [obs for obs in self.cursor.execute(obs_hab_request).fetchall()]
        habref_request = "SELECT cd_typo, lb_code, lb_hab_fr FROM habref WHERE cd_typo = %s AND lb_code = '%s'"
        ref_phyto_request = "SELECT code_phyto, nom_syntaxon_simple FROM catalogue_phyto WHERE code_phyto = '%s'"
        update_obs_hab_request = "UPDATE obs_habitats SET obs_hab_nom_cite = \"%s\" WHERE obs_hab_id = %s"
        update_syntaxon_request = "UPDATE obs_habitats SET syntaxon_nom_retenu = \"%s\" WHERE obs_hab_id = %s"
        for obs_hab in obs_hab_list:
            name_list = []
            if obs_hab['obs_hab_code_typo'] == 0 and obs_hab['obs_hab_code_cite']:
                name_list = [lb['nom_syntaxon_simple'] for lb in
                             self.cursor.execute(ref_phyto_request % obs_hab['obs_hab_code_cite']).fetchall()]
            elif obs_hab['obs_hab_code_cite']:
                name_list = [lb['lb_hab_fr'] for lb in
                             self.cursor.execute(habref_request %
                                                 (obs_hab['obs_hab_code_typo'], obs_hab['obs_hab_code_cite']))
                             .fetchall()]
            if len(name_list) > 0:
                self.cursor.execute(update_obs_hab_request % (name_list[0], obs_hab['obs_hab_id']))
            if obs_hab['obs_hab_code_typo'] == 0 and obs_hab['syntaxon_code_phyto_retenu']:
                syntaxon_list = [lb['nom_syntaxon_simple'] for lb in
                                 self.cursor.execute(
                                     ref_phyto_request % obs_hab['syntaxon_code_phyto_retenu']).fetchall()]
                if len(syntaxon_list) > 0:
                    self.cursor.execute(update_syntaxon_request % (syntaxon_list[0], obs_hab['obs_hab_id']))
        self.connection.commit()

    def sqlite_connection(self, database_file_name) -> Optional[Connection]:
        try:
            self.connection = sqlite3.connect(self.home_path / database_file_name)
            self.connection.row_factory = sqlite3.Row
            self.connection.enable_load_extension(True)
            self.connection.load_extension("mod_spatialite")
        except Error as e:
            message = 'Connection failed - ' + str(e)
            iface.messageBar().pushWarning('Database connection', message)
        finally:
            if self.connection:
                self.cursor = self.connection.cursor()
            return self.connection


class SurveyDataManager:
    def __init__(self, project: QgsProject, form: QgsAttributeForm, layer: QgsVectorLayer, feature: QgsFeature):
        self.project = project
        self.form: QgsAttributeForm = form
        self.layer: QgsVectorLayer = layer
        self.geometry_feature: QgsFeature = feature
        self.list_values_provider: ListValuesProvider = ListValuesProvider(self.project)
        self.sqlite_data_manager: DataManager = DataManager(self.project)
        self.table_widget_item_height = 30

    def habitat_auto_complete(self, select_habitat: QComboBox, catalogue_phyto: QgsVectorLayer, habref: QgsVectorLayer,
                              buttons: QButtonGroup, habitat_cite: QLineEdit, habitat_code_cite: QLineEdit,
                              syntaxon_valide: QLineEdit, syntaxon_code_valide: QLineEdit,
                              survey_feature: QgsFeature = None, obs_habitats: QgsVectorLayer = None,
                              table_syn_habitats: QTableWidget = None):
        code_typo: int = TableWidgetService.get_code_typo(buttons)
        ref_habitat_values: List
        nom_habitat_simple_index: int
        code_habitat_index: int
        code_habitat_retenu_index: int
        ref_phyto: bool

        text_scripted = select_habitat.lineEdit().text()
        if len(text_scripted) == 0:
            code_phyto = self.form.findChild(QLineEdit, "syntaxon_code_cite")
            self.related_habitat_auto_complete(select_habitat, code_phyto, buttons, survey_feature, obs_habitats,
                                               table_syn_habitats)
            select_habitat.showPopup()
        else:
            code_habitat_index, code_habitat_retenu_index, nom_habitat_simple_index, ref_habitat_values, ref_phyto = \
                self.prepare_habitat_list(catalogue_phyto, habref, code_typo)
            select_habitat.lineEdit().returnPressed.disconnect()
            self.generate_habitat_auto_complete(select_habitat, ref_habitat_values, nom_habitat_simple_index,
                                                code_habitat_index, code_habitat_retenu_index, ref_phyto, code_typo,
                                                habitat_cite, habitat_code_cite, syntaxon_valide, syntaxon_code_valide,
                                                survey_feature, obs_habitats, table_syn_habitats)
            select_habitat.lineEdit().returnPressed.connect(
                lambda: self.habitat_auto_complete(select_habitat, catalogue_phyto, habref, buttons, habitat_cite,
                                                   habitat_code_cite, syntaxon_valide, syntaxon_code_valide,
                                                   survey_feature,
                                                   obs_habitats, table_syn_habitats))

    def related_habitat_auto_complete(self, select_habitat: QComboBox, code_phyto: QLineEdit,
                                      typo_buttons: QButtonGroup,
                                      survey_feature: QgsFeature, obs_habitats: QgsVectorLayer,
                                      table_syn_habitats: QTableWidget):
        cd_typo: int = TableWidgetService.get_code_typo(typo_buttons)
        related_habitats_list = self.sqlite_data_manager.get_related_habitats_list_from_code_phyto(code_phyto.text(),
                                                                                                   cd_typo)
        if len(related_habitats_list) > 0:
            # TODO : fonction commune avec populate_select_habitat (harmoniser la fonction pour n'utiliser que des entités de type sqlite)
            select_habitat.clear()
            try:
                select_habitat.view().pressed.disconnect()
            except TypeError:
                pass
            item_model = QStandardItemModel()
            for habitat in related_habitats_list:
                item = QStandardItem()
                item.setSizeHint(QSize(18, 18))
                item.setText(str(habitat['lb_code']) + ' | ' + habitat['lb_hab_fr'])
                item.setData(habitat['lb_code'])
                item.setFont(QFont('Tahoma', 10, QFont.Normal))
                item_model.appendRow(item)
            select_habitat.setModel(item_model)
            select_habitat.showPopup()
            select_habitat.view().pressed.connect(
                lambda line: self.add_typo_habitats(line, select_habitat, survey_feature, related_habitats_list,
                                                    obs_habitats, table_syn_habitats, cd_typo, 'sqlite'))
        else:
            select_habitat.clear()

    def syntaxon_auto_complete(self, select_habitat: QComboBox, catalogue_phyto: QgsVectorLayer,
                               syntaxon_cite: QLineEdit, syntaxon_code_cite: QLineEdit, syntaxon_valide: QLineEdit,
                               syntaxon_code_valide: QLineEdit):
        ref_habitat_values = [c.attributes() for c in catalogue_phyto.getFeatures()]
        syntaxon_field_names = [f.name() for f in catalogue_phyto.fields()]
        nom_habitat_simple_index = syntaxon_field_names.index('nom_syntaxon_simple')
        code_habitat_index = syntaxon_field_names.index('code_phyto')
        code_habitat_retenu_index = syntaxon_field_names.index('code_phyto_syntaxon_retenu')

        select_habitat.lineEdit().returnPressed.disconnect()
        self.generate_habitat_auto_complete(select_habitat, ref_habitat_values, nom_habitat_simple_index,
                                            code_habitat_index, code_habitat_retenu_index, True, 0, syntaxon_cite,
                                            syntaxon_code_cite, syntaxon_valide, syntaxon_code_valide)
        select_habitat.lineEdit().returnPressed.connect(
            lambda: self.syntaxon_auto_complete(select_habitat, catalogue_phyto, syntaxon_cite, syntaxon_code_cite,
                                                syntaxon_valide, syntaxon_code_valide))

    @staticmethod
    def prepare_habitat_list(catalogue_phyto, habref, code_typo):
        if code_typo == 0:
            ref_habitat_values = [value.attributes() for value in catalogue_phyto.getFeatures()]
            ref_habitat_field_names = [f.name() for f in catalogue_phyto.fields()]
            nom_habitat_simple_index = ref_habitat_field_names.index('nom_syntaxon_simple')
            code_habitat_index = ref_habitat_field_names.index('code_phyto')
            code_habitat_retenu_index = ref_habitat_field_names.index('code_phyto_syntaxon_retenu')
            ref_phyto = True
        else:
            ref_habitat_values = [value.attributes() for value in habref.getFeatures() if value['cd_typo'] == code_typo]
            ref_habitat_field_names = [f.name() for f in habref.fields()]
            nom_habitat_simple_index = ref_habitat_field_names.index('lb_hab_fr')
            code_habitat_index = ref_habitat_field_names.index('lb_code')
            code_habitat_retenu_index = 0
            ref_phyto = False
        return code_habitat_index, code_habitat_retenu_index, nom_habitat_simple_index, ref_habitat_values, ref_phyto

    def generate_habitat_auto_complete(self, select_habitat, ref_habitat_values, nom_habitat_simple_index,
                                       code_habitat_index, code_habitat_retenu_index, ref_phyto, code_typo,
                                       habitat_cite, habitat_code_cite, syntaxon_valide, syntaxon_code_valide,
                                       survey_feature: QgsFeature = None, obs_habitats: QgsVectorLayer = None,
                                       table_syn_habitats: QTableWidget = None):
        text_scripted = select_habitat.lineEdit().text()
        select_habitat.hidePopup()
        text_chunks = text_scripted.split(' ')
        found_habitats = self.filter_habitat_list(code_typo, code_habitat_index, nom_habitat_simple_index,
                                                  ref_habitat_values, text_chunks)

        if found_habitats and len(found_habitats) > 0:
            self.populate_select_habitat(code_habitat_index, code_habitat_retenu_index, found_habitats,
                                         nom_habitat_simple_index, ref_phyto, select_habitat)
            select_object_name = select_habitat.objectName()
            if select_object_name == 'select_habitat' or select_object_name == 'syntaxon_recherche':
                select_habitat.view().pressed.connect(
                    lambda line: self.add_validated_habitat(line, select_habitat, found_habitats, habitat_cite,
                                                            habitat_code_cite, syntaxon_valide, syntaxon_code_valide,
                                                            ref_phyto))
            elif select_object_name == 'habitat_recherche':
                select_habitat.view().pressed.connect(
                    lambda line: self.add_typo_habitats(line, select_habitat, survey_feature, found_habitats,
                                                        obs_habitats, table_syn_habitats, code_typo, 'feature'))
        # if len(text_scripted) == 0:
        #     select_habitat.clear()

    @staticmethod
    def filter_habitat_list(code_typo, code_habitat_index, nom_habitat_simple_index, ref_habitat_values, text_chunks):
        found_habitats = None
        if code_typo == 0:
            if len(text_chunks) == 1 and len(text_chunks[0]) > 2:
                found_habitats = [habitat for habitat in ref_habitat_values if
                                  text_chunks[0].lower() == habitat[nom_habitat_simple_index][
                                                            :len(text_chunks[0])].lower()]
            elif len(text_chunks) > 1 and len(text_chunks[0]) > 2:
                found_habitats = [habitat for habitat in ref_habitat_values if
                                  text_chunks[0].lower() == habitat[nom_habitat_simple_index][
                                                            :len(text_chunks[0])].lower() and
                                  text_chunks[1].lower() in habitat[nom_habitat_simple_index][
                                                            len(text_chunks[0]):].lower()]
        else:
            if len(text_chunks) > 0 and len(text_chunks[0]) > 0:
                found_habitats = [habitat for habitat in ref_habitat_values if
                                  text_chunks[0].lower() == habitat[code_habitat_index][
                                                            :len(text_chunks[0])].lower()]
        return found_habitats

    @staticmethod
    def populate_select_habitat(code_habitat_index, code_habitat_retenu_index, found_habitats,
                                nom_habitat_simple_index, ref_phyto, select_habitat):
        select_habitat.clear()
        try:
            select_habitat.view().pressed.disconnect()
        except TypeError:
            pass
        item_model = QStandardItemModel()
        for habitat in found_habitats:
            item = QStandardItem()
            item.setSizeHint(QSize(18, 18))
            item.setText(str(habitat[code_habitat_index]) + ' | ' + habitat[nom_habitat_simple_index])
            item.setData(habitat[code_habitat_index])
            if ref_phyto and habitat[code_habitat_index] == habitat[code_habitat_retenu_index]:
                item.setFont(QFont('Tahoma', 10, QFont.Bold))
            else:
                item.setFont(QFont('Tahoma', 10, QFont.Normal))
            item_model.appendRow(item)
        select_habitat.setModel(item_model)
        select_habitat.showPopup()

    @staticmethod
    def delete_syntaxon(syntaxon_cite: QLineEdit, syntaxon_code_cite: QLineEdit, syntaxon_valide: QLineEdit,
                        syntaxon_code_valide: QLineEdit):
        syntaxon_cite.clear()
        syntaxon_code_cite.clear()
        syntaxon_valide.clear()
        syntaxon_code_valide.clear()

    @staticmethod
    def add_validated_habitat(line, select_habitat: QComboBox, habitats: List[QgsFeature], habitat_cite: QLineEdit,
                              habitat_code_cite: QLineEdit, syntaxon_valide: QLineEdit,
                              syntaxon_code_valide: QLineEdit, ref_phyto: bool):
        select_habitat.view().pressed.disconnect()
        habitat = habitats[line.row()]
        if ref_phyto:
            habitat_cite.setText(str(habitat[4] if habitat[4] else ''))
            habitat_code_cite.setText(str(habitat[2] if habitat[2] else ''))
            syntaxon_valide.setText(str(habitat[7] if habitat[7] else ''))
            syntaxon_code_valide.setText(str(habitat[6] if habitat[6] else ''))
        else:
            habitat_cite.setText(str(habitat[4] if habitat[4] else ''))
            habitat_code_cite.setText(str(habitat[3] if habitat[3] else ''))
            syntaxon_valide.setText('')
            syntaxon_code_valide.setText('')
        select_habitat.hidePopup()
        select_habitat.clear()

    def add_habitat(self, survey_feature: QgsFeature, survey_layer: QgsVectorLayer, obs_habitats: QgsVectorLayer,
                    select_habitat: QComboBox, obs_hab_nom_cite: QLineEdit, obs_hab_code_cite: QLineEdit,
                    table_habitats: QTableWidget, obs_hab_assoc_list: QListWidget, add_widget: QLabel,
                    save_widget: QLabel, typo_buttons: QButtonGroup, add_related_habitats_widget: QLabel = None,
                    related_habitats_list_widget: QListWidget = None):
        # Nécessaire afin de récupérer rel_type_rel_id
        survey_feature = WidgetService.refresh_survey_feature(survey_feature, survey_layer)
        self.save_and_add_habitat(add_widget, None, obs_habitats, save_widget, select_habitat, survey_feature,
                                  obs_hab_code_cite, obs_hab_nom_cite, table_habitats, obs_hab_assoc_list, typo_buttons,
                                  None, add_related_habitats_widget, related_habitats_list_widget)

    def save_habitat(self, survey_feature: QgsFeature, survey_layer: QgsVectorLayer, obs_habitats: QgsVectorLayer,
                     select_habitat: QComboBox,
                     obs_hab_nom_cite: QLineEdit, obs_hab_code_cite: QLineEdit, table_habitats: QTableWidget,
                     obs_hab_assoc_list: QListWidget, add_widget: QLabel, save_widget: QLabel,
                     add_survey_widgets: List[QLabel], typo_buttons: QButtonGroup,
                     add_related_habitats_widget: QLabel = None, related_habitats_list_widget: QListWidget = None):
        # Nécessaire afin de récupérer rel_type_rel_id pour la suppression du taxon dominant
        survey_feature = WidgetService.refresh_survey_feature(survey_feature, survey_layer)
        code_phyto = obs_hab_code_cite.text() if obs_hab_code_cite else None
        if code_phyto:
            row = [i for i in range(table_habitats.rowCount())
                   if table_habitats.item(i, 1).text() == obs_hab_nom_cite.text()
                   and table_habitats.item(i, 0).text() == code_phyto][0]
            obs_habitat = [obs_habitat for obs_habitat in obs_habitats.getFeatures()
                           if obs_habitat['obs_hab_rel_id'] == survey_feature.id()
                           and obs_habitat['obs_hab_nom_cite'] == obs_hab_nom_cite.text()
                           and obs_habitat['obs_hab_code_cite'] == code_phyto][0]
        else:
            row = [i for i in range(table_habitats.rowCount())
                   if table_habitats.item(i, 1).text() == obs_hab_nom_cite.text()][0]
            obs_habitat = [obs_habitat for obs_habitat in obs_habitats.getFeatures()
                           if obs_habitat['obs_hab_rel_id'] == survey_feature.id()
                           and obs_habitat['obs_hab_nom_cite'] == obs_hab_nom_cite.text()][0]

        success = self.save_and_add_habitat(add_widget, obs_habitat, obs_habitats, save_widget, select_habitat,
                                            survey_feature, obs_hab_code_cite, obs_hab_nom_cite, table_habitats,
                                            obs_hab_assoc_list, typo_buttons, add_survey_widgets,
                                            add_related_habitats_widget, related_habitats_list_widget)
        if success:
            table_habitats.removeRow(row)

        if related_habitats_list_widget.count() > 0:
            related_obs_hab_list = [related_habitats_list_widget.item(i) for i in
                                    range(related_habitats_list_widget.count())
                                    if related_habitats_list_widget.item(i).checkState() == Qt.Checked]
            for obs_hab_to_add in related_obs_hab_list:
                self.add_related_habitat(obs_hab_to_add, obs_habitat, obs_habitats, survey_feature, obs_hab_assoc_list,
                                         typo_buttons)

    def save_and_add_habitat(self, add_widget, obs_habitat, obs_habitats, save_widget, select_habitat, survey_feature,
                             obs_hab_code_cite, obs_hab_nom_cite, table_habitats, obs_hab_assoc_list, typo_buttons,
                             add_survey_widgets=None, add_related_habitats_widget=None,
                             related_habitats_list_widget=None) -> bool:
        if not self.check_if_code_habitat_exists(obs_habitat, obs_habitats, survey_feature, obs_hab_nom_cite.text(),
                                                 obs_hab_code_cite.text()):
            select_habitat.hidePopup()
            select_habitat.clear()
            return False

        if obs_hab_nom_cite.text():
            type_forme_values = self.list_values_provider.get_values_from_list_name('TYPE_FORME_VALUES')
            rec_values = self.list_values_provider.get_values_from_list_name('RECOUVREMENT_VALUES')
            typicite_values = self.list_values_provider.get_values_from_list_name('TYPICITE_VALUES')
            obs_habitat = self.save_habitat_observation(survey_feature, obs_habitats, 'obs_hab_rel_id', obs_habitat)
            self.insert_habitat_in_table_widget(obs_habitat, rec_values, table_habitats, type_forme_values,
                                                typicite_values)
            self.refresh_habitat_sub_form(add_survey_widgets, add_widget, save_widget, select_habitat, survey_feature,
                                          obs_habitats, obs_hab_assoc_list, typo_buttons, add_related_habitats_widget,
                                          related_habitats_list_widget)
            return True
        else:
            iface.messageBar().pushWarning('Ajout de syntaxon', 'Vous ne pouvez pas ajouter une observation sans '
                                                                'syntaxon cite.')
            return False

    def add_related_habitat(self, related_habitat_item: QListWidgetItem, obs_habitat: QgsFeature,
                            obs_habitats: QgsVectorLayer,
                            survey_feature: QgsFeature, obs_hab_assoc_list: QListWidget, typo_buttons: QButtonGroup):
        cd_hab_to_add = related_habitat_item.data(Qt.UserRole)
        obs_hab_to_add = self.sqlite_data_manager.get_habitat_from_cd_hab(cd_hab_to_add)
        if self.check_if_code_habitat_exists(None, obs_habitats, survey_feature, obs_hab_to_add['lb_hab_fr'],
                                             obs_hab_to_add['lb_code']):
            new_obs_habitat = self.save_related_habitat_observation(obs_hab_to_add, survey_feature, obs_habitats)
            # Recharger new_obs_habitat pour obtenir son id
            new_obs_habitat = [obs_habitat for obs_habitat in obs_habitats.getFeatures()
                               if obs_habitat['obs_hab_rel_id'] == survey_feature.id()
                               and obs_habitat['obs_hab_nom_cite'] == new_obs_habitat['obs_hab_nom_cite']
                               and obs_habitat['obs_hab_code_cite'] == new_obs_habitat['obs_hab_code_cite']][0]
            obs_hab_id = obs_habitat['obs_hab_id']
            obs_hab_id_to_link = new_obs_habitat['obs_hab_id']
            sqlite_data_manager = DataManager()
            sqlite_data_manager.insert_links_between_obs_hab(obs_hab_id, obs_hab_id_to_link)
            TableWidgetService.populate_obs_assoc_consult_list(survey_feature.id(), obs_habitats, obs_hab_assoc_list,
                                                               typo_buttons)

    def save_habitat_observation(self, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer,
                                 linked_field_name: str = 'obs_hab_rel_id',
                                 obs_habitat: QgsFeature = None) -> QgsFeature:
        if not obs_habitats.isEditable():
            obs_habitats.startEditing()
        if not obs_habitat:
            obs_habitat = QgsFeature()
            obs_habitat.setFields(obs_habitats.fields())
            obs_habitat[linked_field_name] = survey_feature['id']
            obs_habitat['obs_hab_code_typo'] = 0
            obs_hab_uuid = str(uuid.uuid4())
            obs_habitat['obs_hab_uuid'] = obs_hab_uuid
            obs_habitats.addFeature(obs_habitat)
            obs_habitats.updateFeature(obs_habitat)
            obs_habitats.commitChanges()
            obs_habitat = obs_habitats.getFeature(self.sqlite_data_manager.get_obs_habitat_id_from_uuid(obs_hab_uuid))
            obs_habitats.startEditing()
        obs_hab_uuid = obs_habitat['obs_hab_uuid']
        if obs_hab_uuid is None or obs_hab_uuid == "" or obs_hab_uuid == NULL:
            obs_habitat['obs_hab_uuid'] = str(uuid.uuid4())
            obs_habitats.updateFeature(obs_habitat)
        self.save_layer_values(obs_habitat, obs_habitats, survey_feature['rel_type_rel_id'])
        obs_habitats.commitChanges()

        if obs_habitat.id() > 0:
            if survey_feature['rel_type_rel_id'] == 1 and self.form.findChild(QComboBox,
                                                                              "syntaxon_comm_non_sat_id").currentIndex() == 0 and \
                    survey_feature['rel_obs_syntaxon_id'] is not None:
                self.sqlite_data_manager.remove_dominant_taxon_from_obs_hab_id(survey_feature['rel_obs_syntaxon_id'])
            if survey_feature['rel_type_rel_id'] == 2 and self.form.findChild(QComboBox,
                                                                              "obs_hab_comm_non_sat_obs").currentIndex() == 0:
                self.sqlite_data_manager.remove_dominant_taxon_from_obs_hab_id(obs_habitat.id())

        return obs_habitat

    def save_related_habitat_observation(self, obs_hab_to_add, survey_feature: QgsFeature,
                                         obs_habitats: QgsVectorLayer):
        if not obs_habitats.isEditable():
            obs_habitats.startEditing()
        obs_habitat = QgsFeature()
        obs_habitat.setFields(obs_habitats.fields())
        obs_habitat['obs_hab_rel_id'] = survey_feature['id']
        obs_habitat['obs_hab_code_typo'] = obs_hab_to_add['cd_typo']
        obs_habitat['obs_hab_code_cite'] = obs_hab_to_add['lb_code']
        obs_habitat['obs_hab_nom_cite'] = obs_hab_to_add['lb_hab_fr']
        obs_hab_uuid = str(uuid.uuid4())
        obs_habitat['obs_hab_uuid'] = obs_hab_uuid
        obs_habitats.addFeature(obs_habitat)
        obs_habitats.updateFeature(obs_habitat)
        obs_habitats.commitChanges()
        obs_habitat = obs_habitats.getFeature(self.sqlite_data_manager.get_obs_habitat_id_from_uuid(obs_hab_uuid))
        obs_habitats.startEditing()
        obs_hab_uuid = obs_habitat['obs_hab_uuid']
        if obs_hab_uuid is None or obs_hab_uuid == "" or obs_hab_uuid == NULL:
            obs_habitat['obs_hab_uuid'] = str(uuid.uuid4())
            obs_habitats.updateFeature(obs_habitat)
        fields_to_exclude = ['obs_hab_code_typo', 'obs_hab_code_cite', 'obs_hab_nom_cite', 'syntaxon_nom_retenu',
                             'syntaxon_code_phyto_retenu']
        self.save_layer_values(obs_habitat, obs_habitats, survey_feature['rel_type_rel_id'], fields_to_exclude)
        obs_habitats.commitChanges()
        return obs_habitat

    def refresh_habitat_sub_form(self, add_survey_widgets, add_widget, save_widget, select_habitat, survey_feature,
                                 obs_habitats, obs_hab_assoc_list, typo_buttons: QButtonGroup,
                                 add_related_habitats_widget: QLabel = None,
                                 related_habitats_list_widget: QListWidget = None):
        select_habitat.clear()
        self.clean_habitat_sub_form()

        for button in typo_buttons.buttons():
            button.setEnabled(True)
        TableWidgetService.populate_obs_assoc_consult_list(survey_feature.id(), obs_habitats, obs_hab_assoc_list,
                                                           typo_buttons)

        select_habitat.setEnabled(True)
        add_widget.show()
        save_widget.hide()
        if add_related_habitats_widget:
            add_related_habitats_widget.hide()
            if related_habitats_list_widget is not None:
                related_habitats_list_widget.hide()
        if add_survey_widgets:
            for widget in add_survey_widgets:
                widget.hide()

    def clean_habitat_sub_form(self):
        form_inputs = DatabaseSpecificationService.get_inputs_specification('obs_habitats')
        for form_input in form_inputs:
            widget = WidgetService.find_widget(self.form, form_input['input_type'],
                                               form_input['input_name'].split(', ')[0])
            if widget and widget.objectName() != 'obs_hab_code_typo':
                TableWidgetService.clean_widget(widget)
                widget.setEnabled(True)

    @staticmethod
    def check_if_code_habitat_exists(obs_syntaxon, obs_habitats, survey_feature, obs_hab_nom_cite,
                                     obs_hab_code_cite, obs_hab_rel_field_name: str = 'obs_hab_rel_id') -> bool:
        observations_with_same_id = [observation for observation in obs_habitats.getFeatures()
                                     if observation[obs_hab_rel_field_name] == survey_feature.id()
                                     and observation['obs_hab_nom_cite'] == obs_hab_nom_cite
                                     and observation['obs_hab_code_cite'] == obs_hab_code_cite]
        if obs_syntaxon:
            if len(observations_with_same_id) > 0 \
                    and obs_syntaxon['obs_hab_code_cite'] != obs_hab_code_cite:
                iface.messageBar().pushWarning('Ajout d\'observation habitat',
                                               'Vous ne pouvez ajouter qu\'une observation par code habitat.')
                return False
        else:
            if len(observations_with_same_id) > 0:
                iface.messageBar().pushWarning('Ajout d\'observation habitat',
                                               'Une seule observation par code habitat est permise.')
                return False
        return True

    def get_habitats(self, feature_id: int, obs_habitats: QgsVectorLayer, table_habitats: QTableWidget,
                     obs_hab_assoc_list: QListWidget, buttons: QButtonGroup, obs_hab_code_typo: QLineEdit,
                     syntaxon_cplt: QFrame, syntaxon_data_add: QFrame, obs_hab_action_buttons: QFrame):
        code_typo: int = self.prepare_table_habitats(table_habitats, obs_hab_assoc_list, buttons, obs_hab_code_typo,
                                                     syntaxon_cplt, syntaxon_data_add, obs_hab_action_buttons)
        habitats = [observation for observation in obs_habitats.getFeatures()
                    if observation['obs_hab_rel_id'] == feature_id and observation['obs_hab_code_typo'] == code_typo]
        if len(habitats) > 0:
            type_forme_values = self.list_values_provider.get_values_from_list_name('TYPE_FORME_VALUES')
            rec_values = self.list_values_provider.get_values_from_list_name('RECOUVREMENT_VALUES')
            typicite_values = self.list_values_provider.get_values_from_list_name('TYPICITE_VALUES')
            for habitat in habitats:
                self.insert_habitat_in_table_widget(habitat, rec_values, table_habitats, type_forme_values,
                                                    typicite_values)
        TableWidgetService.populate_obs_assoc_consult_list(feature_id, obs_habitats, obs_hab_assoc_list, buttons)

    def insert_habitat_in_table_widget(self, habitat, recouvrement_values, table_habitats, type_forme_values,
                                       typicite_values):
        table_habitats.insertRow(table_habitats.rowCount())
        row = table_habitats.rowCount() - 1
        table_habitats.setItem(row, 0, QTableWidgetItem(
            habitat['obs_hab_code_cite'] if habitat['obs_hab_code_cite'] not in (0, None, NULL) else None))
        table_habitats.setItem(row, 1, QTableWidgetItem(habitat['obs_hab_nom_cite']))
        table_habitats.setItem(row, 2, QTableWidgetItem(
            str(habitat['obs_hab_rec_pct']) if habitat['obs_hab_rec_pct'] not in (0, None, NULL) else None))
        table_habitats.setItem(row, 3, QTableWidgetItem(
            self.list_values_provider.get_value_from_list(recouvrement_values, habitat['obs_hab_recouvrement'])
            if habitat['obs_hab_recouvrement'] not in (0, None, NULL) else None))
        table_habitats.setItem(row, 4, QTableWidgetItem(
            self.list_values_provider.get_value_from_list(type_forme_values, habitat['obs_hab_forme_id'])
            if habitat['obs_hab_forme_id'] not in (0, None, NULL) else None))
        table_habitats.setItem(row, 5, QTableWidgetItem(
            self.list_values_provider.get_value_from_list(typicite_values, habitat['syntaxon_typicite_struct_id'])
            if habitat['syntaxon_typicite_struct_id'] not in (0, None, NULL) else None))
        table_habitats.setItem(row, 6, QTableWidgetItem(
            self.list_values_provider.get_value_from_list(typicite_values, habitat['syntaxon_typicite_flor_id'])
            if habitat['syntaxon_typicite_flor_id'] not in (0, None, NULL) else None))
        table_habitats.setItem(row, 7, SurveyDataManager.get_cross_item())
        table_habitats.setRowHeight(row, self.table_widget_item_height)

    def prepare_table_habitats(self, table_habitats: QTableWidget, obs_hab_assoc_list: QListWidget,
                               buttons: QButtonGroup, obs_hab_code_typo: QLineEdit, syntaxon_cplt: QFrame,
                               syntaxon_data_add: QFrame, obs_hab_action_buttons: QFrame) -> int:
        code_typo: int = TableWidgetService.get_code_typo(buttons)
        header_labels: List = TableWidgetService.get_header_labels_from_typo(code_typo)
        obs_hab_code_typo.setText(str(code_typo))
        widgets_to_display_or_not = [syntaxon_cplt, syntaxon_data_add, obs_hab_action_buttons]

        self.clean_habitat_sub_form()
        if code_typo == 0:
            for widget in widgets_to_display_or_not:
                widget.show()
        else:
            for widget in widgets_to_display_or_not:
                widget.hide()

        obs_hab_assoc_list.clear()
        table_habitats.clear()
        for i in range(table_habitats.rowCount()):
            table_habitats.removeRow(0)
        WidgetService.build_table(table_habitats, header_labels)
        return code_typo

    @staticmethod
    def refresh_hab_subform_data_add_box(buttons: QButtonGroup, syntaxon_data_add: QFrame):
        code_typo: int = TableWidgetService.get_code_typo(buttons)
        if code_typo == 0:
            syntaxon_data_add.show()
        else:
            syntaxon_data_add.hide()

    @staticmethod
    def get_syntaxon(form: QgsAttributeForm, obs_syntaxon_id: int, obs_habitats: QgsVectorLayer):
        obs_syntaxon_list = [observation for observation in obs_habitats.getFeatures()
                             if observation.id() == obs_syntaxon_id]
        if len(obs_syntaxon_list) > 0:
            obs_syntaxon = obs_syntaxon_list[0]
            form_inputs = DatabaseSpecificationService.get_inputs_specification('obs_habitats')
            for form_input in form_inputs:
                for form_input_name in form_input['input_name'].split(', '):
                    widget = WidgetService.find_widget(form, form_input['input_type'], form_input_name.strip())
                    WidgetService.set_input_value(widget, obs_syntaxon[form_input['field_name']])

    def get_typo_habitats(self, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer,
                          table_syn_habitats: QTableWidget):
        obs_syntaxon_id = survey_feature['rel_obs_syntaxon_id']
        linked_obs_hab_list = [row[0] for row in self.sqlite_data_manager.get_linked_obs_hab(obs_syntaxon_id)]
        associated_obs_habitats = obs_habitats.getFeatures(linked_obs_hab_list)
        for observation in associated_obs_habitats:
            row = table_syn_habitats.rowCount()
            code_typo = observation['obs_hab_code_typo']
            typo_widget = QTableWidgetItem(TableWidgetService.get_typo_label_from_code(code_typo))
            typo_widget.setBackground(TableWidgetService.get_typo_color_from_code(code_typo))
            table_syn_habitats.insertRow(row)
            table_syn_habitats.setItem(row, 0, QTableWidgetItem(observation['obs_hab_code_cite']))
            table_syn_habitats.setItem(row, 1, QTableWidgetItem(observation['obs_hab_nom_cite']))
            table_syn_habitats.setItem(row, 2, QTableWidgetItem(str(code_typo)))
            table_syn_habitats.setItem(row, 3, typo_widget)
            table_syn_habitats.setItem(row, 4, SurveyDataManager.get_cross_item())
            table_syn_habitats.setRowHeight(row, self.table_widget_item_height)
        table_syn_habitats.sortItems(0, Qt.AscendingOrder)
        table_syn_habitats.sortItems(3, Qt.DescendingOrder)

    def add_typo_habitats(self, line, select_habitat: QComboBox, survey_feature: QgsFeature,
                          found_habitats: List, obs_habitats: QgsVectorLayer,
                          table_syn_habitats: QTableWidget, code_typo: int, type: str):
        habitat = found_habitats[line.row()]
        # TODO : harmoniser la fonction pour n'utiliser que des entités de type sqlite
        if type == 'feature':
            obs_hab_nom_cite = str(habitat[4] if habitat[4] else '')
            obs_hab_code_cite = str(habitat[3] if habitat[3] else '')
        if type == 'sqlite':
            obs_hab_nom_cite = str(habitat['lb_hab_fr'])
            obs_hab_code_cite = str(habitat['lb_code'])

        if not self.check_if_code_habitat_exists(None, obs_habitats, survey_feature, obs_hab_nom_cite,
                                                 obs_hab_code_cite, 'obs_hab_rel_assoc_id'):
            select_habitat.hidePopup()
            select_habitat.clear()
            return

        self.add_habitat_to_database(code_typo, obs_habitats, survey_feature, obs_hab_nom_cite, obs_hab_code_cite)
        self.add_habitat_to_table_widget(code_typo, table_syn_habitats, obs_hab_nom_cite, obs_hab_code_cite)

        obs_habitat = TableWidgetService.find_observation(obs_hab_code_cite, obs_hab_nom_cite, 'obs_hab_code_cite',
                                                          'obs_hab_nom_cite', 'obs_hab_rel_assoc_id', obs_habitats,
                                                          survey_feature)
        if obs_habitat:
            survey_id = survey_feature['id']
            obs_syntaxon_id: int
            if survey_id is None:
                survey_id = self.sqlite_data_manager.get_survey_id_from_geometry(survey_feature)
            obs_syntaxon_id = self.sqlite_data_manager.get_obs_syntaxon_id_from_survey_id(survey_id)
            # else:
            #     obs_syntaxon_id = int(survey_feature['rel_obs_syntaxon_id'])
            obs_habitat_id = int(obs_habitat['obs_hab_id'])
            self.sqlite_data_manager.insert_links_between_obs_hab(obs_syntaxon_id, obs_habitat_id)

        select_habitat.hidePopup()
        select_habitat.view().pressed.disconnect()
        select_habitat.clear()

    def add_habitat_to_database(self, code_typo: int, obs_habitats: QgsVectorLayer, survey_feature: QgsFeature,
                                obs_hab_nom_cite: str, obs_hab_code_cite: str):
        if not obs_habitats.isEditable():
            obs_habitats.startEditing()
        survey_id = survey_feature['id']
        obs_syntaxon_id: int
        if survey_id is None:
            survey_id = self.sqlite_data_manager.get_survey_id_from_geometry(survey_feature)
        obs_syntaxon_id = self.sqlite_data_manager.get_obs_syntaxon_id_from_survey_id(survey_id)
        # else:
        #     obs_syntaxon_id = survey_feature['rel_obs_syntaxon_id']
        obs_syntaxon = obs_habitats.getFeature(obs_syntaxon_id)
        obs_habitat = QgsFeature()
        obs_habitat.setFields(obs_habitats.fields())
        obs_habitat['obs_hab_rel_assoc_id'] = survey_id
        obs_habitat['obs_hab_nom_cite'] = obs_hab_nom_cite
        obs_habitat['obs_hab_code_cite'] = obs_hab_code_cite
        obs_habitat['obs_hab_code_typo'] = code_typo
        obs_habitat['obs_hab_uuid'] = str(uuid.uuid4())
        if obs_syntaxon is not None:
            obs_habitat['obs_hab_rel_id'] = obs_syntaxon['obs_hab_rel_id']
        obs_habitats.updateFeature(obs_habitat)
        obs_habitats.addFeature(obs_habitat)
        obs_habitats.commitChanges()

    def add_habitat_to_table_widget(self, code_typo: int, table_syn_habitats: QTableWidget, obs_hab_nom_cite: str,
                                    obs_hab_code_cite: str):
        row = table_syn_habitats.rowCount()
        typo_widget = QTableWidgetItem(TableWidgetService.get_typo_label_from_code(code_typo))
        typo_widget.setBackground(TableWidgetService.get_typo_color_from_code(code_typo))
        table_syn_habitats.insertRow(row)
        table_syn_habitats.setItem(row, 0, QTableWidgetItem(obs_hab_code_cite))
        table_syn_habitats.setItem(row, 1, QTableWidgetItem(obs_hab_nom_cite))
        table_syn_habitats.setItem(row, 2, QTableWidgetItem(str(code_typo)))
        table_syn_habitats.setItem(row, 3, typo_widget)
        table_syn_habitats.setItem(row, 4, SurveyDataManager.get_cross_item())
        table_syn_habitats.sortItems(0, Qt.AscendingOrder)
        table_syn_habitats.sortItems(3, Qt.DescendingOrder)
        table_syn_habitats.setRowHeight(row, self.table_widget_item_height)

    def taxon_auto_complete(self, select_taxon: QComboBox, taxref: QgsVectorLayer, taxon_cite: QLineEdit,
                            cd_nom_cite: QLineEdit, taxon_valide: QLineEdit, cd_nom_valide: QLineEdit):
        select_taxon.lineEdit().returnPressed.disconnect()
        found_taxons: Optional[List[QgsFeature]] = None
        text_scripted = select_taxon.lineEdit().text()
        select_taxon.hidePopup()
        taxref_field_names = [field.name() for field in taxref.fields()]
        text_chunks = text_scripted.split(' ')
        lb_nom_index = taxref_field_names.index('lb_nom')
        cd_nom_index = taxref_field_names.index('cd_nom')
        first_word = text_chunks[0]
        len_text_chunk_0 = len(first_word)

        if len_text_chunk_0 > 2:
            found_taxons = self.found_taxons_matching(first_word, len_text_chunk_0, text_chunks, lb_nom_index, taxref)
        if found_taxons and len(found_taxons) > 0:
            self.populate_taxon_list(cd_nom_index, found_taxons, lb_nom_index, select_taxon, taxref_field_names,
                                     taxon_cite, cd_nom_cite, taxon_valide, cd_nom_valide)
        if len(text_scripted) == 0:
            select_taxon.clear()

        select_taxon.lineEdit().returnPressed.connect(
            lambda: self.taxon_auto_complete(select_taxon, taxref, taxon_cite, cd_nom_cite, taxon_valide,
                                             cd_nom_valide))

    def found_taxons_matching(self, first_word: str, len_text_chunk_0: int, text_chunks: List[str], lb_nom_index: int,
                              taxref: QgsVectorLayer) -> Optional[List[QgsFeature]]:
        matching_taxon_ids = self.sqlite_data_manager.get_taxons_from_criteria(text_chunks)
        matching_taxons = []
        for taxon_id in matching_taxon_ids:
            taxon = taxref.getFeature(taxon_id[0])
            matching_taxons.append(taxon.attributes())
        return matching_taxons

    def populate_taxon_list(self, cd_nom_index, found_taxons, lb_nom_index, select_taxon, taxref_field_names,
                            taxon_cite, cd_nom_cite, taxon_valide, cd_nom_valide):
        select_taxon_view = select_taxon.view()
        try:
            select_taxon_view.pressed.disconnect()
        except TypeError:
            pass
        item_model = QStandardItemModel()
        for taxon in found_taxons:
            item = QStandardItem()
            item.setSizeHint(QSize(18, 18))
            item.setData(str(taxon[cd_nom_index]) + ' | ' + taxon[lb_nom_index], role=Qt.DisplayRole)
            item.setData(taxon[cd_nom_index])
            if taxon[cd_nom_index] == taxon[taxref_field_names.index('cd_ref')]:
                item.setFont(QFont('Tahoma', 10, QFont.Bold))
            else:
                item.setFont(QFont('Tahoma', 10, QFont.Normal))
            item_model.appendRow(item)
        select_taxon.clear()
        select_taxon.setModel(item_model)
        select_taxon.showPopup()
        select_taxon_view.pressed.connect(
            lambda line: self.add_validated_taxon(line, select_taxon, found_taxons, taxon_cite, cd_nom_cite,
                                                  taxon_valide, cd_nom_valide))

    @staticmethod
    def add_validated_taxon(line, select_taxon: QComboBox, taxons: List[QgsFeature], taxon_cite: QLineEdit,
                            cd_nom_cite: QLineEdit, taxon_valide: QLineEdit, cd_nom_valide: QLineEdit):
        select_taxon.view().pressed.disconnect()
        taxon = taxons[line.row()]
        taxon_cite.setText(str(taxon[2] if taxon[2] else ''))
        cd_nom_cite.setText(str(taxon[1] if taxon[1] else ''))
        taxon_valide.setText(str(taxon[4] if taxon[4] else ''))
        cd_nom_valide.setText(str(taxon[3] if taxon[3] else ''))
        select_taxon.hidePopup()
        select_taxon.clear()

    def add_taxon(self, survey_feature: QgsFeature, observations: QgsVectorLayer, select_taxon: QComboBox,
                  taxon_cite: QLineEdit, taxon_cd_nom_cite: QLineEdit, table_taxons: QTableWidget, add_widget: QLabel,
                  save_widget: QLabel):
        self.save_and_add_taxon(add_widget, None, observations, save_widget, select_taxon, survey_feature,
                                table_taxons, taxon_cd_nom_cite, taxon_cite)

    def save_taxon(self, survey_feature: QgsFeature, observations: QgsVectorLayer, select_taxon: QComboBox,
                   taxon_cite: QLineEdit, taxon_cd_nom_cite: QLineEdit, table_taxons: QTableWidget, add_widget: QLabel,
                   save_widget: QLabel):
        cd_nom = int(taxon_cd_nom_cite.text()) if taxon_cd_nom_cite and taxon_cd_nom_cite.text() != '' else None
        if cd_nom:
            row = [i for i in range(table_taxons.rowCount())
                   if table_taxons.item(i, 1).text() == taxon_cite.text()
                   and table_taxons.item(i, 0).text() == str(cd_nom)][0]
            observation = [observation for observation in observations.getFeatures()
                           if observation['obs_rel_id'] == survey_feature.id()
                           and observation['cd_nom'] == cd_nom
                           and observation['nom_cite'] == taxon_cite.text()][0]
        else:
            row = [i for i in range(table_taxons.rowCount())
                   if table_taxons.item(i, 1).text() == taxon_cite.text()][0]
            observation = [observation for observation in observations.getFeatures()
                           if observation['obs_rel_id'] == survey_feature.id()
                           and observation['nom_cite'] == taxon_cite.text()][0]

        success = self.save_and_add_taxon(add_widget, observation, observations, save_widget, select_taxon,
                                          survey_feature, table_taxons, taxon_cd_nom_cite, taxon_cite)

        if success:
            table_taxons.removeRow(row)

    def save_and_add_taxon(self, add_widget, observation, observations, save_widget, select_taxon, survey_feature,
                           table_taxons, taxon_cd_nom_cite, taxon_cite) -> bool:
        if not self.check_if_cd_nom_exists(observation, observations, survey_feature, taxon_cite, taxon_cd_nom_cite):
            select_taxon.hidePopup()
            select_taxon.clear()
            return False

        if taxon_cite.text():
            strate_values = self.list_values_provider.get_values_from_list_name('COEF_AD_VALUES')
            observation = self.save_taxon_observation(survey_feature, observations, observation)
            self.insert_taxon_in_table_widget(strate_values, table_taxons, observation)
            self.refresh_taxon_sub_form(add_widget, save_widget, select_taxon)
            return True
        else:
            iface.messageBar().pushWarning('Ajout de taxon', 'Vous ne pouvez pas ajouter une observation sans taxon '
                                                             'cite.')
            return False

    def save_taxon_observation(self, survey_feature: QgsFeature, observations: QgsVectorLayer,
                               observation: QgsFeature = None) -> QgsFeature:
        if not observations.isEditable():
            observations.startEditing()
        if not observation:
            observation = QgsFeature()
            observation.setFields(observations.fields())
            observation['obs_rel_id'] = survey_feature['id']
            observations.updateFeature(observation)
            observations.addFeature(observation)
        obs_uuid = observation['obs_uuid']
        if obs_uuid is None or obs_uuid == "" or obs_uuid == NULL:
            observation['obs_uuid'] = str(uuid.uuid4())
            observations.updateFeature(observation)
        self.save_layer_values(observation, observations)
        observations.commitChanges()
        return observation

    def refresh_taxon_sub_form(self, add_widget, save_widget, select_taxon):
        select_taxon.clear()
        form_inputs = DatabaseSpecificationService.get_inputs_specification('observations')
        for form_input in form_inputs:
            widget = WidgetService.find_widget(self.form, form_input['input_type'],
                                               form_input['input_name'].split(',')[0])
            if widget:
                TableWidgetService.clean_widget(widget)
                widget.setEnabled(True)

        select_taxon.setEnabled(True)
        add_widget.show()
        save_widget.hide()

    @staticmethod
    def check_if_cd_nom_exists(observation, observations, survey_feature, nom_cite, taxon_cd_nom_cite) -> bool:
        cd_nom = int(taxon_cd_nom_cite.text()) if taxon_cd_nom_cite and taxon_cd_nom_cite.text() != '' else None
        observations_with_same_id = [observation for observation in observations.getFeatures()
                                     if observation['obs_rel_id'] == survey_feature.id()
                                     and observation['nom_cite'] == nom_cite.text()
                                     and observation['cd_nom'] == cd_nom]
        if observation:
            if len(observations_with_same_id) > 0 and observation['cd_nom'] != cd_nom:
                iface.messageBar().pushWarning('Ajout de taxon',
                                               'Vous ne pouvez ajouter qu\'une observation par cd_nom.')
                return False
        else:
            if len(observations_with_same_id) > 0:
                iface.messageBar().pushWarning('Ajout de taxon',
                                               'Une seule observation par cd_nom est permise.')
                return False
        return True

    def get_taxons(self, feature_id, observations: QgsVectorLayer, table_taxons: QTableWidget):
        taxa = [observation for observation in observations.getFeatures()
                if observation['obs_rel_id'] == feature_id]
        if len(taxa) > 0:
            strate_values = self.list_values_provider.get_values_from_list_name('COEF_AD_VALUES')
            for taxon in taxa:
                self.insert_taxon_in_table_widget(strate_values, table_taxons, taxon)

    def insert_taxon_in_table_widget(self, strate_values, table_taxons, taxon):
        row = table_taxons.rowCount()
        table_taxons.insertRow(row)
        table_taxons.setItem(row, 0, QTableWidgetItem(
            str(taxon['cd_nom']) if taxon['cd_nom'] not in (None, NULL) else None))
        table_taxons.setItem(row, 1, QTableWidgetItem(
            str(taxon['nom_cite']) if taxon['nom_cite'] not in (None, NULL) else None))
        table_taxons.setItem(row, 2, QTableWidgetItem(
            str(self.list_values_provider.get_value_from_list(strate_values, taxon['strate_arbo']))
            if taxon['strate_arbo'] not in (0, None, NULL) else None))
        table_taxons.setItem(row, 3, QTableWidgetItem(
            str(self.list_values_provider.get_value_from_list(strate_values, taxon['strate_arbu']))
            if taxon['strate_arbu'] not in (0, None, NULL) else None))
        table_taxons.setItem(row, 4, QTableWidgetItem(
            str(self.list_values_provider.get_value_from_list(strate_values, taxon['strate_herb']))
            if taxon['strate_herb'] not in (0, None, NULL) else None))
        table_taxons.setItem(row, 5, QTableWidgetItem(
            str(self.list_values_provider.get_value_from_list(strate_values, taxon['strate_musc']))
            if taxon['strate_musc'] not in (0, None, NULL) else None))
        table_taxons.setItem(row, 6, SurveyDataManager.get_cross_item())
        table_taxons.setRowHeight(row, self.table_widget_item_height)

    @staticmethod
    def get_cross_item() -> QTableWidgetItem:
        cross_item = QTableWidgetItem("><")
        cross_item.setForeground(QBrush(QColor(240, 15, 15)))
        cross_item.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        font = QFont()
        font.setBold(True)
        cross_item.setFont(font)
        return cross_item

    @staticmethod
    def format_jdd_list(raw_jdd_list: List) -> List[dict]:
        target_jdd_list = [{'id': jdd['uuid'], 'value': jdd['libelle_court']} for jdd in raw_jdd_list]
        target_jdd_list[:0] = [{'id': NULL, 'value': NULL}]
        return target_jdd_list

    def get_jdd(self, survey: QgsFeature, active_jdd: QLineEdit):
        survey_uuid = survey['rel_jdd_uuid']
        if survey_uuid != NULL and survey_uuid is not None:
            active_jdd.setText(self.sqlite_data_manager.get_jdd_name_from_uuid(survey_uuid))

    def search_jdd(self, jdd_list: QComboBox):
        jdd_list.lineEdit().returnPressed.disconnect()
        text_scripted = jdd_list.lineEdit().text()
        text_chunks = text_scripted.split(' ')
        if text_scripted != "" and len(text_chunks) > 0:
            jdd_list.hidePopup()
            target_jdd_list = self.format_jdd_list(self.sqlite_data_manager.filter_jdd_list_by_name(text_chunks[0:2]))
            WidgetService.load_combo_box(jdd_list, target_jdd_list)
            jdd_list.lineEdit().clear()
            jdd_list.showPopup()
        else:
            WidgetService.load_combo_box(jdd_list, self.format_jdd_list(self.sqlite_data_manager.get_jdd_list()))

        jdd_list.lineEdit().returnPressed.connect(lambda: self.search_jdd(jdd_list))

    def add_jdd(self, survey: QgsFeature, survey_layer: QgsVectorLayer, jdd_list: QComboBox, active_jdd: QLineEdit):
        # Nécessaire sinon le updateFeature() supprime les valeurs de tous les autres champs
        survey = WidgetService.refresh_survey_feature(survey, survey_layer)
        jdd_uuid = jdd_list.itemData(jdd_list.currentIndex())
        if jdd_uuid != NULL and self.sqlite_data_manager.check_jdd_uuid_exists(jdd_uuid):
            if not survey_layer.isEditable():
                survey_layer.startEditing()
            survey.setAttribute('rel_jdd_uuid', jdd_uuid)
            survey_layer.updateFeature(survey)
            survey_layer.commitChanges()
            active_jdd.setText(self.sqlite_data_manager.get_jdd_name_from_uuid(jdd_uuid))
            jdd_list.setCurrentIndex(0)
        else:
            iface.messageBar().pushWarning('Ajout d\'un JDD', 'Le JDD sélectionné n\'existe pas.')

    @staticmethod
    def delete_jdd(survey: QgsFeature, survey_layer: QgsVectorLayer, jdd_list: QComboBox, active_jdd: QLineEdit):
        survey.setAttribute('rel_jdd_uuid', NULL)
        survey_layer.updateFeature(survey)
        survey_layer.commitChanges()
        survey_layer.startEditing()
        active_jdd.setText("")
        jdd_list.setCurrentIndex(0)

    def replicate_jdd(self, survey: QgsFeature):
        jdd_uuid = survey['rel_jdd_uuid']
        if jdd_uuid != NULL and self.sqlite_data_manager.check_jdd_uuid_exists(jdd_uuid):
            self.sqlite_data_manager.copy_jdd_uuid_to_all_surveys(jdd_uuid)
            iface.messageBar().pushInfo('Réplication d\'un JDD',
                                        'Tous les relevés ont été mis à jour avec le JDD en cours.')
        else:
            iface.messageBar().pushWarning('Réplication d\'un JDD', 'Le JDD à répliquer n\'existe pas.')

    def get_observers(self, survey_id: int, survey_observer_links: QgsVectorLayer, lst_obs: QComboBox,
                      obs_list: QListWidget):
        observers = self.sqlite_data_manager.get_observers()
        observer_list = [{'id': observer[0], 'value': observer[1] + ' ' + observer[2] + ' - ' + observer[3]}
                         for observer in observers]
        observer_list[:0] = [{'id': 0, 'value': NULL}]
        WidgetService.load_combo_box(lst_obs, observer_list)

        observer_ids = [link['obs_id'] for link in survey_observer_links.getFeatures() if link['rel_id'] == survey_id]
        if len(observer_ids) > 0:
            attached_observers = [{'id': observer['id'], 'value': observer['value']} for observer in observer_list
                                  if observer['id'] in observer_ids]
            if len(attached_observers) > 0:
                for observer in attached_observers:
                    obs_list.addItem(observer['value'])

    def add_observer(self, survey_id: int, survey_observer_links: QgsVectorLayer, lst_obs: QComboBox,
                     obs_list: QListWidget) -> bool:
        if not self.check_if_observer_id_exists(survey_observer_links, survey_id, lst_obs):
            lst_obs.setCurrentIndex(0)
            return False

        observer_id = lst_obs.itemData(lst_obs.currentIndex())
        if observer_id != 0:
            new_feature = QgsFeature(survey_observer_links.fields())
            new_feature['obs_id'] = observer_id
            new_feature['rel_id'] = survey_id
            if not survey_observer_links.isEditable():
                survey_observer_links.startEditing()
            survey_observer_links.addFeature(new_feature)
            survey_observer_links.commitChanges()
            obs_list.addItem(lst_obs.currentText())
            lst_obs.setCurrentIndex(0)
            return True
        return False

    @staticmethod
    def check_if_observer_id_exists(survey_observer_links, survey_id, lst_obs) -> bool:
        observer_id = int(lst_obs.currentData()) if lst_obs and lst_obs.currentIndex() != 0 else None
        links_with_same_id = [link for link in survey_observer_links.getFeatures()
                              if link['rel_id'] == survey_id and link['obs_id'] == observer_id]

        if len(links_with_same_id) > 0:
            iface.messageBar().pushWarning('Ajout d\'observateur',
                                           'Un observateur ne peut pas figurer plus d\'une fois.')
            return False
        return True

    @staticmethod
    def delete_observer(survey_id: int, survey_observer_links: QgsVectorLayer, obs_list: QListWidget,
                        observateurs: QgsVectorLayer):
        items = obs_list.selectedItems()
        if len(items) > 0:
            for item in items:
                observer_id = [observer['obs_id'] for observer in observateurs.getFeatures()
                               if observer['obs_email'] == item.text().split(' - ')[1]][0]
                survey_observer_link_list = [link for link in survey_observer_links.getFeatures()
                                             if link['obs_id'] == observer_id and link['rel_id'] == survey_id]
                if len(survey_observer_link_list) > 0:
                    if not survey_observer_links.isEditable():
                        survey_observer_links.startEditing()
                    survey_observer_links.deleteFeature(survey_observer_link_list[0].id())
                    survey_observer_links.commitChanges()
                obs_list.takeItem(obs_list.row(item))

    def get_factors(self, survey_id: int, survey_factor_links: QgsVectorLayer, table_factors: QTableWidget):
        link_ids = [result[0] for result in self.sqlite_data_manager.get_linked_factors_to_survey(survey_id)]
        existing_factors = [link for link in survey_factor_links.getFeatures(link_ids)]
        if len(existing_factors) > 0:
            for factor in existing_factors:
                row = table_factors.rowCount()
                table_factors.insertRow(row)
                factor_id = str(factor['facteur_id'])
                if factor_id not in (0, None, NULL):
                    table_factors.setItem(row, 0, QTableWidgetItem(factor_id))
                    table_factors.setItem(row, 1, QTableWidgetItem(
                        self.sqlite_data_manager.get_value_from_key_and_list_name(factor_id,
                                                                                  'FACTEUR_INFLUENCE_VALUES')))
                if factor['impact_id'] not in (None, NULL):
                    table_factors.setItem(row, 2, QTableWidgetItem(
                        self.sqlite_data_manager.get_value_from_key_and_list_name(str(factor['impact_id']),
                                                                                  'FACTEUR_IMPACT_VALUES')))
                if factor['etat_id'] not in (None, NULL):
                    table_factors.setItem(row, 3, QTableWidgetItem(
                        self.sqlite_data_manager.get_value_from_key_and_list_name(str(factor['etat_id']),
                                                                                  'FACTEUR_ETAT_VALUES')))
                table_factors.setItem(row, 4, SurveyDataManager.get_cross_item())

    def add_factor(self, survey_id: int, survey_factor_links: QgsVectorLayer, table_factors: QTableWidget,
                   factors_combo_box: QComboBox, impact_combo_box: QComboBox, state_combo_box: QComboBox) -> bool:
        if not self.check_if_item_id_exists(None, survey_factor_links, survey_id, factors_combo_box, 'facteur_id'):
            factors_combo_box.clear()
            return False

        if factors_combo_box.itemData(factors_combo_box.currentIndex()) != 0:
            row = table_factors.rowCount()
            table_factors.insertRow(row)
            table_factors.setItem(row, 0, QTableWidgetItem(str(factors_combo_box.currentData())))
            table_factors.setItem(row, 1, QTableWidgetItem(factors_combo_box.currentText()))
            table_factors.setItem(row, 2, QTableWidgetItem(impact_combo_box.currentText()))
            table_factors.setItem(row, 3, QTableWidgetItem(state_combo_box.currentText()))
            table_factors.setItem(row, 4, SurveyDataManager.get_cross_item())
            self.save_link_with_survey(survey_id, survey_factor_links)
            self.refresh_sub_form([factors_combo_box, impact_combo_box, state_combo_box])
            factors_combo_box.clear()
        else:
            iface.messageBar().pushWarning('Ajout de facteur', 'Vous ne pouvez pas ajouter une ligne sans facteur.')
            return False

    def search_factor(self, factors_combo_box: QComboBox):
        factors_combo_box.lineEdit().returnPressed.disconnect()
        text_scripted = factors_combo_box.lineEdit().text()
        text_chunks = text_scripted.split(' ')
        if text_scripted != "" and len(text_chunks) > 0:
            factors_combo_box.hidePopup()
            target_factor_list = self.list_values_provider.get_values_from_list_name_and_criteria(
                'FACTEUR_INFLUENCE_VALUES', text_chunks[0:2])
            WidgetService.load_combo_box(factors_combo_box, target_factor_list)
            factors_combo_box.lineEdit().clear()
            factors_combo_box.showPopup()
        else:
            WidgetService.load_combo_box(
                factors_combo_box, self.list_values_provider.get_values_from_list_name('FACTEUR_INFLUENCE_VALUES'))

        factors_combo_box.lineEdit().returnPressed.connect(lambda: self.search_factor(factors_combo_box))

    def get_strates(self, survey_id: int, survey_strate_links: QgsVectorLayer, table_strates: QTableWidget):
        link_ids = [result[0] for result in self.sqlite_data_manager.get_linked_strates_to_survey(survey_id)]
        existing_strates = [link for link in survey_strate_links.getFeatures(link_ids)]
        if len(existing_strates) > 0:
            for strate in existing_strates:
                row = table_strates.rowCount()
                table_strates.insertRow(row)
                strate_id = strate['strate_id']
                strate_name = self.sqlite_data_manager.get_strate_name(strate_id)
                if strate_id not in (0, None, NULL):
                    table_strates.setItem(row, 0, QTableWidgetItem(str(strate_id)))
                    table_strates.setItem(row, 1, QTableWidgetItem(strate_name))
                if strate['recouvrement'] not in (None, NULL):
                    table_strates.setItem(row, 2, QTableWidgetItem(str(strate['recouvrement'])))
                if strate['hauteur_min'] not in (None, NULL):
                    table_strates.setItem(row, 3, QTableWidgetItem(str(strate['hauteur_min'])))
                if strate['hauteur_modale'] not in (None, NULL):
                    table_strates.setItem(row, 4, QTableWidgetItem(str(strate['hauteur_modale'])))
                if strate['hauteur_max'] not in (None, NULL):
                    table_strates.setItem(row, 5, QTableWidgetItem(str(strate['hauteur_max'])))
                table_strates.setItem(row, 6, SurveyDataManager.get_cross_item())

    def add_strate(self, survey_id: int, survey_strate_links: QgsVectorLayer, table_strates: QTableWidget,
                   strates_combo_box: QComboBox, struct_rec_pct_strate: QLineEdit, struct_haut_mod_strate: QLineEdit,
                   struct_haut_min_strate: QLineEdit, struct_haut_max_strate: QLineEdit) -> bool:
        if not self.check_if_item_id_exists(None, survey_strate_links, survey_id, strates_combo_box, 'strate_id'):
            return False

        if strates_combo_box.itemData(strates_combo_box.currentIndex()) != 0:
            row = table_strates.rowCount()
            table_strates.insertRow(row)
            table_strates.setItem(row, 0, QTableWidgetItem(str(strates_combo_box.currentData())))
            table_strates.setItem(row, 1, QTableWidgetItem(strates_combo_box.currentText()))
            table_strates.setItem(row, 2, QTableWidgetItem(struct_rec_pct_strate.text()))
            table_strates.setItem(row, 3, QTableWidgetItem(struct_haut_min_strate.text()))
            table_strates.setItem(row, 4, QTableWidgetItem(struct_haut_mod_strate.text()))
            table_strates.setItem(row, 5, QTableWidgetItem(struct_haut_max_strate.text()))
            table_strates.setItem(row, 6, SurveyDataManager.get_cross_item())
            self.save_link_with_survey(survey_id, survey_strate_links)
            self.refresh_sub_form([strates_combo_box, struct_rec_pct_strate, struct_haut_mod_strate,
                                   struct_haut_min_strate, struct_haut_max_strate])
        else:
            iface.messageBar().pushWarning('Ajout de strate', 'Vous ne pouvez pas ajouter une ligne sans strate.')
            return False

    @staticmethod
    def remove_dominant_taxon(comm_non_sat_index: int, syntaxon_comm_non_sat_obs_widget: QComboBox,
                              taxon_dominant_code_widget: QLineEdit, taxon_dominant_nom_widget: QLineEdit,
                              obs_hab_taxon_dominant_code_widget: QLineEdit,
                              obs_hab_taxon_dominant_nom_widget: QLineEdit):
        if comm_non_sat_index == 0:
            syntaxon_comm_non_sat_obs_widget.setCurrentIndex(0)  # Both inputs are linked to the same db field
            taxon_dominant_code_widget.clear()
            obs_hab_taxon_dominant_code_widget.clear()
            taxon_dominant_nom_widget.clear()
            obs_hab_taxon_dominant_nom_widget.clear()

    def save_link_with_survey(self, survey_id: int, links_with_survey: QgsVectorLayer,
                              link_with_survey: QgsFeature = None):
        if not links_with_survey.isEditable():
            links_with_survey.startEditing()
        if not link_with_survey:
            link_with_survey = QgsFeature()
            link_with_survey.setFields(links_with_survey.fields())
            link_with_survey['rel_id'] = survey_id
            links_with_survey.updateFeature(link_with_survey)
            links_with_survey.addFeature(link_with_survey)
        self.save_layer_values(link_with_survey, links_with_survey)
        links_with_survey.commitChanges()

    @staticmethod
    def refresh_sub_form(widgets_to_clear: List[QWidget]):
        for widget in widgets_to_clear:
            TableWidgetService.clean_widget(widget)
            widget.setEnabled(True)

    @staticmethod
    def check_if_item_id_exists(link, links, survey_id, combo_box, field_name) -> bool:
        item_id = int(combo_box.currentData()) if combo_box and combo_box.currentIndex() != 0 else None
        links_with_same_id = [link for link in links.getFeatures()
                              if link['rel_id'] == survey_id and link[field_name] == item_id]
        if link:
            if len(links_with_same_id) > 0 and link[field_name] != item_id:
                iface.messageBar().pushWarning('Ajout d\'un item',
                                               'Vous ne pouvez ajouter qu\'une ligne par type.')
                return False
        else:
            if len(links_with_same_id) > 0:
                iface.messageBar().pushWarning('Ajout d\'un item',
                                               'Une seule ligne par type est permise.')
                return False
        return True

    def add_associated_survey(self, button: QLabel, releves: QgsVectorLayer, survey_feature: QgsFeature):
        target_layer: Optional[QgsVectorLayer] = None
        if button.objectName() == 'rel_create_new_polygon_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_surface'][0]
        elif button.objectName() == 'rel_create_new_ligne_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_ligne'][0]
        elif button.objectName() == 'rel_create_new_point_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_point'][0]

        if target_layer:
            if not target_layer.isEditable():
                target_layer.startEditing()
            iface.setActiveLayer(target_layer)
            target_layer.featureAdded.connect(
                lambda fid: SurveyDataManager.feature_added(fid, releves, survey_feature))
            iface.actionAddFeature().trigger()
            self.form.window().close()

    @staticmethod
    def feature_added(fid, releves: QgsVectorLayer, parent_feature: QgsFeature):
        layer = iface.mapCanvas().currentLayer()
        layer.featureAdded.disconnect()
        new_geometry_feature: QgsFeature = layer.getFeature(fid)
        uuid_field_name: str
        if layer.name() == 'station_surface':
            uuid_field_name = 'rel_polygon_uuid'
        elif layer.name() == 'station_ligne':
            uuid_field_name = 'rel_ligne_uuid'
        elif layer.name() == 'station_point':
            uuid_field_name = 'rel_point_uuid'
        else:
            return
        new_survey_feature: QgsFeature = [feature for feature in releves.getFeatures()
                                          if feature[uuid_field_name] == new_geometry_feature['uuid']][0]
        new_survey_feature['rel_parent_id'] = parent_feature.id()
        releves.updateFeature(new_survey_feature)
        releves.commitChanges()
        releves.startEditing()

    def add_survey_associated_to_syntaxon(self, button: QLabel, releves: QgsVectorLayer, obs_habitats: QgsVectorLayer,
                                          survey_feature: QgsFeature, syntaxon_nom_cite: QLineEdit,
                                          syntaxon_code_phyto_cite: QLineEdit):
        target_layer: Optional[QgsVectorLayer] = None
        if button.objectName() == 'rel_syntax_create_new_polygon_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_surface'][0]
        elif button.objectName() == 'rel_syntax_create_new_ligne_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_ligne'][0]
        elif button.objectName() == 'rel_syntax_create_new_point_rel':
            target_layer = [layer for layer in self.project.mapLayers(True).values()
                            if layer.name() == 'station_point'][0]

        if target_layer:
            if not target_layer.isEditable():
                target_layer.startEditing()
            iface.setActiveLayer(target_layer)
            parent_code_phyto = syntaxon_code_phyto_cite.text()
            parent_syntaxon = syntaxon_nom_cite.text()
            target_layer.featureAdded.connect(
                lambda fid: SurveyDataManager.feature_associated_to_syntaxon(fid, releves, obs_habitats,
                                                                             survey_feature.id(), parent_syntaxon,
                                                                             parent_code_phyto))
            iface.actionAddFeature().trigger()
            self.form.window().close()

    @staticmethod
    def feature_associated_to_syntaxon(fid, releves: QgsVectorLayer, obs_habitats: QgsVectorLayer,
                                       parent_id: int, parent_syntaxon: str, parent_code_phyto: str):
        layer = iface.mapCanvas().currentLayer()
        layer.featureAdded.disconnect()
        new_geometry_feature: QgsFeature = layer.getFeature(fid)
        uuid_field_name: str
        if layer.name() == 'station_surface':
            uuid_field_name = 'rel_polygon_uuid'
        elif layer.name() == 'station_ligne':
            uuid_field_name = 'rel_ligne_uuid'
        elif layer.name() == 'station_point':
            uuid_field_name = 'rel_point_uuid'
        else:
            return
        new_survey_feature: QgsFeature = [feature for feature in releves.getFeatures()
                                          if feature[uuid_field_name] == new_geometry_feature['uuid']][0]
        if parent_code_phyto == '':
            obs_syntaxon_list = [obs_syntaxon for obs_syntaxon in obs_habitats.getFeatures()
                                 if obs_syntaxon['obs_hab_rel_id'] == parent_id
                                 and obs_syntaxon['obs_hab_nom_cite'] == parent_syntaxon]
        else:
            obs_syntaxon_list = [obs_syntaxon for obs_syntaxon in obs_habitats.getFeatures()
                                 if obs_syntaxon['obs_hab_rel_id'] == parent_id
                                 and obs_syntaxon['obs_hab_code_cite'] == parent_code_phyto
                                 and obs_syntaxon['obs_hab_nom_cite'] == parent_syntaxon]
        if len(obs_syntaxon_list) > 0:
            if not obs_habitats.isEditable():
                obs_habitats.startEditing()
            if not releves.isEditable():
                releves.startEditing()
            obs_syntaxon = obs_syntaxon_list[0]
            obs_syntaxon['obs_hab_rel_assoc_id'] = new_survey_feature.id()
            new_survey_feature['rel_obs_syntaxon_id'] = obs_syntaxon.id()
            obs_habitats.updateFeature(obs_syntaxon)
            obs_habitats.commitChanges()
            releves.updateFeature(new_survey_feature)
            releves.commitChanges()

    def save_survey(self, survey_layer: QgsVectorLayer, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer):
        if not self.layer.isEditable():
            self.layer.startEditing()
        if not survey_layer.isEditable():
            survey_layer.startEditing()

        survey_feature = survey_layer.getFeature(self.sqlite_data_manager.get_survey_id_from_geometry(survey_feature))
        self.save_layer_values(survey_feature, survey_layer, survey_feature['rel_type_rel_id'])
        if survey_feature['rel_type_rel_id'] != 2:
            self.save_associated_obs_syntaxon(obs_habitats, survey_feature, survey_layer)

        survey_layer.commitChanges()
        obs_habitats.commitChanges()
        self.layer.commitChanges()
        iface.messageBar().pushInfo('Sauvegarde releve', 'Effectuee.')

    def save_associated_obs_syntaxon(self, obs_habitats, survey_feature, survey_layer):
        if self.sqlite_data_manager.obs_syntaxon_id_exists(survey_feature['id']):
            obs_syntaxon = [obs_syntaxon for obs_syntaxon in obs_habitats.getFeatures()
                            if obs_syntaxon['obs_hab_rel_assoc_id'] == survey_feature['id']]
            if len(obs_syntaxon) > 0:
                self.save_habitat_observation(survey_feature, obs_habitats, 'obs_hab_rel_assoc_id', obs_syntaxon[0])
            else:
                iface.messageBar().pushWarning('Sauvegarde syntaxon associé', 'Echec : base de donnée erronée, tentez '
                                                                              'un nettoyage et/ou contactez votre '
                                                                              'administrateur.')
        else:
            self.save_habitat_observation(survey_feature, obs_habitats, 'obs_hab_rel_assoc_id')
            obs_syntaxon = [obs_syntaxon for obs_syntaxon in obs_habitats.getFeatures()
                            if obs_syntaxon['obs_hab_rel_assoc_id'] == survey_feature['id']][0]
            survey_feature['rel_obs_syntaxon_id'] = obs_syntaxon['obs_hab_id']
            survey_layer.updateFeature(survey_feature)

    def activate_survey(self, frel: QFrame, fobs: QFrame, fgen: QFrame, save_rel: QLabel, activate: QLabel,
                        survey_layer: QgsVectorLayer, survey_feature: QgsFeature, obs_habitats: QgsVectorLayer,
                        obs_hab_surface: QLineEdit, obs_hab_surface_label: QLabel):
        if not self.layer.isEditable():
            self.layer.startEditing()
        if not survey_layer.isEditable():
            survey_layer.startEditing()

        rel_type_widget = WidgetService.find_widget(self.form, 'QComboBox', 'rel_type_rel_id')
        rel_protocol_widget = WidgetService.find_widget(self.form, 'QComboBox', 'rel_protocole_id')
        tab_widget = WidgetService.find_widget(self.form, 'QTabWidget', 'tabWidget_phyto')
        fobs_taxons = WidgetService.find_widget(self.form, 'QFrame', "frame_obs_taxons")
        fobs_habitats = WidgetService.find_widget(self.form, 'QFrame', "frame_obs_habitats")
        rel_type_data = rel_type_widget.currentData()
        rel_protocol_data = rel_protocol_widget.currentData()

        if rel_type_data != 0 and rel_protocol_data != 0:
            self.form.save()
            rel_type_widget.setCurrentIndex(rel_type_widget.findData(rel_type_data))
            rel_type_widget.setEnabled(False)
            rel_protocol_widget.setCurrentIndex(rel_protocol_widget.findData(rel_protocol_data))
            self.save_survey(survey_layer, survey_feature, obs_habitats)
            WidgetService.fobs_show(tab_widget, rel_type_widget, rel_protocol_widget, fobs_taxons, fobs_habitats,
                                    activate, obs_hab_surface, obs_hab_surface_label)
            fgen.hide()
            frel.show()
            fobs.hide()
            save_rel.show()
            activate.hide()

    def map_survey(self, survey_layer: QgsVectorLayer, survey_feature: QgsFeature):
        if not self.layer.isEditable():
            self.layer.startEditing()
        if not survey_layer.isEditable():
            survey_layer.startEditing()

        form_inputs = DatabaseSpecificationService.get_inputs_specification('releves')
        for form_input in form_inputs:
            for form_input_name in form_input['input_name'].split(', '):
                widget = WidgetService.find_widget(self.form, form_input['input_type'], form_input_name.strip())
                WidgetService.set_input_value(widget, survey_feature[form_input['field_name']])

    def save_layer_values(self, feature: QgsFeature, layer: QgsVectorLayer, type_rel_id: int = None,
                          fields_to_exclude: List[str] = None):
        if fields_to_exclude is None:
            fields_to_exclude = []
        form_inputs = DatabaseSpecificationService.get_inputs_specification(layer.name())
        form_inputs = [form_input for form_input in form_inputs if form_input['field_name'] not in fields_to_exclude]
        for form_input in form_inputs:
            form_input_list = form_input['input_name'].split(', ')
            for form_input_name in form_input_list:
                widget = WidgetService.find_widget(self.form, form_input['input_type'], form_input_name.strip())
                if widget:
                    value = WidgetService.get_input_value(widget)
                    if len(form_input_list) == 1 or (type_rel_id == 1 and not form_input_name.startswith('obs_hab')) or \
                            (type_rel_id == 2 and form_input_name.startswith('obs_hab')):
                        if form_input['field_type'] == 'REAL':
                            value = value.replace(',', '.')
                        feature.setAttribute(form_input['field_name'], value)
        layer.updateFeature(feature)


class ExtractService:
    def __init__(self, project: QgsProject = None):
        self.project: Optional[QgsProject] = project
        if self.project is None:
            self.project = QgsProject.instance()
        self.connection: Optional[Connection] = None
        self.cursor: Optional[Cursor] = None
        self.home_path: Path = Path(self.project.homePath())

    def get_all_geom_list(self) -> List[Row]:
        result: List[Row] = []
        query_surface = "SELECT uuid, AsWKT(CastAutomagic(geom)) AS geometry FROM station_surface"
        result.extend(self.connection.execute(query_surface).fetchall())
        query_line = "SELECT uuid, AsWKT(CastAutomagic(geom)) AS geometry FROM station_ligne"
        result.extend(self.connection.execute(query_line).fetchall())
        query_point = "SELECT uuid, AsWKT(CastAutomagic(geom)) AS geometry FROM station_point"
        result.extend(self.connection.execute(query_point).fetchall())
        return result

    def get_obs_habitat_list(self) -> List[Row]:
        query = "SELECT obs_hab_id AS id, obs_hab_uuid AS uuid, obs_hab_rel_id AS rel_id, obs_hab_rel_assoc_id AS rel_assoc_id, " \
                "obs_hab_nom_cite AS name, obs_hab_code_cite AS code, obs_hab_code_typo AS typo_code, " \
                "obs_hab_rec_pct AS rec FROM obs_habitats"
        self.check_obs_hab_uuid(self.connection.execute(query).fetchall())
        return self.connection.execute(query).fetchall()

    def check_obs_hab_uuid(self, obs_habitat_list: List[Row]):
        for obs_hab in obs_habitat_list:
            if obs_hab['uuid'] is None or obs_hab['uuid'] == '':
                self.cursor.execute('UPDATE obs_habitats SET obs_hab_uuid = ? WHERE obs_hab_id=?;',
                                    (str(uuid.uuid4()), str(obs_hab['id'])))
                self.connection.commit()

    def get_survey_list(self) -> List[Row]:
        query = "SELECT id, rel_uuid AS uuid, rel_type_rel_id AS type, rel_protocole_id AS protocol, rel_point_uuid AS point_uuid, " \
                "rel_ligne_uuid AS line_uuid, rel_polygon_uuid AS polygon_uuid, rel_jdd_uuid AS jdd FROM releves"
        return self.connection.execute(query).fetchall()

    def get_links_between_obs_habitat(self) -> List[Row]:
        query = "SELECT id_obs_hab_1, id_obs_hab_2 FROM liens_obs_habitat"
        return self.connection.execute(query).fetchall()

    def get_typo_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT obs_hab_code_typo AS typo FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return int(result['typo']) if result and 'typo' in result.keys() else None

    def get_observers(self, survey_id: int) -> List[Row]:
        query = "SELECT rel.id AS id, obs.obs_nom AS nom_observateur, obs.obs_prenom AS prenom_observateur " \
                "FROM releves AS rel " \
                "LEFT JOIN liens_releve_observateur AS rel_obs ON rel.id = rel_obs.rel_id " \
                "LEFT JOIN observateurs AS obs ON rel_obs.obs_id = obs.obs_id " \
                "WHERE rel.id = %s"
        return self.connection.execute(query % survey_id).fetchall()

    def get_pct_rec_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT obs_hab_rec_pct AS rec FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return int(result['rec']) if result and 'rec' in result.keys() and result['rec'] else None

    def get_coef_rec_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT obs_hab_recouvrement AS coef_rec_id, " \
                "(SELECT value FROM listes_valeurs WHERE list_name = 'RECOUVREMENT_VALUES' " \
                "AND obs_habitats.obs_hab_recouvrement = key) AS coef_rec " \
                "FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return result['coef_rec'] if result and 'coef_rec' in result.keys() else None

    def get_typ_flor_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT syntaxon_typicite_flor_id AS typ_flor_id, " \
                "(SELECT value FROM listes_valeurs WHERE list_name = 'TYPICITE_VALUES' " \
                "AND obs_habitats.syntaxon_typicite_flor_id = key) AS typ_flor " \
                "FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return result['typ_flor'] if result and 'typ_flor' in result.keys() else None

    def get_typ_struct_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT syntaxon_typicite_struct_id AS typ_struct_id, " \
                "(SELECT value FROM listes_valeurs WHERE list_name = 'TYPICITE_VALUES' " \
                "AND obs_habitats.syntaxon_typicite_struct_id = key) AS typ_struct " \
                "FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return result['typ_struct'] if result and 'typ_struct' in result.keys() else None

    def get_typ_physio_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT syntaxon_type_physio_id AS typ_physio_id, " \
                "(SELECT value FROM listes_valeurs WHERE list_name = 'TYPE_PHYSIO_VALUES' " \
                "AND obs_habitats.syntaxon_type_physio_id = key) AS typ_physio " \
                "FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return result['typ_physio'] if result and 'typ_physio' in result.keys() else None

    def calculate_observers(self, survey_id: int) -> str:
        observer_list: List[str] = []
        observers_as_dict: List[dict] = self.row_to_dict(self.get_observers(survey_id))
        for observer in observers_as_dict:
            observer_name = str(observer['nom_observateur']).capitalize() if observer['nom_observateur'] else ''
            observer_name += ' ' + str(observer['prenom_observateur']).title() if observer['prenom_observateur'] else ''
            observer_list.append(observer_name)
        return ' & '.join(observer_list)

    def get_code_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT obs_hab_code_cite AS code FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['code'])

    def format_obs_hab_code_list(self, obs_hab_id_list: List[int]) -> str:
        obs_hab_code_list: List[str] = [self.get_code_by_obs_hab_id(code) for code in obs_hab_id_list]
        # print(str(obs_hab_code_list).replace('[', '').replace(']', '').replace(', ', ' / ').replace('\'', ''))
        return str(obs_hab_code_list).replace('[', '').replace(']', '').replace(', ', ' / ').replace('\'', '')

    @staticmethod
    def row_to_dict(rows: List[Row]) -> List[dict]:
        dict_list: List[dict] = []
        for row in rows:
            dict_list.append({key: row[key] for key in row.keys()})
        return dict_list

    @staticmethod
    def decode(text) -> str:
        result: str = ''
        try:
            result = text.decode("utf-8")
        except UnicodeDecodeError:
            pass
        finally:
            return result

    @staticmethod
    def concat(str_list: List[str]) -> str:
        concat_string = ""
        for string in str_list:
            if string:
                concat_string += string
        return concat_string

    def send_message(self, title: str, filename: str):
        message = 'csv file exported to ' + str((self.home_path / filename).absolute())
        try:
            iface.messageBar().pushInfo(title, message)
        except:
            print(message)

    def sqlite_connection(self, database_file_name) -> Optional[Connection]:
        try:
            self.connection = sqlite3.connect(self.home_path / database_file_name)
            self.connection.row_factory = sqlite3.Row
            self.connection.enable_load_extension(True)
            self.connection.load_extension("mod_spatialite")
            self.connection.text_factory = lambda x: self.decode(x)
        except Error as e:
            message = 'Connection failed - ' + str(e)
            iface.messageBar().pushWarning('Database connection', message)
        finally:
            if self.connection:
                self.cursor = self.connection.cursor()
            return self.connection

    def build_target_dict(self) -> List[dict]:
        return NotImplementedError

    def export_csv(self, target_dict: List[dict]):
        return NotImplementedError

    def process(self, database_file_name: str):
        return NotImplementedError


class CartoExtractService(ExtractService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)
        self.filename = 'carto_export.csv'

    def get_name_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT obs_hab_nom_cite AS name FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['name'])

    @staticmethod
    def flatten_by_rel_assoc(field: str, flattened_habitat_list: List[dict], geom_list: List[dict],
                             obs_habitat: dict, survey_list: List[dict]):
        for survey in survey_list:
            if survey['id'] == obs_habitat[field]:
                for geom in geom_list:
                    if geom['uuid'] == survey['geom_uuid']:
                        flattened_habitat_list.append({'obs_id': obs_habitat['id'], 'obs_uuid': obs_habitat['uuid'],
                                                       'name': obs_habitat['name'], 'code': obs_habitat['code'],
                                                       'rec': obs_habitat['rec'], 'typo': obs_habitat['typo_code'],
                                                       'rel_id': survey['id'], 'rel_uuid': survey['uuid'],
                                                       'rel_type': survey['type'], 'geom_uuid': survey['geom_uuid'],
                                                       'geom': geom['geometry']})

    @staticmethod
    def flatten_obs_habitat(obs_habitat_list: List[dict], survey_list: List[dict], geom_list: List[dict]) -> List[dict]:
        flattened_habitat_list: List[dict] = []
        for obs_habitat in obs_habitat_list:
            if obs_habitat['rel_id'] and (obs_habitat['code'] or obs_habitat['name']):
                CartoExtractService.flatten_by_rel_assoc('rel_id', flattened_habitat_list, geom_list, obs_habitat,
                                                         survey_list)
            elif obs_habitat['rel_assoc_id'] and (obs_habitat['code'] or obs_habitat['name']):
                CartoExtractService.flatten_by_rel_assoc('rel_assoc_id', flattened_habitat_list, geom_list, obs_habitat,
                                                         survey_list)
        return flattened_habitat_list

    @staticmethod
    def get_obs_habitat_list_by_geom(flattened_habitat_list: List[dict]) -> List[dict]:
        obs_hab_list_by_geom: List[dict] = []
        geom_list: List[str] = list(set([habitat['geom_uuid'] for habitat in flattened_habitat_list]))
        for geom in geom_list:
            obs_hab_list: List[int] = []
            geometry: str = ''
            rel_id: int = -1
            rel_uuid: str = ''
            for habitat in flattened_habitat_list:
                if habitat['geom_uuid'] == geom:
                    obs_hab_list.append(habitat['obs_id'])
                    geometry = habitat['geom']
                    rel_id = habitat['rel_id']
                    rel_uuid = habitat['rel_uuid']
            if len(obs_hab_list) > 0 and geometry != '' and rel_id > 0:
                obs_hab_list_by_geom.append({'geom_uuid': geom, 'geom': geometry, 'rel_id': rel_id,
                                             'rel_uuid': rel_uuid, 'obs_hab_list': obs_hab_list})
        return obs_hab_list_by_geom

    def get_obs_hab_by_typo(self, obs_hab_list: List[int]) -> dict:
        obs_hab_by_typo: dict = {'0': [], '7': [], '4': [], '22': []}
        for obs_hab in obs_hab_list:
            typo: int = self.get_typo_by_obs_hab_id(obs_hab)
            if typo == 0:
                obs_hab_by_typo['0'].append(obs_hab)
            elif typo == 7:
                obs_hab_by_typo['7'].append(obs_hab)
            elif typo == 4:
                obs_hab_by_typo['4'].append(obs_hab)
            elif typo == 22:
                obs_hab_by_typo['22'].append(obs_hab)
        return obs_hab_by_typo

    def format_obs_hab_code_list(self, obs_hab_id_list: List[int]) -> str:
        obs_hab_code_list: List[str] = [self.get_code_by_obs_hab_id(code) for code in obs_hab_id_list]
        return str(obs_hab_code_list).replace('[', '').replace(']', '').replace(', ', ' / ').replace('\'', '')

    def build_obs_hab_block_0(self, obs_hab: int, obs_hab_list_by_typo: dict, obs_habitat_links: List[dict]) -> dict:
        obs_hab_links: List[int] = [link['id_obs_hab_2'] for link in obs_habitat_links
                                    if link['id_obs_hab_1'] == obs_hab]
        obs_hab_links_by_typo: dict = self.get_obs_hab_by_typo(obs_hab_links)
        eunis_list: List[int] = [eunis for eunis in obs_hab_links_by_typo['7']]
        hic_list: List[int] = [hic for hic in obs_hab_links_by_typo['4']]
        corine_list: List[int] = [corine for corine in obs_hab_links_by_typo['22']]
        obs_hab_block: dict = {'obs_hab_id': obs_hab,
                               '%_rec': self.get_pct_rec_by_obs_hab_id(obs_hab),
                               'coef_rec': self.get_coef_rec_by_obs_hab_id(obs_hab),
                               'typ_flor': self.get_typ_flor_by_obs_hab_id(obs_hab),
                               'typ_struct': self.get_typ_struct_by_obs_hab_id(obs_hab),
                               'typ_physio': self.get_typ_physio_by_obs_hab_id(obs_hab),
                               'syntaxon_id': self.get_code_by_obs_hab_id(obs_hab),
                               'syntaxon': self.get_name_by_obs_hab_id(obs_hab),
                               'eunis': self.format_obs_hab_code_list(eunis_list) if len(eunis_list) > 0 else None,
                               'hic': self.format_obs_hab_code_list(hic_list) if len(hic_list) > 0 else None,
                               'corine': self.format_obs_hab_code_list(corine_list) if len(corine_list) > 0 else None}
        for eunis in eunis_list:
            obs_hab_list_by_typo['7'].remove(eunis) if eunis in obs_hab_list_by_typo['7'] else None
        for hic in hic_list:
            obs_hab_list_by_typo['4'].remove(hic) if hic in obs_hab_list_by_typo['4'] else None
        for corine in corine_list:
            obs_hab_list_by_typo['22'].remove(corine) if corine in obs_hab_list_by_typo['22'] else None
        return obs_hab_block

    def build_obs_hab_block_7(self, obs_hab: int, obs_hab_list_by_typo: dict, obs_habitat_links: List[dict]) -> dict:
        obs_hab_links: List[int] = [link['id_obs_hab_2'] for link in obs_habitat_links
                                    if link['id_obs_hab_1'] == obs_hab]
        obs_hab_links_by_typo: dict = self.get_obs_hab_by_typo(obs_hab_links)
        hic_list: List[int] = [hic for hic in obs_hab_links_by_typo['4']]
        corine_list: List[int] = [corine for corine in obs_hab_links_by_typo['22']]
        obs_hab_block: dict = {'obs_hab_id': obs_hab,
                               '%_rec': self.get_pct_rec_by_obs_hab_id(obs_hab),
                               'coef_rec': self.get_coef_rec_by_obs_hab_id(obs_hab),
                               'typ_flor': self.get_typ_flor_by_obs_hab_id(obs_hab),
                               'typ_struct': self.get_typ_struct_by_obs_hab_id(obs_hab),
                               'typ_physio': self.get_typ_physio_by_obs_hab_id(obs_hab),
                               'syntaxon_id': None,
                               'syntaxon': None,
                               'eunis': self.get_code_by_obs_hab_id(obs_hab),
                               'hic': self.format_obs_hab_code_list(hic_list) if len(hic_list) > 0 else None,
                               'corine': self.format_obs_hab_code_list(corine_list) if len(corine_list) > 0 else None}
        for hic in hic_list:
            obs_hab_list_by_typo['4'].remove(hic) if hic in obs_hab_list_by_typo['4'] else None
        for corine in corine_list:
            obs_hab_list_by_typo['22'].remove(corine) if corine in obs_hab_list_by_typo['22'] else None
        return obs_hab_block

    def build_obs_hab_block_4(self, obs_hab: int, obs_hab_list_by_typo: dict, obs_habitat_links: List[dict]) -> dict:
        obs_hab_links: List[int] = [link['id_obs_hab_2'] for link in obs_habitat_links
                                    if link['id_obs_hab_1'] == obs_hab]
        obs_hab_links_by_typo: dict = self.get_obs_hab_by_typo(obs_hab_links)
        corine_list: List[int] = [corine for corine in obs_hab_links_by_typo['22']]
        obs_hab_block: dict = {'obs_hab_id': obs_hab,
                               '%_rec': self.get_pct_rec_by_obs_hab_id(obs_hab),
                               'coef_rec': self.get_coef_rec_by_obs_hab_id(obs_hab),
                               'typ_flor': self.get_typ_flor_by_obs_hab_id(obs_hab),
                               'typ_struct': self.get_typ_struct_by_obs_hab_id(obs_hab),
                               'typ_physio': self.get_typ_physio_by_obs_hab_id(obs_hab),
                               'syntaxon_id': None,
                               'syntaxon': None,
                               'eunis': None,
                               'hic': self.get_code_by_obs_hab_id(obs_hab),
                               'corine': self.format_obs_hab_code_list(corine_list) if len(corine_list) > 0 else None}
        for corine in corine_list:
            obs_hab_list_by_typo['22'].remove(corine) if corine in obs_hab_list_by_typo['22'] else None
        return obs_hab_block

    def build_obs_hab_block_22(self, obs_hab) -> dict:
        return {'obs_hab_id': obs_hab,
                '%_rec': self.get_pct_rec_by_obs_hab_id(obs_hab),
                'coef_rec': self.get_coef_rec_by_obs_hab_id(obs_hab),
                'typ_flor': self.get_typ_flor_by_obs_hab_id(obs_hab),
                'typ_struct': self.get_typ_struct_by_obs_hab_id(obs_hab),
                'typ_physio': self.get_typ_physio_by_obs_hab_id(obs_hab),
                'syntaxon_id': None,
                'syntaxon': None,
                'eunis': None,
                'hic': None,
                'corine': self.get_code_by_obs_hab_id(obs_hab)}

    def build_obs_habitats_blocks(self, obs_habitat_list_by_geom: List[dict], obs_habitat_links: List[dict]) -> List[
        dict]:
        obs_habitat_blocks_by_geom: List[dict] = []
        for geom in obs_habitat_list_by_geom:
            obs_hab_blocks: List[dict] = []
            obs_hab_list_by_typo: dict = self.get_obs_hab_by_typo(geom['obs_hab_list'])
            for obs_hab in obs_hab_list_by_typo['0']:
                obs_hab_blocks.append(self.build_obs_hab_block_0(obs_hab, obs_hab_list_by_typo, obs_habitat_links))
            for obs_hab in obs_hab_list_by_typo['7']:
                obs_hab_blocks.append(self.build_obs_hab_block_7(obs_hab, obs_hab_list_by_typo, obs_habitat_links))
            for obs_hab in obs_hab_list_by_typo['4']:
                obs_hab_blocks.append(self.build_obs_hab_block_4(obs_hab, obs_hab_list_by_typo, obs_habitat_links))
            for obs_hab in obs_hab_list_by_typo['22']:
                obs_hab_blocks.append(self.build_obs_hab_block_22(obs_hab))
            obs_habitat_blocks_by_geom.append({'geom_uuid': geom['geom_uuid'], 'geom': geom['geom'],
                                               'rel_id': geom['rel_id'], 'rel_uuid': geom['rel_uuid'],
                                               'obs_hab_blocks': obs_hab_blocks})
        return obs_habitat_blocks_by_geom

    @staticmethod
    def sort_blocks(blocks: List[dict]) -> List[dict]:
        sorted_blocks_with_rec: List[dict] = []
        sorted_blocks_others: List[dict] = []
        other_blocks: List[dict] = []
        i, j = 0, 0
        for block in blocks:
            if block['%_rec']:
                sorted_blocks_with_rec.append(block)
            elif block['syntaxon']:
                sorted_blocks_others.append(block)
            else:
                other_blocks.append(block)
        return sorted(sorted_blocks_with_rec, key=itemgetter('%_rec'), reverse=True) + sorted(
            sorted_blocks_others, key=itemgetter('syntaxon')) + other_blocks

    def flatten_obs_habitats_blocks(self, obs_habitat_blocks_by_geom: List[dict]) -> List[dict]:
        flatten_target_dict: List[dict] = []
        for geom in obs_habitat_blocks_by_geom:
            items: List[tuple] = []
            for key, value in geom.items():
                if key != 'obs_hab_blocks':
                    items.append((key, value))
                else:
                    i = 1
                    sorted_blocks = self.sort_blocks(geom['obs_hab_blocks'])
                    for block in sorted_blocks:
                        for k, v in block.items():
                            items.append((k + '_' + str(i), v))
                        i += 1
            flatten_target_dict.append(dict(items))
        return flatten_target_dict

    def build_target_dict(self) -> List[dict]:
        all_geom_list = self.row_to_dict(self.get_all_geom_list())
        obs_habitat_list: List[dict] = self.row_to_dict(self.get_obs_habitat_list())
        obs_habitat_links = self.row_to_dict(self.get_links_between_obs_habitat())
        survey_list: List[dict] = self.row_to_dict(self.get_survey_list())
        survey_list = [{'id': survey['id'], 'uuid': survey['uuid'], 'type': survey['type'], 'geom_uuid': self.concat([
            survey['point_uuid'], survey['line_uuid'], survey['polygon_uuid']])} for survey in survey_list]
        flattened_habitat_list = CartoExtractService.flatten_obs_habitat(obs_habitat_list, survey_list, all_geom_list)
        obs_habitat_list_by_geom: List[dict] = CartoExtractService.get_obs_habitat_list_by_geom(flattened_habitat_list)
        obs_habitat_blocks_by_geom: List[dict] = self.build_obs_habitats_blocks(obs_habitat_list_by_geom,
                                                                                obs_habitat_links)
        target_dict: List[dict] = self.flatten_obs_habitats_blocks(obs_habitat_blocks_by_geom)
        return target_dict

    def export_csv(self, target_dict: List[dict]):
        if len(target_dict) > 0:
            fieldnames: List[str] = []
            for geom in target_dict:
                for key in geom.keys():
                    if key not in fieldnames:
                        fieldnames.append(key)
            with open(self.home_path / self.filename, 'w', encoding='utf-8', newline='') as file:
                writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter=';')
                writer.writeheader()
                writer.writerows(target_dict)

    def process(self, database_file_name: str = SQLITE_FILE):
        self.sqlite_connection(database_file_name)
        target_data = self.build_target_dict()
        if len(target_data) > 0 and self.cursor:
            self.export_csv(target_data)
        self.connection.close()
        self.send_message('Export carto', self.filename)


class ObsHabExtractService(ExtractService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)
        self.filename = 'obs_hab_export.csv'

    def get_date_by_survey_id(self, survey_id: int) -> str:
        query = "SELECT date_obs AS date FROM releves WHERE id = %s"
        result: Row = self.connection.execute(query % survey_id).fetchone()
        return str(result['date']) if result and 'date' in result.keys() and result['date'] else None

    def get_num_by_survey_id(self, survey_id: int) -> str:
        query = "SELECT rel_num AS num FROM releves WHERE id = %s"
        result: Row = self.connection.execute(query % survey_id).fetchone()
        return str(result['num']) if result and 'num' in result.keys() and result['num'] else None

    def get_location_by_survey_id(self, survey_id: int) -> str:
        query = "SELECT rel_lieu_dit AS lieu_dit FROM releves WHERE id = %s"
        result: Row = self.connection.execute(query % survey_id).fetchone()
        return str(result['lieu_dit']) if result and 'lieu_dit' in result.keys() and result['lieu_dit'] else None

    def get_survey_comment_by_survey_id(self, survey_id: int) -> str:
        query = "SELECT commentaires AS comment FROM releves WHERE id = %s"
        result: Row = self.connection.execute(query % survey_id).fetchone()
        return str(result['comment']) if result and 'comment' in result.keys() and result['comment'] else None

    def get_habitat_cite_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT obs_hab_nom_cite AS name FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['name']) if result and 'name' in result.keys() and result['name'] else None

    def get_habitat_code_cite_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT obs_hab_code_cite AS code FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['code']) if result and 'code' in result.keys() and result['code'] else None

    def get_habitat_retenu_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT syntaxon_nom_retenu AS name FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['name']) if result and 'name' in result.keys() and result['name'] else None

    def get_habitat_code_retenu_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT syntaxon_code_phyto_retenu AS code FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['code']) if result and 'code' in result.keys() and result['code'] else None

    def get_habitat_comment_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT obs_hab_commentaire_habitat AS comment FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['comment']) if result and 'comment' in result.keys() and result['comment'] else None

    def get_taxon_dominant_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT syntaxon_comm_non_sat_taxon_dom_nom AS tax_dom FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['tax_dom']) if result and 'tax_dom' in result.keys() and result['tax_dom'] else None

    def get_forme_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT obs_hab_forme_id AS typ_physio_id, " \
                "(SELECT value FROM listes_valeurs WHERE list_name = 'TYPE_FORME_VALUES' " \
                "AND obs_habitats.obs_hab_forme_id = key) AS forme " \
                "FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return result['forme'] if result and 'forme' in result.keys() else None

    def get_type_rel_by_code(self, code_type: int) -> str:
        query = "SELECT value FROM listes_valeurs WHERE list_name = 'TYPE_REL_VALUES' AND key = %s"
        result: Row = self.connection.execute(query % code_type).fetchone()
        return result['value'] if result and 'value' in result.keys() else None

    def get_protocol_by_code(self, code_type: int) -> str:
        query = "SELECT value FROM listes_valeurs WHERE list_name = 'PROTOCOLE_VALUES' AND key = %s"
        result: Row = self.connection.execute(query % code_type).fetchone()
        return result['value'] if result and 'value' in result.keys() else None

    def get_jdd_by_uuid(self, uuid: str) -> str:
        query = "SELECT libelle_court FROM jdd WHERE uuid = '%s'"
        result: Row = self.connection.execute(query % uuid).fetchone()
        return result['libelle_court'] if result and 'libelle_court' in result.keys() else None

    def get_com_basale_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT syntaxon_comm_non_sat_id AS com_basale_id, " \
                "(SELECT value FROM listes_valeurs WHERE list_name = 'COMM_NON_SATUREES_VALUES' " \
                "AND obs_habitats.syntaxon_comm_non_sat_id = key) AS com_sat " \
                "FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return result['com_sat'] if result and 'com_sat' in result.keys() else None

    def get_linked_obs_hab_by_obs_hab_id(self, obs_id: int) -> List[int]:
        query = "SELECT id_obs_hab_1 AS linked_obs_hab_id FROM liens_obs_habitat WHERE id_obs_hab_2 = %s"
        result: List[Row] = self.connection.execute(query % obs_id).fetchall()
        if len(result) > 0:
            return [row['linked_obs_hab_id'] for row in result]
        else:
            return []

    @staticmethod
    def get_typo_by_code(code_typo: int) -> str:
        typo: str = ''
        if code_typo == 0:
            typo = 'Phyto'
        elif code_typo == 4:
            typo = 'HIC'
        elif code_typo == 7:
            typo = 'EUNIS'
        elif code_typo == 22:
            typo = 'Corine'
        return typo

    def get_obs_hab_links_by_obs_hab_id(self, obs_id: int, obs_hab_link_list: List[dict]) -> dict:
        obs_hab_links: dict = {'0': [], '7': [], '4': [], '22': []}
        linked_obs_hab_list: List[int] = self.get_linked_obs_hab_by_obs_hab_id(obs_id)
        for obs_hab in linked_obs_hab_list:
            typo: int = self.get_typo_by_obs_hab_id(obs_hab)
            if typo == 0:
                obs_hab_links['0'].append(obs_hab)
            elif typo == 7:
                obs_hab_links['7'].append(obs_hab)
            elif typo == 4:
                obs_hab_links['4'].append(obs_hab)
            elif typo == 22:
                obs_hab_links['22'].append(obs_hab)
        return obs_hab_links

    def get_formatted_obs_hab_dict(self, geom: dict, obs_hab_links: dict, obs_habitat: dict, obs_id: int, survey: dict,
                                   survey_id: int):
        return {
            'geom_uuid': survey['geom_uuid'],
            'releve_id': survey_id,
            'releve_uuid': survey['uuid'],
            'type_releve': self.get_type_rel_by_code(survey['type']),
            'protocole': self.get_protocol_by_code(survey['protocol']),
            'jdd': self.get_jdd_by_uuid(survey['jdd']),
            'date_releve': self.get_date_by_survey_id(survey_id),
            'observateurs': self.calculate_observers(survey_id),
            'numero': self.get_num_by_survey_id(survey_id),
            'commune': '',
            'lieu-dit': self.get_location_by_survey_id(survey_id),
            'commentaire_releve': self.get_survey_comment_by_survey_id(survey_id),
            'obs_hab_id': obs_id,
            'obs_hab_uuid': obs_habitat['uuid'],
            'habitat_cite': self.get_habitat_cite_by_obs_hab_id(obs_id),
            'habitat_code_cite': self.get_habitat_code_cite_by_obs_hab_id(obs_id),
            'syntaxon_retenu': self.get_habitat_retenu_by_obs_hab_id(obs_id),
            'syntaxon_code_retenu': self.get_habitat_code_retenu_by_obs_hab_id(obs_id),
            'code_typo': self.get_typo_by_code(obs_habitat['typo_code']),
            'recouvrement_pct': self.get_pct_rec_by_obs_hab_id(obs_id),
            'recouvrement_coef': self.get_coef_rec_by_obs_hab_id(obs_id),
            'forme': self.get_forme_by_obs_hab_id(obs_id),
            'typicite_structurelle': self.get_typ_struct_by_obs_hab_id(obs_id),
            'typicite_floristique': self.get_typ_flor_by_obs_hab_id(obs_id),
            'type_physio': self.get_typ_physio_by_obs_hab_id(obs_id),
            'communaute_basale': self.get_com_basale_by_obs_hab_id(obs_id),
            'taxon_dominant': self.get_taxon_dominant_by_obs_hab_id(obs_id),
            'commentaire_obs_hab': self.get_habitat_comment_by_obs_hab_id(obs_id),
            'liens_syntaxons': self.format_obs_hab_code_list(obs_hab_links['0']),
            'liens_HIC': self.format_obs_hab_code_list(obs_hab_links['4']),
            'liens_EUNIS': self.format_obs_hab_code_list(obs_hab_links['7']),
            'liens_Corine': self.format_obs_hab_code_list(obs_hab_links['22']),
            'geom': geom['geometry']
        }

    def flatten_by_rel_assoc(self, field: str, flattened_habitat_list: List[dict], geom_list: List[dict],
                             obs_habitat: dict, survey_list: List[dict], obs_hab_link_list: List[dict]):
        for survey in survey_list:
            survey_id = survey['id']
            if survey_id == obs_habitat[field]:
                for geom in geom_list:
                    if geom['uuid'] == survey['geom_uuid']:
                        obs_id = obs_habitat['id']
                        obs_hab_links: dict = self.get_obs_hab_links_by_obs_hab_id(obs_id, obs_hab_link_list)
                        flattened_habitat_list.append(self.get_formatted_obs_hab_dict(geom, obs_hab_links, obs_habitat,
                                                                                      obs_id, survey, survey_id))

    def flatten_obs_habitat(self, obs_habitat_list: List[dict], survey_list: List[dict], geom_list: List[dict],
                            obs_hab_link_list: List[dict]) -> List[dict]:
        flattened_habitat_list: List[dict] = []
        for obs_habitat in obs_habitat_list:
            if obs_habitat['rel_id'] and (obs_habitat['code'] or obs_habitat['name']):
                self.flatten_by_rel_assoc('rel_id', flattened_habitat_list, geom_list, obs_habitat, survey_list,
                                          obs_hab_link_list)
            elif obs_habitat['rel_assoc_id'] and (obs_habitat['code'] or obs_habitat['name']):
                self.flatten_by_rel_assoc('rel_assoc_id', flattened_habitat_list, geom_list, obs_habitat, survey_list,
                                          obs_hab_link_list)
        return flattened_habitat_list

    @staticmethod
    def sort_obs_hab(flattened_obs_hab_list: List[dict]) -> List[dict]:
        return sorted(sorted(flattened_obs_hab_list, key=itemgetter('code_typo'), reverse=True),
                      key=itemgetter('releve_id'))

    def build_target_dict(self) -> List[dict]:
        all_geom_list: List[dict] = self.row_to_dict(self.get_all_geom_list())
        obs_habitat_list: List[dict] = self.row_to_dict(self.get_obs_habitat_list())
        obs_habitat_links: List[dict] = self.row_to_dict(self.get_links_between_obs_habitat())
        survey_list: List[dict] = self.row_to_dict(self.get_survey_list())
        survey_list = [{'id': survey['id'], 'uuid': survey['uuid'], 'type': survey['type'],
                        'protocol': survey['protocol'], 'jdd': survey['jdd'], 'geom_uuid': self.concat(
                [survey['point_uuid'], survey['line_uuid'], survey['polygon_uuid']])} for survey in survey_list]
        flattened_habitat_list = self.flatten_obs_habitat(obs_habitat_list, survey_list, all_geom_list,
                                                          obs_habitat_links)
        return self.sort_obs_hab(flattened_habitat_list)

    def export_csv(self, target_dict: List[dict]):
        if len(target_dict) > 0:
            fieldnames: List[str] = []
            for geom in target_dict:
                for key in geom.keys():
                    if key not in fieldnames:
                        fieldnames.append(key)
            with open(self.home_path / self.filename, 'w', encoding='utf-8', newline='') as file:
                writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter=';')
                writer.writeheader()
                writer.writerows(target_dict)

    def process(self, database_file_name: str = SQLITE_FILE):
        self.sqlite_connection(database_file_name)
        target_data = self.build_target_dict()
        if len(target_data) > 0 and self.cursor:
            self.export_csv(target_data)
        self.connection.close()
        self.send_message('Export habitat observations', self.filename)


class PhytoTableExtractService(ExtractService):
    fieldnames: List[str]

    def __init__(self, project: QgsProject = None):
        super().__init__(project)
        self.filename = 'phyto_table.csv'

    def get_survey_list(self) -> List[Row]:
        query = """SELECT rel.id AS id, rel_uuid AS uuid, rel.date_obs AS date, rel.rel_rec_total AS recouvrement,
        rel.rel_rec_hauteur AS hauteur, rel.rel_lieu_dit AS lieu_dit, rel.rel_num AS num, rel.rel_surface AS surface,
        rel.rel_altitude AS altitude, rel_hab.obs_hab_code_cite AS code_syntaxon, rel_hab.obs_hab_nom_cite AS syntaxon,
        type.value AS type, forme.value AS forme, proto.value AS protocol, expo.value AS exposition, pente.value AS pente,
        MIN(hauteur_min) FILTER (WHERE strate.value LIKE '%Strate arborée%') AS hauteur_min_arbo,
        ROUND(AVG(hauteur_modale) FILTER (WHERE strate.value LIKE '%Strate arborée%'), 2) AS hauteur_mod_arbo,
        MAX(hauteur_max) FILTER (WHERE strate.value LIKE '%Strate arborée%') AS hauteur_max_arbo,
        MIN(hauteur_min) FILTER (WHERE strate.value LIKE '%Strate arbustive%') AS hauteur_min_arbu,
        ROUND(AVG(hauteur_modale) FILTER (WHERE strate.value LIKE '%Strate arbustive%'), 2) AS hauteur_mod_arbu,
        MAX(hauteur_max) FILTER (WHERE strate.value LIKE '%Strate arbustive%') AS hauteur_max_arbu,
        MIN(hauteur_min) FILTER (WHERE strate.value LIKE '%Strate herbacée%') AS hauteur_min_herb,
        ROUND(AVG(hauteur_modale) FILTER (WHERE strate.value LIKE '%Strate herbacée%'), 2) AS hauteur_mod_herb,
        MAX(hauteur_max) FILTER (WHERE strate.value LIKE '%Strate herbacée%') AS hauteur_max_herb,
        MIN(hauteur_min) FILTER (WHERE strate.value LIKE '%Recouvrement lichénique%' OR strate.value LIKE '%Strate à sphaignes%' OR strate.value LIKE '%Strate des autres bryophytes%' OR strate.value LIKE '%Strate bryolichenique%' OR strate.value LIKE '%Recouvrement bryophytique%') AS hauteur_min_musc,
        ROUND(AVG(hauteur_modale) FILTER (WHERE strate.value LIKE '%Recouvrement lichénique%' OR strate.value LIKE '%Strate à sphaignes%' OR strate.value LIKE '%Strate des autres bryophytes%' OR strate.value LIKE '%Strate bryolichenique%' OR strate.value LIKE '%Recouvrement bryophytique%'), 2) AS hauteur_mod_musc,
        MAX(hauteur_max) FILTER (WHERE strate.value LIKE '%Recouvrement lichénique%' OR strate.value LIKE '%Strate à sphaignes%' OR strate.value LIKE '%Strate des autres bryophytes%' OR strate.value LIKE '%Strate bryolichenique%' OR strate.value LIKE '%Recouvrement bryophytique%') AS hauteur_max_musc,
        MAX(recouvrement) FILTER (WHERE strate.value LIKE '%Strate arborée%') AS recouvrement_arbo,
        MAX(recouvrement) FILTER (WHERE strate.value LIKE '%Strate arbustive%') AS recouvrement_arbu,
        MAX(recouvrement) FILTER (WHERE strate.value LIKE '%Strate herbacée%') AS recouvrement_herb,
        MAX(recouvrement) FILTER (WHERE strate.value LIKE '%Recouvrement lichénique%' OR strate.value LIKE '%Strate à sphaignes%' OR strate.value LIKE '%Strate des autres bryophytes%' OR strate.value LIKE '%Strate bryolichenique%' OR strate.value LIKE '%Recouvrement bryophytique%') AS recouvrement_musc,
        MAX(recouvrement) FILTER (WHERE strate.value LIKE '%Recouvrement algal%') AS recouvrement_alg,
        tax.cd_nom, tax.nom_cite, tax.cd_nom_valide AS cd_ref, tax.nom_valide,
        coef_arbo.value AS coef_arbo, coef_arbu.value AS coef_arbu, coef_herb.value AS coef_herb, coef_musc.value AS coef_musc,
        rel.rel_point_uuid AS point_uuid, rel.rel_ligne_uuid AS line_uuid, rel.rel_polygon_uuid AS polygon_uuid
        FROM releves AS rel
        LEFT JOIN obs_habitats rel_hab ON rel.rel_obs_syntaxon_id = rel_hab.obs_hab_id
        LEFT JOIN observations tax ON rel.id = tax.obs_rel_id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'TYPE_REL_VALUES') type ON type.key = rel.rel_type_rel_id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'FORME_RELEVE_VALUES') forme ON forme.key = rel.rel_forme_id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'PROTOCOLE_VALUES') proto ON proto.key = rel.rel_protocole_id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'EXPOSITION_VALUES') expo ON expo.key = rel.rel_exposition_id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'PENTE_VALUES') pente ON pente.key = rel.rel_pente_id
        LEFT JOIN liens_releve_strate lrs ON lrs.rel_id = rel.id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'STRATES_VALUES') strate ON strate.key = lrs.strate_id
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'COEF_AD_VALUES') coef_arbo ON coef_arbo.key = tax.strate_arbo
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'COEF_AD_VALUES') coef_arbu ON coef_arbu.key = tax.strate_arbu
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'COEF_AD_VALUES') coef_herb ON coef_herb.key = tax.strate_herb
        LEFT JOIN (SELECT * FROM listes_valeurs WHERE list_name = 'COEF_AD_VALUES') coef_musc ON coef_musc.key = tax.strate_musc
        WHERE type.value LIKE 'Relevé Flore/%'
        AND (rel_hab.obs_hab_code_typo = 0 OR rel_hab.obs_hab_code_typo IS NULL)
        GROUP BY rel.id, cd_nom, nom_cite
        """
        return self.connection.execute(query).fetchall()

    @staticmethod
    def get_taxon_list(survey_list: List[dict]) -> List[dict]:
        taxon_list: List[dict] = []
        taxons_dict: List[dict] = [{'name': survey['nom_cite'], 'cd_nom': survey['cd_nom'],
                                    'nom_valide': survey['nom_valide'], 'cd_ref': survey['cd_ref'],
                                    'A_strate': survey['coef_arbo'], 'a_strate': survey['coef_arbu'],
                                    'h_strate': survey['coef_herb'], 'm_strate': survey['coef_musc']}
                                   for survey in survey_list if survey['nom_cite']]
        for taxon in taxons_dict:
            if taxon['A_strate']:
                taxon_list.append({'name': taxon['name'], 'cd_nom': taxon['cd_nom'], 'nom_valide': taxon['nom_valide'],
                                   'cd_ref': taxon['cd_ref'], 'strate': 'A'})
            if taxon['a_strate']:
                taxon_list.append({'name': taxon['name'], 'cd_nom': taxon['cd_nom'], 'nom_valide': taxon['nom_valide'],
                                   'cd_ref': taxon['cd_ref'], 'strate': 'a'})
            if taxon['h_strate']:
                taxon_list.append({'name': taxon['name'], 'cd_nom': taxon['cd_nom'], 'nom_valide': taxon['nom_valide'],
                                   'cd_ref': taxon['cd_ref'], 'strate': 'h'})
            if taxon['m_strate']:
                taxon_list.append({'name': taxon['name'], 'cd_nom': taxon['cd_nom'], 'nom_valide': taxon['nom_valide'],
                                   'cd_ref': taxon['cd_ref'], 'strate': 'm'})
            if taxon['A_strate'] is None and taxon['a_strate'] is None and taxon['h_strate'] is None and taxon[
                'm_strate'] is None:
                taxon_list.append({'name': taxon['name'], 'cd_nom': taxon['cd_nom'], 'nom_valide': taxon['nom_valide'],
                                   'cd_ref': taxon['cd_ref'], 'strate': 'X'})
        taxon_list = [taxon for n, taxon in enumerate(taxon_list) if taxon not in taxon_list[n + 1:]]
        return taxon_list

    def get_uuid_and_geom_from_survey(self, survey: dict) -> (str, str):
        geom_uuid = self.concat([survey['point_uuid'], survey['line_uuid'], survey['polygon_uuid']])
        for geom in self.get_all_geom_list():
            if geom['uuid'] == geom_uuid:
                return geom_uuid, geom['geometry']
        return geom_uuid, None

    @staticmethod
    def concat_heights(survey: dict, stratum: str) -> str:
        try:
            return str(survey[f'hauteur_min_{stratum}'] if survey[f'hauteur_min_{stratum}'] is not None else 'NR') + \
                ' / ' + str(
                    survey[f'hauteur_mod_{stratum}'] if survey[f'hauteur_mod_{stratum}'] is not None else 'NR') + \
                ' / ' + str(survey[f'hauteur_max_{stratum}'] if survey[f'hauteur_max_{stratum}'] is not None else 'NR')
        except Exception:
            iface.messageBar().pushWarning('dict', str(survey))
            return ''

    @staticmethod
    def calculate_taxon_number(survey_list: List[dict], survey_id: int) -> int:
        cd_noms = set([survey['cd_nom'] for survey in survey_list
                       if survey['id'] == survey_id and survey['cd_nom']])
        return len(cd_noms)

    @staticmethod
    def get_city_from_geom(geom: str) -> str:
        # TODO to reactivate when L4Q will be a plugin or manage it crossing city layers
        # city: str = ''
        # try:
        #     in_proj, out_proj = Proj('epsg:2154'), Proj('epsg:4326')
        #     geolocator = Nominatim(user_agent='Lobelia4Qgis')
        #     x1, y1 = wkt.loads(geom).centroid.coords[0]
        #     x2, y2 = transform(in_proj, out_proj, x1, y1)
        #     location = geolocator.reverse(str(x2) + ',' + str(y2))
        #     if location:
        #         address = location.raw['address']
        #         city = address.get('village', '') if address else ''
        # finally:
        #     return city
        return ''

    def get_unique_survey_list(self, survey_list: List[dict]) -> List[dict]:
        unique_survey_list: List[dict] = []
        survey_ids: List[int] = []
        for survey in survey_list:
            if survey['id'] not in survey_ids:
                survey_ids.append(survey['id'])
                geom_uuid, geom = self.get_uuid_and_geom_from_survey(survey)
                unique_survey_list.append({'id': survey['id'], 'type': survey['type'], 'rel_uuid': survey['uuid'],
                                           'protocole': survey['protocol'], 'forme releve': survey['forme'],
                                           'num': survey['num'], 'date': survey['date'], 'geom_uuid': geom_uuid,
                                           'geom': geom, 'commune': self.get_city_from_geom(geom),
                                           'lieu dit': survey['lieu_dit'], 'code syntaxon': survey['code_syntaxon'],
                                           'syntaxon': survey['syntaxon'],
                                           'observateur': self.calculate_observers(survey['id']),
                                           'surface': survey['surface'], 'altitude': survey['altitude'],
                                           'exposition': survey['exposition'], 'pente': survey['pente'],
                                           'hauteur arbo min/mod/max (m)': self.concat_heights(survey, 'arbo'),
                                           'hauteur arbu min/mod/max (m)': self.concat_heights(survey, 'arbu'),
                                           'hauteur herb min/mod/max (m)': self.concat_heights(survey, 'herb'),
                                           'hauteur musc min/mod/max (m)': self.concat_heights(survey, 'musc'),
                                           'hauteur totale (m)': survey['hauteur'],
                                           'rec arbo (%)': survey['recouvrement_arbo'],
                                           'rec arbu (%)': survey['recouvrement_arbu'],
                                           'rec herb (%)': survey['recouvrement_herb'],
                                           'rec musc (%)': survey['recouvrement_musc'],
                                           'rec alg (%)': survey['recouvrement_alg'],
                                           'rec total (%)': survey['recouvrement'],
                                           'nb taxons': self.calculate_taxon_number(survey_list, survey['id'])})
        return unique_survey_list

    @staticmethod
    def get_unique_survey_id_from_survey_id(survey: dict, unique_survey_list: List[dict]):
        i = 1
        for unique_survey in unique_survey_list:
            if survey['id'] == unique_survey['id']:
                return i
            i += 1
        return i

    @staticmethod
    def build_survey_block(target_dict: List[dict], unique_survey_list: List[dict]):
        rows_title = ['id', 'rel_uuid', 'type', 'protocole', 'forme releve', 'num', 'date', 'geom_uuid', 'geom',
                      'commune', 'lieu dit', 'code syntaxon', 'syntaxon', 'observateur', 'surface', 'altitude',
                      'exposition', 'pente', 'hauteur arbo min/mod/max (m)', 'hauteur arbu min/mod/max (m)',
                      'hauteur herb min/mod/max (m)', 'hauteur musc min/mod/max (m)', 'hauteur totale (m)',
                      'rec arbo (%)', 'rec arbu (%)', 'rec herb (%)', 'rec musc (%)', 'rec alg (%)', 'rec total (%)',
                      'nb taxons']
        for row in rows_title:
            line: dict = {'releve_nom_cite': row}
            for i in range(1, len(unique_survey_list) + 1):
                survey = unique_survey_list[i - 1]
                line['R' + str(i)] = survey[row] if row in survey.keys() else None
            target_dict.append(line)

    def build_taxon_block(self, target_dict: List[dict], taxon_list: List[dict], survey_list: List[dict],
                          unique_survey_list: List[dict]):
        for taxon in taxon_list:
            line: dict = {'releve_nom_cite': taxon['name'], 'cd_nom': taxon['cd_nom'],
                          'nom_valide': taxon['nom_valide'], 'cd_ref': taxon['cd_ref'], 'strate': taxon['strate']}
            strate: str = 'coef'
            if taxon['strate'] == 'A':
                strate = 'coef_arbo'
            elif taxon['strate'] == 'a':
                strate = 'coef_arbu'
            elif taxon['strate'] == 'h':
                strate = 'coef_herb'
            elif taxon['strate'] == 'm':
                strate = 'coef_musc'

            for survey in survey_list:
                if survey['cd_nom'] == taxon['cd_nom'] and survey['nom_cite'] == taxon['name'] and survey[
                    'coef_arbo'] is None and survey['coef_arbu'] is None and survey['coef_herb'] is None and survey[
                    'coef_musc'] is None and strate == 'coef':
                    line['R' + str(self.get_unique_survey_id_from_survey_id(survey, unique_survey_list))] = 'X'
                elif survey['cd_nom'] == taxon['cd_nom'] and survey['nom_cite'] == taxon[
                    'name'] and strate != 'coef' and survey[strate]:
                    line['R' + str(self.get_unique_survey_id_from_survey_id(survey, unique_survey_list))] = survey[
                        strate]
            target_dict.append(line)

    def build_target_dict(self) -> List[dict]:
        target_dict: List[dict] = []
        survey_list: List[dict] = self.row_to_dict(self.get_survey_list())
        taxon_list: List[dict] = self.get_taxon_list(survey_list)
        unique_survey_list = self.get_unique_survey_list(survey_list)

        self.fieldnames = ['releve_nom_cite', 'cd_nom', 'nom_valide', 'cd_ref', 'strate']
        for i in range(1, len(unique_survey_list) + 1):
            self.fieldnames.append('R' + str(i))

        self.build_survey_block(target_dict, unique_survey_list)
        self.build_taxon_block(target_dict, taxon_list, survey_list, unique_survey_list)
        return target_dict

    def export_csv(self, target_dict: List[dict]):
        with open(self.home_path / self.filename, 'w', encoding='utf-8', newline='') as file:
            writer = csv.DictWriter(file, fieldnames=self.fieldnames, delimiter=';')
            writer.writeheader()
            writer.writerows(target_dict)

    def process(self, database_file_name: str = SQLITE_FILE):
        self.sqlite_connection(database_file_name)
        target_data = self.build_target_dict()
        if len(target_data) > 0 and self.cursor:
            self.export_csv(target_data)
        self.connection.close()
        self.send_message('Export phyto', self.filename)


class ConnectionHelper:
    def __init__(self, iface_given: QgisInterface = None):
        self.iface = iface_given
        if self.iface is None:
            self.iface = iface

    def test_authentication_config(self) -> bool:
        auth_manager: QgsAuthManager = QgsApplication.authManager()
        config_ids: List[str] = auth_manager.configIds()
        try:
            config_ids.index(AUTH_CONFIG)
        except ValueError:
            self.iface.messageBar().pushWarning('Authentification', 'votre profil Lobelia n\'est pas défini')
            return False
        self.iface.messageBar().pushInfo('Authentification', 'votre profil Lobelia est bien connu')
        return True

    def create_authentication_profile(self):
        auth_manager = QgsApplication.authManager()
        config = QgsAuthMethodConfig()
        config.setMethod('Basic')
        config.setName('lobelia_user')
        config.setId(AUTH_CONFIG)
        config.setConfig('username', 'your_email')
        config.setConfig('password', 'your_lobelia_password')
        auth_manager.storeAuthenticationConfig(config)
        self.iface.messageBar().pushInfo('Authentification', 'un exemple de profil Lobelia a été créé')

    def sqlite_connection(self, project: QgsProject) -> Optional[Connection]:
        connection: Optional[Connection] = None
        try:
            separator = "\\" if platform == "win32" else "/"
            connection = sqlite3.connect(project.homePath() + separator + SQLITE_FILE)
            connection.enable_load_extension(True)
            connection.execute('SELECT load_extension("mod_spatialite")')
            connection.execute('SELECT InitSpatialMetaData(1);')
            connection.execute('PRAGMA case_sensitive_like=ON;')
            connection.row_factory = sqlite3.Row
            connection.text_factory = lambda x: self.decode(x)
            connection.enable_load_extension(False)
        finally:
            return connection

    @staticmethod
    def decode(text) -> str:
        result: str = ''
        try:
            result = text.decode("utf-8")
        except UnicodeDecodeError:
            pass
        finally:
            return result


class CloseEventManager(QObject):
    closed = pyqtSignal()

    def __init__(self, form: QObject):
        super(CloseEventManager, self).__init__(form)
        self.form = form

    def eventFilter(self, obj, event) -> bool:
        if obj == self.form:
            if event.type() == QEvent.Close:
                self.closed.emit()
                return True
        return False


class SurveyFormManager:
    def __init__(self, form: QgsAttributeForm, layer: QgsVectorLayer, feature: QgsFeature):
        self.form = form
        self.layer = layer
        self.feature = feature
        self.project: QgsProject = QgsProject.instance()
        self.data_manager: SurveyDataManager = SurveyDataManager(self.project, form, layer, feature)
        self.geometry_manager: GeometryManager = GeometryManager(layer, feature)
        self.list_values_provider: ListValuesProvider = ListValuesProvider(self.project)
        self.survey_feature: Optional[QgsFeature] = None

        # TODO replace this enumeration of widgets by a big dict, populate by specifications ?
        # global variables to store widgets
        self.frel: Optional[QFrame] = None  # self.frel: Optional[QFrame] = self.form.findChild(QFrame, "frel")
        self.fobs: Optional[QFrame] = None
        self.fgen: Optional[QFrame] = None
        self.fobs_taxons: Optional[QFrame] = None
        self.fobs_syntaxons: Optional[QFrame] = None
        self.frame_syntaxon_complement: Optional[QFrame] = None
        self.frame_syntaxon_data_add: Optional[QFrame] = None

        self.diag_originale: Optional[QCheckBox] = None

        self.labrel: Optional[QLabel] = None
        self.labobs: Optional[QLabel] = None
        self.labgen: Optional[QLabel] = None
        self.save_rel: Optional[QLabel] = None
        self.activate: Optional[QLabel] = None
        self.add_taxon: Optional[QLabel] = None
        self.save_taxon: Optional[QLabel] = None
        self.undo_taxon: Optional[QLabel] = None
        self.add_syntaxon: Optional[QLabel] = None
        self.save_syntaxon: Optional[QLabel] = None
        self.add_related_habitats: Optional[QLabel] = None
        self.undo_syntaxon: Optional[QLabel] = None
        self.cal1_label: Optional[QLabel] = None
        self.cal2_label: Optional[QLabel] = None
        self.rel_create_new_polygon_rel: Optional[QLabel] = None
        self.rel_create_new_line_rel: Optional[QLabel] = None
        self.rel_create_new_point_rel: Optional[QLabel] = None
        self.rel_syntax_create_new_polygon_rel: Optional[QLabel] = None
        self.rel_syntax_create_new_line_rel: Optional[QLabel] = None
        self.rel_syntax_create_new_point_rel: Optional[QLabel] = None
        self.add_obs_btn: Optional[QLabel] = None
        self.suppr_obs_btn: Optional[QLabel] = None
        self.add_strate_struct: Optional[QLabel] = None
        self.add_facteur: Optional[QLabel] = None
        self.wiki_link: Optional[QLabel] = None
        self.extract_phyto_table: Optional[QLabel] = None
        self.extract_carto: Optional[QLabel] = None
        self.extract_obs_hab: Optional[QLabel] = None
        self.clean_db: Optional[QLabel] = None
        self.replicate_jdd: Optional[QLabel] = None
        self.jdd_add: Optional[QLabel] = None
        self.jdd_delete: Optional[QLabel] = None

        self.rel_date_saisie: Optional[QLineEdit] = None
        self.rel_date_min: Optional[QLineEdit] = None
        self.rel_date_max: Optional[QLineEdit] = None
        self.taxon_cite: Optional[QLineEdit] = None
        self.taxon_valide: Optional[QLineEdit] = None
        self.taxon_cd_nom_cite: Optional[QLineEdit] = None
        self.taxon_cd_nom_valide: Optional[QLineEdit] = None
        self.obs_hab_nom_retenu: Optional[QLineEdit] = None
        self.obs_hab_code_retenu: Optional[QLineEdit] = None
        self.obs_hab_nom_cite: Optional[QLineEdit] = None
        self.obs_hab_code_cite: Optional[QLineEdit] = None
        self.syntaxon_retenu: Optional[QLineEdit] = None
        self.syntaxon_code_retenu: Optional[QLineEdit] = None
        self.syntaxon_cite: Optional[QLineEdit] = None
        self.syntaxon_code_cite: Optional[QLineEdit] = None
        self.syntaxon_delete: Optional[QLabel] = None
        self.obs_hab_code_typo: Optional[QLineEdit] = None
        self.struct_rec_pct_strate: Optional[QLineEdit] = None
        self.struct_haut_mod_strate: Optional[QLineEdit] = None
        self.struct_haut_min_strate: Optional[QLineEdit] = None
        self.struct_haut_max_strate: Optional[QLineEdit] = None
        self.taxon_dominant_code: Optional[QLineEdit] = None
        self.taxon_dominant_nom: Optional[QLineEdit] = None
        self.obs_hab_taxon_dominant_code: Optional[QLineEdit] = None
        self.obs_hab_taxon_dominant_nom: Optional[QLineEdit] = None
        self.obs_hab_surface: Optional[QLineEdit] = None
        self.obs_hab_surface_label: Optional[QLabel] = None
        self.active_jdd: Optional[QLineEdit] = None
        self.taches_redox: Optional[QLineEdit] = None
        self.horizon_redox: Optional[QLineEdit] = None
        self.horizon_reduct: Optional[QLineEdit] = None

        self.obs_hab_action_buttons: Optional[QFrame] = None

        self.ctrl: Optional[QGroupBox] = None
        self.hab_subform_data_add_box: Optional[QGroupBox] = None

        self.habitat_buttons: Optional[QButtonGroup] = None
        self.typo_habitat_buttons: Optional[QButtonGroup] = None

        self.observer_list: Optional[QListWidget] = None
        self.obs_hab_assoc_list: Optional[QListWidget] = None
        self.related_habitats_list: Optional[QListWidget] = None

        self.lst_obs: Optional[QComboBox] = None
        self.select_taxon: Optional[QComboBox] = None
        self.select_habitat: Optional[QComboBox] = None
        self.select_strate_struct: Optional[QComboBox] = None
        self.select_facteur: Optional[QComboBox] = None
        self.select_impact_facteur: Optional[QComboBox] = None
        self.select_etat_facteur: Optional[QComboBox] = None
        self.rel_type_rel_id: Optional[QComboBox] = None
        self.rel_protocole_id: Optional[QComboBox] = None
        self.syntaxon_recherche: Optional[QComboBox] = None
        self.habitat_recherche: Optional[QComboBox] = None
        self.syntaxon_comm_non_sat_id: Optional[QComboBox] = None
        self.syntaxon_comm_non_sat_obs: Optional[QComboBox] = None
        self.taxon_dominant_recherche: Optional[QComboBox] = None
        self.select_taxon_dominant: Optional[QComboBox] = None
        self.jdd_list: Optional[QComboBox] = None

        self.syntaxon_diag_originale: Optional[QCheckBox] = None
        self.pres_taches_redox: Optional[QCheckBox] = None
        self.pres_horizon_redox: Optional[QCheckBox] = None
        self.pres_horizon_reduct: Optional[QCheckBox] = None

        self.cal1: QMessageBox = QMessageBox()
        self.cal2: QMessageBox = QMessageBox()

        self.table_taxons: Optional[QTableWidget] = None
        self.table_habitats: Optional[QTableWidget] = None
        self.table_syn_habitats: Optional[QTableWidget] = None
        self.table_strates_struct: Optional[QTableWidget] = None
        self.table_facteurs: Optional[QTableWidget] = None

        self.tab_widget: Optional[QTabWidget] = None

        self.releves: Optional[QgsVectorLayer] = None
        self.observations: Optional[QgsVectorLayer] = None
        self.obs_habitats: Optional[QgsVectorLayer] = None
        self.liens_releve_observateur: Optional[QgsVectorLayer] = None
        self.liens_releve_influence: Optional[QgsVectorLayer] = None
        self.liens_releve_strate: Optional[QgsVectorLayer] = None
        self.liens_obs_habitat: Optional[QgsVectorLayer] = None
        self.observateurs: Optional[QgsVectorLayer] = None
        self.catalogue_phyto: Optional[QgsVectorLayer] = None
        self.habref: Optional[QgsVectorLayer] = None
        self.taxref: Optional[QgsVectorLayer] = None
        self.version: Optional[QgsVectorLayer] = None
        self.jdd: Optional[QgsVectorLayer] = None

    def initialize(self):
        self.define_window_size_and_font()
        self.layer.startEditing()
        self.populate_variables()
        self.populate_layers()
        self.populate_combo_boxes()
        version = [i['version'] for i in self.version.getFeatures()][
            0]  # version = next(self.version.getFeatures())['version']
        self.form.setWindowTitle('Lobelia4Qgis - version ' + str(version))

        add_survey_to_syntaxon_widgets = [self.rel_syntax_create_new_point_rel, self.rel_syntax_create_new_line_rel,
                                          self.rel_syntax_create_new_polygon_rel]

        if self.releves is not None:
            self.survey_feature = self.geometry_manager.get_survey_feature(self.releves)
        else:
            iface.messageBar().pushWarning('Layer error', 'la couche releves n\'est pas disponible')

        self.prepare_table_widget(add_survey_to_syntaxon_widgets)
        self.populate_form()
        self.activate_event_listeners(add_survey_to_syntaxon_widgets)
        self.prepare_startup_interface(add_survey_to_syntaxon_widgets)

    def define_window_size_and_font(self):
        QApplication.setFont(QFont('Tahoma', 10, QFont.Normal))
        available_screen_geometry = QApplication.primaryScreen().availableGeometry()
        dialog_box: QDialog = self.form.findChild(QDialog, "Dialog")
        scroll_area: QWidget = self.form.findChild(QWidget, "scrollAreaWidgetContents")
        self.form.setMaximumWidth(int(available_screen_geometry.width() * 0.8))
        self.form.setMaximumHeight(int(available_screen_geometry.height() * 0.95))
        dialog_box.setMaximumWidth(int(available_screen_geometry.width() * 0.8))
        dialog_box.setMaximumHeight(int(available_screen_geometry.height() * 0.95))
        scroll_area.setMaximumWidth(int(available_screen_geometry.width() * 0.79))
        scroll_area.setMaximumHeight(int(available_screen_geometry.height() * 0.94))

    def populate_form(self):
        feature_id = 0
        if self.survey_feature:
            feature_id = self.survey_feature.id()
        if feature_id > 0:
            self.data_manager.map_survey(self.releves, self.survey_feature)
            self.data_manager.get_habitats(feature_id, self.obs_habitats, self.table_habitats, self.obs_hab_assoc_list,
                                           self.habitat_buttons, self.obs_hab_code_typo, self.frame_syntaxon_complement,
                                           self.frame_syntaxon_data_add, self.obs_hab_action_buttons)
            self.data_manager.get_observers(feature_id, self.liens_releve_observateur, self.lst_obs, self.observer_list)
            self.data_manager.get_jdd(self.survey_feature, self.active_jdd)
            self.data_manager.get_taxons(feature_id, self.observations, self.table_taxons)
            self.data_manager.get_syntaxon(self.form, self.survey_feature['rel_obs_syntaxon_id'], self.obs_habitats)
            self.data_manager.get_typo_habitats(self.survey_feature, self.obs_habitats,
                                                self.table_syn_habitats)  # self.survey_feature['rel_obs_syntaxon_id']
            self.data_manager.get_strates(feature_id, self.liens_releve_strate, self.table_strates_struct)
            self.data_manager.get_factors(feature_id, self.liens_releve_influence, self.table_facteurs)
        else:
            self.rel_type_rel_id.setCurrentIndex(self.rel_type_rel_id.findData(0))

    def populate_variables(self):
        # TODO replace this enumeration by a loop reading specifications
        self.frel = self.form.findChild(QFrame, "frel")
        self.fobs = self.form.findChild(QFrame, "fobs")
        self.fgen = self.form.findChild(QFrame, "fgen")
        self.fobs_taxons = self.form.findChild(QFrame, "frame_obs_taxons")
        self.fobs_syntaxons = self.form.findChild(QFrame, "frame_obs_habitats")
        self.frame_syntaxon_complement = self.form.findChild(QFrame, "frame_syntaxon_complement")
        self.frame_syntaxon_data_add = self.form.findChild(QFrame, "frame_obs_hab_data_add_syntaxon")
        self.diag_originale = self.form.findChild(QCheckBox, "syn_diag_originale")
        self.labrel = self.form.findChild(QLabel, "lab_form_rel")
        self.labobs = self.form.findChild(QLabel, "lab_form_obs")
        self.labgen = self.form.findChild(QLabel, "lab_form_rel_gen")
        self.save_rel = self.form.findChild(QLabel, "save_releve")
        self.activate = self.form.findChild(QLabel, "edit_releve")
        self.add_taxon = self.form.findChild(QLabel, "add_taxon")
        self.save_taxon = self.form.findChild(QLabel, "save_taxon")
        self.undo_taxon = self.form.findChild(QLabel, "undo_taxon")
        self.add_syntaxon = self.form.findChild(QLabel, "lb_add_syntaxon")
        self.save_syntaxon = self.form.findChild(QLabel, "lb_save_syntaxon")
        self.add_related_habitats = self.form.findChild(QLabel, "lb_add_related_habitats")
        self.undo_syntaxon = self.form.findChild(QLabel, "lb_undo_syntaxon")
        self.cal1_label = self.form.findChild(QLabel, "calendrier_min")
        self.cal2_label = self.form.findChild(QLabel, "calendrier_max")
        self.rel_create_new_polygon_rel = self.form.findChild(QLabel, "rel_create_new_polygon_rel")
        self.rel_create_new_line_rel = self.form.findChild(QLabel, "rel_create_new_ligne_rel")
        self.rel_create_new_point_rel = self.form.findChild(QLabel, "rel_create_new_point_rel")
        self.rel_syntax_create_new_polygon_rel = self.form.findChild(QLabel, "rel_syntax_create_new_polygon_rel")
        self.rel_syntax_create_new_line_rel = self.form.findChild(QLabel, "rel_syntax_create_new_ligne_rel")
        self.rel_syntax_create_new_point_rel = self.form.findChild(QLabel, "rel_syntax_create_new_point_rel")
        self.add_obs_btn = self.form.findChild(QLabel, "add_obs_btn")
        self.suppr_obs_btn = self.form.findChild(QLabel, "suppr_obs_btn")
        self.add_strate_struct = self.form.findChild(QLabel, "add_strate_struct")
        self.add_facteur = self.form.findChild(QLabel, "add_facteur")
        self.wiki_link = self.form.findChild(QLabel, "wiki")
        self.extract_phyto_table = self.form.findChild(QLabel, "extract_phyto_table")
        self.extract_carto = self.form.findChild(QLabel, "extract_carto")
        self.extract_obs_hab = self.form.findChild(QLabel, "extract_obs_hab")
        self.clean_db = self.form.findChild(QLabel, "clean_db")
        self.replicate_jdd = self.form.findChild(QLabel, "replicate_jdd")
        self.jdd_add = self.form.findChild(QLabel, "jdd_add")
        self.jdd_delete = self.form.findChild(QLabel, "jdd_delete")
        self.rel_date_saisie = self.form.findChild(QLineEdit, "rel_date_saisie")
        self.rel_date_saisie.setText(QDate.currentDate().toString(Qt.ISODate))

        self.rel_date_min = self.form.findChild(QLineEdit, "rel_date_min")
        self.rel_date_max = self.form.findChild(QLineEdit, "rel_date_max")
        self.taxon_cite = self.form.findChild(QLineEdit, "nom_cite")
        self.taxon_valide = self.form.findChild(QLineEdit, "nom_valide")
        self.taxon_cd_nom_cite = self.form.findChild(QLineEdit, "cd_nom_cite")
        self.taxon_cd_nom_valide = self.form.findChild(QLineEdit, "cd_nom_valide")
        self.obs_hab_nom_retenu = self.form.findChild(QLineEdit, "obs_habitat_nom_retenu")
        self.obs_hab_code_retenu = self.form.findChild(QLineEdit, "obs_habitat_code_retenu")
        self.obs_hab_nom_cite = self.form.findChild(QLineEdit, "obs_habitat_nom_cite")
        self.obs_hab_code_cite = self.form.findChild(QLineEdit, "obs_habitat_code_cite")
        self.syntaxon_retenu = self.form.findChild(QLineEdit, "syntaxon_retenu")
        self.syntaxon_code_retenu = self.form.findChild(QLineEdit, "syntaxon_code_retenu")
        self.syntaxon_cite = self.form.findChild(QLineEdit, "syntaxon_cite")
        self.syntaxon_code_cite = self.form.findChild(QLineEdit, "syntaxon_code_cite")
        self.syntaxon_delete = self.form.findChild(QLabel, "syntaxon_delete")
        self.obs_hab_code_typo = self.form.findChild(QLineEdit, "obs_hab_code_typo")
        self.struct_rec_pct_strate = self.form.findChild(QLineEdit, "strate_rec_pct")
        self.struct_haut_mod_strate = self.form.findChild(QLineEdit, "strate_haut_mod")
        self.struct_haut_min_strate = self.form.findChild(QLineEdit, "strate_haut_min")
        self.struct_haut_max_strate = self.form.findChild(QLineEdit, "strate_haut_max")
        self.syntaxon_comm_non_sat_id = self.form.findChild(QComboBox, "syntaxon_comm_non_sat_id")
        self.taxon_dominant_code = self.form.findChild(QLineEdit, "syntaxon_taxon_dominant_cns_code")
        self.taxon_dominant_nom = self.form.findChild(QLineEdit, "syntaxon_taxon_dominant_cns_nom")
        self.syntaxon_comm_non_sat_obs = self.form.findChild(QComboBox, "obs_hab_comm_non_sat_obs")
        self.obs_hab_taxon_dominant_code = self.form.findChild(QLineEdit, "obs_hab_comm_non_sat_taxon_code")
        self.obs_hab_taxon_dominant_nom = self.form.findChild(QLineEdit, "obs_hab_comm_non_sat_taxon_nom")
        self.obs_hab_surface = self.form.findChild(QLineEdit, "obs_hab_surface")
        self.obs_hab_surface_label = self.form.findChild(QLabel, "obs_hab_surface_label")
        self.active_jdd = self.form.findChild(QLineEdit, "jdd")
        self.taches_redox = self.form.findChild(QLineEdit, "rel_taches_redox")
        self.horizon_redox = self.form.findChild(QLineEdit, "rel_horizon_redox")
        self.horizon_reduct = self.form.findChild(QLineEdit, "rel_horizon_reduct")
        self.obs_hab_action_buttons = self.form.findChild(QFrame, 'obs_hab_action_buttons')
        self.ctrl = self.form.findChild(QGroupBox, 'typo_groupbox')
        self.hab_subform_data_add_box = self.form.findChild(QGroupBox, 'gpbox_donnees_add')
        self.habitat_buttons = self.form.findChild(QButtonGroup, 'habitats_buttons')
        self.typo_habitat_buttons = self.form.findChild(QButtonGroup, 'habitat_buttons')
        self.observer_list = self.form.findChild(QListWidget, "observateurs_list")
        self.obs_hab_assoc_list = self.form.findChild(QListWidget, "list_hab_obs_assoc")
        self.related_habitats_list = self.form.findChild(QListWidget, "list_related_habitats")
        self.lst_obs = self.form.findChild(QComboBox, "lst_obs")
        self.select_taxon = self.form.findChild(QComboBox, "select_taxon")
        self.select_habitat = self.form.findChild(QComboBox, "select_habitat")
        self.rel_type_rel_id = self.form.findChild(QComboBox, "rel_type_rel_id")
        self.rel_protocole_id = self.form.findChild(QComboBox, "rel_protocole_id")
        self.syntaxon_recherche = self.form.findChild(QComboBox, "syntaxon_recherche")
        self.habitat_recherche = self.form.findChild(QComboBox, "habitat_recherche")
        self.select_strate_struct = self.form.findChild(QComboBox, "cb_strate_struct")
        self.select_facteur = self.form.findChild(QComboBox, "cb_facteur_influence")
        self.select_impact_facteur = self.form.findChild(QComboBox, "cb_facteur_impact")
        self.select_etat_facteur = self.form.findChild(QComboBox, "cb_facteur_etat")
        self.taxon_dominant_recherche = self.form.findChild(QComboBox, "taxon_dominant_recherche")
        self.select_taxon_dominant = self.form.findChild(QComboBox, "select_taxon_dominant")
        self.jdd_list = self.form.findChild(QComboBox, "jdd_list")
        self.syntaxon_diag_originale = self.form.findChild(QCheckBox, "syn_diag_originale")
        self.pres_taches_redox = self.form.findChild(QCheckBox, "rel_pres_taches_redox")
        self.pres_horizon_redox = self.form.findChild(QCheckBox, "rel_pres_horizon_redox")
        self.pres_horizon_reduct = self.form.findChild(QCheckBox, "rel_pres_horizon_reduct")
        self.table_habitats = self.form.findChild(QTableWidget, "table_habitats")
        self.table_syn_habitats = self.form.findChild(QTableWidget, "table_syn_habitats")
        self.table_taxons = self.form.findChild(QTableWidget, "table_taxons")
        self.table_strates_struct = self.form.findChild(QTableWidget, "table_strates_struct")
        self.table_facteurs = self.form.findChild(QTableWidget, "table_facteurs")
        self.tab_widget = self.form.findChild(QTabWidget, "tabWidget_phyto")

    def populate_layers(self):
        for layer in self.project.mapLayers().values():
            if layer.name() == 'releves':
                self.releves = layer
            elif layer.name() == 'observations':
                self.observations = layer
            elif layer.name() == 'obs_habitats':
                self.obs_habitats = layer
            elif layer.name() == 'liens_releve_observateur':
                self.liens_releve_observateur = layer
            elif layer.name() == 'liens_releve_influence':
                self.liens_releve_influence = layer
            elif layer.name() == 'liens_releve_strate':
                self.liens_releve_strate = layer
            elif layer.name() == 'liens_obs_habitat':
                self.liens_obs_habitat = layer
            elif layer.name() == 'observateurs':
                self.observateurs = layer
            elif layer.name() == 'catalogue_phyto':
                self.catalogue_phyto = layer
            elif layer.name() == 'taxref':
                self.taxref = layer
            elif layer.name() == 'habref':
                self.habref = layer
            elif layer.name() == 'version':
                self.version = layer
            elif layer.name() == 'jdd':
                self.jdd = layer

    def populate_combo_boxes(self):
        combo_boxes_specification: List[dict] = DatabaseSpecificationService.get_combo_boxes_specification()
        for combo_box_spec in combo_boxes_specification:
            for combo_box_name in combo_box_spec['input_name'].split(', '):
                widget: Optional[QComboBox] = self.form.findChild(QComboBox, combo_box_name)
                if widget is not None and combo_box_name != 'cb_facteur_influence':
                    list_values = self.list_values_provider.get_values_from_list_name(combo_box_spec['input_list_name'])
                    WidgetService.load_combo_box(widget, list_values)

    def prepare_table_widget(self, add_survey_to_syntaxon_widgets):
        WidgetService.build_table(self.table_taxons, TAXONS_HEADER_LABELS)
        self.table_taxons.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_double_clicked(i, self.releves, self.survey_feature, self.observations,
                                                        self.form, self.add_taxon, self.save_taxon, self.select_taxon,
                                                        self.habitat_buttons))
        self.table_taxons.itemClicked.connect(
            lambda i: WidgetService.cell_clicked(i, self.releves, self.survey_feature, self.observations))

        self.table_habitats.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_double_clicked(i, self.releves, self.survey_feature, self.obs_habitats,
                                                        self.form, self.add_syntaxon, self.save_syntaxon,
                                                        self.select_habitat, self.habitat_buttons,
                                                        add_survey_to_syntaxon_widgets, self.add_related_habitats))
        self.table_habitats.itemClicked.connect(
            lambda i: WidgetService.cell_clicked(i, self.releves, self.survey_feature, self.obs_habitats))

        WidgetService.build_table(self.table_syn_habitats, HABITATS_HEADER_LABELS)
        self.table_syn_habitats.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_double_clicked(i, self.releves, self.survey_feature, self.obs_habitats,
                                                        self.form, QLabel(), QLabel(), self.habitat_recherche,
                                                        QButtonGroup())
        )

        WidgetService.build_table(self.table_strates_struct, STRATES_STRUCT_LABELS)
        self.table_strates_struct.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_double_clicked(i, self.releves, self.survey_feature, self.liens_releve_strate,
                                                        self.form, QLabel(), QLabel(), self.select_strate_struct,
                                                        QButtonGroup()))

        WidgetService.build_table(self.table_facteurs, FACTORS_STRUCT_LABELS)
        self.table_facteurs.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_double_clicked(i, self.releves, self.survey_feature,
                                                        self.liens_releve_influence, self.form, QLabel(), QLabel(),
                                                        self.select_facteur, QButtonGroup()))

    def activate_event_listeners(self, add_survey_to_syntaxon_widgets):
        self.rel_type_rel_id.currentIndexChanged.connect(
            lambda: WidgetService.load_protocol_list(self.rel_type_rel_id.currentData(), self.rel_protocole_id))
        self.habitat_buttons.buttonClicked.connect(
            lambda: self.data_manager.get_habitats(self.survey_feature.id(), self.obs_habitats, self.table_habitats,
                                                   self.obs_hab_assoc_list, self.habitat_buttons,
                                                   self.obs_hab_code_typo, self.frame_syntaxon_complement,
                                                   self.frame_syntaxon_data_add, self.obs_hab_action_buttons))
        self.hab_subform_data_add_box.collapsedStateChanged.connect(
            lambda: self.data_manager.refresh_hab_subform_data_add_box(self.habitat_buttons,
                                                                       self.frame_syntaxon_data_add))
        WidgetService.clickable(self.labrel).connect(
            lambda: WidgetService.fshow(self.rel_type_rel_id, self.rel_protocole_id, self.frel, self.fobs,
                                        self.save_rel, self.activate))
        WidgetService.clickable(self.labobs).connect(
            lambda: WidgetService.fshow(self.rel_type_rel_id, self.rel_protocole_id, self.frel, self.fobs,
                                        self.save_rel, self.activate))
        WidgetService.clickable(self.labgen).connect(lambda: WidgetService.fgen_show(self.fgen))
        WidgetService.clickable(self.save_rel).connect(
            lambda: self.data_manager.save_survey(self.releves, self.survey_feature, self.obs_habitats))
        WidgetService.clickable(self.activate).connect(
            lambda: self.data_manager.activate_survey(self.frel, self.fobs, self.fgen, self.save_rel, self.activate,
                                                      self.releves, self.survey_feature, self.obs_habitats,
                                                      self.obs_hab_surface, self.obs_hab_surface_label))
        WidgetService.clickable(self.add_taxon).connect(
            lambda: self.data_manager.add_taxon(self.survey_feature, self.observations, self.select_taxon,
                                                self.taxon_cite, self.taxon_cd_nom_cite, self.table_taxons,
                                                self.add_taxon, self.save_taxon))
        WidgetService.clickable(self.save_taxon).connect(
            lambda: self.data_manager.save_taxon(self.survey_feature, self.observations, self.select_taxon,
                                                 self.taxon_cite, self.taxon_cd_nom_cite, self.table_taxons,
                                                 self.add_taxon, self.save_taxon))
        WidgetService.clickable(self.undo_taxon).connect(
            lambda: self.data_manager.refresh_taxon_sub_form(self.add_taxon, self.save_taxon, self.select_taxon))
        WidgetService.clickable(self.add_syntaxon).connect(
            lambda: self.data_manager.add_habitat(self.survey_feature, self.releves, self.obs_habitats,
                                                  self.select_habitat, self.obs_hab_nom_cite, self.obs_hab_code_cite,
                                                  self.table_habitats, self.obs_hab_assoc_list, self.add_syntaxon,
                                                  self.save_syntaxon, self.habitat_buttons, self.add_related_habitats,
                                                  self.related_habitats_list))
        WidgetService.clickable(self.save_syntaxon).connect(
            lambda: self.data_manager.save_habitat(self.survey_feature, self.releves, self.obs_habitats,
                                                   self.select_habitat, self.obs_hab_nom_cite, self.obs_hab_code_cite,
                                                   self.table_habitats, self.obs_hab_assoc_list, self.add_syntaxon,
                                                   self.save_syntaxon, add_survey_to_syntaxon_widgets,
                                                   self.habitat_buttons, self.add_related_habitats,
                                                   self.related_habitats_list))
        WidgetService.clickable(self.add_related_habitats).connect(lambda: self.related_habitats_list.show())
        WidgetService.clickable(self.undo_syntaxon).connect(
            lambda: self.data_manager.refresh_habitat_sub_form(add_survey_to_syntaxon_widgets, self.add_syntaxon,
                                                               self.save_syntaxon, self.select_habitat,
                                                               self.survey_feature, self.obs_habitats,
                                                               self.obs_hab_assoc_list, self.habitat_buttons,
                                                               self.add_related_habitats, self.related_habitats_list))
        WidgetService.clickable(self.cal1_label).connect(
            lambda: WidgetService.calendar(self.cal1, self.cal1_label, self.rel_date_min, self.rel_date_max,
                                           WidgetService.transform_date_1))
        WidgetService.clickable(self.cal2_label).connect(
            lambda: WidgetService.calendar(self.cal2, self.cal2_label, self.rel_date_min, self.rel_date_max,
                                           WidgetService.transform_date_2))
        WidgetService.clickable(self.jdd_add).connect(
            lambda: self.data_manager.add_jdd(self.survey_feature, self.releves, self.jdd_list, self.active_jdd))
        WidgetService.clickable(self.jdd_delete).connect(
            lambda: self.data_manager.delete_jdd(self.survey_feature, self.releves, self.jdd_list, self.active_jdd))
        WidgetService.clickable(self.rel_create_new_polygon_rel).connect(
            lambda: self.data_manager.add_associated_survey(self.rel_create_new_polygon_rel, self.releves,
                                                            self.survey_feature))
        WidgetService.clickable(self.rel_create_new_point_rel).connect(
            lambda: self.data_manager.add_associated_survey(self.rel_create_new_point_rel, self.releves,
                                                            self.survey_feature))
        WidgetService.clickable(self.rel_create_new_line_rel).connect(
            lambda: self.data_manager.add_associated_survey(self.rel_create_new_line_rel, self.releves,
                                                            self.survey_feature))
        WidgetService.clickable(self.rel_syntax_create_new_polygon_rel).connect(
            lambda: self.data_manager.add_survey_associated_to_syntaxon(self.rel_syntax_create_new_polygon_rel,
                                                                        self.releves, self.obs_habitats,
                                                                        self.survey_feature,
                                                                        self.obs_hab_nom_cite,
                                                                        self.obs_hab_code_cite))
        WidgetService.clickable(self.rel_syntax_create_new_point_rel).connect(
            lambda: self.data_manager.add_survey_associated_to_syntaxon(self.rel_syntax_create_new_point_rel,
                                                                        self.releves, self.obs_habitats,
                                                                        self.survey_feature,
                                                                        self.obs_hab_nom_cite,
                                                                        self.obs_hab_code_cite))
        WidgetService.clickable(self.rel_syntax_create_new_line_rel).connect(
            lambda: self.data_manager.add_survey_associated_to_syntaxon(self.rel_syntax_create_new_line_rel,
                                                                        self.releves, self.obs_habitats,
                                                                        self.survey_feature,
                                                                        self.obs_hab_nom_cite,
                                                                        self.obs_hab_code_cite))
        WidgetService.clickable(self.add_obs_btn).connect(
            lambda: self.data_manager.add_observer(self.survey_feature.id(), self.liens_releve_observateur,
                                                   self.lst_obs, self.observer_list))
        WidgetService.clickable(self.suppr_obs_btn).connect(
            lambda: self.data_manager.delete_observer(self.survey_feature.id(), self.liens_releve_observateur,
                                                      self.observer_list, self.observateurs))
        WidgetService.clickable(self.add_strate_struct).connect(
            lambda: self.data_manager.add_strate(self.survey_feature.id(), self.liens_releve_strate,
                                                 self.table_strates_struct, self.select_strate_struct,
                                                 self.struct_rec_pct_strate, self.struct_haut_mod_strate,
                                                 self.struct_haut_min_strate, self.struct_haut_max_strate))
        WidgetService.clickable(self.add_facteur).connect(
            lambda: self.data_manager.add_factor(self.survey_feature.id(), self.liens_releve_influence,
                                                 self.table_facteurs, self.select_facteur, self.select_impact_facteur,
                                                 self.select_etat_facteur))
        WidgetService.clickable(self.extract_phyto_table).connect(lambda: PhytoTableExtractService().process())
        WidgetService.clickable(self.extract_carto).connect(lambda: CartoExtractService().process())
        WidgetService.clickable(self.extract_obs_hab).connect(lambda: ObsHabExtractService().process())
        WidgetService.clickable(self.clean_db).connect(lambda: DatabaseCleaner().clean())
        WidgetService.clickable(self.replicate_jdd).connect(
            lambda: self.data_manager.replicate_jdd(self.survey_feature))
        WidgetService.clickable(self.wiki_link).connect(
            lambda: QDesktopServices.openUrl(QUrl('https://gitlab.com/lobelia/lobelia-4-qgis/-/wikis/home')))
        self.select_taxon.lineEdit().returnPressed.connect(
            lambda: self.data_manager.taxon_auto_complete(self.select_taxon, self.taxref, self.taxon_cite,
                                                          self.taxon_cd_nom_cite, self.taxon_valide,
                                                          self.taxon_cd_nom_valide))
        self.taxon_dominant_recherche.lineEdit().returnPressed.connect(
            lambda: self.data_manager.taxon_auto_complete(self.taxon_dominant_recherche, self.taxref,
                                                          self.taxon_dominant_nom, self.taxon_dominant_code,
                                                          QLineEdit(), QLineEdit()))
        self.select_taxon_dominant.lineEdit().returnPressed.connect(
            lambda: self.data_manager.taxon_auto_complete(self.select_taxon_dominant, self.taxref,
                                                          self.obs_hab_taxon_dominant_nom,
                                                          self.obs_hab_taxon_dominant_code, QLineEdit(), QLineEdit()))
        self.select_habitat.lineEdit().returnPressed.connect(
            lambda: self.data_manager.habitat_auto_complete(self.select_habitat, self.catalogue_phyto, self.habref,
                                                            self.habitat_buttons, self.obs_hab_nom_cite,
                                                            self.obs_hab_code_cite, self.obs_hab_nom_retenu,
                                                            self.obs_hab_code_retenu))
        self.habitat_recherche.lineEdit().returnPressed.connect(
            lambda: self.data_manager.habitat_auto_complete(self.habitat_recherche, self.catalogue_phyto, self.habref,
                                                            self.typo_habitat_buttons, QLineEdit(), QLineEdit(),
                                                            QLineEdit(), QLineEdit(), self.survey_feature,
                                                            self.obs_habitats, self.table_syn_habitats))
        self.typo_habitat_buttons.buttonClicked.connect(lambda: self.habitat_recherche.clear())
        self.syntaxon_code_cite.textChanged.connect(lambda: self.habitat_recherche.clear())
        WidgetService.clickable(self.habitat_recherche).connect(
            lambda: self.data_manager.related_habitat_auto_complete(self.habitat_recherche, self.syntaxon_code_cite,
                                                                    self.typo_habitat_buttons, self.survey_feature,
                                                                    self.obs_habitats, self.table_syn_habitats))
        self.syntaxon_recherche.lineEdit().returnPressed.connect(
            lambda: self.data_manager.syntaxon_auto_complete(self.syntaxon_recherche, self.catalogue_phyto,
                                                             self.syntaxon_cite, self.syntaxon_code_cite,
                                                             self.syntaxon_retenu, self.syntaxon_code_retenu))
        WidgetService.clickable(self.syntaxon_delete).connect(
            lambda: self.data_manager.delete_syntaxon(self.syntaxon_cite, self.syntaxon_code_cite,
                                                      self.syntaxon_retenu, self.syntaxon_code_retenu))
        self.syntaxon_comm_non_sat_id.currentIndexChanged.connect(
            lambda i: self.data_manager.remove_dominant_taxon(i, self.syntaxon_comm_non_sat_obs,
                                                              self.taxon_dominant_code, self.taxon_dominant_nom,
                                                              self.obs_hab_taxon_dominant_code,
                                                              self.obs_hab_taxon_dominant_nom))
        self.syntaxon_comm_non_sat_obs.currentIndexChanged.connect(
            lambda i: self.data_manager.remove_dominant_taxon(i, self.syntaxon_comm_non_sat_id,
                                                              self.taxon_dominant_code, self.taxon_dominant_nom,
                                                              self.obs_hab_taxon_dominant_code,
                                                              self.obs_hab_taxon_dominant_nom))
        self.jdd_list.lineEdit().returnPressed.connect(lambda: self.data_manager.search_jdd(self.jdd_list))
        self.select_facteur.lineEdit().returnPressed.connect(
            lambda: self.data_manager.search_factor(self.select_facteur))
        self.pres_taches_redox.stateChanged.connect(
            lambda: WidgetService.check_hydromorphic_checkbox(self.pres_taches_redox, self.taches_redox))
        self.pres_horizon_redox.stateChanged.connect(
            lambda: WidgetService.check_hydromorphic_checkbox(self.pres_horizon_redox, self.horizon_redox))
        self.pres_horizon_reduct.stateChanged.connect(
            lambda: WidgetService.check_hydromorphic_checkbox(self.pres_horizon_reduct, self.horizon_reduct))
        WidgetService.closable(self.form.parent()).connect(
            lambda: self.data_manager.save_survey(self.releves, self.survey_feature, self.obs_habitats))

    def prepare_startup_interface(self, add_survey_to_syntaxon_widgets):
        widgets_to_hide = [self.save_taxon, self.save_syntaxon, self.add_related_habitats, self.related_habitats_list,
                           self.syntaxon_code_cite, self.syntaxon_code_retenu, self.taxon_cd_nom_cite,
                           self.taxon_cd_nom_valide, self.rel_create_new_point_rel, self.rel_create_new_line_rel,
                           self.rel_create_new_polygon_rel, self.obs_hab_code_typo, self.obs_hab_code_cite,
                           self.obs_hab_code_retenu, self.diag_originale, self.obs_hab_taxon_dominant_code,
                           self.rel_date_saisie]
        widgets_to_hide.extend(add_survey_to_syntaxon_widgets)
        for widget in widgets_to_hide:
            widget.hide()

        self.frel.hide()
        self.tab_widget.setCurrentIndex(0)
        self.form.hideButtonBox()
        if self.rel_type_rel_id.itemData(self.rel_type_rel_id.currentIndex()) != 0 and self.rel_protocole_id.itemData(
                self.rel_protocole_id.currentIndex()) != 0:
            self.activate.hide()
            self.fgen.hide()

        for widget, form_input in [(self.pres_taches_redox, self.taches_redox),
                                   (self.pres_horizon_redox, self.horizon_redox),
                                   (self.pres_horizon_reduct, self.horizon_reduct)]:
            WidgetService.check_hydromorphic_checkbox(widget, form_input)
        WidgetService.fshow(self.rel_type_rel_id, self.rel_protocole_id, self.frel, self.fobs, self.save_rel,
                            self.activate)
        WidgetService.fobs_show(self.tab_widget, self.rel_type_rel_id, self.rel_protocole_id, self.fobs_taxons,
                                self.fobs_syntaxons, self.save_rel, self.obs_hab_surface, self.obs_hab_surface_label)


class MouseEventManager(QObject):
    clicked = pyqtSignal()

    def __init__(self, widget: QObject):
        super(MouseEventManager, self).__init__(widget)
        self.widget = widget

    def eventFilter(self, obj, event) -> bool:
        if obj == self.widget:
            if event.type() == QEvent.MouseButtonRelease:
                if obj.rect().contains(event.pos()):
                    self.clicked.emit()
                    return True
        return False


class ComboBox(QComboBox):
    out = pyqtSignal()

    def hidePopup(self) -> None:
        self.out.emit()


class LineEdit(QLineEdit):
    out = pyqtSignal()

    def focusOutEvent(self, a0: QFocusEvent) -> None:
        self.out.emit()


class ClickableLabel(QLabel):
    clicked = pyqtSignal()

    def __init__(self, widget: QLabel):
        super().__init__(widget)

    def mouseReleaseEvent(self, ev: QMouseEvent) -> None:
        self.clicked.emit()


class WidgetService:
    @staticmethod
    def load_combo_box(combo_box: QComboBox, values: List[dict]):
        combo_box.clear()
        for value in values:
            if value['value']:
                combo_box.addItem(value['value'], value['id'])
            else:
                combo_box.addItem('', value['id'])

    @staticmethod
    def clickable(widget: QObject) -> pyqtSignal():
        mouse_event = MouseEventManager(widget)
        widget.installEventFilter(mouse_event)
        return mouse_event.clicked

    @staticmethod
    def closable(form: QgsAttributeForm) -> pyqtSignal():
        close_event = CloseEventManager(form)
        form.installEventFilter(close_event)
        return close_event.closed

    @staticmethod
    def build_table(table: QTableWidget, header_labels: List):
        table.setEditTriggers(QTableWidget.NoEditTriggers)
        table.setColumnCount(len(header_labels))
        table.setHorizontalHeaderLabels(header_labels)

        header = table.horizontalHeader()
        for i in range(len(header_labels)):
            if i != 1:
                header.setSectionResizeMode(i, QHeaderView.ResizeToContents)
            else:
                header.setSectionResizeMode(i, QHeaderView.Stretch)

    @staticmethod
    def find_widget(form, widget_type, widget_name) -> QWidget:
        if widget_type == 'QComboBox':
            return form.findChild(QComboBox, widget_name)
        elif widget_type == 'QLineEdit':
            return form.findChild(QLineEdit, widget_name)
        elif widget_type == 'QTextEdit':
            return form.findChild(QTextEdit, widget_name)
        elif widget_type == 'QPlainTextEdit':
            return form.findChild(QPlainTextEdit, widget_name)
        elif widget_type == 'QCheckBox':
            return form.findChild(QCheckBox, widget_name)
        elif widget_type == 'QTabWidget':
            return form.findChild(QTabWidget, widget_name)
        elif widget_type == 'QFrame':
            return form.findChild(QFrame, widget_name)
        return QWidget()

    @staticmethod
    def get_input_value(widget):
        if isinstance(widget, QLineEdit):
            return widget.text()
        elif isinstance(widget, QTextEdit):
            return widget.toPlainText()
        elif isinstance(widget, QPlainTextEdit):
            return widget.toPlainText()
        elif isinstance(widget, QCheckBox):
            state = widget.checkState()
            if state == Qt.Unchecked:
                return -1
            elif state == Qt.PartiallyChecked:
                return NULL
            elif state == Qt.Checked:
                return 1
            return 0
        elif isinstance(widget, QComboBox):
            return widget.currentData()

    @staticmethod
    def set_input_value(widget, value):  # Pas d'attribution de valeur si value est NULL (QVariant)
        if isinstance(widget, QLineEdit) and not isinstance(value, QVariant):
            widget.setText(str(value))
        elif isinstance(widget, QTextEdit) and not isinstance(value, QVariant):
            widget.setPlainText(str(value))
        elif isinstance(widget, QPlainTextEdit) and not isinstance(value, QVariant):
            widget.setPlainText(str(value))
        elif isinstance(widget, QCheckBox):
            if value == 1:
                widget.setCheckState(Qt.Checked)
            elif value == 0 or value == NULL or value is None:
                widget.setCheckState(Qt.PartiallyChecked)
            elif value == -1:
                widget.setCheckState(Qt.Unchecked)
        elif isinstance(widget, QComboBox) and not isinstance(value, QVariant):
            widget.setCurrentIndex(widget.findData(value))

    @staticmethod
    def cell_double_clicked(i, survey_layer: QgsVectorLayer, survey_feature: QgsFeature, observations: QgsVectorLayer,
                            form: QgsAttributeForm, add_widget: QLabel, save_widget: QLabel, select_widget: QComboBox,
                            typo_buttons: QButtonGroup, add_survey_widgets: List[QLabel] = None,
                            add_related_habitats_widget: QLabel = None):
        table: QTableWidget = i.tableWidget()
        table.itemDoubleClicked.disconnect()
        table_name = table.objectName()
        col_name = table.horizontalHeaderItem(i.column()).text()
        row = i.row()

        if survey_feature.id() < 0:  # Lorsqu'un feature vient d'être créé, son identifiant est négatif s'il n'a pas été rechargé
            survey_feature = WidgetService.refresh_survey_feature(survey_feature, survey_layer)

        if col_name == 'suppr.':
            if add_widget.isVisible() or table_name == 'table_strates_struct' or table_name == 'table_facteurs':
                WidgetService.delete_observation(observations, row, survey_feature, table, table_name)
            if table_name == 'table_syn_habitats':
                WidgetService.delete_typo_habitat(observations, row, survey_feature, table)
            WidgetService.reconnect_cell_double_click(table, survey_layer, survey_feature, observations, form,
                                                      add_widget,
                                                      save_widget, select_widget, typo_buttons, add_survey_widgets)
            return

        add_survey_widgets = [] if add_survey_widgets is None else add_survey_widgets
        if table_name == 'table_habitats':
            TableWidgetService.edit_syntaxon(survey_feature, row, observations, table, form, add_widget, save_widget,
                                             add_survey_widgets, select_widget, typo_buttons,
                                             add_related_habitats_widget)
        elif table_name == 'table_taxons':
            TableWidgetService.edit_taxon(survey_feature, row, observations, table, form, add_widget, save_widget,
                                          add_survey_widgets, select_widget, typo_buttons)
        WidgetService.reconnect_cell_double_click(table, survey_layer, survey_feature, observations, form, add_widget,
                                                  save_widget, select_widget, typo_buttons, add_survey_widgets,
                                                  add_related_habitats_widget)

    @staticmethod
    def reconnect_cell_double_click(table, survey_layer, survey_feature, observations, form, add_widget, save_widget,
                                    select_widget, typo_buttons, add_survey_widgets,
                                    add_related_habitats_widget: QLabel = None):
        table.itemDoubleClicked.connect(
            lambda item: WidgetService.cell_double_clicked(item, survey_layer, survey_feature, observations, form,
                                                           add_widget, save_widget, select_widget, typo_buttons,
                                                           add_survey_widgets, add_related_habitats_widget))

    @staticmethod
    def cell_clicked(item_clicked: QTableWidgetItem, survey_layer: QgsVectorLayer, survey_feature: QgsFeature,
                     observations: QgsVectorLayer):
        table_widget = item_clicked.tableWidget()
        table_widget.itemClicked.disconnect()
        table_name = table_widget.objectName()
        col_name = table_widget.horizontalHeaderItem(item_clicked.column()).text()

        if survey_feature.id() < 0:
            survey_feature = WidgetService.refresh_survey_feature(survey_feature, survey_layer)
        WidgetService.process_click(item_clicked, table_widget, survey_feature, observations, table_name, col_name)

        table_widget.itemClicked.connect(
            lambda i: WidgetService.cell_clicked(i, survey_layer, survey_feature, observations))

    @staticmethod
    def refresh_survey_feature(survey_feature: QgsFeature, survey_layer: QgsVectorLayer) -> QgsFeature:
        data_manager = DataManager()
        survey_feature_id = data_manager.get_survey_id_from_geometry(survey_feature)
        survey_feature = survey_layer.getFeature(survey_feature_id)
        return survey_feature

    @staticmethod
    def process_click(item_clicked: QTableWidgetItem, table_widget: QTableWidget, survey_feature: QgsFeature,
                      observations: QgsVectorLayer, table_name: str, col_name: str):
        if table_name == 'table_taxons' and col_name.startswith('strate_'):
            strate_values = ListValuesProvider().get_values_from_list_name('COEF_AD_VALUES')
            TableWidgetService.open_list_values_widget(survey_feature, observations, table_widget, item_clicked,
                                                       strate_values, 'cd_nom', 'nom_cite', 'obs_rel_id')
        if table_name == 'table_habitats':
            if col_name == 'coef. rec.':
                list_name = 'RECOUVREMENT_VALUES'
            elif col_name == 'forme':
                list_name = 'TYPE_FORME_VALUES'
            elif col_name == 'typ. struct.':
                list_name = 'TYPICITE_VALUES'
            elif col_name == 'typ. flor.':
                list_name = 'TYPICITE_VALUES'
            elif col_name == '% rec.':
                list_name = None
            else:
                return
            if list_name is not None:
                values = ListValuesProvider().get_values_from_list_name(list_name)
                TableWidgetService.open_list_values_widget(survey_feature, observations, table_widget, item_clicked,
                                                           values, 'obs_hab_code_cite', 'obs_hab_nom_cite',
                                                           'obs_hab_rel_id')
            else:
                TableWidgetService.edit_item(survey_feature, observations, table_widget, item_clicked,
                                             'obs_hab_code_cite', 'obs_hab_nom_cite', 'obs_hab_rel_id')

    @staticmethod
    def delete_observation(observations, row, survey_feature, table, table_name):
        if table_name == 'table_habitats':
            code_cell_value = table.item(row, 0).text() if table.item(row, 0) else None
            obs_habitat = TableWidgetService.find_observation(code_cell_value, table.item(row, 1).text(),
                                                              'obs_hab_code_cite', 'obs_hab_nom_cite',
                                                              'obs_hab_rel_id', observations, survey_feature)
            TableWidgetService.del_row(survey_feature, row, observations, table, 'obs_hab_rel_id',
                                       'obs_hab_code_cite', 'obs_hab_nom_cite', code_cell_value,
                                       table.item(row, 1).text())
            if obs_habitat:
                obs_habitat_id = int(obs_habitat['obs_hab_id'])
                sqlite_data_manager = DataManager()
                sqlite_data_manager.remove_links_by_obs_hab_id(obs_habitat_id)
                if obs_habitat['obs_hab_code_typo'] == 0 and obs_habitat['obs_hab_rel_assoc_id'] is not None:
                    sqlite_data_manager.clean_obs_hab_link_in_assoc_survey(obs_habitat['obs_hab_rel_assoc_id'])
                    sqlite_data_manager.clean_other_obs_hab_link_to_assoc_survey(obs_habitat['obs_hab_rel_assoc_id'])

        elif table_name == 'table_taxons':
            code_cell_value = int(table.item(row, 0).text()) \
                if table.item(row, 0) and table.item(row, 0).text() != '' else None
            TableWidgetService.del_row(survey_feature, row, observations, table, 'obs_rel_id', 'cd_nom', 'nom_cite',
                                       code_cell_value, table.item(row, 1).text())
        elif table_name == 'table_strates_struct':
            code_cell_value = int(table.item(row, 0).text()) \
                if table.item(row, 0) and table.item(row, 0).text() != '' else None
            TableWidgetService.del_row(survey_feature, row, observations, table, 'rel_id', 'strate_id', 'strate_id',
                                       code_cell_value, code_cell_value)
        elif table_name == 'table_facteurs':
            code_cell_value = int(table.item(row, 0).text()) \
                if table.item(row, 0) and table.item(row, 0).text() != '' else None
            TableWidgetService.del_row(survey_feature, row, observations, table, 'rel_id', 'facteur_id', 'facteur_id',
                                       code_cell_value, code_cell_value)

    @staticmethod
    def delete_typo_habitat(observations, row, survey_feature, table):
        code_cell_value = table.item(row, 0).text() if table.item(row, 0) else None
        obs_habitat = TableWidgetService.find_observation(code_cell_value, table.item(row, 1).text(),
                                                          'obs_hab_code_cite', 'obs_hab_nom_cite',
                                                          'obs_hab_rel_assoc_id', observations, survey_feature)
        if obs_habitat:
            sqlite_data_manager = DataManager()
            survey_id = survey_feature['id']
            obs_syntaxon_id: int
            if survey_id is None:
                survey_id = sqlite_data_manager.get_survey_id_from_geometry(survey_feature)
                obs_syntaxon_id = sqlite_data_manager.get_obs_syntaxon_id_from_survey_id(survey_id)
            else:
                obs_syntaxon_id = int(survey_feature['rel_obs_syntaxon_id'])
            obs_habitat_id = int(obs_habitat['obs_hab_id'])
            if sqlite_data_manager.is_obs_hab_linked_to_other(obs_habitat_id, obs_syntaxon_id):
                sqlite_data_manager.remove_links_between_obs_hab(obs_habitat_id, obs_syntaxon_id)
                sqlite_data_manager.clean_link_to_assoc_survey(obs_habitat_id)
                table.removeRow(row)
            else:
                sqlite_data_manager.remove_links_by_obs_hab_id(obs_habitat_id)
                TableWidgetService.del_row(survey_feature, row, observations, table, 'obs_hab_rel_assoc_id',
                                           'obs_hab_code_cite', 'obs_hab_nom_cite', code_cell_value,
                                           table.item(row, 1).text())
        else:
            iface.messageBar().pushWarning('Suppression d\'observation habitat', 'Vous ne pouvez pas supprimer une '
                                                                                 'observation qui n\'a pas ete creee '
                                                                                 'dans ce releve.')

    @staticmethod
    def check_hydromorphic_checkbox(checkbox: QCheckBox, linked_input: QLineEdit):
        state = checkbox.checkState()
        if state == Qt.Unchecked or state == Qt.PartiallyChecked:
            linked_input.clear()
            linked_input.setEnabled(False)
        else:
            linked_input.setEnabled(True)

    @staticmethod
    def fshow(rel_type_widget: QComboBox, rel_protocol_widget: QComboBox, frel: QFrame, fobs: QFrame,
              save_rel: QLabel, activate: QLabel):
        if rel_type_widget.itemData(rel_type_widget.currentIndex()) != 0 and rel_protocol_widget.itemData(
                rel_protocol_widget.currentIndex()) != 0 and activate.isHidden():
            rel_type_widget.setEnabled(False)
            if frel.isHidden():
                frel.show()
                fobs.hide()
            else:
                frel.hide()
                fobs.show()
        else:
            frel.hide()
            fobs.hide()
            save_rel.hide()

    @staticmethod
    def fgen_show(fgen: QFrame):
        if fgen.isHidden():
            fgen.show()
        else:
            fgen.hide()

    @staticmethod
    def fobs_show(tab_widget: QTabWidget, rel_type_widget: QComboBox, rel_protocol_widget: QComboBox,
                  fobs_taxons: QFrame, fobs_syntaxons: QFrame, activate: QLabel, obs_hab_surface: QLineEdit,
                  obs_hab_surface_label: QLabel):
        current_data = rel_type_widget.currentData()
        current_protocol_data = rel_protocol_widget.currentData()
        WidgetService.load_protocol_list(current_data, rel_protocol_widget)

        if rel_protocol_widget.findData(current_protocol_data) > -1:
            rel_protocol_widget.setCurrentIndex(rel_protocol_widget.findData(current_protocol_data))
            rel_protocol_widget.removeItem(rel_protocol_widget.findData(0))

        if current_data == 1:
            fobs_taxons.show()
            fobs_syntaxons.hide()
            removed = 0
            for i in range(len(tab_widget)):
                if tab_widget.widget(i - removed).objectName() in ['tab_typo_syntaxon']:
                    tab_widget.removeTab(i - removed)
                    removed += 1
            obs_hab_surface.hide()
            obs_hab_surface_label.hide()
            activate.setEnabled(True)
        elif current_data == 2:
            fobs_taxons.hide()
            fobs_syntaxons.show()
            removed = 0
            for i in range(len(tab_widget)):
                if tab_widget.widget(i - removed).objectName() in ['tab_descr_vege', 'tab_structure', 'tab_typologie']:
                    tab_widget.removeTab(i - removed)
                    removed += 1
            activate.setEnabled(True)
        else:
            fobs_taxons.hide()
            fobs_syntaxons.hide()
            activate.setEnabled(False)

    @staticmethod
    def calendar(calendar_box, calendar_label, rel_date_min, rel_date_max, transform_function):
        if calendar_label.isEnabled():
            calendar_box.setWindowTitle("Calendrier")
            msglt = calendar_box.layout()
            qcal1 = QCalendarWidget()
            msglt.addWidget(qcal1)
            qcal1.clicked.connect(lambda date: transform_function(date, rel_date_min, rel_date_max, calendar_box))
            calendar_box.exec_()

    @staticmethod
    def transform_date_1(date, rel_date_min: QLineEdit, rel_date_max: QLineEdit, cal1: QMessageBox):
        rel_date_min.setText(str(date.toString('yyyy-MM-dd')))
        rel_date_max.setText(str(date.toString('yyyy-MM-dd')))
        cal1.close()

    @staticmethod
    def transform_date_2(date, rel_date_min: QLineEdit, rel_date_max: QLineEdit, cal2: QMessageBox):
        # TODO adapt date below...
        if date < QDate.fromString(rel_date_min.text(), 'yyyy-MM-dd'):
            rel_date_max.setStyleSheet("background : rgba(255, 1, 1, 80)")
        else:
            rel_date_max.setStyleSheet("background : white")
            rel_date_max.setText(str(date.toString('yyyy-MM-dd')))
            cal2.close()

    @staticmethod
    def load_protocol_list(rel_type_rel_id: int, rel_protocol_widget: QComboBox):
        list_values_provider = ListValuesProvider()
        protocole_values = list_values_provider.get_values_from_list_name('PROTOCOLE_VALUES')
        protocol_list = [{'id': protocol['id'], 'value': protocol['value']} for protocol in protocole_values
                         if protocol['order'] in (0, rel_type_rel_id)]
        WidgetService.load_combo_box(rel_protocol_widget, protocol_list)


class TableWidgetService:
    @staticmethod
    def edit_syntaxon(survey_feature: QgsFeature, row: int, observations: QgsVectorLayer, sheet: QTableWidget,
                      form: QgsAttributeForm, add_widget: QLabel, save_widget: QLabel, add_survey_widgets: List[QLabel],
                      select_widget: QComboBox, typo_buttons: QButtonGroup, add_related_habitats_widget: QLabel):
        code_phyto = sheet.item(row, 0).text() if sheet.item(row, 0) and sheet.item(row, 0).text() != '' else None
        TableWidgetService.edit_observation(survey_feature, observations, form, add_widget, save_widget,
                                            add_survey_widgets, select_widget, typo_buttons, 'obs_habitat_',
                                            'obs_hab_rel_id', 'obs_hab_code_cite', 'obs_hab_nom_cite', code_phyto,
                                            sheet.item(row, 1).text(), add_related_habitats_widget)

    @staticmethod
    def edit_taxon(survey_feature: QgsFeature, row: int, observations: QgsVectorLayer, sheet: QTableWidget,
                   form: QgsAttributeForm, add_widget: QLabel, save_widget: QLabel, add_survey_widgets: List[QLabel],
                   select_widget: QComboBox, typo_buttons: QButtonGroup):
        cd_nom = int(sheet.item(row, 0).text()) if sheet.item(row, 0) and sheet.item(row, 0).text() != '' else None
        TableWidgetService.edit_observation(survey_feature, observations, form, add_widget, save_widget,
                                            add_survey_widgets, select_widget, typo_buttons, 'nom_', 'obs_rel_id',
                                            'cd_nom', 'nom_cite', cd_nom, sheet.item(row, 1).text())

    @staticmethod
    def edit_observation(survey_feature: QgsFeature, observations: QgsVectorLayer, form: QgsAttributeForm,
                         add_widget: QLabel, save_widget: QLabel, add_survey_widgets: List[QLabel],
                         select_widget: QComboBox, typo_buttons: QButtonGroup, keyword: str, obs_rel_field_name: str,
                         obs_code_field_name: str, obs_name_field_name: str, id_to_compare: object,
                         text_to_compare: object, add_related_habitats_widget: QLabel = None):
        form_inputs = DatabaseSpecificationService.get_inputs_specification(observations.name())
        if len(form_inputs) == 0:
            return
        observation = TableWidgetService.find_observation(id_to_compare, text_to_compare, obs_code_field_name,
                                                          obs_name_field_name, obs_rel_field_name, observations,
                                                          survey_feature)
        if observation:
            code_typo = TableWidgetService.get_code_typo(typo_buttons)
            TableWidgetService.hydrate_form(form, form_inputs, keyword, observation)
            if observations.name() == 'obs_habitats':
                TableWidgetService.populate_obs_assoc_list(survey_feature, observation, observations, form,
                                                           typo_buttons)
                if code_typo == 0:
                    TableWidgetService.populate_related_habitats_list(survey_feature, observations, form, id_to_compare)

            for button in typo_buttons.buttons():
                button.setEnabled(False)
            select_widget.setEnabled(False)
            add_widget.hide()
            save_widget.show()

            if add_related_habitats_widget and code_typo == 0:
                add_related_habitats_widget.show()

            for widget in add_survey_widgets:
                widget.show()

    @staticmethod
    def hydrate_form(form: QgsAttributeForm, form_inputs: List[dict], keyword: str, observation: QgsFeature):
        for form_input in form_inputs:
            widget = WidgetService.find_widget(form, form_input['input_type'],
                                               form_input['input_name'].split(',')[0])
            if widget:
                TableWidgetService.clean_widget(widget)
                field_names = [form_input['field_name'] for form_input in form_inputs
                               if form_input['input_name'].split(',')[0] == widget.objectName()]
                if len(field_names) > 0 and observation:
                    WidgetService.set_input_value(widget, observation[field_names[0]])
                if keyword in widget.objectName():
                    widget.setEnabled(False)

    @staticmethod
    def populate_obs_assoc_list(survey_feature: QgsFeature, observation: QgsFeature, observations: QgsVectorLayer,
                                form: QgsAttributeForm, typo_buttons: QButtonGroup):
        obs_hab_list_widget: QListWidget = form.findChild(QListWidget, 'list_hab_obs_assoc')
        try:
            obs_hab_list_widget.itemChanged.disconnect()
        except:
            pass
        obs_hab_list_widget.clear()
        if obs_hab_list_widget is not None:
            code_typo = TableWidgetService.get_code_typo(typo_buttons)
            consulted_obs_hab_id = observation['obs_hab_id']
            obs_hab_list = [observation for observation in observations.getFeatures()
                            if observation['obs_hab_rel_id'] == survey_feature.id()
                            and observation['obs_hab_code_typo'] != code_typo]
            for obs_hab in obs_hab_list:
                row = obs_hab_list_widget.count()
                obs_hab_item = TableWidgetService.create_obs_hab_assoc_item(consulted_obs_hab_id, obs_hab)
                obs_hab_list_widget.insertItem(row, obs_hab_item)
            obs_hab_list_widget.sortItems(Qt.DescendingOrder)
            obs_hab_list_widget.itemChanged.connect(
                lambda item: TableWidgetService.manage_association_between_obs_hab(item, obs_hab_list_widget,
                                                                                   observation, observations))

    @staticmethod
    def create_obs_hab_assoc_item(consulted_obs_hab_id: int, obs_hab: QgsFeature):
        sqlite_data_manager = DataManager()
        obs_hab_typo = obs_hab['obs_hab_code_typo']
        obs_hab_id = obs_hab['obs_hab_id']
        label = TableWidgetService.get_typo_label_from_code(obs_hab_typo) + ' - '
        label += str(obs_hab['obs_hab_code_cite']) + ' - ' + obs_hab['obs_hab_nom_cite']
        rec_list: List[str] = []
        if obs_hab['obs_hab_rec_pct'] not in (0, None, NULL):
            rec_list.append(str(obs_hab['obs_hab_rec_pct']) + '%')
        if obs_hab['obs_hab_recouvrement'] not in (0, None, NULL):
            list_values_provider = ListValuesProvider()
            recouvrement_values = list_values_provider.get_values_from_list_name('RECOUVREMENT_VALUES')
            rec_list.append('coef. ' + list_values_provider.get_value_from_list(
                recouvrement_values, obs_hab['obs_hab_recouvrement']))
        label += ' (recouvrement : ' + ', '.join(rec_list) + ')' if len(rec_list) > 0 else ''
        is_linked = sqlite_data_manager.are_obs_hab_linked(obs_hab_id, consulted_obs_hab_id)
        obs_hab_item = QListWidgetItem()
        obs_hab_item.setText(label)
        obs_hab_item.setData(Qt.UserRole, obs_hab_id)
        obs_hab_item.setBackground(TableWidgetService.get_typo_color_from_code(obs_hab_typo))
        obs_hab_item.setCheckState(Qt.Checked if is_linked else Qt.Unchecked)
        return obs_hab_item

    @staticmethod
    def manage_association_between_obs_hab(list_item: QListWidgetItem, obs_hab_list_widget: QListWidget,
                                           obs_hab: QgsFeature, observations: QgsVectorLayer):
        obs_hab_list_widget.itemChanged.disconnect()
        obs_hab_id = obs_hab['obs_hab_id']
        obs_hab_id_to_link = list_item.data(Qt.UserRole)
        obs_hab_to_link = observations.getFeature(obs_hab_id_to_link)
        sqlite_data_manager = DataManager()
        if list_item.checkState() == 2:
            sqlite_data_manager.insert_links_between_obs_hab(obs_hab_id, obs_hab_id_to_link)
        else:
            sqlite_data_manager.remove_links_between_obs_hab(obs_hab_id, obs_hab_id_to_link)
            if obs_hab['obs_hab_code_typo'] == 0:
                observations.startEditing()
                obs_hab_to_link['obs_hab_rel_assoc_id'] = None
                observations.updateFeature(obs_hab_to_link)
                observations.commitChanges()
            elif obs_hab_to_link['obs_hab_code_typo'] == 0:
                observations.startEditing()
                obs_hab['obs_hab_rel_assoc_id'] = None
                observations.updateFeature(obs_hab)
                observations.commitChanges()
        obs_hab_list_widget.itemChanged.connect(
            lambda item: TableWidgetService.manage_association_between_obs_hab(item, obs_hab_list_widget,
                                                                               obs_hab, observations))

    @staticmethod
    def populate_obs_assoc_consult_list(survey_feature_id: int, observations: QgsVectorLayer,
                                        obs_hab_assoc_list: QListWidget, typo_buttons: QButtonGroup):
        try:
            obs_hab_assoc_list.itemChanged.disconnect()
        except:
            pass
        obs_hab_assoc_list.clear()
        code_typo = TableWidgetService.get_code_typo(typo_buttons)
        obs_hab_list = [observation for observation in observations.getFeatures()
                        if observation['obs_hab_rel_id'] == survey_feature_id
                        and observation['obs_hab_code_typo'] != code_typo]
        for obs_hab in obs_hab_list:
            row = obs_hab_assoc_list.count()
            obs_hab_item = TableWidgetService.create_obs_hab_assoc_item(0, obs_hab)
            obs_hab_item.setFlags(Qt.NoItemFlags)
            obs_hab_item.setForeground(QBrush(QColor(50, 50, 50)))
            obs_hab_assoc_list.insertItem(row, obs_hab_item)
        obs_hab_assoc_list.sortItems(Qt.DescendingOrder)

    @staticmethod
    def populate_related_habitats_list(survey_feature: QgsFeature, observations: QgsVectorLayer,
                                       form: QgsAttributeForm, code_phyto: object):
        related_habitats_list_widget: QListWidget = form.findChild(QListWidget, 'list_related_habitats')
        try:
            related_habitats_list_widget.itemChanged.disconnect()
        except:
            pass
        related_habitats_list_widget.clear()
        if related_habitats_list_widget is not None:
            sqlite_data_manager = DataManager()
            related_habitats_list = sqlite_data_manager.get_related_habitats_list_from_code_phyto(code_phyto)
            if len(related_habitats_list) > 0:
                obs_hab_list = [str(obs_hab['obs_hab_code_typo']) + ' - ' + obs_hab['obs_hab_code_cite']
                                for obs_hab in observations.getFeatures()
                                if obs_hab['obs_hab_rel_id'] == survey_feature.id()
                                and obs_hab['obs_hab_code_typo'] != 0]
                for hab in related_habitats_list:
                    row = related_habitats_list_widget.count()
                    hab_item = TableWidgetService.create_related_hab_item(hab, obs_hab_list)
                    related_habitats_list_widget.insertItem(row, hab_item)
                related_habitats_list_widget.sortItems(Qt.DescendingOrder)

    @staticmethod
    def create_related_hab_item(hab, obs_hab_list: List[str]):
        hab_code_typo = hab['cd_typo']
        label = TableWidgetService.get_typo_label_from_code(hab_code_typo) + ' - '
        label += str(hab['lb_code']) + ' - ' + hab['lb_hab_fr']
        hab_item = QListWidgetItem()
        hab_item.setText(label)
        hab_item.setData(Qt.UserRole, hab['cd_hab'])
        hab_item.setBackground(TableWidgetService.get_typo_color_from_code(hab_code_typo))
        if str(hab['cd_typo']) + ' - ' + hab['lb_code'] not in obs_hab_list:
            hab_item.setCheckState(Qt.Unchecked)
        return hab_item

    @staticmethod
    def del_row(survey_feature: QgsFeature, row: int, observations: QgsVectorLayer, table: QTableWidget,
                obs_rel_field_name: str, obs_code_field_name: str, obs_name_field_name: str, id_to_compare: object,
                text_to_compare: object):
        observation = TableWidgetService.find_observation(id_to_compare, text_to_compare, obs_code_field_name,
                                                          obs_name_field_name, obs_rel_field_name, observations,
                                                          survey_feature)
        if observation:
            observations.deleteFeature(observation.id())
            observations.commitChanges()
            table.removeRow(row)

    @staticmethod
    def open_list_values_widget(survey_feature: QgsFeature, observations: QgsVectorLayer, table_widget: QTableWidget,
                                item_clicked: QTableWidgetItem, values: List[dict], obs_code_field_name: str,
                                obs_name_field_name: str, obs_rel_field_name: str):
        list_widget: ComboBox = ComboBox()
        item_model = QStandardItemModel()
        for value in values:
            item = QStandardItem()
            item.setSizeHint(QSize(18, 18))
            item.setText(value['value'])
            item.setData(value['id'])
            item_model.appendRow(item)
        list_widget.setModel(item_model)
        table_widget.setCellWidget(item_clicked.row(), item_clicked.column(), list_widget)
        list_widget.showPopup()
        out_signal = list_widget.out
        list_widget.view().pressed.connect(
            lambda choice: TableWidgetService.update_observation_value(survey_feature, observations, table_widget,
                                                                       item_clicked, list_widget, out_signal,
                                                                       values, choice, obs_code_field_name,
                                                                       obs_name_field_name, obs_rel_field_name))
        out_signal.connect(
            lambda: TableWidgetService.rewind_observation_value(survey_feature, observations, table_widget,
                                                                item_clicked, list_widget, out_signal, values,
                                                                obs_code_field_name, obs_name_field_name,
                                                                obs_rel_field_name))

    @staticmethod
    def edit_item(survey_feature: QgsFeature, observations: QgsVectorLayer, table_widget: QTableWidget,
                  item_clicked: QTableWidgetItem, obs_code_field_name: str, obs_name_field_name: str,
                  obs_rel_field_name: str):
        edit_widget: LineEdit = LineEdit()
        table_widget_name = table_widget.objectName()
        item_clicked_code, item_clicked_name = TableWidgetService.get_item_clicked_code_and_name(item_clicked,
                                                                                                 table_widget,
                                                                                                 table_widget_name)
        observation = TableWidgetService.find_observation(item_clicked_code, item_clicked_name, obs_code_field_name,
                                                          obs_name_field_name, obs_rel_field_name, observations,
                                                          survey_feature)
        edit_widget.setText(str(observation['obs_hab_rec_pct']) if observation['obs_hab_rec_pct'] != NULL else '')
        table_widget.setCellWidget(item_clicked.row(), item_clicked.column(), edit_widget)
        edit_widget.setFocus()
        out_signal = edit_widget.out
        edit_widget.returnPressed.connect(
            lambda: TableWidgetService.update_observation_value(survey_feature, observations, table_widget,
                                                                item_clicked, edit_widget, out_signal, [], None,
                                                                obs_code_field_name, obs_name_field_name,
                                                                obs_rel_field_name))
        out_signal.connect(
            lambda: TableWidgetService.rewind_observation_value(survey_feature, observations, table_widget,
                                                                item_clicked, edit_widget, out_signal, [],
                                                                obs_code_field_name, obs_name_field_name,
                                                                obs_rel_field_name))

    @staticmethod
    def update_observation_value(survey_feature: QgsFeature, observations: QgsVectorLayer,
                                 table_widget: QTableWidget, item_clicked: QTableWidgetItem, widget: QWidget,
                                 out_signal, values: List[dict], choice: QModelIndex, obs_code_field_name: str,
                                 obs_name_field_name: str, obs_rel_field_name: str):
        observation, table_widget_name = TableWidgetService.get_observation_id(item_clicked, obs_code_field_name,
                                                                               obs_name_field_name,
                                                                               obs_rel_field_name, observations,
                                                                               out_signal, survey_feature,
                                                                               table_widget, widget)
        column_label, value_to_store = TableWidgetService.get_field_and_value(choice, item_clicked, table_widget,
                                                                              table_widget_name, values, widget)
        if observation is not None and column_label != '' and (value_to_store is None or value_to_store > -1):
            observation[column_label] = value_to_store if value_to_store is not None else NULL
            observations.updateFeature(observation)
            observations.commitChanges()
            table_widget.removeCellWidget(item_clicked.row(), item_clicked.column())
            value_to_store = '' if value_to_store is None else value_to_store
            table_widget.setItem(item_clicked.row(), item_clicked.column(), QTableWidgetItem(
                str(value_to_store) if choice is None else choice.data(), 0))

    @staticmethod
    def rewind_observation_value(survey_feature: QgsFeature, observations: QgsVectorLayer,
                                 table_widget: QTableWidget, item_clicked: QTableWidgetItem, widget: QWidget,
                                 out_signal, values: List[dict], obs_code_field_name: str, obs_name_field_name: str,
                                 obs_rel_field_name: str):
        observation, table_widget_name = TableWidgetService.get_observation_id(item_clicked, obs_code_field_name,
                                                                               obs_name_field_name,
                                                                               obs_rel_field_name, observations,
                                                                               out_signal, survey_feature,
                                                                               table_widget, widget)
        column_header, column_label = '', ''
        if table_widget_name == 'table_taxons':
            column_label = table_widget.horizontalHeaderItem(item_clicked.column()).text()
        elif table_widget_name == 'table_habitats':
            column_header = table_widget.horizontalHeaderItem(item_clicked.column()).text()
            if column_header == 'forme':
                column_label = 'obs_hab_forme_id'
            elif column_header == 'coef. rec.':
                column_label = 'obs_hab_recouvrement'
            elif column_header == 'typ. struct.':
                column_label = 'syntaxon_typicite_struct_id'
            elif column_header == 'typ. flor.':
                column_label = 'syntaxon_typicite_flor_id'
            else:
                column_label = 'obs_hab_rec_pct'
        if observation is not None and column_label != '':
            table_widget.removeCellWidget(item_clicked.row(), item_clicked.column())
            value = observation[column_label]
            if column_header == '% rec.':
                value_to_display = value if value not in (0, None, NULL) else ''
            else:
                value_to_display = ListValuesProvider().get_value_from_list(
                    values, observation[column_label]) if value not in (0, None, NULL) else ''
            table_widget.setItem(item_clicked.row(), item_clicked.column(), QTableWidgetItem(str(value_to_display), 0))

    @staticmethod
    def get_observation_id(item_clicked, obs_code_field_name, obs_name_field_name, obs_rel_field_name, observations,
                           out_signal, survey_feature, table_widget, widget):
        if isinstance(widget, QComboBox):
            widget.view().pressed.disconnect()
        elif isinstance(widget, QLineEdit):
            widget.returnPressed.disconnect()
        out_signal.disconnect()
        table_widget_name = table_widget.objectName()
        item_clicked_code, item_clicked_name = TableWidgetService.get_item_clicked_code_and_name(item_clicked,
                                                                                                 table_widget,
                                                                                                 table_widget_name)
        observation = TableWidgetService.find_observation(item_clicked_code, item_clicked_name, obs_code_field_name,
                                                          obs_name_field_name, obs_rel_field_name, observations,
                                                          survey_feature)
        return observation, table_widget_name

    @staticmethod
    def get_item_clicked_code_and_name(item_clicked, table_widget, table_widget_name):
        item = table_widget.item(item_clicked.row(), 0)
        item_clicked_code = item.text() if item and item.text() != '' else None
        if table_widget_name == 'table_taxons' and item_clicked_code is not None:
            item_clicked_code = int(item_clicked_code)
        item_clicked_name = table_widget.item(item_clicked.row(), 1).text()
        return item_clicked_code, item_clicked_name

    @staticmethod
    def get_field_and_value(choice, item_clicked, table_widget, table_widget_name, values, widget):
        column_label, value_to_store = '', -1
        if table_widget_name == 'table_taxons':
            column_label = table_widget.horizontalHeaderItem(item_clicked.column()).text()
            value_to_store = ListValuesProvider().get_id_from_value(values, choice.data())
        elif table_widget_name == 'table_habitats':
            column_header = table_widget.horizontalHeaderItem(item_clicked.column()).text()
            if column_header == 'forme':
                column_label = 'obs_hab_forme_id'
            elif column_header == 'coef. rec.':
                column_label = 'obs_hab_recouvrement'
            elif column_header == 'typ. struct.':
                column_label = 'syntaxon_typicite_struct_id'
            elif column_header == 'typ. flor.':
                column_label = 'syntaxon_typicite_flor_id'
            else:
                column_label = 'obs_hab_rec_pct'
            if column_header == '% rec.':
                value_to_store = int(widget.text()) if widget.text() != '' else None
            else:
                value_to_store = ListValuesProvider().get_id_from_value(values, choice.data())
        return column_label, value_to_store

    @staticmethod
    def find_observation(id_to_compare, text_to_compare, obs_code_field_name, obs_name_field_name, obs_rel_field_name,
                         observations, survey_feature) -> Optional[QgsFeature]:
        survey_id = survey_feature['id']
        if survey_id is None or survey_id < 0:
            sqlite_data_manager = DataManager()
            survey_id = sqlite_data_manager.get_survey_id_from_geometry(survey_feature)
        id_to_compare = None if id_to_compare == '' else id_to_compare
        observation_result: List[QgsFeature] = [observation for observation in observations.getFeatures()
                                                if observation[obs_rel_field_name] == survey_id
                                                and observation[obs_code_field_name] == id_to_compare
                                                and observation[obs_name_field_name] == text_to_compare]
        if len(observation_result) == 0:
            return None
        else:
            if not observations.isEditable():
                observations.startEditing()
            return observation_result[0]

    @staticmethod
    def clean_widget(widget):
        if isinstance(widget, QLineEdit):
            widget.setText("")
        elif isinstance(widget, QPlainTextEdit) or isinstance(widget, QTextEdit):
            widget.setPlainText("")
        elif isinstance(widget, QComboBox):
            widget.setCurrentIndex(0)
        elif isinstance(widget, QCheckBox):
            widget.setCheckState(Qt.Unchecked)

    @staticmethod
    def get_code_typo(buttons: QButtonGroup) -> int:
        button_name = buttons.checkedButton().objectName()
        if button_name == "rb_phyto":
            return 0
        elif button_name == "rb_hic" or button_name == "rb_typo_hic":
            return 4
        elif button_name == "rb_eunis" or button_name == "rb_typo_eunis":
            return 7
        elif button_name == "rb_cb" or button_name == "rb_typo_cb":
            return 22
        return 0

    @staticmethod
    def get_header_labels_from_typo(code_typo: int) -> List:
        if code_typo == 0:
            return SYNTAXONS_HEADER_LABELS
        elif code_typo == 4:
            return HIC_HEADER_LABELS
        elif code_typo == 7:
            return EUNIS_HEADER_LABELS
        elif code_typo == 22:
            return CORINE_HEADER_LABELS
        return []

    @staticmethod
    def get_typo_label_from_code(code_typo: int) -> str:
        if code_typo == 0:
            return "Phyto"
        elif code_typo == 4:
            return "HIC"
        elif code_typo == 7:
            return "EUNIS"
        elif code_typo == 22:
            return "Corine"
        return ""

    @staticmethod
    def get_typo_color_from_code(code_typo: int) -> QColor:
        if code_typo == 0:
            return QColor(Qt.white)
        elif code_typo == 4:
            return QColor(221, 153, 255)
        elif code_typo == 7:
            return QColor(255, 178, 127)
        elif code_typo == 22:
            return QColor(153, 255, 137)
        return QColor(Qt.white)

    @staticmethod
    def do_nothing():
        pass

    @staticmethod
    def add_new_survey(self):
        pass


import datetime
import sys

DEBUGMODE = True


def form_open(dialog, layer, feature):
    iface.messageBar().pushInfo('Habitats application', 'launched')

    if dialog.editable() or feature.id() > 0:
        project = QgsProject.instance()
        # TODO build or alter database through another way (plugin ?), properly not at form launch...
        database_builder: DatabaseBuilder = DatabaseBuilder(project)
        database_builder.build_or_alter()
        iface.messageBar().pushInfo('Database connection', 'database mounted and complete')

        form_manager: SurveyFormManager = SurveyFormManager(dialog, layer, feature)
        form_manager.initialize()
        iface.messageBar().pushInfo('Habitats form', 'on air')


if __name__ == '__main__':
    print("%s - Lobelia4QGis command line tool launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    if hasattr(sys, 'argv') and len(sys.argv) > 1:
        option = sys.argv[1]
        if option == 'clean':
            print("%s - clean process launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            database_cleaner = DatabaseCleaner()
            database_cleaner.clean()
            print("%s - clean process ended" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        elif option == 'extract_phyto':
            print(
                "%s - phyto table extraction process launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            extract_service = PhytoTableExtractService()
            extract_service.process()
            print("%s - phyto table extraction process ended" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        elif option == 'extract_obs_hab':
            print("%s - habitat observation extraction process launched" % datetime.datetime.now().strftime(
                "%Y-%m-%d %H:%M:%S"))
            extract_service = ObsHabExtractService()
            extract_service.process()
            print("%s - habitat observation extraction process ended" % datetime.datetime.now().strftime(
                "%Y-%m-%d %H:%M:%S"))
        elif option == 'extract_carto':
            print("%s - carto extraction process launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            extract_service = CartoExtractService()
            extract_service.process()
            print("%s - carto extraction process ended" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        else:
            print(
                "%s - unknown option : clean, extract_phyto, extract_carto available" % datetime.datetime.now().strftime(
                    "%Y-%m-%d %H:%M:%S"))
    else:
        print("# usage :", "python survey_form.py <option>")
        print("# options available : clean, extract_phyto, extract_obs_hab, extract_carto")
    print("%s - Lobelia4QGis command line tool closed" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
