import sqlite3
from pathlib import Path
from sqlite3 import Connection, Cursor, Error
from typing import Optional

from qgis.core import QgsApplication, QgsProject, QgsVectorFileWriter
from qgis.utils import iface


class PolygonsIngestService:
    def __init__(self):
        self.project: Optional[QgsProject] = None
        self.project_dir: Optional[Path] = None
        self.sqlite_conn: Optional[Connection] = None
        self.sqlite_cur: Optional[Cursor] = None

    def sqlite_connection(self, database_file_name) -> Optional[Connection]:
        try:
            self.sqlite_conn = sqlite3.connect(self.project_dir / database_file_name)
        except Error as e:
            message = 'Connection failed - ' + str(e)
            iface.messageBar().pushWarning('Database connection', message)
        finally:
            if self.sqlite_conn:
                self.sqlite_conn.enable_load_extension(True)
                self.sqlite_conn.load_extension("mod_spatialite")
                self.sqlite_conn.row_factory = sqlite3.Row
                self.sqlite_cur = self.sqlite_conn.cursor()
            return self.sqlite_conn

    def process(self, l4q_sqlite_file_name, carto_habitats_sqlite_file_name):
        self.project_dir = Path(self.project.homePath())

        # Export carto_habitats Shapefile to SQLite format
        carto_habitats_sqlite_file_path = self.project_dir / carto_habitats_sqlite_file_name
        layer = self.project.mapLayersByName("carto_habitats")[0]
        transform_context = self.project.transformContext()
        save_options = QgsVectorFileWriter.SaveVectorOptions()
        save_options.layerName = layer.name()
        save_options.driverName = 'SQLite'
        save_options.layerOptions = ['SPATIALITE=YES']
        error = QgsVectorFileWriter.writeAsVectorFormatV3(layer=layer,
                                                          fileName=str(carto_habitats_sqlite_file_path),
                                                          transformContext=transform_context,
                                                          options=save_options)
        if error[0] != QgsVectorFileWriter.NoError:
            print(error)
        # Attach the two SQLite databases
        self.sqlite_connection(l4q_sqlite_file_name)
        self.sqlite_cur.execute(f"ATTACH DATABASE '{str(carto_habitats_sqlite_file_path)}' AS carto_habitats_db")
        # Check on station_surface
        station_surface_entities = self.sqlite_cur.execute("SELECT * FROM station_surface ORDER BY id").fetchall()
        for entity in station_surface_entities:
            sql = f"""SELECT sf.id AS sf_id, sf.uuid AS sf_uuid, sf.geom AS sf_geom, ch.id AS polygon_id, ST_GeomFromWKB(ch.GEOMETRY, 2154) AS polygon_geom
            FROM station_surface sf
            JOIN carto_habitats_db.carto_habitats ch ON ST_Intersects(ST_GeomFromWKB(ch.GEOMETRY, 2154), sf.geom)
            WHERE sf.id = {entity['id']}"""
            carto_habitats_polygons = self.sqlite_cur.execute(sql).fetchall()
            if len(carto_habitats_polygons) == 0:
                print(f"L'entité station_surface id={entity['id']} n'a pas de polygone équivalent dans carto_habitats !")
                print('LA MISE À JOUR DE LA COUCHE station_surface A ÉCHOUÉ')
                return
            if len(carto_habitats_polygons) > 1:
                print(f"L'entité station_surface id={entity['id']} est à cheval sur plusieurs polygones carto_habitats...")
                print('LA MISE À JOUR DE LA COUCHE station_surface A ÉCHOUÉ')
                return
        # Check on carto_habitats
        carto_habitats_polygons = self.sqlite_cur.execute("SELECT * FROM carto_habitats_db.carto_habitats ORDER BY id").fetchall()
        for polygon in carto_habitats_polygons:
            sql = f"""SELECT ch.id AS polygon_id, ST_GeomFromWKB(ch.GEOMETRY, 2154) AS polygon_geom, sf.id AS sf_id, sf.uuid AS sf_uuid, sf.geom AS sf_geom
            FROM carto_habitats_db.carto_habitats ch
            JOIN station_surface sf ON ST_Intersects(ST_GeomFromWKB(ch.GEOMETRY, 2154), sf.geom)
            WHERE ch.id = {polygon['id']}"""
            station_surface_entities = self.sqlite_cur.execute(sql).fetchall()
            if len(station_surface_entities) > 1:
                print(f"Le polygone carto_habitats id={polygon['id']} contient plusieurs entités station_surface !")
                print('LA MISE À JOUR DE LA COUCHE station_surface A ÉCHOUÉ')
                return
            if len(station_surface_entities) == 0:
                print(f"INFO: Le polygone carto_habitats id={polygon['id']} ne contient aucune entité station_surface")
        # Replace station_surface entities with carto_habitats polygons if everything is ok
        sql = """WITH data_to_update AS (
            SELECT sf.id AS sf_id, sf.uuid AS sf_uuid, sf.geom AS sf_geom, ch.id AS polygon_id, ST_GeomFromWKB(ch.GEOMETRY, 2154) AS polygon_geom
            FROM station_surface sf
            LEFT JOIN carto_habitats_db.carto_habitats ch ON ST_Intersects(ST_GeomFromWKB(ch.GEOMETRY, 2154), sf.geom)
        )
        UPDATE station_surface
        SET geom = d.polygon_geom
        FROM data_to_update d
        WHERE station_surface.uuid = d.sf_uuid"""
        self.sqlite_cur.execute(sql)
        self.sqlite_conn.close()
        # Remove carto_habitats_sqlite SQLite
        carto_habitats_sqlite_file_path.unlink()
        print('La couche station_surface a été mise à jour avec succès !')

    def process_from_qgis(self, l4q_sqlite_file_name, polygons_sqlite_file_name):
        self.project = QgsProject.instance()
        self.process(l4q_sqlite_file_name, polygons_sqlite_file_name)

    def process_outside_qgis(self, l4q_sqlite_file_name, polygons_sqlite_file_name):
        QgsApplication.setPrefixPath("/usr", True)
        qgs = QgsApplication([], False)
        qgs.initQgis()

        self.project = QgsProject.instance()
        self.project.read('projet_lobelia4qgis.qgz')
        self.process(l4q_sqlite_file_name, polygons_sqlite_file_name)

        qgs.exitQgis()


l4q_sqlite = 'donnees_habitats.sqlite'
carto_habitats_sqlite = 'carto_habitats_sqlite.sqlite'
if __name__ == '__main__':
    ingest_service = PolygonsIngestService()
    ingest_service.process_outside_qgis(l4q_sqlite, carto_habitats_sqlite)
else:
    ingest_service = PolygonsIngestService()
    ingest_service.process_from_qgis(l4q_sqlite, carto_habitats_sqlite)

