from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QFocusEvent, QMouseEvent
from PyQt5.QtWidgets import QComboBox, QLineEdit, QLabel


class ComboBox(QComboBox):
    out = pyqtSignal()

    def hidePopup(self) -> None:
        self.out.emit()


class LineEdit(QLineEdit):
    out = pyqtSignal()

    def focusOutEvent(self, a0: QFocusEvent) -> None:
        self.out.emit()


class ClickableLabel(QLabel):
    clicked = pyqtSignal()

    def __init__(self, widget: QLabel):
        super().__init__(widget)

    def mouseReleaseEvent(self, ev: QMouseEvent) -> None:
        self.clicked.emit()
