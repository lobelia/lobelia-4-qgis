
import csv
import datetime
import sqlite3
import sys
from pathlib import Path
from sqlite3 import Connection, Cursor, Error
from typing import Optional, List

from qgis.core import QgsProject


class LinksManager:
    def __init__(self, project: QgsProject = None):
        self.project: Optional[QgsProject] = project
        if self.project is None:
            self.project = QgsProject.instance()
        self.connection: Optional[Connection] = None
        self.cursor: Optional[Cursor] = None
        self.home_path: Path = Path(self.project.homePath())

    def check_obs_hab_exists_by_id(self, obs_hab_id: int) -> bool:
        result: List = []
        request = "SELECT obs_hab_id FROM obs_habitats WHERE obs_hab_id=%s"
        if self.cursor:
            result = self.cursor.execute(request % obs_hab_id).fetchall()
        return len(result) > 0

    def check_obs_hab_ids_survey_is_equal(self, obs_hab_id_1: int, obs_hab_id_2: int) -> bool:
        result: bool = False
        request = "SELECT obs_hab_rel_id FROM obs_habitats WHERE obs_hab_id=%s"
        if self.cursor:
            rel_id_1 = self.cursor.execute(request % obs_hab_id_1).fetchone()[0]
            rel_id_2 = self.cursor.execute(request % obs_hab_id_2).fetchone()[0]
            result = rel_id_1 == rel_id_2
        return result

    def check_link_exists_by_ids(self, obs_hab_id_1: int, obs_hab_id_2: int) -> bool:
        result: List = []
        request = "SELECT id_obs_hab_1, id_obs_hab_2 FROM liens_obs_habitat WHERE id_obs_hab_1=%s AND id_obs_hab_2=%s"
        if self.cursor:
            result = self.cursor.execute(request % (obs_hab_id_1, obs_hab_id_2)).fetchall()
        return len(result) > 0

    def insert_new_link(self, obs_hab_id_1: int, obs_hab_id_2: int):
        request = "INSERT INTO liens_obs_habitat (id_obs_hab_1, id_obs_hab_2) VALUES (%s, %s)"
        if self.cursor:
            self.cursor.execute(request % (obs_hab_id_1, obs_hab_id_2))
            self.connection.commit()

    def get_obs_hab_id_list(self, source_csv_file: str) -> List[dict]:
        target_list: List[dict]
        with open(self.home_path / source_csv_file, 'r', encoding='utf-8') as source:
            target_list = [{'id_obs_hab_1': line['id_obs_hab_1'], 'id_obs_hab_2': line['id_obs_hab_2']}
                           for line in csv.DictReader(source, delimiter=';')]
        return target_list

    def get_obs_hab_links(self) -> List[dict]:
        target_list: List[dict] = []
        request = "SELECT id_obs_hab_1, id_obs_hab_2 FROM liens_obs_habitat"
        if self.cursor:
            result = self.cursor.execute(request).fetchall()
            target_list = [{'id_obs_hab_1': line['id_obs_hab_1'], 'id_obs_hab_2': line['id_obs_hab_2']}
                           for line in result]
        return target_list

    def import_links(self, obs_hab_id_list: List[dict]):
        for link in obs_hab_id_list:
            obs_hab_id_1, obs_hab_id_2 = link['id_obs_hab_1'], link['id_obs_hab_2']
            if self.check_obs_hab_exists_by_id(obs_hab_id_1) and self.check_obs_hab_exists_by_id(
                    obs_hab_id_2):
                if not self.check_obs_hab_ids_survey_is_equal(obs_hab_id_1, obs_hab_id_2):
                    print("%s - links import - survey issue with %s" %
                          (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), link))
                    break
                if not self.check_link_exists_by_ids(obs_hab_id_1, obs_hab_id_2):
                    self.insert_new_link(obs_hab_id_1, obs_hab_id_2)
            else:
                print("%s - links import - obs_hab_id issue with %s" %
                      (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), link))

    def check_links(self, obs_hab_id_list: List[dict]):
        for link in obs_hab_id_list:
            obs_hab_id_1, obs_hab_id_2 = link['id_obs_hab_1'], link['id_obs_hab_2']
            if self.check_obs_hab_exists_by_id(obs_hab_id_1) and self.check_obs_hab_exists_by_id(obs_hab_id_2):
                if not self.check_link_exists_by_ids(obs_hab_id_2, obs_hab_id_1):
                    self.insert_new_link(obs_hab_id_2, obs_hab_id_1)
            else:
                print("%s - links checking - obs_hab_id issue with %s" %
                      (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), link))

    def sqlite_connection(self, database_file_name) -> Optional[Connection]:
        try:
            self.connection = sqlite3.connect(self.home_path / database_file_name)
            self.connection.row_factory = sqlite3.Row
            self.connection.enable_load_extension(True)
            self.connection.load_extension("mod_spatialite")
        except Error as e:
            message = 'Connection failed - ' + str(e)
            print('Database connection', message)
        finally:
            if self.connection:
                self.cursor = self.connection.cursor()
            return self.connection

    def process(self, source_csv_file: str):
        self.sqlite_connection('donnees_habitats.sqlite')
        self.import_links(self.get_obs_hab_id_list(source_csv_file))
        self.check_links(self.get_obs_hab_links())


if __name__ == '__main__':
    print("%s - Lobelia4QGis command line script launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    if hasattr(sys, 'argv') and len(sys.argv) > 2:
        option = sys.argv[1]
        source_file = sys.argv[2]
        if option == 'import':
            print("%s - links import launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            geometry_manager = LinksManager()
            geometry_manager.process(source_file)
            print("%s - links import ended" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        else:
            print("%s - unknown option : import available" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    else:
        print("# usage :", "python scripts/import_links_between_obs_hab.py import <csv source file>")
        print("# note 1:", "csv source file must be composed by 2 columns id_obs_hab_1 and id_obs_hab_2")
        print("# note 2:", "always launch this command from project root directory")
    print("%s - Lobelia4QGis command line script closed" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
