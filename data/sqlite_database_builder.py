from sqlite3 import Cursor, Connection
from typing import Optional, List

from qgis.core import QgsProject
from qgis.utils import iface

from .database_specification_service import DatabaseSpecificationService
from ..helper.connection_helper import ConnectionHelper


class DatabaseBuilder:
    def __init__(self, project: QgsProject):
        self.spec: Optional[dict] = DatabaseSpecificationService.get_spec()
        self.connection: Optional[Connection] = ConnectionHelper(iface).sqlite_connection(project)
        self.cursor: Optional[Cursor]
        if self.connection:
            self.cursor = self.connection.cursor()

    def build_or_alter(self):
        existing_tables: List[str] = []
        tables_to_create: List[str] = []

        if self.get_database_version() != self.spec['version']:
            for table in self.spec['tables']:
                table_name = table['name']
                if self.table_exists(table_name):
                    existing_tables.append(table_name)
                else:
                    tables_to_create.append(table_name)

            for table_name in existing_tables:
                self.alter_table(table_name)

            for table_name in tables_to_create:
                self.create_table(table_name)

            self.update_database_version(self.spec['version'])

    def table_exists(self, table_name: str) -> bool:
        request = "SELECT count(name) FROM sqlite_master WHERE type='table' AND name='" + table_name + "'"
        self.cursor.execute(request)
        fetchone = self.cursor.fetchone()
        return fetchone[0] == 1

    def alter_table(self, table_name: str):
        altered = False
        table_specification = [table for table in self.spec['tables'] if table['name'] == table_name][0]
        target_columns = [(field['name'], field['type']) for field in table_specification['fields']]
        existing_columns = [i[1] for i in self.cursor.execute('PRAGMA table_info(%s)' % table_name)]

        for column in target_columns:
            if column[0] not in existing_columns:
                self.cursor.execute("ALTER TABLE " + table_name + " ADD COLUMN '%s' '%s'" % (column[0], column[1]))
                altered = True

        if table_specification['is_geometric']:
            geometry_specification = table_specification['geometry']
            geometry_name = geometry_specification['name']
            if geometry_name not in existing_columns:
                self.add_geometry_column(table_name, geometry_specification)
                altered = True

        if 'indices' in table_specification:
            self.add_indices(table_specification)

        if altered:
            iface.messageBar().pushInfo('Database builder', table_name + ' table altered')
        self.connection.commit()

    def create_table(self, table_name: str):
        table_specification = [table for table in self.spec['tables'] if table['name'] == table_name][0]
        columns = [(field['name'], field['type'], field['complement'] if 'complement' in field else '')
                   for field in table_specification['fields']]
        fields = []

        for column in columns:
            fields.append(' '.join(column))

        joined_fields = ", ".join(fields)
        request = "CREATE TABLE IF NOT EXISTS " + table_name + "(" + joined_fields + ")"
        self.cursor.execute(request)

        if table_specification['is_geometric']:
            self.add_geometry_column(table_name, table_specification['geometry'])

        if 'indices' in table_specification:
            self.add_indices(table_specification)

        iface.messageBar().pushInfo('Database builder', table_name + ' table created')
        self.connection.commit()

    def add_geometry_column(self, table_name: str, geometry):
        geom_specification = (table_name, geometry['name'], geometry['projection'], geometry['type'], geometry['axes'])
        geom_request = "SELECT AddGeometryColumn('%s', '%s', %s, '%s', '%s')"
        self.cursor.execute(geom_request % geom_specification)
        iface.messageBar().pushInfo('Database builder', table_name + ' - geometry added')

    def add_indices(self, table_specification: dict):
        table_name = table_specification['name']
        existing_indices = [i[1] for i in self.cursor.execute('PRAGMA index_list(%s)' % table_name)]
        indices = [(index['name'], table_name,
                    index['field'] + ' ' + index['complement'] if 'complement' in index else index['field'])
                   for index in table_specification['indices']]
        request = "CREATE INDEX %s ON %s (%s)"
        for index in indices:
            if index[0] not in existing_indices:
                self.cursor.execute(request % index)
                iface.messageBar().pushInfo('Database builder', table_name + ' - index ' + index[0] + ' added')

    def get_database_version(self) -> str:
        result: str = ""
        if self.cursor:
            request = "SELECT version FROM version WHERE id=1"
            result = self.cursor.execute(request).fetchone()[0]
        return result

    def update_database_version(self, version: str):
        if self.cursor:
            request = "UPDATE version SET version=? WHERE id=1"
            self.cursor.execute(request, (version,))
            self.connection.commit()
