from typing import List

from qgis.PyQt.QtCore import NULL
from qgis.core import QgsProject

from ..data.sqlite_data_manager import DataManager


class ListValuesProvider:
    def __init__(self, project: QgsProject = QgsProject.instance()):
        self.data_manager = DataManager(project)

    def get_values_from_list_name(self, list_name: str) -> List[dict]:
        result_list = self.data_manager.get_list_values_from_list_name(list_name)
        returned_list = [{'id': value[0], 'value': value[1], 'order': value[2]}
                         for value in result_list if value[2] > -1]
        return returned_list

    def get_values_from_list_name_and_criteria(self, list_name: str, criteria: List[str]) -> List[dict]:
        result_list = self.data_manager.get_list_values_from_list_name_and_criteria(list_name, criteria)
        returned_list = [{'id': value[0], 'value': value[1], 'order': value[2]}
                         for value in result_list if value[2] > -1]
        return returned_list

    @staticmethod
    def get_value_from_list(_list: List[dict], _id: int):
        for line in _list:
            if line['id'] == _id:
                return line['value']
        return NULL

    @staticmethod
    def get_id_from_value(_list: List[dict], value: str):
        for line in _list:
            if line['value'] == value:
                return line['id']
        return None
