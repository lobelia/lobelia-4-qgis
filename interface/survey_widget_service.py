from typing import List

from PyQt5.QtCore import QDate, QVariant, Qt, QObject, pyqtSignal
from PyQt5.QtWidgets import QComboBox, QTableWidget, QHeaderView, QCalendarWidget, QFrame, QMessageBox, QLineEdit
from PyQt5.QtWidgets import QTableWidgetItem, QTabWidget
from PyQt5.QtWidgets import QTextEdit, QCheckBox, QButtonGroup, QLabel, QWidget, QPlainTextEdit
from qgis.PyQt.QtCore import NULL
from qgis.core import QgsVectorLayer, QgsFeature
from qgis.gui import QgsAttributeForm
from qgis.utils import iface

from .survey_close_event_manager import CloseEventManager
from .survey_mouse_event_manager import MouseEventManager
from .table_widget_service import TableWidgetService
from ..data.list_values_provider import ListValuesProvider
from ..data.sqlite_data_manager import DataManager


class WidgetService:
    @staticmethod
    def load_combo_box(combo_box: QComboBox, values: List[dict]):
        combo_box.clear()
        for value in values:
            if value['value']:
                combo_box.addItem(value['value'], value['id'])
            else:
                combo_box.addItem('', value['id'])

    @staticmethod
    def clickable(widget: QObject) -> pyqtSignal():
        mouse_event = MouseEventManager(widget)
        widget.installEventFilter(mouse_event)
        return mouse_event.clicked

    @staticmethod
    def closable(form: QgsAttributeForm) -> pyqtSignal():
        close_event = CloseEventManager(form)
        form.installEventFilter(close_event)
        return close_event.closed

    @staticmethod
    def build_table(table: QTableWidget, header_labels: List):
        table.setEditTriggers(QTableWidget.NoEditTriggers)
        table.setColumnCount(len(header_labels))
        table.setHorizontalHeaderLabels(header_labels)

        header = table.horizontalHeader()
        for i in range(len(header_labels)):
            if i != 1:
                header.setSectionResizeMode(i, QHeaderView.ResizeToContents)
            else:
                header.setSectionResizeMode(i, QHeaderView.Stretch)

    @staticmethod
    def find_widget(form, widget_type, widget_name) -> QWidget:
        if widget_type == 'QComboBox':
            return form.findChild(QComboBox, widget_name)
        elif widget_type == 'QLineEdit':
            return form.findChild(QLineEdit, widget_name)
        elif widget_type == 'QTextEdit':
            return form.findChild(QTextEdit, widget_name)
        elif widget_type == 'QPlainTextEdit':
            return form.findChild(QPlainTextEdit, widget_name)
        elif widget_type == 'QCheckBox':
            return form.findChild(QCheckBox, widget_name)
        elif widget_type == 'QTabWidget':
            return form.findChild(QTabWidget, widget_name)
        elif widget_type == 'QFrame':
            return form.findChild(QFrame, widget_name)
        return QWidget()

    @staticmethod
    def get_input_value(widget):
        if isinstance(widget, QLineEdit):
            return widget.text()
        elif isinstance(widget, QTextEdit):
            return widget.toPlainText()
        elif isinstance(widget, QPlainTextEdit):
            return widget.toPlainText()
        elif isinstance(widget, QCheckBox):
            state = widget.checkState()
            if state == Qt.Unchecked:
                return -1
            elif state == Qt.PartiallyChecked:
                return NULL
            elif state == Qt.Checked:
                return 1
            return 0
        elif isinstance(widget, QComboBox):
            return widget.currentData()

    @staticmethod
    def set_input_value(widget, value):  # Pas d'attribution de valeur si value est NULL (QVariant)
        if isinstance(widget, QLineEdit) and not isinstance(value, QVariant):
            widget.setText(str(value))
        elif isinstance(widget, QTextEdit) and not isinstance(value, QVariant):
            widget.setPlainText(str(value))
        elif isinstance(widget, QPlainTextEdit) and not isinstance(value, QVariant):
            widget.setPlainText(str(value))
        elif isinstance(widget, QCheckBox):
            if value == 1:
                widget.setCheckState(Qt.Checked)
            elif value == 0 or value == NULL or value is None:
                widget.setCheckState(Qt.PartiallyChecked)
            elif value == -1:
                widget.setCheckState(Qt.Unchecked)
        elif isinstance(widget, QComboBox) and not isinstance(value, QVariant):
            widget.setCurrentIndex(widget.findData(value))

    @staticmethod
    def cell_double_clicked(i, survey_layer: QgsVectorLayer, survey_feature: QgsFeature, observations: QgsVectorLayer,
                            form: QgsAttributeForm, add_widget: QLabel, save_widget: QLabel, select_widget: QComboBox,
                            typo_buttons: QButtonGroup, add_survey_widgets: List[QLabel] = None,
                            add_related_habitats_widget: QLabel = None):
        table: QTableWidget = i.tableWidget()
        table.itemDoubleClicked.disconnect()
        table_name = table.objectName()
        col_name = table.horizontalHeaderItem(i.column()).text()
        row = i.row()

        if survey_feature.id() < 0:  # Lorsqu'un feature vient d'être créé, son identifiant est négatif s'il n'a pas été rechargé
            survey_feature = WidgetService.refresh_survey_feature(survey_feature, survey_layer)

        if col_name == 'suppr.':
            if add_widget.isVisible() or table_name == 'table_strates_struct' or table_name == 'table_facteurs':
                WidgetService.delete_observation(observations, row, survey_feature, table, table_name)
            if table_name == 'table_syn_habitats':
                WidgetService.delete_typo_habitat(observations, row, survey_feature, table)
            WidgetService.reconnect_cell_double_click(table, survey_layer, survey_feature, observations, form, add_widget,
                                                      save_widget, select_widget, typo_buttons, add_survey_widgets)
            return

        add_survey_widgets = [] if add_survey_widgets is None else add_survey_widgets
        if table_name == 'table_habitats':
            TableWidgetService.edit_syntaxon(survey_feature, row, observations, table, form, add_widget, save_widget,
                                             add_survey_widgets, select_widget, typo_buttons, add_related_habitats_widget)
        elif table_name == 'table_taxons':
            TableWidgetService.edit_taxon(survey_feature, row, observations, table, form, add_widget, save_widget,
                                          add_survey_widgets, select_widget, typo_buttons)
        WidgetService.reconnect_cell_double_click(table, survey_layer, survey_feature, observations, form, add_widget,
                                                  save_widget, select_widget, typo_buttons, add_survey_widgets,
                                                  add_related_habitats_widget)

    @staticmethod
    def reconnect_cell_double_click(table, survey_layer, survey_feature, observations, form, add_widget, save_widget,
                                    select_widget, typo_buttons, add_survey_widgets, add_related_habitats_widget: QLabel = None):
        table.itemDoubleClicked.connect(
            lambda item: WidgetService.cell_double_clicked(item, survey_layer, survey_feature, observations, form,
                                                           add_widget, save_widget, select_widget, typo_buttons,
                                                           add_survey_widgets, add_related_habitats_widget))

    @staticmethod
    def cell_clicked(item_clicked: QTableWidgetItem, survey_layer: QgsVectorLayer, survey_feature: QgsFeature,
                     observations: QgsVectorLayer):
        table_widget = item_clicked.tableWidget()
        table_widget.itemClicked.disconnect()
        table_name = table_widget.objectName()
        col_name = table_widget.horizontalHeaderItem(item_clicked.column()).text()

        if survey_feature.id() < 0:
            survey_feature = WidgetService.refresh_survey_feature(survey_feature, survey_layer)
        WidgetService.process_click(item_clicked, table_widget, survey_feature, observations, table_name, col_name)

        table_widget.itemClicked.connect(
            lambda i: WidgetService.cell_clicked(i, survey_layer, survey_feature, observations))

    @staticmethod
    def refresh_survey_feature(survey_feature: QgsFeature, survey_layer: QgsVectorLayer) -> QgsFeature:
        data_manager = DataManager()
        survey_feature_id = data_manager.get_survey_id_from_geometry(survey_feature)
        survey_feature = survey_layer.getFeature(survey_feature_id)
        return survey_feature

    @staticmethod
    def process_click(item_clicked: QTableWidgetItem, table_widget: QTableWidget, survey_feature: QgsFeature,
                      observations: QgsVectorLayer, table_name: str, col_name: str):
        if table_name == 'table_taxons' and col_name.startswith('strate_'):
            strate_values = ListValuesProvider().get_values_from_list_name('COEF_AD_VALUES')
            TableWidgetService.open_list_values_widget(survey_feature, observations, table_widget, item_clicked,
                                                       strate_values, 'cd_nom', 'nom_cite', 'obs_rel_id')
        if table_name == 'table_habitats':
            if col_name == 'coef. rec.':
                list_name = 'RECOUVREMENT_VALUES'
            elif col_name == 'forme':
                list_name = 'TYPE_FORME_VALUES'
            elif col_name == 'typ. struct.':
                list_name = 'TYPICITE_VALUES'
            elif col_name == 'typ. flor.':
                list_name = 'TYPICITE_VALUES'
            elif col_name == '% rec.':
                list_name = None
            else:
                return
            if list_name is not None:
                values = ListValuesProvider().get_values_from_list_name(list_name)
                TableWidgetService.open_list_values_widget(survey_feature, observations, table_widget, item_clicked,
                                                           values, 'obs_hab_code_cite', 'obs_hab_nom_cite',
                                                           'obs_hab_rel_id')
            else:
                TableWidgetService.edit_item(survey_feature, observations, table_widget, item_clicked,
                                             'obs_hab_code_cite', 'obs_hab_nom_cite', 'obs_hab_rel_id')

    @staticmethod
    def delete_observation(observations, row, survey_feature, table, table_name):
        if table_name == 'table_habitats':
            code_cell_value = table.item(row, 0).text() if table.item(row, 0) else None
            obs_habitat = TableWidgetService.find_observation(code_cell_value, table.item(row, 1).text(),
                                                              'obs_hab_code_cite', 'obs_hab_nom_cite',
                                                              'obs_hab_rel_id', observations, survey_feature)
            TableWidgetService.del_row(survey_feature, row, observations, table, 'obs_hab_rel_id',
                                       'obs_hab_code_cite', 'obs_hab_nom_cite', code_cell_value,
                                       table.item(row, 1).text())
            if obs_habitat:
                obs_habitat_id = int(obs_habitat['obs_hab_id'])
                sqlite_data_manager = DataManager()
                sqlite_data_manager.remove_links_by_obs_hab_id(obs_habitat_id)
                if obs_habitat['obs_hab_code_typo'] == 0 and obs_habitat['obs_hab_rel_assoc_id'] is not None:
                    sqlite_data_manager.clean_obs_hab_link_in_assoc_survey(obs_habitat['obs_hab_rel_assoc_id'])
                    sqlite_data_manager.clean_other_obs_hab_link_to_assoc_survey(obs_habitat['obs_hab_rel_assoc_id'])

        elif table_name == 'table_taxons':
            code_cell_value = int(table.item(row, 0).text()) \
                if table.item(row, 0) and table.item(row, 0).text() != '' else None
            TableWidgetService.del_row(survey_feature, row, observations, table, 'obs_rel_id', 'cd_nom', 'nom_cite',
                                       code_cell_value, table.item(row, 1).text())
        elif table_name == 'table_strates_struct':
            code_cell_value = int(table.item(row, 0).text()) \
                if table.item(row, 0) and table.item(row, 0).text() != '' else None
            TableWidgetService.del_row(survey_feature, row, observations, table, 'rel_id', 'strate_id', 'strate_id',
                                       code_cell_value, code_cell_value)
        elif table_name == 'table_facteurs':
            code_cell_value = int(table.item(row, 0).text()) \
                if table.item(row, 0) and table.item(row, 0).text() != '' else None
            TableWidgetService.del_row(survey_feature, row, observations, table, 'rel_id', 'facteur_id', 'facteur_id',
                                       code_cell_value, code_cell_value)

    @staticmethod
    def delete_typo_habitat(observations, row, survey_feature, table):
        code_cell_value = table.item(row, 0).text() if table.item(row, 0) else None
        obs_habitat = TableWidgetService.find_observation(code_cell_value, table.item(row, 1).text(),
                                                          'obs_hab_code_cite', 'obs_hab_nom_cite',
                                                          'obs_hab_rel_assoc_id', observations, survey_feature)
        if obs_habitat:
            sqlite_data_manager = DataManager()
            survey_id = survey_feature['id']
            obs_syntaxon_id: int
            if survey_id is None:
                survey_id = sqlite_data_manager.get_survey_id_from_geometry(survey_feature)
                obs_syntaxon_id = sqlite_data_manager.get_obs_syntaxon_id_from_survey_id(survey_id)
            else:
                obs_syntaxon_id = int(survey_feature['rel_obs_syntaxon_id'])
            obs_habitat_id = int(obs_habitat['obs_hab_id'])
            if sqlite_data_manager.is_obs_hab_linked_to_other(obs_habitat_id, obs_syntaxon_id):
                sqlite_data_manager.remove_links_between_obs_hab(obs_habitat_id, obs_syntaxon_id)
                sqlite_data_manager.clean_link_to_assoc_survey(obs_habitat_id)
                table.removeRow(row)
            else:
                sqlite_data_manager.remove_links_by_obs_hab_id(obs_habitat_id)
                TableWidgetService.del_row(survey_feature, row, observations, table, 'obs_hab_rel_assoc_id',
                                           'obs_hab_code_cite', 'obs_hab_nom_cite', code_cell_value,
                                           table.item(row, 1).text())
        else:
            iface.messageBar().pushWarning('Suppression d\'observation habitat', 'Vous ne pouvez pas supprimer une '
                                                                                 'observation qui n\'a pas ete creee '
                                                                                 'dans ce releve.')

    @staticmethod
    def check_hydromorphic_checkbox(checkbox: QCheckBox, linked_input: QLineEdit):
        state = checkbox.checkState()
        if state == Qt.Unchecked or state == Qt.PartiallyChecked:
            linked_input.clear()
            linked_input.setEnabled(False)
        else:
            linked_input.setEnabled(True)

    @staticmethod
    def fshow(rel_type_widget: QComboBox, rel_protocol_widget: QComboBox, frel: QFrame, fobs: QFrame,
              save_rel: QLabel, activate: QLabel):
        if rel_type_widget.itemData(rel_type_widget.currentIndex()) != 0 and rel_protocol_widget.itemData(
                rel_protocol_widget.currentIndex()) != 0 and activate.isHidden():
            rel_type_widget.setEnabled(False)
            if frel.isHidden():
                frel.show()
                fobs.hide()
            else:
                frel.hide()
                fobs.show()
        else:
            frel.hide()
            fobs.hide()
            save_rel.hide()

    @staticmethod
    def fgen_show(fgen: QFrame):
        if fgen.isHidden():
            fgen.show()
        else:
            fgen.hide()

    @staticmethod
    def fobs_show(tab_widget: QTabWidget, rel_type_widget: QComboBox, rel_protocol_widget: QComboBox,
                  fobs_taxons: QFrame, fobs_syntaxons: QFrame, activate: QLabel, obs_hab_surface: QLineEdit,
                  obs_hab_surface_label: QLabel):
        current_data = rel_type_widget.currentData()
        current_protocol_data = rel_protocol_widget.currentData()
        WidgetService.load_protocol_list(current_data, rel_protocol_widget)

        if rel_protocol_widget.findData(current_protocol_data) > -1:
            rel_protocol_widget.setCurrentIndex(rel_protocol_widget.findData(current_protocol_data))
            rel_protocol_widget.removeItem(rel_protocol_widget.findData(0))

        if current_data == 1:
            fobs_taxons.show()
            fobs_syntaxons.hide()
            removed = 0
            for i in range(len(tab_widget)):
                if tab_widget.widget(i - removed).objectName() in ['tab_typo_syntaxon']:
                    tab_widget.removeTab(i - removed)
                    removed += 1
            obs_hab_surface.hide()
            obs_hab_surface_label.hide()
            activate.setEnabled(True)
        elif current_data == 2:
            fobs_taxons.hide()
            fobs_syntaxons.show()
            removed = 0
            for i in range(len(tab_widget)):
                if tab_widget.widget(i - removed).objectName() in ['tab_descr_vege', 'tab_structure', 'tab_typologie']:
                    tab_widget.removeTab(i - removed)
                    removed += 1
            activate.setEnabled(True)
        else:
            fobs_taxons.hide()
            fobs_syntaxons.hide()
            activate.setEnabled(False)

    @staticmethod
    def calendar(calendar_box, calendar_label, rel_date_min, rel_date_max, transform_function):
        if calendar_label.isEnabled():
            calendar_box.setWindowTitle("Calendrier")
            msglt = calendar_box.layout()
            qcal1 = QCalendarWidget()
            msglt.addWidget(qcal1)
            qcal1.clicked.connect(lambda date: transform_function(date, rel_date_min, rel_date_max, calendar_box))
            calendar_box.exec_()

    @staticmethod
    def transform_date_1(date, rel_date_min: QLineEdit, rel_date_max: QLineEdit, cal1: QMessageBox):
        rel_date_min.setText(str(date.toString('yyyy-MM-dd')))
        rel_date_max.setText(str(date.toString('yyyy-MM-dd')))
        cal1.close()

    @staticmethod
    def transform_date_2(date, rel_date_min: QLineEdit, rel_date_max: QLineEdit, cal2: QMessageBox):
        # TODO adapt date below...
        if date < QDate.fromString(rel_date_min.text(), 'yyyy-MM-dd'):
            rel_date_max.setStyleSheet("background : rgba(255, 1, 1, 80)")
        else:
            rel_date_max.setStyleSheet("background : white")
            rel_date_max.setText(str(date.toString('yyyy-MM-dd')))
            cal2.close()

    @staticmethod
    def load_protocol_list(rel_type_rel_id: int, rel_protocol_widget: QComboBox):
        list_values_provider = ListValuesProvider()
        protocole_values = list_values_provider.get_values_from_list_name('PROTOCOLE_VALUES')
        protocol_list = [{'id': protocol['id'], 'value': protocol['value']} for protocol in protocole_values
                         if protocol['order'] in (0, rel_type_rel_id)]
        WidgetService.load_combo_box(rel_protocol_widget, protocol_list)
