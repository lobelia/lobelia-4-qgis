import csv
from operator import itemgetter
from sqlite3 import Row
from typing import List

from qgis.core import QgsProject

from .a_extract_service import ExtractService
from ..constants.connection_constants import SQLITE_FILE


class ObsHabExtractService(ExtractService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)
        self.filename = 'obs_hab_export.csv'

    def get_date_by_survey_id(self, survey_id: int) -> str:
        query = "SELECT date_obs AS date FROM releves WHERE id = %s"
        result: Row = self.connection.execute(query % survey_id).fetchone()
        return str(result['date']) if result and 'date' in result.keys() and result['date'] else None

    def get_num_by_survey_id(self, survey_id: int) -> str:
        query = "SELECT rel_num AS num FROM releves WHERE id = %s"
        result: Row = self.connection.execute(query % survey_id).fetchone()
        return str(result['num']) if result and 'num' in result.keys() and result['num'] else None

    def get_location_by_survey_id(self, survey_id: int) -> str:
        query = "SELECT rel_lieu_dit AS lieu_dit FROM releves WHERE id = %s"
        result: Row = self.connection.execute(query % survey_id).fetchone()
        return str(result['lieu_dit']) if result and 'lieu_dit' in result.keys() and result['lieu_dit'] else None

    def get_survey_comment_by_survey_id(self, survey_id: int) -> str:
        query = "SELECT commentaires AS comment FROM releves WHERE id = %s"
        result: Row = self.connection.execute(query % survey_id).fetchone()
        return str(result['comment']) if result and 'comment' in result.keys() and result['comment'] else None

    def get_habitat_cite_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT obs_hab_nom_cite AS name FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['name']) if result and 'name' in result.keys() and result['name'] else None

    def get_habitat_code_cite_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT obs_hab_code_cite AS code FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['code']) if result and 'code' in result.keys() and result['code'] else None

    def get_habitat_retenu_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT syntaxon_nom_retenu AS name FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['name']) if result and 'name' in result.keys() and result['name'] else None

    def get_habitat_code_retenu_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT syntaxon_code_phyto_retenu AS code FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['code']) if result and 'code' in result.keys() and result['code'] else None

    def get_habitat_comment_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT obs_hab_commentaire_habitat AS comment FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['comment']) if result and 'comment' in result.keys() and result['comment'] else None

    def get_taxon_dominant_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT syntaxon_comm_non_sat_taxon_dom_nom AS tax_dom FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['tax_dom']) if result and 'tax_dom' in result.keys() and result['tax_dom'] else None

    def get_forme_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT obs_hab_forme_id AS typ_physio_id, " \
                "(SELECT value FROM listes_valeurs WHERE list_name = 'TYPE_FORME_VALUES' " \
                "AND obs_habitats.obs_hab_forme_id = key) AS forme " \
                "FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return result['forme'] if result and 'forme' in result.keys() else None

    def get_type_rel_by_code(self, code_type: int) -> str:
        query = "SELECT value FROM listes_valeurs WHERE list_name = 'TYPE_REL_VALUES' AND key = %s"
        result: Row = self.connection.execute(query % code_type).fetchone()
        return result['value'] if result and 'value' in result.keys() else None

    def get_protocol_by_code(self, code_type: int) -> str:
        query = "SELECT value FROM listes_valeurs WHERE list_name = 'PROTOCOLE_VALUES' AND key = %s"
        result: Row = self.connection.execute(query % code_type).fetchone()
        return result['value'] if result and 'value' in result.keys() else None

    def get_jdd_by_uuid(self, uuid: str) -> str:
        query = "SELECT libelle_court FROM jdd WHERE uuid = '%s'"
        result: Row = self.connection.execute(query % uuid).fetchone()
        return result['libelle_court'] if result and 'libelle_court' in result.keys() else None

    def get_com_basale_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT syntaxon_comm_non_sat_id AS com_basale_id, " \
                "(SELECT value FROM listes_valeurs WHERE list_name = 'COMM_NON_SATUREES_VALUES' " \
                "AND obs_habitats.syntaxon_comm_non_sat_id = key) AS com_sat " \
                "FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return result['com_sat'] if result and 'com_sat' in result.keys() else None

    def get_linked_obs_hab_by_obs_hab_id(self, obs_id: int) -> List[int]:
        query = "SELECT id_obs_hab_1 AS linked_obs_hab_id FROM liens_obs_habitat WHERE id_obs_hab_2 = %s"
        result: List[Row] = self.connection.execute(query % obs_id).fetchall()
        if len(result) > 0:
            return [row['linked_obs_hab_id'] for row in result]
        else:
            return []

    @staticmethod
    def get_typo_by_code(code_typo: int) -> str:
        typo: str = ''
        if code_typo == 0:
            typo = 'Phyto'
        elif code_typo == 4:
            typo = 'HIC'
        elif code_typo == 7:
            typo = 'EUNIS'
        elif code_typo == 22:
            typo = 'Corine'
        return typo

    def get_obs_hab_links_by_obs_hab_id(self, obs_id: int, obs_hab_link_list: List[dict]) -> dict:
        obs_hab_links: dict = {'0': [], '7': [], '4': [], '22': []}
        linked_obs_hab_list: List[int] = self.get_linked_obs_hab_by_obs_hab_id(obs_id)
        for obs_hab in linked_obs_hab_list:
            typo: int = self.get_typo_by_obs_hab_id(obs_hab)
            if typo == 0:
                obs_hab_links['0'].append(obs_hab)
            elif typo == 7:
                obs_hab_links['7'].append(obs_hab)
            elif typo == 4:
                obs_hab_links['4'].append(obs_hab)
            elif typo == 22:
                obs_hab_links['22'].append(obs_hab)
        return obs_hab_links

    def get_formatted_obs_hab_dict(self, geom: dict, obs_hab_links: dict, obs_habitat: dict, obs_id: int, survey: dict,
                                   survey_id: int):
        return {
            'geom_uuid': survey['geom_uuid'],
            'releve_id': survey_id,
            'releve_uuid': survey['uuid'],
            'type_releve': self.get_type_rel_by_code(survey['type']),
            'protocole': self.get_protocol_by_code(survey['protocol']),
            'jdd': self.get_jdd_by_uuid(survey['jdd']),
            'date_releve': self.get_date_by_survey_id(survey_id),
            'observateurs': self.calculate_observers(survey_id),
            'numero': self.get_num_by_survey_id(survey_id),
            'commune': '',
            'lieu-dit': self.get_location_by_survey_id(survey_id),
            'commentaire_releve': self.get_survey_comment_by_survey_id(survey_id),
            'obs_hab_id': obs_id,
            'obs_hab_uuid': obs_habitat['uuid'],
            'habitat_cite': self.get_habitat_cite_by_obs_hab_id(obs_id),
            'habitat_code_cite': self.get_habitat_code_cite_by_obs_hab_id(obs_id),
            'syntaxon_retenu': self.get_habitat_retenu_by_obs_hab_id(obs_id),
            'syntaxon_code_retenu': self.get_habitat_code_retenu_by_obs_hab_id(obs_id),
            'code_typo': self.get_typo_by_code(obs_habitat['typo_code']),
            'recouvrement_pct': self.get_pct_rec_by_obs_hab_id(obs_id),
            'recouvrement_coef': self.get_coef_rec_by_obs_hab_id(obs_id),
            'forme': self.get_forme_by_obs_hab_id(obs_id),
            'typicite_structurelle': self.get_typ_struct_by_obs_hab_id(obs_id),
            'typicite_floristique': self.get_typ_flor_by_obs_hab_id(obs_id),
            'type_physio': self.get_typ_physio_by_obs_hab_id(obs_id),
            'communaute_basale': self.get_com_basale_by_obs_hab_id(obs_id),
            'taxon_dominant': self.get_taxon_dominant_by_obs_hab_id(obs_id),
            'commentaire_obs_hab': self.get_habitat_comment_by_obs_hab_id(obs_id),
            'liens_syntaxons': self.format_obs_hab_code_list(obs_hab_links['0']),
            'liens_HIC': self.format_obs_hab_code_list(obs_hab_links['4']),
            'liens_EUNIS': self.format_obs_hab_code_list(obs_hab_links['7']),
            'liens_Corine': self.format_obs_hab_code_list(obs_hab_links['22']),
            'geom': geom['geometry']
        }

    def flatten_by_rel_assoc(self, field: str, flattened_habitat_list: List[dict], geom_list: List[dict],
                             obs_habitat: dict, survey_list: List[dict], obs_hab_link_list: List[dict]):
        for survey in survey_list:
            survey_id = survey['id']
            if survey_id == obs_habitat[field]:
                for geom in geom_list:
                    if geom['uuid'] == survey['geom_uuid']:
                        obs_id = obs_habitat['id']
                        obs_hab_links: dict = self.get_obs_hab_links_by_obs_hab_id(obs_id, obs_hab_link_list)
                        flattened_habitat_list.append(self.get_formatted_obs_hab_dict(geom, obs_hab_links, obs_habitat,
                                                                                      obs_id, survey, survey_id))

    def flatten_obs_habitat(self, obs_habitat_list: List[dict], survey_list: List[dict], geom_list: List[dict],
                            obs_hab_link_list: List[dict]) -> List[dict]:
        flattened_habitat_list: List[dict] = []
        for obs_habitat in obs_habitat_list:
            if obs_habitat['rel_id'] and (obs_habitat['code'] or obs_habitat['name']):
                self.flatten_by_rel_assoc('rel_id', flattened_habitat_list, geom_list, obs_habitat, survey_list,
                                          obs_hab_link_list)
            elif obs_habitat['rel_assoc_id'] and (obs_habitat['code'] or obs_habitat['name']):
                self.flatten_by_rel_assoc('rel_assoc_id', flattened_habitat_list, geom_list, obs_habitat, survey_list,
                                          obs_hab_link_list)
        return flattened_habitat_list

    @staticmethod
    def sort_obs_hab(flattened_obs_hab_list: List[dict]) -> List[dict]:
        return sorted(sorted(flattened_obs_hab_list, key=itemgetter('code_typo'), reverse=True),
                      key=itemgetter('releve_id'))

    def build_target_dict(self) -> List[dict]:
        all_geom_list: List[dict] = self.row_to_dict(self.get_all_geom_list())
        obs_habitat_list: List[dict] = self.row_to_dict(self.get_obs_habitat_list())
        obs_habitat_links: List[dict] = self.row_to_dict(self.get_links_between_obs_habitat())
        survey_list: List[dict] = self.row_to_dict(self.get_survey_list())
        survey_list = [{'id': survey['id'], 'uuid': survey['uuid'], 'type': survey['type'],
                        'protocol': survey['protocol'], 'jdd': survey['jdd'], 'geom_uuid': self.concat(
                [survey['point_uuid'], survey['line_uuid'], survey['polygon_uuid']])} for survey in survey_list]
        flattened_habitat_list = self.flatten_obs_habitat(obs_habitat_list, survey_list, all_geom_list,
                                                          obs_habitat_links)
        return self.sort_obs_hab(flattened_habitat_list)

    def export_csv(self, target_dict: List[dict]):
        if len(target_dict) > 0:
            fieldnames: List[str] = []
            for geom in target_dict:
                for key in geom.keys():
                    if key not in fieldnames:
                        fieldnames.append(key)
            with open(self.home_path / self.filename, 'w', encoding='utf-8', newline='') as file:
                writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter=';')
                writer.writeheader()
                writer.writerows(target_dict)

    def process(self, database_file_name: str = SQLITE_FILE):
        self.sqlite_connection(database_file_name)
        target_data = self.build_target_dict()
        if len(target_data) > 0 and self.cursor:
            self.export_csv(target_data)
        self.connection.close()
        self.send_message('Export habitat observations', self.filename)
