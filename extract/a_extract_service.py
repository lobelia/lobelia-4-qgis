import sqlite3
import uuid
from pathlib import Path
from sqlite3 import Connection, Cursor, Error, Row
from typing import Optional, List

from qgis.core import QgsProject
from qgis.utils import iface


class ExtractService:
    def __init__(self, project: QgsProject = None):
        self.project: Optional[QgsProject] = project
        if self.project is None:
            self.project = QgsProject.instance()
        self.connection: Optional[Connection] = None
        self.cursor: Optional[Cursor] = None
        self.home_path: Path = Path(self.project.homePath())

    def get_all_geom_list(self) -> List[Row]:
        result: List[Row] = []
        query_surface = "SELECT uuid, AsWKT(CastAutomagic(geom)) AS geometry FROM station_surface"
        result.extend(self.connection.execute(query_surface).fetchall())
        query_line = "SELECT uuid, AsWKT(CastAutomagic(geom)) AS geometry FROM station_ligne"
        result.extend(self.connection.execute(query_line).fetchall())
        query_point = "SELECT uuid, AsWKT(CastAutomagic(geom)) AS geometry FROM station_point"
        result.extend(self.connection.execute(query_point).fetchall())
        return result

    def get_obs_habitat_list(self) -> List[Row]:
        query = "SELECT obs_hab_id AS id, obs_hab_uuid AS uuid, obs_hab_rel_id AS rel_id, obs_hab_rel_assoc_id AS rel_assoc_id, " \
                "obs_hab_nom_cite AS name, obs_hab_code_cite AS code, obs_hab_code_typo AS typo_code, " \
                "obs_hab_rec_pct AS rec FROM obs_habitats"
        self.check_obs_hab_uuid(self.connection.execute(query).fetchall())
        return self.connection.execute(query).fetchall()

    def check_obs_hab_uuid(self, obs_habitat_list: List[Row]):
        for obs_hab in obs_habitat_list:
            if obs_hab['uuid'] is None or obs_hab['uuid'] == '':
                self.cursor.execute('UPDATE obs_habitats SET obs_hab_uuid = ? WHERE obs_hab_id=?;',
                                    (str(uuid.uuid4()), str(obs_hab['id'])))
                self.connection.commit()

    def get_survey_list(self) -> List[Row]:
        query = "SELECT id, rel_uuid AS uuid, rel_type_rel_id AS type, rel_protocole_id AS protocol, rel_point_uuid AS point_uuid, " \
                "rel_ligne_uuid AS line_uuid, rel_polygon_uuid AS polygon_uuid, rel_jdd_uuid AS jdd FROM releves"
        return self.connection.execute(query).fetchall()

    def get_links_between_obs_habitat(self) -> List[Row]:
        query = "SELECT id_obs_hab_1, id_obs_hab_2 FROM liens_obs_habitat"
        return self.connection.execute(query).fetchall()

    def get_typo_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT obs_hab_code_typo AS typo FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return int(result['typo']) if result and 'typo' in result.keys() else None

    def get_observers(self, survey_id: int) -> List[Row]:
        query = "SELECT rel.id AS id, obs.obs_nom AS nom_observateur, obs.obs_prenom AS prenom_observateur " \
                "FROM releves AS rel " \
                "LEFT JOIN liens_releve_observateur AS rel_obs ON rel.id = rel_obs.rel_id " \
                "LEFT JOIN observateurs AS obs ON rel_obs.obs_id = obs.obs_id " \
                "WHERE rel.id = %s"
        return self.connection.execute(query % survey_id).fetchall()

    def get_pct_rec_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT obs_hab_rec_pct AS rec FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return int(result['rec']) if result and 'rec' in result.keys() and result['rec'] else None

    def get_coef_rec_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT obs_hab_recouvrement AS coef_rec_id, " \
                "(SELECT value FROM listes_valeurs WHERE list_name = 'RECOUVREMENT_VALUES' " \
                "AND obs_habitats.obs_hab_recouvrement = key) AS coef_rec " \
                "FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return result['coef_rec'] if result and 'coef_rec' in result.keys() else None

    def get_typ_flor_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT syntaxon_typicite_flor_id AS typ_flor_id, " \
                "(SELECT value FROM listes_valeurs WHERE list_name = 'TYPICITE_VALUES' " \
                "AND obs_habitats.syntaxon_typicite_flor_id = key) AS typ_flor " \
                "FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return result['typ_flor'] if result and 'typ_flor' in result.keys() else None

    def get_typ_struct_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT syntaxon_typicite_struct_id AS typ_struct_id, " \
                "(SELECT value FROM listes_valeurs WHERE list_name = 'TYPICITE_VALUES' " \
                "AND obs_habitats.syntaxon_typicite_struct_id = key) AS typ_struct " \
                "FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return result['typ_struct'] if result and 'typ_struct' in result.keys() else None

    def get_typ_physio_by_obs_hab_id(self, obs_hab_id: int) -> int:
        query = "SELECT syntaxon_type_physio_id AS typ_physio_id, " \
                "(SELECT value FROM listes_valeurs WHERE list_name = 'TYPE_PHYSIO_VALUES' " \
                "AND obs_habitats.syntaxon_type_physio_id = key) AS typ_physio " \
                "FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return result['typ_physio'] if result and 'typ_physio' in result.keys() else None

    def calculate_observers(self, survey_id: int) -> str:
        observer_list: List[str] = []
        observers_as_dict: List[dict] = self.row_to_dict(self.get_observers(survey_id))
        for observer in observers_as_dict:
            observer_name = str(observer['nom_observateur']).capitalize() if observer['nom_observateur'] else ''
            observer_name += ' ' + str(observer['prenom_observateur']).title() if observer['prenom_observateur'] else ''
            observer_list.append(observer_name)
        return ' & '.join(observer_list)

    def get_code_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT obs_hab_code_cite AS code FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['code'])

    def format_obs_hab_code_list(self, obs_hab_id_list: List[int]) -> str:
        obs_hab_code_list: List[str] = [self.get_code_by_obs_hab_id(code) for code in obs_hab_id_list]
        # print(str(obs_hab_code_list).replace('[', '').replace(']', '').replace(', ', ' / ').replace('\'', ''))
        return str(obs_hab_code_list).replace('[', '').replace(']', '').replace(', ', ' / ').replace('\'', '')

    @staticmethod
    def row_to_dict(rows: List[Row]) -> List[dict]:
        dict_list: List[dict] = []
        for row in rows:
            dict_list.append({key: row[key] for key in row.keys()})
        return dict_list

    @staticmethod
    def decode(text) -> str:
        result: str = ''
        try:
            result = text.decode("utf-8")
        except UnicodeDecodeError:
            pass
        finally:
            return result

    @staticmethod
    def concat(str_list: List[str]) -> str:
        concat_string = ""
        for string in str_list:
            if string:
                concat_string += string
        return concat_string

    def send_message(self, title: str, filename: str):
        message = 'csv file exported to ' + str((self.home_path / filename).absolute())
        try:
            iface.messageBar().pushInfo(title, message)
        except:
            print(message)

    def sqlite_connection(self, database_file_name) -> Optional[Connection]:
        try:
            self.connection = sqlite3.connect(self.home_path / database_file_name)
            self.connection.row_factory = sqlite3.Row
            self.connection.enable_load_extension(True)
            self.connection.load_extension("mod_spatialite")
            self.connection.text_factory = lambda x: self.decode(x)
        except Error as e:
            message = 'Connection failed - ' + str(e)
            iface.messageBar().pushWarning('Database connection', message)
        finally:
            if self.connection:
                self.cursor = self.connection.cursor()
            return self.connection

    def build_target_dict(self) -> List[dict]:
        return NotImplementedError

    def export_csv(self, target_dict: List[dict]):
        return NotImplementedError

    def process(self, database_file_name: str):
        return NotImplementedError
