from PyQt5.QtCore import pyqtSignal, QObject, QEvent


class CloseEventManager(QObject):
    closed = pyqtSignal()

    def __init__(self, form: QObject):
        super(CloseEventManager, self).__init__(form)
        self.form = form

    def eventFilter(self, obj, event) -> bool:
        if obj == self.form:
            if event.type() == QEvent.Close:
                self.closed.emit()
                return True
        return False
