import csv
import sys
from typing import List

from qgis.core import QgsProject

from ingest_service import IngestService


class TaxrefGroupIngestService(IngestService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)

    def build_target_dict(self, file_name: str) -> List[tuple]:
        target_data: List[tuple]
        with open(self.home_path / file_name, 'r', encoding='utf-8') as file:
            data = csv.DictReader(file, delimiter=';')
            target_data = [(line['cd_nom'], line['lb_nom'], line['cd_ref'], line['nom_valide'])
                           for line in data]
        return target_data

    def import_groups_added_to_taxref(self, data) -> bool:
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.executemany('INSERT INTO taxref (cd_nom, lb_nom, cd_ref, nom_valide) VALUES (?, ?, ?, ?);', data)
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def process(self, file_name: str, database_file_name: str):
        self.sqlite_connection(database_file_name)
        target_data = self.build_target_dict(file_name)
        if len(target_data) > 0 and self.sqlite_cur:
            self.import_groups_added_to_taxref(target_data)
        self.sqlite_conn.close()


if __name__ == '__main__':
    ingest_service = TaxrefGroupIngestService()
    ingest_service.process(sys.argv[1], 'donnees_habitats.sqlite')
