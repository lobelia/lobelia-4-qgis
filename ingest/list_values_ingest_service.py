import csv
import sys
from typing import List

from qgis.core import QgsProject

from ingest_service import IngestService


class ListValuesIngestService(IngestService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)
        self.list_values_dict = [
            {
                "list_name": 'BOIS_MORT_VALUES',
                "default_value": True,
                "key": 'id+1',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'foret_ancienne.ref_bois_mort_presence'
            },
            {
                "list_name": 'CARHAB_BIOTOPES_VALUES',
                "default_value": True
            },
            {
                "list_name": 'CARHAB_PHYSIO_VALUES',
                "default_value": True
            },
            {
                "list_name": 'CLASSE_EFFECTIFS_VALUES',
                "default_value": False,
                "key": 'classe_effectifs_id',
                "relation": 0,
                "import_value": 'classe_effectifs_libelle',
                "table": 'public.classe_effectifs'
            },
            {
                "list_name": 'CLASSE_SURFACES_VALUES',
                "default_value": False,
                "key": 'classe_surfaces_id',
                "relation": 0,
                "import_value": 'classe_surfaces_libelle',
                "table": 'public.classe_surfaces'
            },
            {
                "list_name": 'COEF_AD_VALUES',
                "default_value": True,
                "key": 'ind_ab_id',
                "relation": 0,
                "import_value": 'ind_ab_lib',
                "table": 'referentiel.indice_abondance',
                "where": 'ind_ab_id <= 12 AND ind_ab_id != 11'
            },
            {
                "list_name": 'COHERENCE_TEMPORELLE_VALUES',
                "default_value": True,
                "key": 'id',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'referentiel.ref_coherence_tempo'
            },
            {
                "list_name": 'COMM_NON_SATUREES_VALUES',
                "default_value": True,
                "key": 'id',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'referentiel.ref_com_non_sat'
            },
            {
                "list_name": 'ELEMENTS_GROSSIERS_VALUES',
                "default_value": True,
                "key": 'ele_gros_id',
                "relation": 0,
                "import_value": 'ele_gros_lib',
                "table": 'referentiel.ref_element_grossier'
            },
            {
                "list_name": 'ENNEIGEMENT_VALUES',
                "default_value": True,
                "key": 'id',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'referentiel.ref_enneigement'
            },
            {
                "list_name": 'EXHAUSTIVITE_VALUES',
                "default_value": False,
                "key": 'id',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'referentiel.ref_exhaustivite'
            },
            {
                "list_name": 'EXPOSITION_VALUES',
                "default_value": True,
                "key": 'expo_id',
                "value": 'expo_lib',
                "relation": 0,
                "import_value": 'expo_abrev',
                "table": 'referentiel.ref_expositions'
            },
            {
                "list_name": 'FACTEUR_ETAT_VALUES',
                "default_value": True,
                "key": 'valeur',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'referentiel.ref_etat_fact_evol'
            },
            {
                "list_name": 'FACTEUR_IMPACT_VALUES',
                "default_value": True,
                "key": 'impact_fact_evol_id',
                "relation": 0,
                "import_value": 'impact_fact_evol_libelle',
                "table": 'referentiel.ref_impact_facteur_evolution'
            },
            {
                "list_name": 'FACTEUR_INFLUENCE_VALUES',
                "default_value": True,
                "key": 'id',
                "value": "CASE WHEN libelle IS NOT NULL THEN CONCAT(code, ' - ', libelle) ELSE CONCAT(code, ' - ', libelle_corresp_znieff) END",
                "relation": 'niveau',
                "import_value": 'COALESCE(libelle, libelle_corresp_znieff)',
                "table": 'referentiel.ref_facteurs_influence'
            },
            {
                "list_name": 'FORME_HUMUS_VALUES',
                "default_value": True,
                "key": 't_humus_id',
                "relation": 0,
                "import_value": 't_humus_lib',
                "table": 'referentiel.type_humus'
            },
            {
                "list_name": 'FORME_RELEVE_VALUES',
                "default_value": True,
                "key": 'id',
                "value": 'libelle',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'referentiel.ref_forme_releve'
            },
            {
                "list_name": 'FREQUENCE_IMMERSION_VALUES',
                "default_value": True,
                "key": 'freq_imm_id',
                "relation": 0,
                "import_value": 'freq_imm_lib',
                "table": 'referentiel.freq_immersion'
            },
            {
                "list_name": 'HUMIDITE_VALUES',
                "default_value": True,
                "key": 'humidite_id',
                "relation": 0,
                "import_value": 'humidite_lib',
                "table": 'referentiel.ref_humidite'
            },
            {
                "list_name": 'INDICE_AGREGATION_VALUES',
                "default_value": True,
                "key": 'indice_agregation_id',
                "relation": 0,
                "import_value": 'indice_agregation_libelle',
                "table": 'referentiel.ref_indice_agregation'
            },
            {
                "list_name": 'NIV_TROPHIE_VALUES',
                "default_value": True,
                "key": 'niv_trophie_id',
                "relation": 0,
                "import_value": 'niv_trophie_lib',
                "table": 'referentiel.ref_niv_trophie'
            },
            {
                "list_name": 'OMBRAGE_VALUES',
                "default_value": True,
                "key": 'ombrage_id',
                "relation": 0,
                "import_value": 'ombrage_lib',
                "table": 'referentiel.ref_ombrages'
            },
            {
                "list_name": 'PENTE_VALUES',
                "default_value": True,
                "key": 'pente_id',
                "relation": 0,
                "import_value": 'pente_lib',
                "table": 'referentiel.ref_pentes'
            },
            {
                "list_name": 'PH_SOL_VALUES',
                "default_value": True,
                "key": 'ph_sol_id',
                "relation": 0,
                "import_value": 'ph_sol_lib',
                "table": 'referentiel.ref_ph_sol'
            },
            {
                "list_name": 'PROTOCOLE_VALUES',
                "default_value": True,
                "key": 'id',
                "relation": 'type_rel_id',
                "import_value": 'libelle',
                "table": 'referentiel.ref_protocole_releve',
                "where": 'id NOT IN (3, 8, 9, 12, 13, 14)'
            },
            {
                "list_name": 'QUALITE_RELEVE_VALUES',
                "default_value": True,
                "key": 'qual_r_id',
                "relation": 0,
                "import_value": 'qual_r_lib',
                "table": 'referentiel.ref_qualites_rel'
            },
            {
                "list_name": 'REACTION_HCL_VALUES',
                "default_value": True,
                "key": 'id',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'referentiel.ref_react_hcl'
            },
            {
                "list_name": 'RECOUVREMENT_VALUES',
                "default_value": True,
                "key": 'ind_ab_id',
                "relation": 0,
                "import_value": 'ind_ab_lib',
                "table": 'referentiel.indice_abondance',
                "where": 'ind_ab_id <= 10 AND ind_ab_id != 1'
            },
            {
                "list_name": 'ROCHE_MERE_VALUES',
                "default_value": True,
                "key": 'roche_mere_id',
                "value": "CONCAT(roche_mere_id::varchar(255), ' - ', roche_mere_lib)",
                "relation": 'CASE WHEN LENGTH(roche_mere_id::varchar(255)) = 1 THEN 1'
                            'WHEN LENGTH(roche_mere_id::varchar(255)) = 2 THEN 2'
                            'WHEN LENGTH(roche_mere_id::varchar(255)) = 4 THEN 3'
                            'WHEN LENGTH(roche_mere_id::varchar(255)) = 5 THEN 4 END',
                "import_value": 'roche_mere_lib',
                "table": 'referentiel.roche_mere',
                "where": 'roche_mere_id < 100000',
                "order_by": 'roche_mere_id::varchar(255)'
            },
            {
                "list_name": 'SALINITE_VALUES',
                "default_value": True,
                "key": 'salinite_id',
                "relation": 0,
                "import_value": 'salinite_lib',
                "table": 'referentiel.ref_salinite'
            },
            {
                "list_name": 'STATUT_DETERMINATION_VALUES',
                "default_value": True,
                "key": 'stat_det_id',
                "relation": 0,
                "import_value": 'stat_det_code',
                "table": 'public.statut_det',
                "where": 'stat_det_id NOT IN (103, 104, 105)'
            },
            {
                "list_name": 'STATUT_PHENOLOGIQUE_VALUES',
                "default_value": True,
                "key": 'stat_pheno_id',
                "relation": 0,
                "import_value": 'stat_pheno_libelle',
                "table": 'public.statut_pheno'
            },
            {
                "list_name": 'STATUT_SPONTANEITE_VALUES',
                "default_value": True,
                "key": 'stat_spon_id',
                "relation": 0,
                "import_value": 'stat_spon_libelle',
                "table": 'public.statut_spon'
            },
            {
                "list_name": 'STRATES_VALUES',
                "default_value": True,
                "key": 'strate_id',
                "value": "CONCAT('(', strate_abrv, ') ', strate_lib)",
                "relation": 0,
                "import_value": 'strate_lib',
                "table": 'referentiel.ref_strates_phyto',
                "where": 'strate_id NOT IN (2, 3, 5, 6, 9, 10, 11)'
            },
            {
                "list_name": 'SUPPORT_VALUES',
                "default_value": True,
                "key": 'support_id',
                "relation": 0,
                "import_value": 'support_lib',
                "table": 'referentiel.ref_support'
            },
            {
                "list_name": 'TAILLE_COURS_EAU_VALUES',
                "default_value": True,
                "key": 'id',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'referentiel.ref_taille_piece_eau'
            },
            {
                "list_name": 'TECHNIQUE_COLLECTE_VALUES',
                "default_value": True,
                "key": 'id',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'ref_habitat.ref_soh_technique_collecte',
                "where": 'id IN (2, 17, 19, 28, 30, 32, 33, 34)'
            },
            {
                "list_name": 'TEXTURE_SOL_VALUES',
                "default_value": True,
                "key": 'texture_id',
                "relation": 0,
                "import_value": 'texture_lib',
                "table": 'referentiel.texture',
                "where": 'texture_id IN (11, 12, 13)'
            },
            {
                "list_name": 'THERMIE_VALUES',
                "default_value": True,
                "key": 'id',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'referentiel.ref_thermie'
            },
            {
                "list_name": 'TOPOGRAPHIE_VALUES',
                "default_value": True,
                "key": 'topo_id',
                "relation": 0,
                "import_value": 'topo_lib',
                "table": 'referentiel.topographie',
                "where": 'topo_id != 17'
            },
            {
                "list_name": 'TYPE_BIO_VALUES',
                "default_value": True,
                "key": 't_bio_id',
                "relation": 0,
                "import_value": 't_bio_lib',
                "table": 'referentiel.type_biologique'
            },
            {
                "list_name": 'TYPE_DETERMINATION_VALUES',
                "default_value": True,
                "key": 'id',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'referentiel.ref_type_determination'
            },
            {
                "list_name": 'TYPE_FORME_VALUES',
                "default_value": True,
                "key": 'id',
                "value": "CASE WHEN symbole IS NOT NULL THEN CONCAT(symbole, ' - ', libelle) ELSE libelle END",
                "relation": 0,
                "import_value": 'libelle',
                "table": 'referentiel.ref_forme'
            },
            {
                "list_name": 'TYPE_PHYSIO_VALUES',
                "default_value": True,
                "key": 'physio_id',
                "relation": 0,
                "import_value": 'physio_lib',
                "table": 'referentiel.physionomie',
                "where": 'physio_id != 28'
            },
            {
                "list_name": 'TYPE_REL_VALUES',
                "default_value": True,
                "key": 'id',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'referentiel.ref_type_releve',
            },
            {
                "list_name": 'TYPE_SOL_VALUES',
                "default_value": True,
                "key": 't_sol_id',
                "relation": 0,
                "import_value": 't_sol_lib',
                "table": 'referentiel.type_sol'
            },
            {
                "list_name": 'TYPICITE_VALUES',
                "default_value": True,
                "key": 'typicite_id',
                "relation": 0,
                "import_value": 'typicite_lib',
                "table": 'referentiel.typicite'
            },
            {
                "list_name": 'UNITE_VEGE_VALUES',
                "default_value": True,
                "key": 'id_unite_vege',
                "relation": 0,
                "import_value": 'nom_unite_vege',
                "table": 'habitat.unite_vegetation'
            },
            {
                "list_name": 'VITESSE_EAU_VALUES',
                "default_value": True,
                "key": 'id',
                "relation": 0,
                "import_value": 'libelle',
                "table": 'referentiel.ref_vitesse_eau'
            }
        ]

    def get_list_values_from_pg(self):
        sql_list = []
        for lv in self.list_values_dict:
            if lv['default_value']:
                sql_list.append(self.select_default_value(lv['list_name']))
            if 'table' in lv:
                value = lv['value'] if 'value' in lv else lv['import_value']
                where = lv['where'] if 'where' in lv else None
                order_by = lv['order_by'] if 'order_by' in lv else None
                sql_list.append(self.select_list_values_from_pg(lv['list_name'], lv['key'], value, lv['relation'], lv['import_value'], lv['table'], where=where, order_by=order_by))
        sql = ' UNION ALL '.join(sql_list)
        self.pg_cur.execute(sql)
        fieldnames = [field[0] for field in self.pg_cur.description]
        list_values = self.pg_cur.fetchall()
        return list_values, fieldnames

    @staticmethod
    def select_list_values_from_pg(list_name, key, value, relation, import_value, table, where=None, order_by=None):
        sql = f"""SELECT '{list_name}' AS list_name, {key} AS key, {value} AS value, {str(relation)} AS relation, {import_value} AS import_value
        FROM {table}"""
        sql_where = f" WHERE {where}" if where is not None else ""
        sql_order_by = f" ORDER BY {order_by}" if order_by is not None else f" ORDER BY key"
        return "(" + sql + sql_where + sql_order_by + ")"

    @staticmethod
    def select_default_value(list_name):
        sql = f"SELECT '{list_name}' AS list_name, 0 AS key, '' AS value, 0 AS relation, '' AS import_value"
        return sql

    def save_list_values_into_csv(self, csv_file_name, list_values, fieldnames):
        with open(self.home_path / csv_file_name, 'w', encoding='utf-8') as output_file:
            line = ';'.join(header for header in fieldnames) + '\n'
            output_file.write(line)
            for value in list_values:
                value = ['"'+str(v).replace('"', '""')+'"' if v not in [None] else '' for v in value]  # Guillemets doubles pour les retours à la ligne et les ;
                line = ';'.join(v for v in value) + '\n'
                output_file.write(line)

    def build_target_dict(self, file_name: str) -> List[tuple]:
        target_data: List[tuple]
        with open(self.home_path / file_name, 'r', encoding='utf-8') as file:
            data = csv.DictReader(file, delimiter=';')
            target_data = [(line['list_name'], line['key'], line['value'], line['relation'], line['import_value']) for line in data]
            # target_data = [tuple(list(line.values())) for line in data]
        return target_data

    def delete_current_content(self) -> bool:
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.execute('DELETE FROM listes_valeurs;')
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def import_new_list_values(self, data) -> bool:
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.executemany('INSERT INTO listes_valeurs (list_name, key, value, relation, import_value)'
                                        ' VALUES (?, ?, ?, ?, ?);', data)
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def process_pg_to_sqlite(self, pg_file_name: str, csv_file_name: str, sqlite_file_name: str):
        self.pg_connection(pg_file_name)
        list_values, fieldnames = self.get_list_values_from_pg()
        self.save_list_values_into_csv(csv_file_name, list_values, fieldnames)
        self.sqlite_connection(sqlite_file_name)
        if len(list_values) > 0 and self.sqlite_cur:
            self.delete_current_content()
            self.import_new_list_values(list_values)
        self.pg_conn.close()
        self.sqlite_conn.close()

    def process_pg_to_csv(self, pg_file_name: str, csv_file_name: str):
        self.pg_connection(pg_file_name)
        list_values, fieldnames = self.get_list_values_from_pg()
        self.save_list_values_into_csv(csv_file_name, list_values, fieldnames)
        self.pg_conn.close()

    def process_csv_to_sqlite(self, csv_file_name: str, sqlite_file_name: str):
        self.sqlite_connection(sqlite_file_name)
        # self.sqlite_cur.execute('ALTER TABLE listes_valeurs ADD import_value TEXT;')
        # self.sqlite_conn.commit()
        target_data = self.build_target_dict(csv_file_name)
        if len(target_data) > 0 and self.sqlite_cur:
            self.delete_current_content()
            self.import_new_list_values(target_data)
        self.sqlite_conn.close()


if __name__ == '__main__':
    ingest_service = ListValuesIngestService()
    if sys.argv[1] == '-pg-to-sqlite':
        ingest_service.process_pg_to_sqlite('.lobelia_db.env', sys.argv[2], 'donnees_habitats.sqlite')
    elif sys.argv[1] == '-pg-to-csv':
        ingest_service.process_pg_to_csv('.lobelia_db.env', sys.argv[2])
    elif sys.argv[1] == '-csv-to-sqlite':
        ingest_service.process_csv_to_sqlite(sys.argv[2], 'donnees_habitats.sqlite')
    else:
        print("ERREUR : Le 1er argument (devant faire référence à la méthode d'import) n'a pas été reconnu")
