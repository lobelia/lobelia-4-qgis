import datetime
import sqlite3
import sys
from pathlib import Path
from sqlite3 import Connection, Cursor, Error, Row
from typing import Optional, List

from shapely import wkt
from shapely.geometry import MultiPolygon
from shapely.ops import unary_union

from qgis.core import QgsProject


class GeometryMergeManager:
    def __init__(self, project: QgsProject = None):
        self.project: Optional[QgsProject] = project
        if self.project is None:
            self.project = QgsProject.instance()
        self.connection: Optional[Connection] = None
        self.cursor: Optional[Cursor] = None
        self.home_path: Path = Path(self.project.homePath())

    def get_all_geom_list(self) -> List[Row]:
        result: List[Row] = []
        query_surface = "SELECT uuid, AsWKT(CastAutomagic(geom)) AS geometry FROM station_surface"
        result.extend(self.connection.execute(query_surface).fetchall())
        query_line = "SELECT uuid, AsWKT(CastAutomagic(geom)) AS geometry FROM station_ligne"
        result.extend(self.connection.execute(query_line).fetchall())
        query_point = "SELECT uuid, AsWKT(CastAutomagic(geom)) AS geometry FROM station_point"
        result.extend(self.connection.execute(query_point).fetchall())
        return result

    @staticmethod
    def row_to_dict(rows: List[Row]) -> List[dict]:
        dict_list: List[dict] = []
        for row in rows:
            dict_list.append({key: row[key] for key in row.keys()})
        return dict_list

    @staticmethod
    def list_duplicate_uuids(uuid_list: List[dict]) -> List[dict]:
        uuids: List[str] = []
        duplicates: List[str] = []
        duplicated_uuids: List[dict] = []
        for uuid in uuid_list:
            if uuid['uuid'] not in uuids:
                uuids.append(uuid['uuid'])
            elif uuid['uuid'] not in duplicates:
                duplicates.append(uuid['uuid'])
                duplicated_uuids.append({'uuid': uuid['uuid'], 'type': uuid['geometry'].split('(')[0]})
        return duplicated_uuids

    @staticmethod
    def merge_stations(uuid_list: List[dict], duplicated_uuids: List[dict]) -> List[dict]:
        merged_stations: List[dict] = []
        for uuid in duplicated_uuids:
            stations_to_merge: List[str] = [wkt.loads(station['geometry'])
                                            for station in uuid_list if station['uuid'] == uuid['uuid']]
            merged_geometry = unary_union(stations_to_merge)
            if merged_geometry.geometryType() == 'Polygon':
                merged_geometry = MultiPolygon([merged_geometry])
            merged_stations.append({'uuid': uuid['uuid'], 'geometry': merged_geometry, 'type': uuid['type']})
        return merged_stations

    @staticmethod
    def get_table_name(geom_type: str) -> str:
        table: str = ''
        if geom_type == 'MULTIPOLYGON':
            table = 'station_surface'
        elif geom_type == 'MULTILINESTRING':
            table = 'station_ligne'
        elif geom_type == 'MULTIPOINT':
            table = 'station_point'
        return table

    def delete_duplicated_uuid(self, uuid: dict):
        request = "DELETE FROM %s WHERE uuid='%s'"
        table_name = self.get_table_name(uuid['type'])
        if self.cursor and table_name != '':
            self.cursor.execute(request % (table_name, uuid['uuid']))
            self.connection.commit()

    def insert_merged_station(self, station: dict):
        request = "INSERT INTO %s (uuid, geom) VALUES ('%s', st_geomfromtext('%s', 2154))"
        table_name = self.get_table_name(station['type'])
        if self.cursor and table_name != '':
            self.cursor.execute(request % (table_name, station['uuid'], station['geometry']))
            self.connection.commit()

    def merge_geometries(self, uuid_list: List[dict]):
        duplicated_uuids: List[dict] = self.list_duplicate_uuids(uuid_list)
        merged_stations: List[dict] = self.merge_stations(uuid_list, duplicated_uuids)

        for uuid in duplicated_uuids:
            self.delete_duplicated_uuid(uuid)
            pass

        for station in merged_stations:
            self.insert_merged_station(station)

    def sqlite_connection(self, database_file_name) -> Optional[Connection]:
        try:
            self.connection = sqlite3.connect(self.home_path / database_file_name)
            self.connection.row_factory = sqlite3.Row
            self.connection.enable_load_extension(True)
            self.connection.load_extension("mod_spatialite")
        except Error as e:
            message = 'Connection failed - ' + str(e)
            print('Database connection', message)
        finally:
            if self.connection:
                self.cursor = self.connection.cursor()
            return self.connection

    def process(self):
        self.sqlite_connection('donnees_habitats.sqlite')
        self.merge_geometries(self.row_to_dict(self.get_all_geom_list()))


if __name__ == '__main__':
    print("%s - Lobelia4QGis command line script launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    if hasattr(sys, 'argv') and len(sys.argv) > 1:
        option = sys.argv[1]
        if option == 'merge':
            print("%s - merge process launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            geometry_manager = GeometryMergeManager()
            geometry_manager.process()
            print("%s - merge process ended" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        else:
            print("%s - unknown option : merge available" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    else:
        print("# usage :", "python scripts/merge_polygon_with_same_uuid.py merge")
        print("# note  :", "always launch this command from project root directory")
    print("%s - Lobelia4QGis command line script closed" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
