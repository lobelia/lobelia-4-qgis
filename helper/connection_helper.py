import sqlite3
from sqlite3 import Connection
from typing import List, Optional
from sys import platform

from qgis.core import QgsProject, QgsApplication, QgsAuthMethodConfig, QgsAuthManager
from qgis.gui import QgisInterface
from qgis.utils import iface

from ..constants.connection_constants import SQLITE_FILE, AUTH_CONFIG


class ConnectionHelper:
    def __init__(self, iface_given: QgisInterface = None):
        self.iface = iface_given
        if self.iface is None:
            self.iface = iface

    def test_authentication_config(self) -> bool:
        auth_manager: QgsAuthManager = QgsApplication.authManager()
        config_ids: List[str] = auth_manager.configIds()
        try:
            config_ids.index(AUTH_CONFIG)
        except ValueError:
            self.iface.messageBar().pushWarning('Authentification', 'votre profil Lobelia n\'est pas défini')
            return False
        self.iface.messageBar().pushInfo('Authentification', 'votre profil Lobelia est bien connu')
        return True

    def create_authentication_profile(self):
        auth_manager = QgsApplication.authManager()
        config = QgsAuthMethodConfig()
        config.setMethod('Basic')
        config.setName('lobelia_user')
        config.setId(AUTH_CONFIG)
        config.setConfig('username', 'your_email')
        config.setConfig('password', 'your_lobelia_password')
        auth_manager.storeAuthenticationConfig(config)
        self.iface.messageBar().pushInfo('Authentification', 'un exemple de profil Lobelia a été créé')

    def sqlite_connection(self, project: QgsProject) -> Optional[Connection]:
        connection: Optional[Connection] = None
        try:
            separator = "\\" if platform == "win32" else "/"
            connection = sqlite3.connect(project.homePath() + separator + SQLITE_FILE)
            connection.enable_load_extension(True)
            connection.execute('SELECT load_extension("mod_spatialite")')
            connection.execute('SELECT InitSpatialMetaData(1);')
            connection.execute('PRAGMA case_sensitive_like=ON;')
            connection.row_factory = sqlite3.Row
            connection.text_factory = lambda x: self.decode(x)
            connection.enable_load_extension(False)
        finally:
            return connection

    @staticmethod
    def decode(text) -> str:
        result: str = ''
        try:
            result = text.decode("utf-8")
        except UnicodeDecodeError:
            pass
        finally:
            return result
