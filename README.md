# Lobelia 4 QGIS
- Version de QGIS utilisée : 3.28 LTR
- Version de python utilisée pour le développement : 3.10

**Ce projet QGIS est un outil de saisie de relevés floristiques ou d'habitats au format Lobelia.**
Il permet de :

- Saisir des relevés par une entrée géographique (point/ligne/polygone)
- Décrire finement le relevé (métadonnées)
- Associer une ou plusieurs observations de taxons (rattachement possible à Taxref v16) et/ou de syntaxons (catalogue 
interne)

## Comment contribuer ?

Le code exécuté par le projet est présenté selon plusieurs fichiers organisés par répertoire, chacun accueillant le code
d'une classe python.  
QGIS ne supportant par contre pas bien ce mode de développement multi fichiers, afin de profiter des nouvelles
fonctionnalités que vous ajouterez, il vous faudra fusionner votre code au sein d'un fichier unique grace au script
fourni.  
Le fichier unique nommé `survey_form.py` sera créé à la racine du projet et pourra être déployé tel quel en production.

Pour générer ce fichier unique :

```
python3 scripts/unique_file_generator.py
```

**Nota**

- Le projet assigne déjà la méthode `form_open` de ce fichier comme code associé aux couches de relevés.
- Le fichier `survey_form.py` est commité afin de rendre le repository fonctionnel dès son clonage. Aussi il faut
  veiller à **générer un fichier propre** à chaque commit.

## Comment importer ses référentiels ?

Les commandes qui sont explicitées dans cette partie doivent être lancées **depuis la racine du projet QGIS**.

Les imports de référentiels depuis la BDD Lobelia nécessitent la création d'un fichier `.lobelia_db.env` **à la racine 
du projet QGIS** contenant les **paramètres de connexion à la BDD** :
```
[POSTGRES_CONNECTION]
HOST=
DATABASE=
USER=
PASSWORD=
PORT=
```

### Importer son catalogue phyto

* Depuis un fichier csv : lancer le script `cat_phyto_ingest_service.py` en mode `-csv-to-sqlite`

Il est nécessaire de disposer d'un fichier csv (séparateur point-virgule et encodé en UTF-8 sans BOM) contenant la 
description du référentiel syntaxonomique, avec les noms de colonnes suivants : `REF_CBNBP`, `id_syntaxon`, `syntaxon`, 
`aut`, `id_retenu`, `id_supra`, `pres_CBNBP`. Ils peuvent bien sûr être adaptés dans le script.
```
python3 ingest/cat_phyto_ingest_service.py -csv-to-sqlite resources/Ref_syntaxons.csv
```
... avec pour 2ème argument le chemin relatif vers le fichier csv source (`resources/Ref_syntaxons.csv`).

### Importer le catalogue des habitats (Habref)

* Depuis la BDD Lobelia : lancer le script `habref_ingest_service.py` en mode `-pg-to-sqlite`
```
python3 ingest/habref_ingest_service.py -pg-to-sqlite resources/Habref.csv
```
Cette commande enregistre également les données dans un fichier csv dont le chemin relatif est précisé en 2ème argument 
(`resources/Habref.csv`).

* Depuis un fichier csv : lancer le script `habref_ingest_service.py` en mode `-csv-to-sqlite`

Il est nécessaire de disposer d'un fichier csv (séparateur point-virgule et encodé en UTF-8 sans BOM) contenant la 
description des habitats utilisés dans Lobelia, avec les noms de colonnes suivants : `cd_hab`, `cd_typo`, `lb_code`, 
`lb_hab_fr`, `cd_hab_sup`. Ils peuvent bien sûr être adaptés dans le script.
```
python3 ingest/habref_ingest_service.py -csv-to-sqlite resources/Habref.csv
```
... avec pour 2ème argument le chemin relatif vers le fichier csv source (`resources/Habref.csv`).

* Dans le cas d'un import depuis la BDD Lobelia avec **nécessité d'ajustement de données**, il est possible de procéder 
en 2 étapes :
   * D'abord enregistrer les données dans un fichier csv : lancer le script `habref_ingest_service.py` en mode `-pg-to-csv`
   ```
   python3 ingest/habref_ingest_service.py -pg-to-csv resources/Habref.csv
   ```
   ... avec pour 2ème argument le chemin relatif du fichier csv créé (`resources/Habref.csv`).
   * Puis importer les données depuis ce fichier csv : lancer le script `habref_ingest_service.py` en mode `-csv-to-sqlite`
   ```
   python3 ingest/habref_ingest_service.py -csv-to-sqlite resources/Habref.csv
   ```
   ... avec pour 2ème argument le chemin relatif vers le fichier csv source créé à l'étape précédente 
   (`resources/Habref.csv`).

### Importer la table de correspondance entre syntaxons et habitats

* Depuis un fichier csv : lancer le script `corresp_syntaxons_habitats_ingest_service.py` en mode `-csv-to-sqlite`

Il est nécessaire de disposer d'un fichier csv (séparateur point-virgule et encodé en UTF-8 sans BOM) contenant la 
correspondance entre le référentiel syntaxonomique et le référentiel habitats, avec les noms de colonnes suivants : 
`code_phyto`, `cd_typo`, `cd_hab`, `lb_code`, `lb_hab_fr`. Ils peuvent bien sûr être adaptés dans le script.
```
python3 ingest/corresp_syntaxons_habitats_ingest_service.py -csv-to-sqlite resources/Corresp_syntaxons_habitats.csv
```
... avec pour 2ème argument le chemin relatif vers le fichier csv source (`resources/Corresp_syntaxons_habitats.csv`).

### Importer le catalogue des taxons (Taxref)

* Depuis la BDD Lobelia : lancer le script `taxref_ingest_service.py` en mode `-pg-to-sqlite`
```
python3 ingest/taxref_ingest_service.py -pg-to-sqlite resources/Taxref.csv
```
Cette commande enregistre également les données dans un fichier csv dont le chemin relatif est précisé en 2ème argument 
(`resources/Taxref.csv`).

* Depuis un fichier csv : lancer le script `taxref_ingest_service.py` en mode `-csv-to-sqlite`

Il est nécessaire de disposer d'un fichier csv (séparateur point-virgule et encodé en UTF-8 sans BOM) contenant la 
description des taxons utilisés dans Lobelia, avec les noms de colonnes suivants : `cd_nom`, `lb_nom`, `cd_ref`, 
`nom_valide`. Ils peuvent bien sûr être adaptés dans le script.
```
python3 ingest/taxref_ingest_service.py -csv-to-sqlite resources/Taxref.csv
```
... avec pour 2ème argument le chemin relatif vers le fichier csv source (`resources/Taxref.csv`).

* Dans le cas d'un import depuis la BDD Lobelia avec **nécessité d'ajustement de données**, il est possible de procéder 
en 2 étapes :
   * D'abord enregistrer les données dans un fichier csv : lancer le script `taxref_ingest_service.py` en mode `-pg-to-csv`
   ```
   python3 ingest/taxref_ingest_service.py -pg-to-csv resources/Taxref.csv
   ```
   ... avec pour 2ème argument le chemin relatif du fichier csv créé (`resources/Taxref.csv`).
   * Puis importer les données depuis ce fichier csv : lancer le script `taxref_ingest_service.py` en mode `-csv-to-sqlite`
   ```
   python3 ingest/taxref_ingest_service.py -csv-to-sqlite resources/Taxref.csv
   ```
   ... avec pour 2ème argument le chemin relatif vers le fichier csv source créé à l'étape précédente 
   (`resources/Taxref.csv`).

### Importer les listes de valeurs

* Depuis la BDD Lobelia : lancer le script `list_values_ingest_service.py` en mode `-pg-to-sqlite`
```
python3 ingest/list_values_ingest_service.py -pg-to-sqlite resources/Listes_valeurs.csv
```
Cette commande enregistre également les données dans un fichier csv dont le chemin relatif est précisé en 2ème argument 
(`resources/Listes_valeurs.csv`).

* Depuis un fichier csv : lancer le script `list_values_ingest_service.py` en mode `-csv-to-sqlite`

Il est nécessaire de disposer d'un fichier csv (séparateur point-virgule et encodé en UTF-8 sans BOM) contenant la 
description des listes de valeurs utilisées dans Lobelia, avec les noms de colonnes suivants : `list_name`, `key`, 
`value`, `relation`, `import_value`. Ils peuvent bien sûr être adaptés dans le script.
```
python3 ingest/list_values_ingest_service.py -csv-to-sqlite resources/Listes_valeurs.csv
```
... avec pour 2ème argument le chemin relatif vers le fichier csv source (`resources/Listes_valeurs.csv`).

* Dans le cas d'un import depuis la BDD Lobelia avec **nécessité d'ajustement de données**, il est possible de procéder 
en 2 étapes :
   * D'abord enregistrer les données dans un fichier csv : lancer le script `list_values_ingest_service.py` en mode `-pg-to-csv`
   ```
   python3 ingest/list_values_ingest_service.py -pg-to-csv resources/Listes_valeurs.csv
   ```
   ... avec pour 2ème argument le chemin relatif du fichier csv créé (`resources/Listes_valeurs.csv`).
   * Puis importer les données depuis ce fichier csv : lancer le script `list_values_ingest_service.py` en mode `-csv-to-sqlite`
   ```
   python3 ingest/list_values_ingest_service.py -csv-to-sqlite resources/Listes_valeurs.csv
   ```
   ... avec pour 2ème argument le chemin relatif vers le fichier csv source créé à l'étape précédente 
   (`resources/Listes_valeurs.csv`).

### Importer la liste des observateurs

* Depuis la BDD Lobelia : lancer le script `observers_ingest_service.py` en mode `-pg-to-sqlite`
```
python3 ingest/observers_ingest_service.py -pg-to-sqlite resources/Observateurs.csv
```
Cette commande enregistre également les données dans un fichier csv dont le chemin relatif est précisé en 2ème argument 
(`resources/Observateurs.csv`).

* Depuis un fichier csv : lancer le script `observers_ingest_service.py` en mode `-csv-to-sqlite`

Il est nécessaire de disposer d'un fichier csv (séparateur point-virgule et encodé en UTF-8 sans BOM) contenant la liste 
des observateurs, avec les noms de colonnes suivants : `id`, `prenom`, `nom`, `email`, `active`. Ils peuvent bien sûr 
être adaptés dans le script.
```
python3 ingest/observers_ingest_service.py -csv-to-sqlite resources/Observateurs.csv
```
... avec pour 2ème argument le chemin relatif vers le fichier csv source (`resources/Observateurs.csv`).

* Dans le cas d'un import depuis la BDD Lobelia avec **nécessité d'ajustement de données**, il est possible de procéder 
en 2 étapes :
   * D'abord enregistrer les données dans un fichier csv : lancer le script `observers_ingest_service.py` en mode `-pg-to-csv`
   ```
   python3 ingest/observers_ingest_service.py -pg-to-csv resources/Observateurs.csv
   ```
   ... avec pour 2ème argument le chemin relatif du fichier csv créé (`resources/Observateurs.csv`).
   * Puis importer les données depuis ce fichier csv : lancer le script `observers_ingest_service.py` en mode `-csv-to-sqlite`
   ```
   python3 ingest/observers_ingest_service.py -csv-to-sqlite resources/Observateurs.csv
   ```
   ... avec pour 2ème argument le chemin relatif vers le fichier csv source créé à l'étape précédente 
   (`resources/Observateurs.csv`).
   
### Importer la liste des jeux de données

* Depuis la BDD Lobelia : lancer le script `jdd_ingest_service.py` en mode `-pg-to-sqlite`
```
python3 ingest/jdd_ingest_service.py -pg-to-sqlite resources/Jdd.csv
```
Cette commande enregistre également les données dans un fichier csv dont le chemin relatif est précisé en 2ème argument 
(`resources/Jdd.csv`).

* Depuis un fichier csv : lancer le script `jdd_ingest_service.py` en mode `-csv-to-sqlite`

Il est nécessaire de disposer d'un fichier csv (séparateur point-virgule et encodé en UTF-8 sans BOM) contenant la liste 
des jeux de données, avec les noms de colonnes suivants : `id`, `identifiant_jdd`, `libelle_court`, `libelle`, 
`description`, `date_debut`, `date_fin`. Ils peuvent bien sûr être adaptés dans le script.
```
python3 ingest/jdd_ingest_service.py -csv-to-sqlite resources/Jdd.csv
```
... avec pour 2ème argument le chemin relatif vers le fichier csv source (`resources/Jdd.csv`).

* Dans le cas d'un import depuis la BDD Lobelia avec **nécessité d'ajustement de données**, il est possible de procéder 
en 2 étapes :
   * D'abord enregistrer les données dans un fichier csv : lancer le script `jdd_ingest_service.py` en mode `-pg-to-csv`
   ```
   python3 ingest/jdd_ingest_service.py -pg-to-csv resources/Jdd.csv
   ```
   ... avec pour 2ème argument le chemin relatif du fichier csv créé (`resources/Jdd.csv`).
   * Puis importer les données depuis ce fichier csv : lancer le script `jdd_ingest_service.py` en mode `-csv-to-sqlite`
   ```
   python3 ingest/jdd_ingest_service.py -csv-to-sqlite resources/Jdd.csv
   ```
   ... avec pour 2ème argument le chemin relatif vers le fichier csv source créé à l'étape précédente 
   (`resources/Jdd.csv`).

## Comment exporter un fichier csv pour la cartographie ?

Cette fonction est disponible depuis l'interface : formulaire `Relevé`, onglet `Informations générales`, bloc rétractable
`Opérations en base de données`.  

Toutefois il est possible de lancer une extraction en ligne de commande également :  
- Lancer le script `survey_form.py` depuis la console avec l'option `extract_carto` :
    ```
    python3 survey_form.py extract_carto
    ```
- Le fichier csv est sauvegardé à la racine du projet sous le nom `carto_export.csv` 

## Comment exporter un tableau phyto au format csv ?

Cette fonction est disponible depuis l'interface : formulaire `Relevé`, onglet `Informations générales`, bloc rétractable 
`Opérations en base de données`.

Toutefois il est possible de lancer une extraction en ligne de commande également :  
- Lancer le script `survey_form.py` depuis la console avec l'option `extract_phyto` :
    ```
    python3 survey_form.py extract_phyto
    ```
- Le fichier csv est sauvegardé à la racine du projet sous le nom `phyto_table.csv`

## Comment nettoyer une base de données L4Q ?

Cette fonction est disponible depuis l'interface : formulaire `Relevé`, onglet `Informations générales`, bloc rétractable 
`Opérations en base de données`.

Toutefois il est possible de lancer une extraction en ligne de commande également :  
- Lancer le script `survey_form.py` depuis la console avec l'option `clean` :
    ```
    python3 survey_form.py clean
    ```

## Comment générer un paquet de livrables ?

1. Lancer le script `release_package_generator.py` depuis la console.  
   Le script est toujours à lancer depuis la racine du projet QGIS :
    ```
    python3 scripts/release_package_generator.py
    ```
2. Un fichier `lobelia_qgis_vX.X.zip` est généré à la racine du projet contenant l'ensemble des fichiers nécessaires au
   déploiement de l'application. Ce fichier peut être utilisé comme source attachée à la déclaration d'une version sur 
   le dépôt Gitlab.

## Comment générer la documentation utilisateur ?

La notice d'installation et la documentation utilisateur sont disponibles dans le
[wiki](https://gitlab.com/lobelia/lobelia-4-qgis/-/wikis/home) du dépôt.  
Afin de pouvoir embarquer cette documentation sur le terrain, il est nécessaire de transformer en pdf ces ressources
gérées via Gitlab sous la forme de fichiers markdown.  
_Pré-requis : le poste qui lance le script doit disposer du package [ghostscript](https://www.ghostscript.com/)._  

1. Cloner dans un nouveau répertoire **en dehors du projet actuel** le wiki Gitlab :
   ```
    git clone git@gitlab.com:lobelia/lobelia-4-qgis.wiki.git
    ```
   Si vous avez déjà cloné ce repo, n'oubliez pas de le **mettre à jour** (`git pull`) avant de passer aux étapes
   suivantes !

2. Ajuster la variable `wiki_repo_path` dans le script `pdf_documentation_generator.py`:
   ```
   wiki_repo_path = <my path to the wiki repo>
   ```
3. Lancer le script suivant depuis la racine du projet en console :
    ```
    python3 scripts/pdf_documentation_generator.py
    ```
4. 2 fichiers sont générés à la racine du projet : `installation.pdf` et `documentation-utilisateur.pdf`.  
   Ces fichiers seront embarqués dans le paquet de livrables décrit ci-dessus.  
