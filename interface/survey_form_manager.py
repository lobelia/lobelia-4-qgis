from typing import Optional, List

from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QDesktopServices, QFont
from PyQt5.QtWidgets import QFrame, QLabel, QTableWidget, QLineEdit, QComboBox, QGroupBox, QListWidget, QTabWidget
from PyQt5.QtWidgets import QMessageBox, QCheckBox, QButtonGroup, QApplication, QWidget, QDialog
from qgis.core import QgsFeature, QgsVectorLayer, QgsProject
from qgis.gui import QgsAttributeForm
from qgis.utils import iface
from PyQt5.QtCore import QDate, Qt

from .survey_widget_service import WidgetService
from ..constants.header_labels_values import *
from ..data.database_specification_service import DatabaseSpecificationService
from ..data.geometry_manager import GeometryManager
from ..data.list_values_provider import ListValuesProvider
from ..data.sqlite_database_cleaner import DatabaseCleaner
from ..data.survey_data_manager import SurveyDataManager
from ..extract.carto_extract_service import CartoExtractService
from ..extract.obs_hab_extract_service import ObsHabExtractService
from ..extract.phyto_table_extract_service import PhytoTableExtractService


class SurveyFormManager:
    def __init__(self, form: QgsAttributeForm, layer: QgsVectorLayer, feature: QgsFeature):
        self.form = form
        self.layer = layer
        self.feature = feature
        self.project: QgsProject = QgsProject.instance()
        self.data_manager: SurveyDataManager = SurveyDataManager(self.project, form, layer, feature)
        self.geometry_manager: GeometryManager = GeometryManager(layer, feature)
        self.list_values_provider: ListValuesProvider = ListValuesProvider(self.project)
        self.survey_feature: Optional[QgsFeature] = None

        # TODO replace this enumeration of widgets by a big dict, populate by specifications ?
        # global variables to store widgets
        self.frel: Optional[QFrame] = None  # self.frel: Optional[QFrame] = self.form.findChild(QFrame, "frel")
        self.fobs: Optional[QFrame] = None
        self.fgen: Optional[QFrame] = None
        self.fobs_taxons: Optional[QFrame] = None
        self.fobs_syntaxons: Optional[QFrame] = None
        self.frame_syntaxon_complement: Optional[QFrame] = None
        self.frame_syntaxon_data_add: Optional[QFrame] = None

        self.diag_originale: Optional[QCheckBox] = None

        self.labrel: Optional[QLabel] = None
        self.labobs: Optional[QLabel] = None
        self.labgen: Optional[QLabel] = None
        self.save_rel: Optional[QLabel] = None
        self.activate: Optional[QLabel] = None
        self.add_taxon: Optional[QLabel] = None
        self.save_taxon: Optional[QLabel] = None
        self.undo_taxon: Optional[QLabel] = None
        self.add_syntaxon: Optional[QLabel] = None
        self.save_syntaxon: Optional[QLabel] = None
        self.add_related_habitats: Optional[QLabel] = None
        self.undo_syntaxon: Optional[QLabel] = None
        self.cal1_label: Optional[QLabel] = None
        self.cal2_label: Optional[QLabel] = None
        self.rel_create_new_polygon_rel: Optional[QLabel] = None
        self.rel_create_new_line_rel: Optional[QLabel] = None
        self.rel_create_new_point_rel: Optional[QLabel] = None
        self.rel_syntax_create_new_polygon_rel: Optional[QLabel] = None
        self.rel_syntax_create_new_line_rel: Optional[QLabel] = None
        self.rel_syntax_create_new_point_rel: Optional[QLabel] = None
        self.add_obs_btn: Optional[QLabel] = None
        self.suppr_obs_btn: Optional[QLabel] = None
        self.add_strate_struct: Optional[QLabel] = None
        self.add_facteur: Optional[QLabel] = None
        self.wiki_link: Optional[QLabel] = None
        self.extract_phyto_table: Optional[QLabel] = None
        self.extract_carto: Optional[QLabel] = None
        self.extract_obs_hab: Optional[QLabel] = None
        self.clean_db: Optional[QLabel] = None
        self.replicate_jdd: Optional[QLabel] = None
        self.jdd_add: Optional[QLabel] = None
        self.jdd_delete: Optional[QLabel] = None

        self.rel_date_saisie: Optional[QLineEdit] = None
        self.rel_date_min: Optional[QLineEdit] = None
        self.rel_date_max: Optional[QLineEdit] = None
        self.taxon_cite: Optional[QLineEdit] = None
        self.taxon_valide: Optional[QLineEdit] = None
        self.taxon_cd_nom_cite: Optional[QLineEdit] = None
        self.taxon_cd_nom_valide: Optional[QLineEdit] = None
        self.obs_hab_nom_retenu: Optional[QLineEdit] = None
        self.obs_hab_code_retenu: Optional[QLineEdit] = None
        self.obs_hab_nom_cite: Optional[QLineEdit] = None
        self.obs_hab_code_cite: Optional[QLineEdit] = None
        self.syntaxon_retenu: Optional[QLineEdit] = None
        self.syntaxon_code_retenu: Optional[QLineEdit] = None
        self.syntaxon_cite: Optional[QLineEdit] = None
        self.syntaxon_code_cite: Optional[QLineEdit] = None
        self.syntaxon_delete: Optional[QLabel] = None
        self.obs_hab_code_typo: Optional[QLineEdit] = None
        self.struct_rec_pct_strate: Optional[QLineEdit] = None
        self.struct_haut_mod_strate: Optional[QLineEdit] = None
        self.struct_haut_min_strate: Optional[QLineEdit] = None
        self.struct_haut_max_strate: Optional[QLineEdit] = None
        self.taxon_dominant_code: Optional[QLineEdit] = None
        self.taxon_dominant_nom: Optional[QLineEdit] = None
        self.obs_hab_taxon_dominant_code: Optional[QLineEdit] = None
        self.obs_hab_taxon_dominant_nom: Optional[QLineEdit] = None
        self.obs_hab_surface: Optional[QLineEdit] = None
        self.obs_hab_surface_label: Optional[QLabel] = None
        self.active_jdd: Optional[QLineEdit] = None
        self.taches_redox: Optional[QLineEdit] = None
        self.horizon_redox: Optional[QLineEdit] = None
        self.horizon_reduct: Optional[QLineEdit] = None

        self.obs_hab_action_buttons: Optional[QFrame] = None

        self.ctrl: Optional[QGroupBox] = None
        self.hab_subform_data_add_box: Optional[QGroupBox] = None

        self.habitat_buttons: Optional[QButtonGroup] = None
        self.typo_habitat_buttons: Optional[QButtonGroup] = None

        self.observer_list: Optional[QListWidget] = None
        self.obs_hab_assoc_list: Optional[QListWidget] = None
        self.related_habitats_list: Optional[QListWidget] = None

        self.lst_obs: Optional[QComboBox] = None
        self.select_taxon: Optional[QComboBox] = None
        self.select_habitat: Optional[QComboBox] = None
        self.select_strate_struct: Optional[QComboBox] = None
        self.select_facteur: Optional[QComboBox] = None
        self.select_impact_facteur: Optional[QComboBox] = None
        self.select_etat_facteur: Optional[QComboBox] = None
        self.rel_type_rel_id: Optional[QComboBox] = None
        self.rel_protocole_id: Optional[QComboBox] = None
        self.syntaxon_recherche: Optional[QComboBox] = None
        self.habitat_recherche: Optional[QComboBox] = None
        self.syntaxon_comm_non_sat_id: Optional[QComboBox] = None
        self.syntaxon_comm_non_sat_obs: Optional[QComboBox] = None
        self.taxon_dominant_recherche: Optional[QComboBox] = None
        self.select_taxon_dominant: Optional[QComboBox] = None
        self.jdd_list: Optional[QComboBox] = None

        self.syntaxon_diag_originale: Optional[QCheckBox] = None
        self.pres_taches_redox: Optional[QCheckBox] = None
        self.pres_horizon_redox: Optional[QCheckBox] = None
        self.pres_horizon_reduct: Optional[QCheckBox] = None

        self.cal1: QMessageBox = QMessageBox()
        self.cal2: QMessageBox = QMessageBox()

        self.table_taxons: Optional[QTableWidget] = None
        self.table_habitats: Optional[QTableWidget] = None
        self.table_syn_habitats: Optional[QTableWidget] = None
        self.table_strates_struct: Optional[QTableWidget] = None
        self.table_facteurs: Optional[QTableWidget] = None

        self.tab_widget: Optional[QTabWidget] = None

        self.releves: Optional[QgsVectorLayer] = None
        self.observations: Optional[QgsVectorLayer] = None
        self.obs_habitats: Optional[QgsVectorLayer] = None
        self.liens_releve_observateur: Optional[QgsVectorLayer] = None
        self.liens_releve_influence: Optional[QgsVectorLayer] = None
        self.liens_releve_strate: Optional[QgsVectorLayer] = None
        self.liens_obs_habitat: Optional[QgsVectorLayer] = None
        self.observateurs: Optional[QgsVectorLayer] = None
        self.catalogue_phyto: Optional[QgsVectorLayer] = None
        self.habref: Optional[QgsVectorLayer] = None
        self.taxref: Optional[QgsVectorLayer] = None
        self.version: Optional[QgsVectorLayer] = None
        self.jdd: Optional[QgsVectorLayer] = None

    def initialize(self):
        self.define_window_size_and_font()
        self.layer.startEditing()
        self.populate_variables()
        self.populate_layers()
        self.populate_combo_boxes()
        version = [i['version'] for i in self.version.getFeatures()][0]  # version = next(self.version.getFeatures())['version']
        self.form.setWindowTitle('Lobelia4Qgis - version ' + str(version))

        add_survey_to_syntaxon_widgets = [self.rel_syntax_create_new_point_rel, self.rel_syntax_create_new_line_rel,
                                          self.rel_syntax_create_new_polygon_rel]

        if self.releves is not None:
            self.survey_feature = self.geometry_manager.get_survey_feature(self.releves)
        else:
            iface.messageBar().pushWarning('Layer error', 'la couche releves n\'est pas disponible')

        self.prepare_table_widget(add_survey_to_syntaxon_widgets)
        self.populate_form()
        self.activate_event_listeners(add_survey_to_syntaxon_widgets)
        self.prepare_startup_interface(add_survey_to_syntaxon_widgets)

    def define_window_size_and_font(self):
        QApplication.setFont(QFont('Tahoma', 10, QFont.Normal))
        available_screen_geometry = QApplication.primaryScreen().availableGeometry()
        dialog_box: QDialog = self.form.findChild(QDialog, "Dialog")
        scroll_area: QWidget = self.form.findChild(QWidget, "scrollAreaWidgetContents")
        self.form.setMaximumWidth(int(available_screen_geometry.width() * 0.8))
        self.form.setMaximumHeight(int(available_screen_geometry.height() * 0.95))
        dialog_box.setMaximumWidth(int(available_screen_geometry.width() * 0.8))
        dialog_box.setMaximumHeight(int(available_screen_geometry.height() * 0.95))
        scroll_area.setMaximumWidth(int(available_screen_geometry.width() * 0.79))
        scroll_area.setMaximumHeight(int(available_screen_geometry.height() * 0.94))

    def populate_form(self):
        feature_id = 0
        if self.survey_feature:
            feature_id = self.survey_feature.id()
        if feature_id > 0:
            self.data_manager.map_survey(self.releves, self.survey_feature)
            self.data_manager.get_habitats(feature_id, self.obs_habitats, self.table_habitats, self.obs_hab_assoc_list,
                                           self.habitat_buttons, self.obs_hab_code_typo, self.frame_syntaxon_complement,
                                           self.frame_syntaxon_data_add, self.obs_hab_action_buttons)
            self.data_manager.get_observers(feature_id, self.liens_releve_observateur, self.lst_obs, self.observer_list)
            self.data_manager.get_jdd(self.survey_feature, self.active_jdd)
            self.data_manager.get_taxons(feature_id, self.observations, self.table_taxons)
            self.data_manager.get_syntaxon(self.form, self.survey_feature['rel_obs_syntaxon_id'], self.obs_habitats)
            self.data_manager.get_typo_habitats(self.survey_feature, self.obs_habitats, self.table_syn_habitats)  # self.survey_feature['rel_obs_syntaxon_id']
            self.data_manager.get_strates(feature_id, self.liens_releve_strate, self.table_strates_struct)
            self.data_manager.get_factors(feature_id, self.liens_releve_influence, self.table_facteurs)
        else:
            self.rel_type_rel_id.setCurrentIndex(self.rel_type_rel_id.findData(0))

    def populate_variables(self):
        # TODO replace this enumeration by a loop reading specifications
        self.frel = self.form.findChild(QFrame, "frel")
        self.fobs = self.form.findChild(QFrame, "fobs")
        self.fgen = self.form.findChild(QFrame, "fgen")
        self.fobs_taxons = self.form.findChild(QFrame, "frame_obs_taxons")
        self.fobs_syntaxons = self.form.findChild(QFrame, "frame_obs_habitats")
        self.frame_syntaxon_complement = self.form.findChild(QFrame, "frame_syntaxon_complement")
        self.frame_syntaxon_data_add = self.form.findChild(QFrame, "frame_obs_hab_data_add_syntaxon")
        self.diag_originale = self.form.findChild(QCheckBox, "syn_diag_originale")
        self.labrel = self.form.findChild(QLabel, "lab_form_rel")
        self.labobs = self.form.findChild(QLabel, "lab_form_obs")
        self.labgen = self.form.findChild(QLabel, "lab_form_rel_gen")
        self.save_rel = self.form.findChild(QLabel, "save_releve")
        self.activate = self.form.findChild(QLabel, "edit_releve")
        self.add_taxon = self.form.findChild(QLabel, "add_taxon")
        self.save_taxon = self.form.findChild(QLabel, "save_taxon")
        self.undo_taxon = self.form.findChild(QLabel, "undo_taxon")
        self.add_syntaxon = self.form.findChild(QLabel, "lb_add_syntaxon")
        self.save_syntaxon = self.form.findChild(QLabel, "lb_save_syntaxon")
        self.add_related_habitats = self.form.findChild(QLabel, "lb_add_related_habitats")
        self.undo_syntaxon = self.form.findChild(QLabel, "lb_undo_syntaxon")
        self.cal1_label = self.form.findChild(QLabel, "calendrier_min")
        self.cal2_label = self.form.findChild(QLabel, "calendrier_max")
        self.rel_create_new_polygon_rel = self.form.findChild(QLabel, "rel_create_new_polygon_rel")
        self.rel_create_new_line_rel = self.form.findChild(QLabel, "rel_create_new_ligne_rel")
        self.rel_create_new_point_rel = self.form.findChild(QLabel, "rel_create_new_point_rel")
        self.rel_syntax_create_new_polygon_rel = self.form.findChild(QLabel, "rel_syntax_create_new_polygon_rel")
        self.rel_syntax_create_new_line_rel = self.form.findChild(QLabel, "rel_syntax_create_new_ligne_rel")
        self.rel_syntax_create_new_point_rel = self.form.findChild(QLabel, "rel_syntax_create_new_point_rel")
        self.add_obs_btn = self.form.findChild(QLabel, "add_obs_btn")
        self.suppr_obs_btn = self.form.findChild(QLabel, "suppr_obs_btn")
        self.add_strate_struct = self.form.findChild(QLabel, "add_strate_struct")
        self.add_facteur = self.form.findChild(QLabel, "add_facteur")
        self.wiki_link = self.form.findChild(QLabel, "wiki")
        self.extract_phyto_table = self.form.findChild(QLabel, "extract_phyto_table")
        self.extract_carto = self.form.findChild(QLabel, "extract_carto")
        self.extract_obs_hab = self.form.findChild(QLabel, "extract_obs_hab")
        self.clean_db = self.form.findChild(QLabel, "clean_db")
        self.replicate_jdd = self.form.findChild(QLabel, "replicate_jdd")
        self.jdd_add = self.form.findChild(QLabel, "jdd_add")
        self.jdd_delete = self.form.findChild(QLabel, "jdd_delete")
        self.rel_date_saisie = self.form.findChild(QLineEdit, "rel_date_saisie")
        self.rel_date_saisie.setText(QDate.currentDate().toString(Qt.ISODate))

        self.rel_date_min = self.form.findChild(QLineEdit, "rel_date_min")
        self.rel_date_max = self.form.findChild(QLineEdit, "rel_date_max")
        self.taxon_cite = self.form.findChild(QLineEdit, "nom_cite")
        self.taxon_valide = self.form.findChild(QLineEdit, "nom_valide")
        self.taxon_cd_nom_cite = self.form.findChild(QLineEdit, "cd_nom_cite")
        self.taxon_cd_nom_valide = self.form.findChild(QLineEdit, "cd_nom_valide")
        self.obs_hab_nom_retenu = self.form.findChild(QLineEdit, "obs_habitat_nom_retenu")
        self.obs_hab_code_retenu = self.form.findChild(QLineEdit, "obs_habitat_code_retenu")
        self.obs_hab_nom_cite = self.form.findChild(QLineEdit, "obs_habitat_nom_cite")
        self.obs_hab_code_cite = self.form.findChild(QLineEdit, "obs_habitat_code_cite")
        self.syntaxon_retenu = self.form.findChild(QLineEdit, "syntaxon_retenu")
        self.syntaxon_code_retenu = self.form.findChild(QLineEdit, "syntaxon_code_retenu")
        self.syntaxon_cite = self.form.findChild(QLineEdit, "syntaxon_cite")
        self.syntaxon_code_cite = self.form.findChild(QLineEdit, "syntaxon_code_cite")
        self.syntaxon_delete = self.form.findChild(QLabel, "syntaxon_delete")
        self.obs_hab_code_typo = self.form.findChild(QLineEdit, "obs_hab_code_typo")
        self.struct_rec_pct_strate = self.form.findChild(QLineEdit, "strate_rec_pct")
        self.struct_haut_mod_strate = self.form.findChild(QLineEdit, "strate_haut_mod")
        self.struct_haut_min_strate = self.form.findChild(QLineEdit, "strate_haut_min")
        self.struct_haut_max_strate = self.form.findChild(QLineEdit, "strate_haut_max")
        self.syntaxon_comm_non_sat_id = self.form.findChild(QComboBox, "syntaxon_comm_non_sat_id")
        self.taxon_dominant_code = self.form.findChild(QLineEdit, "syntaxon_taxon_dominant_cns_code")
        self.taxon_dominant_nom = self.form.findChild(QLineEdit, "syntaxon_taxon_dominant_cns_nom")
        self.syntaxon_comm_non_sat_obs = self.form.findChild(QComboBox, "obs_hab_comm_non_sat_obs")
        self.obs_hab_taxon_dominant_code = self.form.findChild(QLineEdit, "obs_hab_comm_non_sat_taxon_code")
        self.obs_hab_taxon_dominant_nom = self.form.findChild(QLineEdit, "obs_hab_comm_non_sat_taxon_nom")
        self.obs_hab_surface = self.form.findChild(QLineEdit, "obs_hab_surface")
        self.obs_hab_surface_label = self.form.findChild(QLabel, "obs_hab_surface_label")
        self.active_jdd = self.form.findChild(QLineEdit, "jdd")
        self.taches_redox = self.form.findChild(QLineEdit, "rel_taches_redox")
        self.horizon_redox = self.form.findChild(QLineEdit, "rel_horizon_redox")
        self.horizon_reduct = self.form.findChild(QLineEdit, "rel_horizon_reduct")
        self.obs_hab_action_buttons = self.form.findChild(QFrame, 'obs_hab_action_buttons')
        self.ctrl = self.form.findChild(QGroupBox, 'typo_groupbox')
        self.hab_subform_data_add_box = self.form.findChild(QGroupBox, 'gpbox_donnees_add')
        self.habitat_buttons = self.form.findChild(QButtonGroup, 'habitats_buttons')
        self.typo_habitat_buttons = self.form.findChild(QButtonGroup, 'habitat_buttons')
        self.observer_list = self.form.findChild(QListWidget, "observateurs_list")
        self.obs_hab_assoc_list = self.form.findChild(QListWidget, "list_hab_obs_assoc")
        self.related_habitats_list = self.form.findChild(QListWidget, "list_related_habitats")
        self.lst_obs = self.form.findChild(QComboBox, "lst_obs")
        self.select_taxon = self.form.findChild(QComboBox, "select_taxon")
        self.select_habitat = self.form.findChild(QComboBox, "select_habitat")
        self.rel_type_rel_id = self.form.findChild(QComboBox, "rel_type_rel_id")
        self.rel_protocole_id = self.form.findChild(QComboBox, "rel_protocole_id")
        self.syntaxon_recherche = self.form.findChild(QComboBox, "syntaxon_recherche")
        self.habitat_recherche = self.form.findChild(QComboBox, "habitat_recherche")
        self.select_strate_struct = self.form.findChild(QComboBox, "cb_strate_struct")
        self.select_facteur = self.form.findChild(QComboBox, "cb_facteur_influence")
        self.select_impact_facteur = self.form.findChild(QComboBox, "cb_facteur_impact")
        self.select_etat_facteur = self.form.findChild(QComboBox, "cb_facteur_etat")
        self.taxon_dominant_recherche = self.form.findChild(QComboBox, "taxon_dominant_recherche")
        self.select_taxon_dominant = self.form.findChild(QComboBox, "select_taxon_dominant")
        self.jdd_list = self.form.findChild(QComboBox, "jdd_list")
        self.syntaxon_diag_originale = self.form.findChild(QCheckBox, "syn_diag_originale")
        self.pres_taches_redox = self.form.findChild(QCheckBox, "rel_pres_taches_redox")
        self.pres_horizon_redox = self.form.findChild(QCheckBox, "rel_pres_horizon_redox")
        self.pres_horizon_reduct = self.form.findChild(QCheckBox, "rel_pres_horizon_reduct")
        self.table_habitats = self.form.findChild(QTableWidget, "table_habitats")
        self.table_syn_habitats = self.form.findChild(QTableWidget, "table_syn_habitats")
        self.table_taxons = self.form.findChild(QTableWidget, "table_taxons")
        self.table_strates_struct = self.form.findChild(QTableWidget, "table_strates_struct")
        self.table_facteurs = self.form.findChild(QTableWidget, "table_facteurs")
        self.tab_widget = self.form.findChild(QTabWidget, "tabWidget_phyto")

    def populate_layers(self):
        for layer in self.project.mapLayers().values():
            if layer.name() == 'releves':
                self.releves = layer
            elif layer.name() == 'observations':
                self.observations = layer
            elif layer.name() == 'obs_habitats':
                self.obs_habitats = layer
            elif layer.name() == 'liens_releve_observateur':
                self.liens_releve_observateur = layer
            elif layer.name() == 'liens_releve_influence':
                self.liens_releve_influence = layer
            elif layer.name() == 'liens_releve_strate':
                self.liens_releve_strate = layer
            elif layer.name() == 'liens_obs_habitat':
                self.liens_obs_habitat = layer
            elif layer.name() == 'observateurs':
                self.observateurs = layer
            elif layer.name() == 'catalogue_phyto':
                self.catalogue_phyto = layer
            elif layer.name() == 'taxref':
                self.taxref = layer
            elif layer.name() == 'habref':
                self.habref = layer
            elif layer.name() == 'version':
                self.version = layer
            elif layer.name() == 'jdd':
                self.jdd = layer

    def populate_combo_boxes(self):
        combo_boxes_specification: List[dict] = DatabaseSpecificationService.get_combo_boxes_specification()
        for combo_box_spec in combo_boxes_specification:
            for combo_box_name in combo_box_spec['input_name'].split(', '):
                widget: Optional[QComboBox] = self.form.findChild(QComboBox, combo_box_name)
                if widget is not None and combo_box_name != 'cb_facteur_influence':
                    list_values = self.list_values_provider.get_values_from_list_name(combo_box_spec['input_list_name'])
                    WidgetService.load_combo_box(widget, list_values)

    def prepare_table_widget(self, add_survey_to_syntaxon_widgets):
        WidgetService.build_table(self.table_taxons, TAXONS_HEADER_LABELS)
        self.table_taxons.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_double_clicked(i, self.releves, self.survey_feature, self.observations,
                                                        self.form, self.add_taxon, self.save_taxon, self.select_taxon,
                                                        self.habitat_buttons))
        self.table_taxons.itemClicked.connect(
            lambda i: WidgetService.cell_clicked(i, self.releves, self.survey_feature, self.observations))

        self.table_habitats.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_double_clicked(i, self.releves, self.survey_feature, self.obs_habitats,
                                                        self.form, self.add_syntaxon, self.save_syntaxon,
                                                        self.select_habitat, self.habitat_buttons,
                                                        add_survey_to_syntaxon_widgets, self.add_related_habitats))
        self.table_habitats.itemClicked.connect(
            lambda i: WidgetService.cell_clicked(i, self.releves, self.survey_feature, self.obs_habitats))

        WidgetService.build_table(self.table_syn_habitats, HABITATS_HEADER_LABELS)
        self.table_syn_habitats.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_double_clicked(i, self.releves, self.survey_feature, self.obs_habitats,
                                                        self.form, QLabel(), QLabel(), self.habitat_recherche,
                                                        QButtonGroup())
        )

        WidgetService.build_table(self.table_strates_struct, STRATES_STRUCT_LABELS)
        self.table_strates_struct.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_double_clicked(i, self.releves, self.survey_feature, self.liens_releve_strate,
                                                        self.form, QLabel(), QLabel(), self.select_strate_struct,
                                                        QButtonGroup()))

        WidgetService.build_table(self.table_facteurs, FACTORS_STRUCT_LABELS)
        self.table_facteurs.itemDoubleClicked.connect(
            lambda i: WidgetService.cell_double_clicked(i, self.releves, self.survey_feature,
                                                        self.liens_releve_influence, self.form, QLabel(), QLabel(),
                                                        self.select_facteur, QButtonGroup()))

    def activate_event_listeners(self, add_survey_to_syntaxon_widgets):
        self.rel_type_rel_id.currentIndexChanged.connect(
            lambda: WidgetService.load_protocol_list(self.rel_type_rel_id.currentData(), self.rel_protocole_id))
        self.habitat_buttons.buttonClicked.connect(
            lambda: self.data_manager.get_habitats(self.survey_feature.id(), self.obs_habitats, self.table_habitats,
                                                   self.obs_hab_assoc_list, self.habitat_buttons,
                                                   self.obs_hab_code_typo, self.frame_syntaxon_complement,
                                                   self.frame_syntaxon_data_add, self.obs_hab_action_buttons))
        self.hab_subform_data_add_box.collapsedStateChanged.connect(
            lambda: self.data_manager.refresh_hab_subform_data_add_box(self.habitat_buttons,
                                                                       self.frame_syntaxon_data_add))
        WidgetService.clickable(self.labrel).connect(
            lambda: WidgetService.fshow(self.rel_type_rel_id, self.rel_protocole_id, self.frel, self.fobs,
                                        self.save_rel, self.activate))
        WidgetService.clickable(self.labobs).connect(
            lambda: WidgetService.fshow(self.rel_type_rel_id, self.rel_protocole_id, self.frel, self.fobs,
                                        self.save_rel, self.activate))
        WidgetService.clickable(self.labgen).connect(lambda: WidgetService.fgen_show(self.fgen))
        WidgetService.clickable(self.save_rel).connect(
            lambda: self.data_manager.save_survey(self.releves, self.survey_feature, self.obs_habitats))
        WidgetService.clickable(self.activate).connect(
            lambda: self.data_manager.activate_survey(self.frel, self.fobs, self.fgen, self.save_rel, self.activate,
                                                      self.releves, self.survey_feature, self.obs_habitats,
                                                      self.obs_hab_surface, self.obs_hab_surface_label))
        WidgetService.clickable(self.add_taxon).connect(
            lambda: self.data_manager.add_taxon(self.survey_feature, self.observations, self.select_taxon,
                                                self.taxon_cite, self.taxon_cd_nom_cite, self.table_taxons,
                                                self.add_taxon, self.save_taxon))
        WidgetService.clickable(self.save_taxon).connect(
            lambda: self.data_manager.save_taxon(self.survey_feature, self.observations, self.select_taxon,
                                                 self.taxon_cite, self.taxon_cd_nom_cite, self.table_taxons,
                                                 self.add_taxon, self.save_taxon))
        WidgetService.clickable(self.undo_taxon).connect(
            lambda: self.data_manager.refresh_taxon_sub_form(self.add_taxon, self.save_taxon, self.select_taxon))
        WidgetService.clickable(self.add_syntaxon).connect(
            lambda: self.data_manager.add_habitat(self.survey_feature, self.releves, self.obs_habitats,
                                                  self.select_habitat, self.obs_hab_nom_cite, self.obs_hab_code_cite,
                                                  self.table_habitats, self.obs_hab_assoc_list, self.add_syntaxon,
                                                  self.save_syntaxon, self.habitat_buttons, self.add_related_habitats,
                                                  self.related_habitats_list))
        WidgetService.clickable(self.save_syntaxon).connect(
            lambda: self.data_manager.save_habitat(self.survey_feature, self.releves, self.obs_habitats,
                                                   self.select_habitat, self.obs_hab_nom_cite, self.obs_hab_code_cite,
                                                   self.table_habitats, self.obs_hab_assoc_list, self.add_syntaxon,
                                                   self.save_syntaxon, add_survey_to_syntaxon_widgets,
                                                   self.habitat_buttons, self.add_related_habitats,
                                                   self.related_habitats_list))
        WidgetService.clickable(self.add_related_habitats).connect(lambda: self.related_habitats_list.show())
        WidgetService.clickable(self.undo_syntaxon).connect(
            lambda: self.data_manager.refresh_habitat_sub_form(add_survey_to_syntaxon_widgets, self.add_syntaxon,
                                                               self.save_syntaxon, self.select_habitat,
                                                               self.survey_feature, self.obs_habitats,
                                                               self.obs_hab_assoc_list, self.habitat_buttons,
                                                               self.add_related_habitats, self.related_habitats_list))
        WidgetService.clickable(self.cal1_label).connect(
            lambda: WidgetService.calendar(self.cal1, self.cal1_label, self.rel_date_min, self.rel_date_max,
                                           WidgetService.transform_date_1))
        WidgetService.clickable(self.cal2_label).connect(
            lambda: WidgetService.calendar(self.cal2, self.cal2_label, self.rel_date_min, self.rel_date_max,
                                           WidgetService.transform_date_2))
        WidgetService.clickable(self.jdd_add).connect(
            lambda: self.data_manager.add_jdd(self.survey_feature, self.releves, self.jdd_list, self.active_jdd))
        WidgetService.clickable(self.jdd_delete).connect(
            lambda: self.data_manager.delete_jdd(self.survey_feature, self.releves, self.jdd_list, self.active_jdd))
        WidgetService.clickable(self.rel_create_new_polygon_rel).connect(
            lambda: self.data_manager.add_associated_survey(self.rel_create_new_polygon_rel, self.releves,
                                                            self.survey_feature))
        WidgetService.clickable(self.rel_create_new_point_rel).connect(
            lambda: self.data_manager.add_associated_survey(self.rel_create_new_point_rel, self.releves,
                                                            self.survey_feature))
        WidgetService.clickable(self.rel_create_new_line_rel).connect(
            lambda: self.data_manager.add_associated_survey(self.rel_create_new_line_rel, self.releves,
                                                            self.survey_feature))
        WidgetService.clickable(self.rel_syntax_create_new_polygon_rel).connect(
            lambda: self.data_manager.add_survey_associated_to_syntaxon(self.rel_syntax_create_new_polygon_rel,
                                                                        self.releves, self.obs_habitats,
                                                                        self.survey_feature,
                                                                        self.obs_hab_nom_cite,
                                                                        self.obs_hab_code_cite))
        WidgetService.clickable(self.rel_syntax_create_new_point_rel).connect(
            lambda: self.data_manager.add_survey_associated_to_syntaxon(self.rel_syntax_create_new_point_rel,
                                                                        self.releves, self.obs_habitats,
                                                                        self.survey_feature,
                                                                        self.obs_hab_nom_cite,
                                                                        self.obs_hab_code_cite))
        WidgetService.clickable(self.rel_syntax_create_new_line_rel).connect(
            lambda: self.data_manager.add_survey_associated_to_syntaxon(self.rel_syntax_create_new_line_rel,
                                                                        self.releves, self.obs_habitats,
                                                                        self.survey_feature,
                                                                        self.obs_hab_nom_cite,
                                                                        self.obs_hab_code_cite))
        WidgetService.clickable(self.add_obs_btn).connect(
            lambda: self.data_manager.add_observer(self.survey_feature.id(), self.liens_releve_observateur,
                                                   self.lst_obs, self.observer_list))
        WidgetService.clickable(self.suppr_obs_btn).connect(
            lambda: self.data_manager.delete_observer(self.survey_feature.id(), self.liens_releve_observateur,
                                                      self.observer_list, self.observateurs))
        WidgetService.clickable(self.add_strate_struct).connect(
            lambda: self.data_manager.add_strate(self.survey_feature.id(), self.liens_releve_strate,
                                                 self.table_strates_struct, self.select_strate_struct,
                                                 self.struct_rec_pct_strate, self.struct_haut_mod_strate,
                                                 self.struct_haut_min_strate, self.struct_haut_max_strate))
        WidgetService.clickable(self.add_facteur).connect(
            lambda: self.data_manager.add_factor(self.survey_feature.id(), self.liens_releve_influence,
                                                 self.table_facteurs, self.select_facteur, self.select_impact_facteur,
                                                 self.select_etat_facteur))
        WidgetService.clickable(self.extract_phyto_table).connect(lambda: PhytoTableExtractService().process())
        WidgetService.clickable(self.extract_carto).connect(lambda: CartoExtractService().process())
        WidgetService.clickable(self.extract_obs_hab).connect(lambda: ObsHabExtractService().process())
        WidgetService.clickable(self.clean_db).connect(lambda: DatabaseCleaner().clean())
        WidgetService.clickable(self.replicate_jdd).connect(
            lambda: self.data_manager.replicate_jdd(self.survey_feature))
        WidgetService.clickable(self.wiki_link).connect(
            lambda: QDesktopServices.openUrl(QUrl('https://gitlab.com/lobelia/lobelia-4-qgis/-/wikis/home')))
        self.select_taxon.lineEdit().returnPressed.connect(
            lambda: self.data_manager.taxon_auto_complete(self.select_taxon, self.taxref, self.taxon_cite,
                                                          self.taxon_cd_nom_cite, self.taxon_valide,
                                                          self.taxon_cd_nom_valide))
        self.taxon_dominant_recherche.lineEdit().returnPressed.connect(
            lambda: self.data_manager.taxon_auto_complete(self.taxon_dominant_recherche, self.taxref,
                                                          self.taxon_dominant_nom, self.taxon_dominant_code,
                                                          QLineEdit(), QLineEdit()))
        self.select_taxon_dominant.lineEdit().returnPressed.connect(
            lambda: self.data_manager.taxon_auto_complete(self.select_taxon_dominant, self.taxref,
                                                          self.obs_hab_taxon_dominant_nom,
                                                          self.obs_hab_taxon_dominant_code, QLineEdit(), QLineEdit()))
        self.select_habitat.lineEdit().returnPressed.connect(
            lambda: self.data_manager.habitat_auto_complete(self.select_habitat, self.catalogue_phyto, self.habref,
                                                            self.habitat_buttons, self.obs_hab_nom_cite,
                                                            self.obs_hab_code_cite, self.obs_hab_nom_retenu,
                                                            self.obs_hab_code_retenu))
        self.habitat_recherche.lineEdit().returnPressed.connect(
            lambda: self.data_manager.habitat_auto_complete(self.habitat_recherche, self.catalogue_phyto, self.habref,
                                                            self.typo_habitat_buttons, QLineEdit(), QLineEdit(),
                                                            QLineEdit(), QLineEdit(), self.survey_feature,
                                                            self.obs_habitats, self.table_syn_habitats))
        self.typo_habitat_buttons.buttonClicked.connect(lambda: self.habitat_recherche.clear())
        self.syntaxon_code_cite.textChanged.connect(lambda: self.habitat_recherche.clear())
        WidgetService.clickable(self.habitat_recherche).connect(
            lambda: self.data_manager.related_habitat_auto_complete(self.habitat_recherche, self.syntaxon_code_cite,
                                                                    self.typo_habitat_buttons, self.survey_feature,
                                                                    self.obs_habitats, self.table_syn_habitats))
        self.syntaxon_recherche.lineEdit().returnPressed.connect(
            lambda: self.data_manager.syntaxon_auto_complete(self.syntaxon_recherche, self.catalogue_phyto,
                                                             self.syntaxon_cite, self.syntaxon_code_cite,
                                                             self.syntaxon_retenu, self.syntaxon_code_retenu))
        WidgetService.clickable(self.syntaxon_delete).connect(
            lambda: self.data_manager.delete_syntaxon(self.syntaxon_cite, self.syntaxon_code_cite,
                                                      self.syntaxon_retenu, self.syntaxon_code_retenu))
        self.syntaxon_comm_non_sat_id.currentIndexChanged.connect(
            lambda i: self.data_manager.remove_dominant_taxon(i, self.syntaxon_comm_non_sat_obs, self.taxon_dominant_code, self.taxon_dominant_nom,
                                                              self.obs_hab_taxon_dominant_code, self.obs_hab_taxon_dominant_nom))
        self.syntaxon_comm_non_sat_obs.currentIndexChanged.connect(
            lambda i: self.data_manager.remove_dominant_taxon(i, self.syntaxon_comm_non_sat_id, self.taxon_dominant_code, self.taxon_dominant_nom,
                                                              self.obs_hab_taxon_dominant_code, self.obs_hab_taxon_dominant_nom))
        self.jdd_list.lineEdit().returnPressed.connect(lambda: self.data_manager.search_jdd(self.jdd_list))
        self.select_facteur.lineEdit().returnPressed.connect(
            lambda: self.data_manager.search_factor(self.select_facteur))
        self.pres_taches_redox.stateChanged.connect(
            lambda: WidgetService.check_hydromorphic_checkbox(self.pres_taches_redox, self.taches_redox))
        self.pres_horizon_redox.stateChanged.connect(
            lambda: WidgetService.check_hydromorphic_checkbox(self.pres_horizon_redox, self.horizon_redox))
        self.pres_horizon_reduct.stateChanged.connect(
            lambda: WidgetService.check_hydromorphic_checkbox(self.pres_horizon_reduct, self.horizon_reduct))
        WidgetService.closable(self.form.parent()).connect(
            lambda: self.data_manager.save_survey(self.releves, self.survey_feature, self.obs_habitats))

    def prepare_startup_interface(self, add_survey_to_syntaxon_widgets):
        widgets_to_hide = [self.save_taxon, self.save_syntaxon, self.add_related_habitats, self.related_habitats_list,
                           self.syntaxon_code_cite, self.syntaxon_code_retenu, self.taxon_cd_nom_cite,
                           self.taxon_cd_nom_valide, self.rel_create_new_point_rel, self.rel_create_new_line_rel,
                           self.rel_create_new_polygon_rel, self.obs_hab_code_typo, self.obs_hab_code_cite,
                           self.obs_hab_code_retenu, self.diag_originale, self.obs_hab_taxon_dominant_code, self.rel_date_saisie]
        widgets_to_hide.extend(add_survey_to_syntaxon_widgets)
        for widget in widgets_to_hide:
            widget.hide()

        self.frel.hide()
        self.tab_widget.setCurrentIndex(0)
        self.form.hideButtonBox()
        if self.rel_type_rel_id.itemData(self.rel_type_rel_id.currentIndex()) != 0 and self.rel_protocole_id.itemData(
                self.rel_protocole_id.currentIndex()) != 0:
            self.activate.hide()
            self.fgen.hide()

        for widget, form_input in [(self.pres_taches_redox, self.taches_redox),
                                   (self.pres_horizon_redox, self.horizon_redox),
                                   (self.pres_horizon_reduct, self.horizon_reduct)]:
            WidgetService.check_hydromorphic_checkbox(widget, form_input)
        WidgetService.fshow(self.rel_type_rel_id, self.rel_protocole_id, self.frel, self.fobs, self.save_rel,
                            self.activate)
        WidgetService.fobs_show(self.tab_widget, self.rel_type_rel_id, self.rel_protocole_id, self.fobs_taxons,
                                self.fobs_syntaxons, self.save_rel, self.obs_hab_surface, self.obs_hab_surface_label)
