import csv
import sys
from typing import List

from qgis.core import QgsProject

from ingest_service import IngestService


class CatPhytoIngestService(IngestService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)

    def build_target_dict(self, file_name: str) -> List[tuple]:
        target_data: List[tuple]
        with open(self.home_path / file_name, 'r', encoding='utf-8') as file:
            data = csv.DictReader(file, delimiter=';')
            target_data = [(line['id_supra'], line['id_syntaxon'], line['syntaxon'], line['aut'], line['id_retenu'],
                            int(line['pres_CBNBP'])) for line in data if line['REF_CBNBP'] == 'VRAI']
        return target_data

    def delete_current_content(self) -> bool:
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.execute('DELETE FROM catalogue_phyto;')
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def import_new_cat(self, data) -> bool:
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.executemany(
                'INSERT INTO catalogue_phyto (code_phyto_parent, code_phyto, nom_syntaxon_simple, '
                'nom_auteur_syntaxon_simple, code_phyto_syntaxon_retenu, presence) '
                'VALUES (?, ?, ?, ?, ?, ?);', data)
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def update_retained_syntaxon_name(self):
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.execute('SELECT id, code_phyto_syntaxon_retenu FROM catalogue_phyto')
            syntaxons = self.sqlite_cur.fetchall()
            for syntaxon in syntaxons:
                self.sqlite_cur.execute('SELECT id, nom_syntaxon_simple FROM catalogue_phyto WHERE code_phyto=?',
                                        (syntaxon[1],))
                result = self.sqlite_cur.fetchone()
                self.sqlite_cur.execute('UPDATE catalogue_phyto SET nom_syntaxon_retenu=? WHERE id=?;',
                                        (result[1], syntaxon[0]))
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def process_csv_to_sqlite(self, file_name: str, database_file_name: str):
        self.sqlite_connection(database_file_name)
        target_data = self.build_target_dict(file_name)
        if len(target_data) > 0 and self.sqlite_cur:
            self.delete_current_content()
            self.import_new_cat(target_data)
            self.update_retained_syntaxon_name()
        self.sqlite_conn.close()


if __name__ == '__main__':
    ingest_service = CatPhytoIngestService()
    if sys.argv[1] == '-csv-to-sqlite':
        ingest_service.process_csv_to_sqlite(sys.argv[2], 'donnees_habitats.sqlite')
    else:
        print("ERREUR : Le 1er argument (devant faire référence à la méthode d'import) n'a pas été reconnu")
