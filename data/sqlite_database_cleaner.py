import sqlite3
from pathlib import Path
from sqlite3 import Connection, Cursor, Error, Row
from typing import Optional, List

from qgis.core import QgsProject
from qgis.utils import iface

from ..constants.connection_constants import SQLITE_FILE


class DatabaseCleaner:
    def __init__(self, project: QgsProject = None):
        self.project: Optional[QgsProject] = project
        if self.project is None:
            self.project = QgsProject.instance()
        self.connection: Optional[Connection] = None
        self.cursor: Optional[Cursor] = None
        self.home_path: Path = Path(self.project.homePath())
        self.geometry_tables = [('station_point', 'point'), ('station_ligne', 'ligne'), ('station_surface', 'polygon')]

    def clean(self):
        self.clean_orphans()
        try:
            iface.messageBar().pushInfo('Database cleaning', 'orphans cleaning OK (1/2)')
        except AttributeError as e:
            pass
        self.correct_encoding()
        try:
            iface.messageBar().pushInfo('Database cleaning', 'encoding correction OK (2/2)')
        except AttributeError as e:
            pass

    def clean_orphans(self):
        self.sqlite_connection(SQLITE_FILE)
        self.clean_survey_orphans()
        self.clean_observations_orphans()
        self.clean_obs_syntaxon_orphans()
        self.clean_phantom_obs_hab()
        self.clean_duplicated_obs_syntaxon()
        self.clean_orphan_links()

    def clean_survey_orphans(self):
        orphans_request = "SELECT releves.id FROM releves LEFT JOIN "
        join_clause = []
        for geometry_table in self.geometry_tables:
            join_clause.append(geometry_table[0] + " on " + geometry_table[0] + ".uuid = releves.rel_" +
                               geometry_table[1] + "_uuid ")
        orphans_request += "LEFT JOIN ".join(join_clause) + "WHERE "
        where_clause = []
        for geometry_table in self.geometry_tables:
            where_clause.append(geometry_table[0] + ".uuid is NULL")
        orphans_request += " AND ".join(where_clause)
        self.cursor.execute(orphans_request)
        orphans = [orphan[0] for orphan in self.cursor.fetchall()]

        if len(orphans) > 0:
            delete_request: str
            if len(orphans) == 1:
                delete_request = "DELETE FROM releves WHERE id = " + str(orphans[0])
            else:
                delete_request = "DELETE FROM releves WHERE id in " + str(tuple(orphans))
            self.cursor.execute(delete_request)
            self.connection.commit()

    def clean_observations_orphans(self):
        request = "SELECT observations.obs_id FROM observations LEFT JOIN releves on releves.id = " \
                  "observations.obs_rel_id WHERE releves.id is NULL"
        self.cursor.execute(request)
        orphans = [orphan[0] for orphan in self.cursor.fetchall()]

        if len(orphans) > 0:
            delete_request: str
            if len(orphans) == 1:
                delete_request = "DELETE FROM observations WHERE obs_id = " + str(orphans[0])
            else:
                delete_request = "DELETE FROM observations WHERE obs_id in " + str(tuple(orphans))
            self.cursor.execute(delete_request)
            self.connection.commit()

    def delete_obs_hab_from_list(self, obs_hab_list_to_delete: List[int]):
        if len(obs_hab_list_to_delete) > 0:
            delete_request: str
            if len(obs_hab_list_to_delete) == 1:
                delete_request = "DELETE FROM obs_habitats WHERE obs_hab_id = " + str(obs_hab_list_to_delete[0])
            else:
                delete_request = "DELETE FROM obs_habitats WHERE obs_hab_id in " + str(tuple(obs_hab_list_to_delete))
            self.cursor.execute(delete_request)
            self.connection.commit()

    def clean_obs_syntaxon_orphans(self):
        obs_hab_request = "SELECT obs_habitats.obs_hab_id FROM obs_habitats"
        obs_hab_list = [obs[0] for obs in self.cursor.execute(obs_hab_request).fetchall()]
        veg_request = "SELECT obs_habitats.obs_hab_id FROM obs_habitats LEFT JOIN releves on releves.id = " \
                      "obs_habitats.obs_hab_rel_id WHERE releves.id is NOT NULL"
        veg_obs_hab_list = [obs[0] for obs in self.cursor.execute(veg_request).fetchall()]
        phyto_orphans = "SELECT obs_habitats.obs_hab_id FROM obs_habitats LEFT JOIN releves on releves.id = " \
                        "obs_habitats.obs_hab_rel_assoc_id WHERE releves.id is NOT NULL"
        phyto_obs_hab_list = [obs[0] for obs in self.cursor.execute(phyto_orphans).fetchall()]

        orphans: List[int] = []
        for obs in obs_hab_list:
            if obs not in veg_obs_hab_list and obs not in phyto_obs_hab_list:
                orphans.append(obs)
        self.delete_obs_hab_from_list(orphans)

    def clean_phantom_obs_hab(self):
        veg_survey_with_obs_hab_req = "SELECT id, rel_obs_syntaxon_id FROM releves " \
                                      "WHERE rel_type_rel_id = 2 AND rel_obs_syntaxon_id NOT NULL"
        veg_survey_with_obs_hab_list = self.cursor.execute(veg_survey_with_obs_hab_req).fetchall()
        obs_hab_to_delete = [survey[1] for survey in veg_survey_with_obs_hab_list]
        self.delete_obs_hab_from_list(obs_hab_to_delete)

        survey_update_req = "UPDATE releves SET rel_obs_syntaxon_id = NULL WHERE id = %s"
        for survey in veg_survey_with_obs_hab_list:
            self.cursor.execute(survey_update_req % survey[0])
        self.connection.commit()

    def clean_duplicated_obs_syntaxon(self):
        survey_with_duplicated_obs_syntaxon_request = "SELECT obs_hab_rel_assoc_id FROM obs_habitats " \
                                                      "WHERE obs_hab_code_typo = 0 AND obs_hab_rel_assoc_id NOT NULL " \
                                                      "GROUP BY obs_hab_rel_assoc_id, obs_hab_code_typo " \
                                                      "HAVING COUNT(*) > 1"
        get_obs_syntaxon_id_to_keep_request = "SELECT rel_obs_syntaxon_id FROM releves " \
                                              "WHERE id = %s AND rel_obs_syntaxon_id NOT NULL"
        duplicated_obs_hab_ids_request = "SELECT obs_hab_id FROM obs_habitats WHERE obs_hab_rel_assoc_id = %s " \
                                         "AND obs_hab_id != %s AND obs_hab_code_typo = 0"
        update_obs_hab_links_one = "UPDATE liens_obs_habitat SET id_obs_hab_1 = %s WHERE id_obs_hab_1 = %s"
        update_obs_hab_links_two = "UPDATE liens_obs_habitat SET id_obs_hab_2 = %s WHERE id_obs_hab_2 = %s"
        delete_request = "DELETE FROM obs_habitats WHERE obs_hab_id = %s"
        survey_with_duplicated_obs_syntaxon_list = [survey[0] for survey in self.cursor.execute(
            survey_with_duplicated_obs_syntaxon_request).fetchall()]
        for survey in survey_with_duplicated_obs_syntaxon_list:
            obs_syntaxon_id_to_keep = self.cursor.execute(get_obs_syntaxon_id_to_keep_request % survey).fetchone()
            if obs_syntaxon_id_to_keep:
                obs_hab_id_list_to_delete = [obs_hab[0] for obs_hab in self.cursor.execute(
                    duplicated_obs_hab_ids_request % (survey, obs_syntaxon_id_to_keep[0])).fetchall()]
                for obs_hab_id_to_delete in obs_hab_id_list_to_delete:
                    self.cursor.execute(update_obs_hab_links_one % (obs_syntaxon_id_to_keep[0], obs_hab_id_to_delete))
                    self.cursor.execute(update_obs_hab_links_two % (obs_syntaxon_id_to_keep[0], obs_hab_id_to_delete))
                    self.cursor.execute(delete_request % obs_hab_id_to_delete)
        self.connection.commit()

    def clean_orphan_links(self):
        get_links_request = "SELECT id_obs_hab_1, id_obs_hab_2 FROM liens_obs_habitat"
        get_obs_hab = "SELECT obs_hab_id FROM obs_habitats WHERE obs_hab_id = %s"
        delete_request = "DELETE FROM liens_obs_habitat WHERE id_obs_hab_1 = %s OR id_obs_hab_2 = %s"
        for link in self.cursor.execute(get_links_request).fetchall():
            if not self.cursor.execute(get_obs_hab % link['id_obs_hab_1']).fetchone():
                self.cursor.execute(delete_request % (link['id_obs_hab_1'], link['id_obs_hab_1']))
        self.connection.commit()

    def correct_encoding(self):
        self.sqlite_connection(SQLITE_FILE)
        self.correct_obs_hab_labels()

    def correct_obs_hab_labels(self):
        obs_hab_request = "SELECT obs_hab_id, obs_hab_code_cite, obs_hab_code_typo, syntaxon_code_phyto_retenu " \
                          "FROM obs_habitats"
        obs_hab_list: List[Row] = [obs for obs in self.cursor.execute(obs_hab_request).fetchall()]
        habref_request = "SELECT cd_typo, lb_code, lb_hab_fr FROM habref WHERE cd_typo = %s AND lb_code = '%s'"
        ref_phyto_request = "SELECT code_phyto, nom_syntaxon_simple FROM catalogue_phyto WHERE code_phyto = '%s'"
        update_obs_hab_request = "UPDATE obs_habitats SET obs_hab_nom_cite = \"%s\" WHERE obs_hab_id = %s"
        update_syntaxon_request = "UPDATE obs_habitats SET syntaxon_nom_retenu = \"%s\" WHERE obs_hab_id = %s"
        for obs_hab in obs_hab_list:
            name_list = []
            if obs_hab['obs_hab_code_typo'] == 0 and obs_hab['obs_hab_code_cite']:
                name_list = [lb['nom_syntaxon_simple'] for lb in
                             self.cursor.execute(ref_phyto_request % obs_hab['obs_hab_code_cite']).fetchall()]
            elif obs_hab['obs_hab_code_cite']:
                name_list = [lb['lb_hab_fr'] for lb in
                             self.cursor.execute(habref_request %
                                                 (obs_hab['obs_hab_code_typo'], obs_hab['obs_hab_code_cite']))
                                 .fetchall()]
            if len(name_list) > 0:
                self.cursor.execute(update_obs_hab_request % (name_list[0], obs_hab['obs_hab_id']))
            if obs_hab['obs_hab_code_typo'] == 0 and obs_hab['syntaxon_code_phyto_retenu']:
                syntaxon_list = [lb['nom_syntaxon_simple'] for lb in
                                 self.cursor.execute(
                                     ref_phyto_request % obs_hab['syntaxon_code_phyto_retenu']).fetchall()]
                if len(syntaxon_list) > 0:
                    self.cursor.execute(update_syntaxon_request % (syntaxon_list[0], obs_hab['obs_hab_id']))
        self.connection.commit()

    def sqlite_connection(self, database_file_name) -> Optional[Connection]:
        try:
            self.connection = sqlite3.connect(self.home_path / database_file_name)
            self.connection.row_factory = sqlite3.Row
            self.connection.enable_load_extension(True)
            self.connection.load_extension("mod_spatialite")
        except Error as e:
            message = 'Connection failed - ' + str(e)
            iface.messageBar().pushWarning('Database connection', message)
        finally:
            if self.connection:
                self.cursor = self.connection.cursor()
            return self.connection
