# coding: utf-8
import datetime
import sys

from qgis.core import QgsProject
from qgis.utils import iface

from ..data.sqlite_database_builder import DatabaseBuilder
from ..data.sqlite_database_cleaner import DatabaseCleaner
from ..extract.carto_extract_service import CartoExtractService
from ..extract.phyto_table_extract_service import PhytoTableExtractService
from ..extract.obs_hab_extract_service import ObsHabExtractService
from ..interface.survey_form_manager import SurveyFormManager

DEBUGMODE = True


def form_open(dialog, layer, feature):
    iface.messageBar().pushInfo('Habitats application', 'launched')

    if dialog.editable() or feature.id() > 0:
        project = QgsProject.instance()
        # TODO build or alter database through another way (plugin ?), properly not at form launch...
        database_builder: DatabaseBuilder = DatabaseBuilder(project)
        database_builder.build_or_alter()
        iface.messageBar().pushInfo('Database connection', 'database mounted and complete')

        form_manager: SurveyFormManager = SurveyFormManager(dialog, layer, feature)
        form_manager.initialize()
        iface.messageBar().pushInfo('Habitats form', 'on air')


if __name__ == '__main__':
    print("%s - Lobelia4QGis command line tool launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    if hasattr(sys, 'argv') and len(sys.argv) > 1:
        option = sys.argv[1]
        if option == 'clean':
            print("%s - clean process launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            database_cleaner = DatabaseCleaner()
            database_cleaner.clean()
            print("%s - clean process ended" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        elif option == 'extract_phyto':
            print("%s - phyto table extraction process launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            extract_service = PhytoTableExtractService()
            extract_service.process()
            print("%s - phyto table extraction process ended" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        elif option == 'extract_obs_hab':
            print("%s - habitat observation extraction process launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            extract_service = ObsHabExtractService()
            extract_service.process()
            print("%s - habitat observation extraction process ended" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        elif option == 'extract_carto':
            print("%s - carto extraction process launched" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            extract_service = CartoExtractService()
            extract_service.process()
            print("%s - carto extraction process ended" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        else:
            print("%s - unknown option : clean, extract_phyto, extract_carto available" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    else:
        print("# usage :", "python survey_form.py <option>")
        print("# options available : clean, extract_phyto, extract_obs_hab, extract_carto")
    print("%s - Lobelia4QGis command line tool closed" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
