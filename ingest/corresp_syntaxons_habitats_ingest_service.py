import csv
import sys
from typing import List

from qgis.core import QgsProject

from ingest_service import IngestService


class CorrespSyntaxonsHabitatsIngestService(IngestService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)

    def build_target_dict(self, file_name: str) -> List[tuple]:
        target_data: List[tuple]
        with open(self.home_path / file_name, 'r', encoding='utf-8') as file:
            data = csv.DictReader(file, delimiter=';')
            target_data = [(line['code_phyto'], line['cd_typo'], line['cd_hab'], line['lb_code'], line['lb_hab_fr'])
                           for line in data]
            # target_data = [tuple(list(line.values())) for line in data]
        return target_data

    def delete_current_content(self) -> bool:
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.execute('DELETE FROM corresp_syntaxons_habitats;')
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def import_new_corresp_syntaxons_habitats(self, data) -> bool:
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.executemany('INSERT INTO corresp_syntaxons_habitats (code_phyto, cd_typo, cd_hab, lb_code, lb_hab_fr)'
                                        ' VALUES (?, ?, ?, ?, ?);', data)
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def process_csv_to_sqlite(self, file_name: str, database_file_name: str):
        self.sqlite_connection(database_file_name)
        target_data = self.build_target_dict(file_name)
        if len(target_data) > 0 and self.sqlite_cur:
            self.delete_current_content()
            self.import_new_corresp_syntaxons_habitats(target_data)
        self.sqlite_conn.close()


if __name__ == '__main__':
    ingest_service = CorrespSyntaxonsHabitatsIngestService()
    if sys.argv[1] == '-csv-to-sqlite':
        ingest_service.process_csv_to_sqlite(sys.argv[2], 'donnees_habitats.sqlite')
    else:
        print("ERREUR : Le 1er argument (devant faire référence à la méthode d'import) n'a pas été reconnu")
