import uuid
from typing import Optional

from qgis.PyQt.QtCore import NULL
from qgis.core import QgsFeature, QgsVectorLayer, QgsProject, QgsGeometry, QgsWkbTypes

from ..data.sqlite_data_manager import DataManager


class GeometryManager:
    def __init__(self, layer: QgsVectorLayer, feature: QgsFeature):
        self.geometry_layer: QgsVectorLayer = layer
        self.geometry_feature: QgsFeature = feature
        self.survey_feature: Optional[QgsFeature] = None

    def get_survey_feature(self, releves: QgsVectorLayer) -> QgsFeature:
        geometry_layer_name = self.geometry_layer.name()
        uuid_index = self.geometry_layer.fields().indexFromName('uuid')
        geometry_uuid = self.geometry_feature[uuid_index]  # geometry_uuid = self.geometry_feature['uuid']
        data_manager = DataManager()
        if geometry_uuid is None or geometry_uuid == "" \
                or data_manager.count_geometry_uuid(geometry_layer_name, geometry_uuid) > 1:
            geometry_uuid = self.set_geometry_uuid()

        rel_field_name = self.get_geometry_field_name(geometry_layer_name)

        releve_list = [releve for releve in releves.getFeatures() if releve[rel_field_name] == geometry_uuid]
        if len(releve_list) > 0:
            self.survey_feature = releve_list[0]
            survey_uuid = self.survey_feature['rel_uuid']
            if survey_uuid is None or survey_uuid == "" or survey_uuid == NULL:
                self.survey_feature.setAttribute('rel_uuid', str(uuid.uuid4()))
                releves.updateFeature(self.survey_feature)
                releves.commitChanges()
        else:
            self.survey_feature = self.create_new_releve(releves)
            self.survey_feature.setAttribute(rel_field_name, geometry_uuid)
            self.survey_feature.setAttribute('rel_uuid', str(uuid.uuid4()))
            releves.updateFeature(self.survey_feature)
            releves.commitChanges()

        return self.survey_feature

    def set_geometry_uuid(self) -> str:
        if not self.geometry_layer.isEditable():
            self.geometry_layer.startEditing()
        geometry_uuid = str(uuid.uuid4())
        self.geometry_feature.setAttribute(self.geometry_layer.fields().indexFromName('uuid'), geometry_uuid)
        self.geometry_layer.updateFeature(self.geometry_feature)
        self.geometry_layer.endEditCommand()
        self.geometry_layer.commitChanges()
        self.geometry_layer.startEditing()
        return geometry_uuid

    @staticmethod
    def get_geometry_field_name(geometry_layer_name) -> str:
        if geometry_layer_name == 'station_surface':
            return 'rel_polygon_uuid'
        elif geometry_layer_name == 'station_ligne':
            return 'rel_ligne_uuid'
        elif geometry_layer_name == 'station_point':
            return 'rel_point_uuid'
        else:
            return ''

    @staticmethod
    def create_new_releve(releves) -> QgsFeature:
        releve = QgsFeature()
        releve.setFields(releves.fields())
        if not releves.isEditable():
            releves.startEditing()
        releves.addFeature(releve)
        releves.updateFeature(releve)
        releves.endEditCommand()
        return releve

    def remove_overlaps(self) -> QgsGeometry:
        new_geometry: QgsGeometry = self.geometry_feature.geometry()
        if self.geometry_layer.geometryType() != QgsWkbTypes.PolygonGeometry:
            return new_geometry
        data_manager = DataManager()
        uuid_index = self.geometry_layer.fields().indexFromName('uuid')
        new_geometry_uuid = self.geometry_feature[uuid_index]
        new_geometry_survey_type = data_manager.get_survey_type_from_geometry(new_geometry_uuid)

        for geom_feature in self.geometry_layer.getFeatures():
            geom_uuid = geom_feature[uuid_index]
            geom_survey_type = data_manager.get_survey_type_from_geometry(geom_uuid)
            if geom_uuid != new_geometry_uuid and geom_survey_type == new_geometry_survey_type \
                    and geom_feature.geometry().intersects(new_geometry):
                new_geometry = new_geometry.difference(geom_feature.geometry())
        new_geometry = new_geometry.buffer(0, 20)

        if not self.geometry_layer.isEditable():
            self.geometry_layer.startEditing()
        new_geometry_feature_id = data_manager.get_station_id_from_geometry(self.geometry_layer, self.geometry_feature)
        new_geometry.convertToMultiType()
        self.geometry_layer.changeGeometry(new_geometry_feature_id, new_geometry)
        self.geometry_layer.commitChanges()
        return new_geometry
