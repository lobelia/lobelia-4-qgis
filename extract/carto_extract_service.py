import csv
from operator import itemgetter
from sqlite3 import Row
from typing import List

from qgis.core import QgsProject

from .a_extract_service import ExtractService
from ..constants.connection_constants import SQLITE_FILE


class CartoExtractService(ExtractService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)
        self.filename = 'carto_export.csv'

    def get_name_by_obs_hab_id(self, obs_hab_id: int) -> str:
        query = "SELECT obs_hab_nom_cite AS name FROM obs_habitats WHERE obs_hab_id = %s"
        result: Row = self.connection.execute(query % obs_hab_id).fetchone()
        return str(result['name'])

    @staticmethod
    def flatten_by_rel_assoc(field: str, flattened_habitat_list: List[dict], geom_list: List[dict],
                             obs_habitat: dict, survey_list: List[dict]):
        for survey in survey_list:
            if survey['id'] == obs_habitat[field]:
                for geom in geom_list:
                    if geom['uuid'] == survey['geom_uuid']:
                        flattened_habitat_list.append({'obs_id': obs_habitat['id'], 'obs_uuid': obs_habitat['uuid'],
                                                       'name': obs_habitat['name'], 'code': obs_habitat['code'],
                                                       'rec': obs_habitat['rec'], 'typo': obs_habitat['typo_code'],
                                                       'rel_id': survey['id'], 'rel_uuid': survey['uuid'],
                                                       'rel_type': survey['type'], 'geom_uuid': survey['geom_uuid'],
                                                       'geom': geom['geometry']})

    @staticmethod
    def flatten_obs_habitat(obs_habitat_list: List[dict], survey_list: List[dict], geom_list: List[dict]) -> List[dict]:
        flattened_habitat_list: List[dict] = []
        for obs_habitat in obs_habitat_list:
            if obs_habitat['rel_id'] and (obs_habitat['code'] or obs_habitat['name']):
                CartoExtractService.flatten_by_rel_assoc('rel_id', flattened_habitat_list, geom_list, obs_habitat,
                                                         survey_list)
            elif obs_habitat['rel_assoc_id'] and (obs_habitat['code'] or obs_habitat['name']):
                CartoExtractService.flatten_by_rel_assoc('rel_assoc_id', flattened_habitat_list, geom_list, obs_habitat,
                                                         survey_list)
        return flattened_habitat_list

    @staticmethod
    def get_obs_habitat_list_by_geom(flattened_habitat_list: List[dict]) -> List[dict]:
        obs_hab_list_by_geom: List[dict] = []
        geom_list: List[str] = list(set([habitat['geom_uuid'] for habitat in flattened_habitat_list]))
        for geom in geom_list:
            obs_hab_list: List[int] = []
            geometry: str = ''
            rel_id: int = -1
            rel_uuid: str = ''
            for habitat in flattened_habitat_list:
                if habitat['geom_uuid'] == geom:
                    obs_hab_list.append(habitat['obs_id'])
                    geometry = habitat['geom']
                    rel_id = habitat['rel_id']
                    rel_uuid = habitat['rel_uuid']
            if len(obs_hab_list) > 0 and geometry != '' and rel_id > 0:
                obs_hab_list_by_geom.append({'geom_uuid': geom, 'geom': geometry, 'rel_id': rel_id,
                                             'rel_uuid': rel_uuid, 'obs_hab_list': obs_hab_list})
        return obs_hab_list_by_geom

    def get_obs_hab_by_typo(self, obs_hab_list: List[int]) -> dict:
        obs_hab_by_typo: dict = {'0': [], '7': [], '4': [], '22': []}
        for obs_hab in obs_hab_list:
            typo: int = self.get_typo_by_obs_hab_id(obs_hab)
            if typo == 0:
                obs_hab_by_typo['0'].append(obs_hab)
            elif typo == 7:
                obs_hab_by_typo['7'].append(obs_hab)
            elif typo == 4:
                obs_hab_by_typo['4'].append(obs_hab)
            elif typo == 22:
                obs_hab_by_typo['22'].append(obs_hab)
        return obs_hab_by_typo

    def format_obs_hab_code_list(self, obs_hab_id_list: List[int]) -> str:
        obs_hab_code_list: List[str] = [self.get_code_by_obs_hab_id(code) for code in obs_hab_id_list]
        return str(obs_hab_code_list).replace('[', '').replace(']', '').replace(', ', ' / ').replace('\'', '')

    def build_obs_hab_block_0(self, obs_hab: int, obs_hab_list_by_typo: dict, obs_habitat_links: List[dict]) -> dict:
        obs_hab_links: List[int] = [link['id_obs_hab_2'] for link in obs_habitat_links
                                    if link['id_obs_hab_1'] == obs_hab]
        obs_hab_links_by_typo: dict = self.get_obs_hab_by_typo(obs_hab_links)
        eunis_list: List[int] = [eunis for eunis in obs_hab_links_by_typo['7']]
        hic_list: List[int] = [hic for hic in obs_hab_links_by_typo['4']]
        corine_list: List[int] = [corine for corine in obs_hab_links_by_typo['22']]
        obs_hab_block: dict = {'obs_hab_id': obs_hab,
                               '%_rec': self.get_pct_rec_by_obs_hab_id(obs_hab),
                               'coef_rec': self.get_coef_rec_by_obs_hab_id(obs_hab),
                               'typ_flor': self.get_typ_flor_by_obs_hab_id(obs_hab),
                               'typ_struct': self.get_typ_struct_by_obs_hab_id(obs_hab),
                               'typ_physio': self.get_typ_physio_by_obs_hab_id(obs_hab),
                               'syntaxon_id': self.get_code_by_obs_hab_id(obs_hab),
                               'syntaxon': self.get_name_by_obs_hab_id(obs_hab),
                               'eunis': self.format_obs_hab_code_list(eunis_list) if len(eunis_list) > 0 else None,
                               'hic': self.format_obs_hab_code_list(hic_list) if len(hic_list) > 0 else None,
                               'corine': self.format_obs_hab_code_list(corine_list) if len(corine_list) > 0 else None}
        for eunis in eunis_list:
            obs_hab_list_by_typo['7'].remove(eunis) if eunis in obs_hab_list_by_typo['7'] else None
        for hic in hic_list:
            obs_hab_list_by_typo['4'].remove(hic) if hic in obs_hab_list_by_typo['4'] else None
        for corine in corine_list:
            obs_hab_list_by_typo['22'].remove(corine) if corine in obs_hab_list_by_typo['22'] else None
        return obs_hab_block

    def build_obs_hab_block_7(self, obs_hab: int, obs_hab_list_by_typo: dict, obs_habitat_links: List[dict]) -> dict:
        obs_hab_links: List[int] = [link['id_obs_hab_2'] for link in obs_habitat_links
                                    if link['id_obs_hab_1'] == obs_hab]
        obs_hab_links_by_typo: dict = self.get_obs_hab_by_typo(obs_hab_links)
        hic_list: List[int] = [hic for hic in obs_hab_links_by_typo['4']]
        corine_list: List[int] = [corine for corine in obs_hab_links_by_typo['22']]
        obs_hab_block: dict = {'obs_hab_id': obs_hab,
                               '%_rec': self.get_pct_rec_by_obs_hab_id(obs_hab),
                               'coef_rec': self.get_coef_rec_by_obs_hab_id(obs_hab),
                               'typ_flor': self.get_typ_flor_by_obs_hab_id(obs_hab),
                               'typ_struct': self.get_typ_struct_by_obs_hab_id(obs_hab),
                               'typ_physio': self.get_typ_physio_by_obs_hab_id(obs_hab),
                               'syntaxon_id': None,
                               'syntaxon': None,
                               'eunis': self.get_code_by_obs_hab_id(obs_hab),
                               'hic': self.format_obs_hab_code_list(hic_list) if len(hic_list) > 0 else None,
                               'corine': self.format_obs_hab_code_list(corine_list) if len(corine_list) > 0 else None}
        for hic in hic_list:
            obs_hab_list_by_typo['4'].remove(hic) if hic in obs_hab_list_by_typo['4'] else None
        for corine in corine_list:
            obs_hab_list_by_typo['22'].remove(corine) if corine in obs_hab_list_by_typo['22'] else None
        return obs_hab_block

    def build_obs_hab_block_4(self, obs_hab: int, obs_hab_list_by_typo: dict, obs_habitat_links: List[dict]) -> dict:
        obs_hab_links: List[int] = [link['id_obs_hab_2'] for link in obs_habitat_links
                                    if link['id_obs_hab_1'] == obs_hab]
        obs_hab_links_by_typo: dict = self.get_obs_hab_by_typo(obs_hab_links)
        corine_list: List[int] = [corine for corine in obs_hab_links_by_typo['22']]
        obs_hab_block: dict = {'obs_hab_id': obs_hab,
                               '%_rec': self.get_pct_rec_by_obs_hab_id(obs_hab),
                               'coef_rec': self.get_coef_rec_by_obs_hab_id(obs_hab),
                               'typ_flor': self.get_typ_flor_by_obs_hab_id(obs_hab),
                               'typ_struct': self.get_typ_struct_by_obs_hab_id(obs_hab),
                               'typ_physio': self.get_typ_physio_by_obs_hab_id(obs_hab),
                               'syntaxon_id': None,
                               'syntaxon': None,
                               'eunis': None,
                               'hic': self.get_code_by_obs_hab_id(obs_hab),
                               'corine': self.format_obs_hab_code_list(corine_list) if len(corine_list) > 0 else None}
        for corine in corine_list:
            obs_hab_list_by_typo['22'].remove(corine) if corine in obs_hab_list_by_typo['22'] else None
        return obs_hab_block

    def build_obs_hab_block_22(self, obs_hab) -> dict:
        return {'obs_hab_id': obs_hab,
                '%_rec': self.get_pct_rec_by_obs_hab_id(obs_hab),
                'coef_rec': self.get_coef_rec_by_obs_hab_id(obs_hab),
                'typ_flor': self.get_typ_flor_by_obs_hab_id(obs_hab),
                'typ_struct': self.get_typ_struct_by_obs_hab_id(obs_hab),
                'typ_physio': self.get_typ_physio_by_obs_hab_id(obs_hab),
                'syntaxon_id': None,
                'syntaxon': None,
                'eunis': None,
                'hic': None,
                'corine': self.get_code_by_obs_hab_id(obs_hab)}

    def build_obs_habitats_blocks(self, obs_habitat_list_by_geom: List[dict], obs_habitat_links: List[dict]) -> List[dict]:
        obs_habitat_blocks_by_geom: List[dict] = []
        for geom in obs_habitat_list_by_geom:
            obs_hab_blocks: List[dict] = []
            obs_hab_list_by_typo: dict = self.get_obs_hab_by_typo(geom['obs_hab_list'])
            for obs_hab in obs_hab_list_by_typo['0']:
                obs_hab_blocks.append(self.build_obs_hab_block_0(obs_hab, obs_hab_list_by_typo, obs_habitat_links))
            for obs_hab in obs_hab_list_by_typo['7']:
                obs_hab_blocks.append(self.build_obs_hab_block_7(obs_hab, obs_hab_list_by_typo, obs_habitat_links))
            for obs_hab in obs_hab_list_by_typo['4']:
                obs_hab_blocks.append(self.build_obs_hab_block_4(obs_hab, obs_hab_list_by_typo, obs_habitat_links))
            for obs_hab in obs_hab_list_by_typo['22']:
                obs_hab_blocks.append(self.build_obs_hab_block_22(obs_hab))
            obs_habitat_blocks_by_geom.append({'geom_uuid': geom['geom_uuid'], 'geom': geom['geom'],
                                               'rel_id': geom['rel_id'], 'rel_uuid': geom['rel_uuid'],
                                               'obs_hab_blocks': obs_hab_blocks})
        return obs_habitat_blocks_by_geom

    @staticmethod
    def sort_blocks(blocks: List[dict]) -> List[dict]:
        sorted_blocks_with_rec: List[dict] = []
        sorted_blocks_others: List[dict] = []
        other_blocks: List[dict] = []
        i, j = 0, 0
        for block in blocks:
            if block['%_rec']:
                sorted_blocks_with_rec.append(block)
            elif block['syntaxon']:
                sorted_blocks_others.append(block)
            else:
                other_blocks.append(block)
        return sorted(sorted_blocks_with_rec, key=itemgetter('%_rec'), reverse=True) + sorted(
            sorted_blocks_others, key=itemgetter('syntaxon')) + other_blocks

    def flatten_obs_habitats_blocks(self, obs_habitat_blocks_by_geom: List[dict]) -> List[dict]:
        flatten_target_dict: List[dict] = []
        for geom in obs_habitat_blocks_by_geom:
            items: List[tuple] = []
            for key, value in geom.items():
                if key != 'obs_hab_blocks':
                    items.append((key, value))
                else:
                    i = 1
                    sorted_blocks = self.sort_blocks(geom['obs_hab_blocks'])
                    for block in sorted_blocks:
                        for k, v in block.items():
                            items.append((k + '_' + str(i), v))
                        i += 1
            flatten_target_dict.append(dict(items))
        return flatten_target_dict

    def build_target_dict(self) -> List[dict]:
        all_geom_list = self.row_to_dict(self.get_all_geom_list())
        obs_habitat_list: List[dict] = self.row_to_dict(self.get_obs_habitat_list())
        obs_habitat_links = self.row_to_dict(self.get_links_between_obs_habitat())
        survey_list: List[dict] = self.row_to_dict(self.get_survey_list())
        survey_list = [{'id': survey['id'], 'uuid': survey['uuid'], 'type': survey['type'], 'geom_uuid': self.concat([
            survey['point_uuid'], survey['line_uuid'], survey['polygon_uuid']])} for survey in survey_list]
        flattened_habitat_list = CartoExtractService.flatten_obs_habitat(obs_habitat_list, survey_list, all_geom_list)
        obs_habitat_list_by_geom: List[dict] = CartoExtractService.get_obs_habitat_list_by_geom(flattened_habitat_list)
        obs_habitat_blocks_by_geom: List[dict] = self.build_obs_habitats_blocks(obs_habitat_list_by_geom,
                                                                                obs_habitat_links)
        target_dict: List[dict] = self.flatten_obs_habitats_blocks(obs_habitat_blocks_by_geom)
        return target_dict

    def export_csv(self, target_dict: List[dict]):
        if len(target_dict) > 0:
            fieldnames: List[str] = []
            for geom in target_dict:
                for key in geom.keys():
                    if key not in fieldnames:
                        fieldnames.append(key)
            with open(self.home_path / self.filename, 'w', encoding='utf-8', newline='') as file:
                writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter=';')
                writer.writeheader()
                writer.writerows(target_dict)

    def process(self, database_file_name: str = SQLITE_FILE):
        self.sqlite_connection(database_file_name)
        target_data = self.build_target_dict()
        if len(target_data) > 0 and self.cursor:
            self.export_csv(target_data)
        self.connection.close()
        self.send_message('Export carto', self.filename)
