import csv
import sys
from typing import List

from qgis.core import QgsProject

from ingest_service import IngestService


class ObserversIngestService(IngestService):
    def __init__(self, project: QgsProject = None):
        super().__init__(project)

    def get_observers_from_pg(self):
        sql = """SELECT o.obs_id AS id, obs_prenom AS prenom, obs_nom AS nom, email, 1 AS active
        FROM public.observateurs o
        LEFT JOIN public.user u ON u.obs_id = o.obs_id
        --LEFT JOIN public.rel_obseur_sec_asso rosa ON rosa.obs_id = o.obs_id
        WHERE obs_tag like '%cbnbp%' AND email IS NOT NULL AND obs_prenom IS NOT NULL
        --GROUP BY o.obs_id, email, org_id
        ORDER BY obs_nom, obs_prenom"""
        self.pg_cur.execute(sql)
        fieldnames = [field[0] for field in self.pg_cur.description]
        observers = self.pg_cur.fetchall()
        return observers, fieldnames

    def save_observers_into_csv(self, csv_file_name, observers, fieldnames):
        with open(self.home_path / csv_file_name, 'w', encoding='utf-8') as output_file:
            line = ';'.join(header for header in fieldnames) + '\n'
            output_file.write(line)
            for observer in observers:
                observer = ['"'+str(obs).replace('"', '""')+'"' if obs not in [None] else '' for obs in observer]  # Guillemets doubles pour les retours à la ligne et les ;
                line = ';'.join(obs for obs in observer) + '\n'
                output_file.write(line)

    def build_target_dict(self, file_name: str) -> List[tuple]:
        target_data: List[tuple]
        with open(self.home_path / file_name, 'r', encoding='utf-8') as file:
            data = csv.DictReader(file, delimiter=';')
            target_data = [(line['id'], line['prenom'], line['nom'], line['email'], line['active']) for line in data]
            # target_data = [tuple(list(line.values())) for line in data]
        return target_data

    def delete_current_content(self) -> bool:
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.execute('DELETE FROM observateurs;')
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def import_new_observers(self, data) -> bool:
        if self.sqlite_conn and self.sqlite_cur:
            self.sqlite_cur.executemany('INSERT INTO observateurs (obs_id, obs_prenom, obs_nom, obs_email, obs_turned_on)'
                                        ' VALUES (?, ?, ?, ?, ?);', data)
            self.sqlite_conn.commit()
            return True
        else:
            return False

    def process_pg_to_sqlite(self, pg_file_name: str, csv_file_name: str, sqlite_file_name: str):
        self.pg_connection(pg_file_name)
        observers, fieldnames = self.get_observers_from_pg()
        self.save_observers_into_csv(csv_file_name, observers, fieldnames)
        self.sqlite_connection(sqlite_file_name)
        if len(observers) > 0 and self.sqlite_cur:
            self.delete_current_content()
            self.import_new_observers(observers)
        self.pg_conn.close()
        self.sqlite_conn.close()

    def process_pg_to_csv(self, pg_file_name: str, csv_file_name: str):
        self.pg_connection(pg_file_name)
        observers, fieldnames = self.get_observers_from_pg()
        self.save_observers_into_csv(csv_file_name, observers, fieldnames)
        self.pg_conn.close()

    def process_csv_to_sqlite(self, csv_file_name: str, sqlite_file_name: str):
        self.sqlite_connection(sqlite_file_name)
        target_data = self.build_target_dict(csv_file_name)
        if len(target_data) > 0 and self.sqlite_cur:
            self.delete_current_content()
            self.import_new_observers(target_data)
        self.sqlite_conn.close()


if __name__ == '__main__':
    ingest_service = ObserversIngestService()
    if sys.argv[1] == '-pg-to-sqlite':
        ingest_service.process_pg_to_sqlite('.lobelia_db.env', sys.argv[2], 'donnees_habitats.sqlite')
    elif sys.argv[1] == '-pg-to-csv':
        ingest_service.process_pg_to_csv('.lobelia_db.env', sys.argv[2])
    elif sys.argv[1] == '-csv-to-sqlite':
        ingest_service.process_csv_to_sqlite(sys.argv[2], 'donnees_habitats.sqlite')
    else:
        print("ERREUR : Le 1er argument (devant faire référence à la méthode d'import) n'a pas été reconnu")
