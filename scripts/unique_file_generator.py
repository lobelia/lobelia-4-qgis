import os
from pathlib import Path
from typing import List

# Select only python code files to aggregate
dir_path = Path.cwd()
my_code_file_list: List[Path] = []
for file in dir_path.rglob('*.py'):
    file_to_exclude = ['__init__.py', 'unique_file_generator.py', 'ingest_service.py', 'cat_phyto_ingest_service.py',
                       'habref_ingest_service.py', 'list_values_ingest_service.py', 'observers_ingest_service.py',
                       'jdd_ingest_service.py', 'taxref_ingest_service.py', 'taxref_group_ingest_service.py',
                       'corresp_syntaxons_habitats_ingest_service.py', 'release_package_generator.py',
                       'pdf_documentation_generator.py', 'merge_polygon_with_same_uuid.py', 'import_links_between_obs_hab.py',
                       'import_obs_hab_list_and_links.py', 'manage_v1.1.6_changes_in_database.py', 'survey_form.py',
                       'DEPRECATED_update_polygons_with_carto_habitats.py']
    if file.name not in file_to_exclude and not file.resolve().__str__().__contains__('venv'):
        my_code_file_list.append(file)
my_code_file_list.sort()

# Aggregate code
if Path.exists(dir_path / "survey_form.py"):
    Path.unlink(dir_path / "survey_form.py")

with open(dir_path / "survey_form.py", "a", encoding="utf-8") as target:
    target.write('# coding: utf-8\n')

for file in my_code_file_list:
    with open(dir_path / file, "r", encoding="utf-8") as source, \
            open(dir_path / "survey_form.py", "a", encoding="utf-8") as target:
        for line in source:
            if line.startswith("import") or (line.startswith("from") and not line.startswith("from .")):
                # TODO improve the imports removing the duplicates
                target.write(line)

for file in my_code_file_list:
    if file.name != "survey_form_base.py" and file.parent.name != "utils":
        with open(dir_path / file, "r", encoding="utf-8") as source, \
                open(dir_path / "survey_form.py", "a", encoding="utf-8") as target:
            for line in source:
                if not line.startswith("import") and not line.startswith("from") and not line.startswith(
                        "#") and not line.startswith("\"\"\""):
                    target.write(line)

with open(dir_path / "scripts/survey_form_base.py", "r", encoding="utf-8") as source, \
        open(dir_path / "survey_form.py", "a", encoding="utf-8") as target:
    for line in source:
        if not line.startswith("from") and not line.startswith("#") and not line.startswith("\"\"\""):
            target.write(line)
